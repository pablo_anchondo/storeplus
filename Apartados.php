<?php  
   session_start();
   include("conexion.php");
   if (isset($_SESSION['user'])) { // Determina si se ha iniciado sesión 
   	echo "";
   }else{
   	echo '<script> window.location="index.php"; </script>';
   }   
   $profile = $_SESSION['user'];
   $Identificador = $_SESSION["Id_User"];
   $dominio = $_SESSION["dominio"];
   $Almacen = $_SESSION['Almacen'];   
?>
<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="img/favicon.ico">
   <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="css/select2-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/select2.css">
   <link rel="stylesheet" type="text/css" href="fonts/style.css">
   <link rel="stylesheet" type="text/css" href="css/paneles.css">
   <link rel="stylesheet" type="text/css" href="css/navbar.css">
   <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
   <link rel="stylesheet" type="text/css" href="css/estilos.css">
   <link rel="stylesheet" type="text/css" href="css/Tablas.css">
   <script type="text/javascript" src="js/Apartados.js" ></script>
   <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
   <title>Store-Plus</title>
</head>
<body>
   <?php // Consultas para llenar los select
        $querito = "select Cliente from cobranza where Almacen = ".$Almacen." AND Id_User = ".$Identificador." GROUP BY Cliente ORDER BY Cliente";
        $result = $cbd->query($querito);
        
        
        $queryVend = "select Vendedor from cobranza where Almacen = ".$Almacen." AND Id_User = ".$Identificador." GROUP BY Vendedor";
        $resultVend = $cbd->query($queryVend);
      
    ?>
   <nav class="navbar navbar-default navbar-fixed-static navcolor">
      <!--// Navigation bar -->
      <div class="container-fluid">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="menu.php"><img src="img/favicon.ico"></a>
         </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
               <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
               <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
               <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
               <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
               <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
               <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
               <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
               <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
               <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
               <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li><a href="logout.php">Cerrar Sesión</a></li>
                  </ul>
               </li>
            </ul>
         </div>
      </div>
   </nav>
   <!-- Contenedor proncipal -->
   <div class="container-fluid">
   <div class="cabezera" align="center">
      <h3 class="Titulo">Cobranza</h3>
   </div>
   <div class="contenido">
      <div class="tablita table-responsive table-bordered contenido">
         <br>
         <div class="container-fluid">
            <div class="form-group">
               <div class="form-group">
                  <label class="control-label  col-lg-2">
                     <h4 class="textoBlack"><strong><i class="icon-user-tie"></i> Cliente:</strong></h4>
                  </label>
                  <div class="col-lg-10" id="datCobr">
                     <select class="form-control" id="Clientes" onchange="GetValues()">
                        <option value="nulo">Seleccionar Cliente</option>
                        <?php while ($filaClientes = mysqli_fetch_array($result)){ ?>
                        <option value="<?php echo $filaClientes['Cliente'];?>"><?php echo $filaClientes['Cliente'] ;?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
            </div>
         </div>
         <div id="tablaCobr" class="table-responsive container-fluid ">
            <!-- Se crea la tabla -->
            <table id="TableBody" class="table table-striped table-bordered TableBody">
            <table id="TableBody" class="table table-striped table-bordered TextBlack">
               <tr>
                  <td align="center" class="TituloVerde" COLSPAN="9">Detalles de Cobranza</td>
               </tr>
               <tr>
                  <th class="headAzul">Cobranza</th>
                  <th class="headAzul">Fecha</th>
                  <th class="headAzul">Vencimiento</th>
                  <th class="headAzul">Total</th>
                  <th class="headAzul">Abonos</th>
                  <th class="headAzul">Adeudo</th>
                  <th class="headAzul">Vendedor</th>
                  <th class="headAzul">Detalles</th>
                  <th class="headAzul">Abonar</th>
               </tr>
            </table>
         </div>
      </div>
   </div>
   <br>  
   <br>
   <div class="container">
      <div class="modal fade " id="Abonos">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header panel-header HeadPanel">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title" align="center">Abonos</h2>
               </div>
               <div class="modal-body" align="center">
                  <div class="alert alert-success alert-dismissible" id="alertaAbono" align="center">
                  </div>
                  <table class="table">
                     <tr>
                        <td>
                           <h4 class="textoBlack"><strong><i class="icon-list-numbered"></i> Cobranza</strong></h4>
                        </td>
                        <td><input type="text" class="form-control textColor" id="CobranzaID"></td>
                     </tr>
                     <tr>
                        <td>
                           <h4 class="textoBlack"><strong><i class="icon-user-tie"></i> Vendedor</strong></h4>
                        </td>
                        <td><input type="text" class="form-control textColor" id="vendedorF"></td>
                     </tr>
                     <tr>
                        <td>
                           <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Adeudo</strong></h4>
                        </td>
                        <td><input type="text" class="form-control textColor" id="Adeudo"></td>
                     </tr>
                     <tr>
                        <td>
                           <h4 class="textoBlack"><strong><i class="icon-credit-card"></i> Nuevo Abono</strong></h4>
                        </td>
                        <td><input type="text" class="form-control textColor" id="Abono"></td>
                     </tr>
                     <tr>
                        <td class="col-lg-6"><button onclick="Abonar()" id="abonar" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                        <td class="col-lg-6"><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
   <div class="modal fade " id="Detalle">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header panel-header HeadPanel">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
               <h2 class="modal-title" align="center">Detalles de Apartado</h2>
            </div>
            <div class="modal-body" align="center">
               <div id="contenido" class="container-fluid">
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="modal fade " id="DelCobranza">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header panel-header HeadPanel">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title" align="center">Eliminar</h2>
               </div>
               <div class="modal-body" align="center">
                  <div class="alert alert-success alert-dismissible" id="alertaDel" align="center">
                  </div>
                  <table class="table">
                     <tr>
                        <td>
                           <h4 class="textoBlack"><strong><i class="icon-list-numbered"></i> Cobranza</strong></h4>
                        </td>
                        <td><input type="text" class="form-control textColor" id="CobranzaIDDel"></td>
                     </tr>
                     <tr>
                        <td>
                           <h4 class="textoBlack"><strong><i class="icon-user-tie"></i> Vendedor</strong></h4>
                        </td>
                        <td><input type="text" class="form-control textColor" id="vendedorFDel"></td>
                     </tr>
                     <tr>
                        <td>
                           <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Adeudo</strong></h4>
                        </td>
                        <td><input type="text" class="form-control textColor" id="AdeudoDel"></td>
                     </tr>
                     <tr>
                        <td class="col-lg-6"><button onclick="EliminarCobranza()" id="abonar" class="btn btn-success col-xs-12 col-lg-12">Eliminar</button></td>
                        <td class="col-lg-6"><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script src="js/jquery.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <script type="text/javascript" src="js/select2.min.js" ></script>
      <script type="text/javascript" src="js/select2_locale_es.js" ></script>
      <script>
        $(document).ready(function() { 
            $("#Clientes").select2({
        
                });

        });
      </script>
</body>
</html>