<?php  
   session_start();

   include("conexion.php");
   // Determina si se ha iniciado sesión 
   if (isset($_SESSION['user'])) {
   	echo "";
   }else{
   	echo '<script> window.location="index.php"; </script>';
   }

   if (isset($_SESSION['Almacen'])) {
    echo "";
} //isset($_SESSION['user'])
else {
    echo '<script> window.location="VentanaAlmacen.php"; </script>';
}

    if (isset($_SESSION['ValControl'])) {
        echo '<script> window.location="index.php"; </script>';
    } //isset($_SESSION['Vendedor'])
    else {
        echo "";
    }
   
   $profile = $_SESSION['user'];
   $Identificador = $_SESSION["Id_User"];
   $dominio = $_SESSION["dominio"];
   
   
   ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/clients.js"></script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
      <!--// Navigation bar -->
      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li class="active"><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
                  <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <?php // Consultas para llenar los select
         $querito = 'select * from departamentos where Id_User = '.$Identificador.' order by Departamento ASC ';
         $result = $cbd->query($querito);
         
         $queryImp = 'select * from impuestos where Id_User = '.$Identificador;
         $result2 = $cbd->query($queryImp);
         
         $queryUni = 'select * from unidades where Id_User = '.$Identificador;
         $result3 = $cbd->query($queryUni);
         
         $queryUni = 'select * from MetPago where Id_User = '.$Identificador;
         $result4 = $cbd->query($queryUni);
         
       ?>
      <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Control</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
               <br>
               <div class="alert alert-success alert-dismissible" id="alerta" align="center">
                  <strong>Datos del cliente modificados</strong>
               </div>
               <div class="alert alert-success alert-dismissible" id="alertaAdd" align="center">
                  <strong>Cliente Dado de alta</strong>
               </div>
               <div class="alert alert-success alert-dismissible" id="alertaDel" align="center">
                  <strong>Cliente Dado de baja</strong>
               </div>
               <button onclick="buscarDatos()" class="btn bg col-xs-12 col-sm-12 col-md-3 col-lg-4">Clientes</button>
               <button onclick="buscarProv()" class="btn bg col-xs-12 col-sm-12 col-md-3 col-lg-4">Proveedores</button>
               <button onclick="buscarVendedor()" class="btn bg col-xs-12 col-sm-12 col-md-3 col-lg-4">Vendedores</button><br><br>
               <button href="#Unidades" data-toggle="modal" class="btn bg col-xs-12 col-sm-12 col-md-3 col-lg-4">Unidades</button>
               <button  href="#Dpto" data-toggle="modal" class="btn bg col-xs-12 col-sm-12 col-md-3 col-lg-4">Departamentos</button>
               <button href="#MPAGO" data-toggle="modal"  class="btn bg col-xs-12 col-sm-12 col-md-3 col-lg-4">Metodos de Pago</button>
               <br>
               <br>
               <br>
               <br>
               <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 table-responsive" id="datosCliente">
               </div>
            </div>
         </div>
      </div>
      <!--// Mensajes de alerta --> 
      <br>
      <!--// Modal de agregar cliente --> 
      <div class="container">
      <div class="modal fade " id="Agregar">
         <div class="modal-dialog tamno-modal">
            <div class="modal-content">
               <div class="modal-header panel-header HeadPanel">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title" align="center">Agregar Cliente</h2>
               </div>
               <div class="modal-body" align="center">
                  <div class="modal-body" align="center">
                     <div class="alert alert-danger alert-dismissible" id="alertaMax13" align="center">
                        <strong>Numero de caracteres invalido</strong>
                     </div>
                     <table class="table table-striped">
                        <tr>
                           <td>Nombre</td>
                           <td><input class="form-control" id="NombreC" onblur="existecliente(this)"></td>
                           <td>País</td>
                           <td><input class="form-control" id="PaisC"></td>
                        </tr>
                        <tr>
                           <td>Lada</td>
                           <td><input onkeypress="return validarTel(this, 3, event)" class="form-control" id="LadaC"></td>
                           <td>Telefono</td>
                           <td><input onkeypress="return validarTel(this, 7, event)" class="form-control" id="TelC"></td>
                        </tr>
                        <tr>
                           <td>Estado</td>
                           <td><input class="form-control" id="StateC"></td>
                           <td>Ciudad.</td>
                           <td><input class="form-control" id="CityC"></td>
                        </tr>
                        <tr>
                           <td>Localidad</td>
                           <td><input class="form-control" id="LocC"></td>
                           <td>Calle</td>
                           <td><input class="form-control" id="CalleC"></td>
                        </tr>
                        <tr>
                           <td>Numero</td>
                           <td><input onkeypress="return validaR(event)" class="form-control" id="NumeroC"></td>
                           <td>C.P</td>
                           <td><input onkeypress="return validaR(event)" class="form-control" id="cpC"></td>
                        </tr>
                        <tr>
                           <td>Col.</td>
                           <td><input class="form-control" id="ColC"></td>
                           <td>RFC.</td>
                           <td><input onkeypress="return validarTamano(this, 13)" class="form-control" id="RFCC"></td>
                        </tr>
                        <tr>
                           <td>E-Mail</td>
                           <td><input class="form-control" id="MailC"></td>
                        </tr>
                        <tr>
                           <td>Clave Interna</td>
                           <td><input class="form-control" id="ClaveI"></td>
                        </tr>
                     </table>
                     <label class="text3 pull-left"> Observaciónes </label>
                     <textarea name="observ" id="observ" class="form-control" placeholder="Observaciónes"></textarea>
                     <table class="table">
                        <tr>
                           <td><button onclick="AddClient()" id="add" name="add" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--// Modal de Modificar Cliente --> 
      <div class="container">
         <div class="modal fade " id="Modificar">
            <div class="modal-dialog tamno-modal">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Modificar Cliente</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <div class="alert alert-danger alert-dismissible" id="alertaMax13U" align="center">
                        <strong>Numero de caracteres invalido</strong>
                     </div>
                     <table class="table table-striped">
                        <tr>
                           <td>Nombre</td>
                           <td><input class="form-control" id="NombreCM" ></td>
                           <td>País</td>
                           <td><input class="form-control" id="PaisCM"></td>
                        </tr>
                        <tr>
                           <td>Lada</td>
                           <td><input  onkeypress="return validarTel(this, 3, event)" class="form-control" id="LadaCM"></td>
                           <td>Telefono</td>
                           <td><input  onkeypress="return validarTel(this, 7, event)" class="form-control" id="TelCM"></td>
                        </tr>
                        <tr>
                           <td>Estado</td>
                           <td><input class="form-control" id="StateCM"></td>
                           <td>Ciudad.</td>
                           <td><input class="form-control" id="CityCM"></td>
                        </tr>
                        <tr>
                           <td>Localidad</td>
                           <td><input class="form-control" id="LocCM"></td>
                           <td>Calle</td>
                           <td><input class="form-control" id="CalleCM"></td>
                        </tr>
                        <tr>
                           <td>Numero</td>
                           <td><input onkeypress="return validaR(event)" class="form-control" id="NumeroCM"></td>
                           <td>C.P</td>
                           <td><input onkeypress="return validaR(event)" class="form-control" id="cpCM"></td>
                        </tr>
                        <tr>
                           <td>Col.</td>
                           <td><input class="form-control" id="ColCM"></td>
                           <td>RFC.</td>
                           <td><input onkeypress="return validarTamano(this, 13)" class="form-control" id="RFCCM"></td>
                        </tr>
                        <tr>
                           <td>E-Mail</td>
                           <td><input class="form-control" id="MailCM"></td>
                        </tr>
                        <tr>
                           <td>Clave Interna</td>
                           <td><input class="form-control" id="ClaveIM"></td>
                        </tr>
                     </table>
                     <label class="text3 pull-left"> Observaciónes </label>
                     <textarea name="observM" id="observM" class="form-control" placeholder="Observaciónes"></textarea>
                     <table class="table">
                        <tr>
                           <td><button id="ModificarC" onclick="UpdateCliente()" name="mod" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--// Modal de Baja Cliente --> 
      <div class="container">
         <div class="modal fade " id="Eliminar">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Baja Cliente</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <h3>Seguro que desea dar de baja el cliente</h3>
                     <br>
                     <table class="table">
                        <tr>
                           <td><button  id="delete" onclick="deleteClient()" name="delete" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--// Modal de Modificar Proveedor --> 
      <div class="container" align="center">
         <div class="modal fade " id="ModificarProv">
            <div class="modal-dialog tamno-modal">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Modificar Proveedor</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaProv" align="center">
                        <strong>Proveedor Modificado</strong>
                     </div>
                     <div class="alert alert-danger alert-dismissible" id="alertaErrorProv" align="center">
                     </div>
                     <div class="alert alert-danger alert-dismissible" id="alertaMax" align="center">
                        <strong>Numero de caracteres invalido</strong>
                     </div>
                     <table class="table table-striped">
                        <tr>
                           <td><label class="text3"> Nombre </label></td>
                           <td><input class="form-control" id="NombreProv"></td>
                           <td><label class="text3"> País </label></td>
                           <td><input class="form-control" id="PaisProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Lada </label></td>
                           <td><input onkeypress="return validarTel(this, 3, event)" class="form-control" id="LadaP"></td>
                           <td><label class="text3"> Telefono </label></td>
                           <td><input onkeypress="return validarTel(this, 7, event)" class="form-control" id="TelProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Estado </label></td>
                           <td><input class="form-control" id="StateProv"></td>
                           <td><label class="text3"> Ciudad </label></td>
                           <td><input class="form-control" id="CityProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Colonia </label></td>
                           <td><input class="form-control" id="ColProv"></td>
                           <td><label class="text3"> Localidad </label></td>
                           <td><input class="form-control" id="PobProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Código Postal </label></td>
                           <td><input onkeypress="return valida(event)" class="form-control" id="CpProv"></td>
                           <td><label class="text3"> R.F.C </label></td>
                           <td><input onkeypress="return validarTamano(this, 13)" class="form-control" id="RfcProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> E-Mail </label></td>
                           <td><input class="form-control" id="MailProv"></td>
                           <td><label class="text3"> Sitio web  </label></td>
                           <td><input class="form-control" id="WebProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3 pull-left"> Observaciónes </label></td>
                           <td><textarea name="observ" id="observ" class="form-control" placeholder="Observaciónes"></textarea></td>
                        </tr>
                     </table>
                     <br>
                     <button onclick="UpdtProv()" class="btn-success btn col-xs-5 col-lg-5 pull-left">Agregar</button>
                     <button data-dismiss="modal" class="btn-danger btn col-xs-5 col-lg-5 pull-right">Cancelar</button>
                     <br>
                     <br>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--// Modal de Eliminar un proveedor --> 
      <div class="container">
         <div class="modal fade " id="EliminarProv">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Eliminar un proveedor</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <h3>Seguro que desea eliminar</h3>
                     <br>
                     <div class="alert alert-success alert-dismissible" id="alertaDel2" align="center">
                        <strong>Proveedor Eliminado</strong>
                     </div>
                     <table class="table">
                        <tr>
                           <td><button onclick="Borrar()" id="delete" name="delete" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--// Modal de Departamentos --> 
      <div class="container">
         <div class="modal fade " id="Dpto">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Departamentos</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <br>
                     <table class="table">
                        <tr>
                           <td><label class="text3"> Departamento </label></td>
                           <td>
                              <select class="form-control" id="dpt">
                                 <?php while ($fila = mysqli_fetch_array($result)){ ?>
                                 <option value="<?php echo $fila['id_Dept'];?>"><?php echo $fila['Departamento'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Nuevo Nombre </label></td>
                           <td><input class="form-control" id="newdepto"></td>
                        </tr>
                        <tr>
                           <td class="col-xs-6 col-lg-6" ><button class="btn btn-success col-xs-12 col-lg-12" onclick="ModDpto()" >Modificar</button></td>
                           <td class="col-xs-6 col-lg-6"><button data-dismiss="modal" class="btn btn-danger col-xs-12 col-lg-12" >Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--// Modal de Impuestos --> 
      <div class="container">
         <div class="modal fade " id="Imp">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Impuestos</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <br>
                     <table class="table">
                        <tr>
                           <td><label class="text3"> Impuesto </label></td>
                           <td>
                              <select class="form-control" id="impu">
                                 <?php while ($fila2 = mysqli_fetch_array($result2)){ ?>
                                 <option value="<?php echo $fila2['id'];?>"><?php echo $fila2['Impuesto'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Nombre </label></td>
                           <td><input class="form-control" id="newImp"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Porcentaje </label></td>
                           <td><input class="form-control" id="newPor"></td>
                        </tr>
                        <tr>
                           <td class="col-xs-6 col-lg-6" ><button class="btn btn-success col-xs-12 col-lg-12" onclick="ModImp()" >Modificar</button></td>
                           <td class="col-xs-6 col-lg-6"><button data-dismiss="modal" class="btn btn-danger col-xs-12 col-lg-12" >Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--// Modal de Unidades --> 
      <div class="container">
         <div class="modal fade " id="Unidades">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Unidades</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <br>
                     <table class="table">
                        <tr>
                           <td><label class="text3"> Unidad </label></td>
                           <td>
                              <select class="form-control" id="unidad">
                                 <?php while ($fila3 = mysqli_fetch_array($result3)){ ?>
                                 <option value="<?php echo $fila3['Codigo'];?>"><?php echo $fila3['Unidad'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Nombre Unidad </label></td>
                           <td><input class="form-control" id="newUni"></td>
                        </tr>
                        <tr>
                           <td class="col-xs-6 col-lg-6" ><button class="btn btn-success col-xs-12 col-lg-12" onclick="ModUni()" >Modificar</button></td>
                           <td class="col-xs-6 col-lg-6"><button data-dismiss="modal" class="btn btn-danger col-xs-12 col-lg-12" >Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--// Modal de Metodos de Mago --> 
      <div class="container">
         <div class="modal fade " id="MPAGO">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Metodos de Mago</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <div class="alert alert-success alert-dismissible" id="aletrMP" align="center">
                     </div>
                     <br>
                     <table class="table">
                        <tr>
                           <td><label class="text3"> Registrados </label></td>
                           <td>
                              <select class="form-control" id="mPago">
                                 <?php while ($fila4 = mysqli_fetch_array($result4)){ ?>
                                 <option value="<?php echo $fila4['id'];?>"><?php echo $fila4['Mpago'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Metodo de Pago </label></td>
                           <td><input class="form-control" id="ValorMpago"></td>
                        </tr>
                     </table>
                     <button class="btn btn-success col-xs-12 col-lg-4" onclick="AddMpago()">Agregar</button>
                     <button class="btn btn-warning col-xs-12 col-lg-4" onclick="ModMpago()">Modificar</button>
                     <button  class="btn btn-danger col-xs-12 col-lg-4" onclick="DelMpago()" >Eliminar</button>
                     <br>
                     <br>
                  </div>
               </div>
            </div>
         </div>
      </div>

            <div class="container">
         <div class="modal fade " id="ModVendedor">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Asignar Privilegios</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaVend" align="center">
                     </div>
                     <table class="table">

                        <tr>
                            <td COLSPAN='2'>
                            <div class="form-check form-check-inline" align="center">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input separachk" type="checkbox" id="Inventario">
                            <label class="form-check-label" for="Inventario"> Inventario </label>
                            <input class="form-check-input separachk" type="checkbox" id="Compras">
                            <label class="form-check-label" for="Compras"> Compras </label>
                            <input class="form-check-input separachk" type="checkbox" id="Control">
                            <label class="form-check-label" for="Control"> Control </label>
                            <input class="form-check-input separachk" type="checkbox" id="Reportes">
                            <label class="form-check-label" for="Reportes"> Reportes </label>
                            <input class="form-check-input separachk" type="checkbox" id="Operaciones">
                            <label class="form-check-label" for="Operaciones"> Operaciones </label>
                        </div>
                        </div>
                            </td>
                        </tr>

                        <tr>
                           <td class="col-lg-6"><button onclick="ModVendedor()" id="delete" name="delete" class="btn btn-success col-xs-12 col-lg-12">Modificar</button></td>
                           <td class="col-lg-6"><button class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="container">
         <div class="modal fade " id="EliminarVendedores">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Baja Vendedor</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <h3>Seguro que desea dar de baja el vendedor</h3>
                     <br>
                     <table class="table">
                        <tr>
                           <td><button  id="delete" onclick="deleteVendedor()"  class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>