<?php
    // Incluimos la librería de PDF
	require('fpdf/fpdf.php');
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión
	if (isset($_SESSION['user'])) {
	} //isset($_SESSION['user'])
	else {
		echo '<script> window.location="index.php"; </script>';
	}
	// Determina si se ha iniciado la claveCliente
	if (isset($_SESSION['claveCliente'])) {
	} //isset($_SESSION['claveCliente'])
	else {
		echo '<script> window.location="menu.php"; </script>';
	}
	// Determina si se ha iniciado el vendedor
	if (isset($_SESSION["vendedorT"])) {
	} //isset($_SESSION["vendedorT"])
	else {
		echo '<script> window.location="menu.php"; </script>';
	}
	// Inicializamos variables de sesión
	$Identificador = $_SESSION["Id_User"];
	$Cliente       = $_SESSION['claveCliente'];
	$Vendedor      = $_SESSION["vendedorT"];
	$Nombre        = "";
	if ($Cliente == "Todo") {
		$Nombre = "Todos";
	} //$Cliente == "Todo"
	else {
		$queryCl = 'select Nombre from clients where Id_User = ' . $Identificador . ' AND ClaveCliente = ' . $Cliente;
		$ResCl   = $cbd->query($queryCl);
		$filaCl  = mysqli_fetch_array($ResCl);
		$Nombre  = $filaCl['Nombre'];
	}
	class PDF extends FPDF
	{
		// Cabecera de página
		function Header()
		{
			include("conexion.php");
			$Identificador = $_SESSION["Id_User"];
			$Cliente       = $_SESSION['claveCliente'];
			$Vendedor      = $_SESSION["vendedorT"];
			$Nombre        = "";
			if ($Cliente == "Todo") {
				$Nombre = "Todos";
			} //$Cliente == "Todo"
			else {
				$queryCl = 'select Nombre from clients where Id_User = ' . $Identificador . ' AND ClaveCliente = ' . $Cliente;
				$ResCl   = $cbd->query($queryCl);
				$filaCl  = mysqli_fetch_array($ResCl);
				$Nombre  = $filaCl['Nombre'];
			}
			$Almacen = $_SESSION["Almacen"];
			$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
			$ResEmp   = $cbd->query($queryEmp);
			$filaEmp  = mysqli_fetch_array($ResEmp);
			$this->SetFont('Arial', 'B', 13);
			if ($filaEmp['img'] == "ProImg/sinImg.jpg") {
				$this->SetY(15);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
				$this->SetX(55);
			} //$filaEmp['img'] == "ProImg/sinImg.jpg"
			else {
				$this->Image($filaEmp['img'], 10, 7, 40, 28);
				$this->SetY(15);
				$this->SetX(55);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
			}
			$this->SetFont('Arial', 'B', 13);
			$this->Cell(51, 10, utf8_decode($filaEmp['Nombre']), 0, 1, 'C');
			$this->Ln(9);
			$this->Line(10, 35, 199, 35);
			$this->SetFont('Arial', 'B', 10);
			$this->Cell(100, 10, utf8_decode('Reporte por Cliente'), 0, 0, 'L');
			$this->Ln(5);
			$this->Cell(100, 10, utf8_decode('Cliente ' . $Nombre), 0, 0, 'L');
			$this->Ln(15);
			$this->SetFont('Arial', '', 9);
			$this->Cell(65, 6, utf8_decode('Nombre'), 1, 0, 'C');
			$this->Cell(20, 6, utf8_decode('Cobranza'), 1, 0, 'C');
			$this->Cell(22.5, 6, 'Fecha', 1, 0, 'C');
			$this->Cell(22.5, 6, 'Vencimiento', 1, 0, 'C');
			$this->Cell(20, 6, 'Total', 1, 0, 'C');
			$this->Cell(20, 6, 'Abono.', 1, 0, 'C');
			$this->Cell(20, 6, 'Adeudo', 1, 1, 'C');
		}
	}
	// Se crea el PDF
	$pdf = new PDF();
	// Agrega nueva página
	$pdf->AddPage();
	//$Vendedor = $_SESSION["vendedorT"];
	$pdf->SetFont('Arial', '', 9);
	// Se determina si son todos los clientes o solo uno
	if ($Cliente == 'Todo') {
		$queryProds = "SELECT * FROM cobranza WHERE Id_User = " . $Identificador . " AND Vendedor = '$Vendedor' AND Almacen = " . $_SESSION["Almacen"] . " order by Cliente";
	} //$Cliente == 'Todo'
	else {
		$queryProds = "SELECT * FROM cobranza WHERE Id_User = " . $Identificador . " AND Vendedor = '$Vendedor' AND Almacen = " . $_SESSION["Almacen"] . " AND Cliente = " . "'" . $Nombre . "'";
	}
	$ResProds = $cbd->query($queryProds);
	while ($filaProds = mysqli_fetch_array($ResProds)) {
		// Se llenan las partidas
		$pdf->Cell(65, 6, utf8_decode($filaProds['Cliente']), 0, 0, 'C');
		$pdf->Cell(20, 6, utf8_decode($filaProds['id']), 0, 0, 'C');
		$pdf->Cell(22.5, 6, $filaProds['Fecha'], 0, 0, 'C');
		$pdf->Cell(22.5, 6, $filaProds['Vencimiento'], 0, 0, 'C');
		$pdf->Cell(20, 6, "$" . $filaProds['Total'], 0, 0, 'C');
		$pdf->Cell(20, 6, "$" . $filaProds['Abono'], 0, 0, 'C');
		$pdf->Cell(25, 6, "$" . $filaProds['Adeudo'], 0, 1, 'C');
	} //$filaProds = mysqli_fetch_array($ResProds)
	$pdf->Ln(8);
	if ($Cliente == 'Todo') {
		$queryTot = "SELECT  Sum(Abono) as Abono, Sum(Adeudo) as Adeudo,  Sum(Total) as Total FROM cobranza WHERE Id_User = " . $Identificador . " AND Vendedor = '$Vendedor' AND Almacen = " . $_SESSION["Almacen"];
	} //$Cliente == 'Todo'
	else {
		$queryTot = "SELECT  Sum(Abono) as Abono, Sum(Adeudo) as Adeudo,  Sum(Total) as Total FROM cobranza WHERE Id_User = " . $Identificador . " AND Vendedor = '$Vendedor' AND Almacen = " . $_SESSION["Almacen"] . " AND Cliente = " . "'" . $Nombre . "'";
	}
	$ResTot  = $cbd->query($queryTot);
	$filaTot = mysqli_fetch_array($ResTot);
	$pdf->Cell(65, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(20, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(22.5, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(22.5, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(20, 6, "$" . $filaTot['Total'], 1, 0, 'C');
	$pdf->Cell(20, 6, "$" . $filaTot['Abono'], 1, 0, 'C');
	$pdf->Cell(20, 6, "$" . $filaTot['Adeudo'], 1, 1, 'C');
	// Se muestra el PDF en pantalla
	$pdf->Output();
?>