<?php
	// Incluimos la librería de PDF
	require('fpdf/fpdf.php');
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión
	if (isset($_SESSION['user'])) {
		echo "";
	} //isset($_SESSION['user'])
	else {
		echo '<script> window.location="index.php"; </script>';
	}
	// Inicializamos variables de sesión
	$Identificador = $_SESSION["Id_User"];
	$FI            = $_SESSION["FI"];
	$FF            = $_SESSION["FF"];
	$vendedor      = $_SESSION["vendedorT"];
	class PDF extends FPDF
	{
		// Cabecera de página
		function Header()
		{
			include("conexion.php");
			$Identificador = $_SESSION["Id_User"];
			$FI            = $_SESSION["FI"];
			$FF            = $_SESSION["FF"];
			$vendedor      = $_SESSION["vendedorT"];
			$Almacen = $_SESSION["Almacen"];
			$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
			$ResEmp        = $cbd->query($queryEmp);
			$filaEmp       = mysqli_fetch_array($ResEmp);
			$queryV        = "select * from ventas where Id_User = " . $Identificador . " AND Vendedor = '$vendedor' AND Almacen = " . $_SESSION["Almacen"] . " AND (Fecha BETWEEN " . $FI . " AND " . $FF . ")";
			$ResV          = $cbd->query($queryV);
			$filaV         = mysqli_fetch_array($ResV);
			$this->SetFont('Arial', 'B', 13);
			if ($filaEmp['img'] == "ProImg/sinImg.jpg") {
				$this->SetY(15);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
				$this->SetX(55);
			} //$filaEmp['img'] == "ProImg/sinImg.jpg"
			else {
				$this->Image($filaEmp['img'], 10, 7, 40, 28);
				$this->SetY(15);
				$this->SetX(55);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
			}
			$this->Cell(51, 10, utf8_decode($filaEmp['Nombre']), 0, 1, 'C');
			$this->Ln(9);
			$this->Line(10, 35, 199, 35);
			$this->SetFont('Arial', 'B', 10);
			$this->Cell(100, 10, utf8_decode('Ventas del ' . $FI . ' al ' . $FF), 0, 0, 'L');
			$this->Ln(15);
			$this->SetFont('Arial', 'B', 9);
			$this->Cell(38.4, 6, utf8_decode('Venta'), 1, 0, 'C');
			$this->Cell(38.4, 6, utf8_decode('Fecha'), 1, 0, 'C');
			$this->Cell(38.4, 6, 'Importe', 1, 0, 'C');
			$this->Cell(38.4, 6, 'Impuesto.', 1, 0, 'C');
			$this->Cell(38.4, 6, 'Total', 1, 1, 'C');
		}
	}
	// Se crea el PDF
	$pdf = new PDF();
	// Agrega nueva página
	$pdf->AddPage();
	$pdf->SetFont('Arial', '', 9);
	$queryV = "select * from ventas where Id_User = " . $Identificador . " AND Vendedor = '$vendedor' AND Almacen = " . $_SESSION["Almacen"] . " AND (Fecha BETWEEN " . "'" . $FI . "'" . " AND " . "'" . $FF . "'" . ')';
	$ResV   = $cbd->query($queryV);
	$Total  = 0;
	$Import = 0;
	$Iva    = 0;
	while ($filaV = mysqli_fetch_array($ResV)) {
		$pdf->Cell(38.4, 6, utf8_decode($filaV['Venta']), 0, 0, 'C');
		$pdf->Cell(38.4, 6, utf8_decode($filaV['Fecha']), 0, 0, 'C');
		$pdf->Cell(38.4, 6, $filaV['Importe'], 0, 0, 'C');
		$pdf->Cell(38.4, 6, $filaV['Impuesto'], 0, 0, 'C');
		$pdf->Cell(38.4, 6, $filaV['Total'], 0, 1, 'C');
		$Total  = $Total + $filaV['Total'];
		$Import = $Import + $filaV['Importe'];
		$Iva    = $Iva + $filaV['Impuesto'];
	} //$filaV = mysqli_fetch_array($ResV)
	$pdf->SetX(125.23);
	$pdf->SetFont('Arial', 'B', 9);
	$pdf->Cell(38.4, 6, 'Importe Final', 1, 0, 'C');
	$pdf->SetFont('Arial', '', 9);
	$pdf->Cell(38.4, 6, round($Import, 2), 1, 1, 'C');
	$pdf->SetFont('Arial', 'B', 9);
	$pdf->SetX(125.23);
	$pdf->Cell(38.4, 6, 'Impuesto Final', 1, 0, 'C');
	$pdf->SetFont('Arial', '', 9);
	$pdf->Cell(38.4, 6, round($Iva, 2), 1, 1, 'C');
	$pdf->SetFont('Arial', 'B', 9);
	$pdf->SetX(125.23);
	$pdf->Cell(38.4, 6, 'Total Final', 1, 0, 'C');
	$pdf->SetFont('Arial', '', 9);
	$pdf->Cell(38.4, 6, round($Total, 2), 1, 1, 'C');
	// Se muestra el PDF en pantalla
	$pdf->Output();
?>