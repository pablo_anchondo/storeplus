document.writeln('<script src="js/jQuery v3.3.1.js"></script>'); //Incluir JQUERY
var src = "http://localhost/Proyectos/SPLocal/";
window.onload = (function() {
    $('#alerta').hide(); //Oculta las alertas cuando carga la pagina  
    $('#alertaAlm').hide();
    FechaC();
});



function mostrarAlm(mensaje, color) { //Función para mostrar la alerta en pantalla con un mensaje y color verde o rojo
    if (color == "rojo") {
        $("#alertaAlm").removeClass("alert-success");
        $("#alertaAlm").addClass("alert-danger");
    } else {
        $("#alertaAlm").removeClass("alert-danger");
        $("#alertaAlm").addClass("alert-success");
    }
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaAlm').html(data);
    $('#alertaAlm').show('fade');
    setTimeout(function() {
        $('#alertaAlm').hide('fade');
        window.location="menu.php";
    }, 1000);
}

function CrearModal() { //Abre el modal para seleccionar el almacén y no podrá cerrarse hasta seleccionarlo
    $('#SelectAlmacen').modal({
        backdrop: 'static',
        keyboard: false
    });
}

function FechaC() { //Función para obtener los días restantes del servicio para el cliente y mostrarlo en pantalla
    var id = "corteFecha";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var Corte = new Date(json.Corte);
            var Fecha = new Date(json.Fecha);
            var Dat = json.Corte;
            var dias_en_milisegundos = 86400000;
            var dif_en_milisegundos = Corte - Fecha;
            var diff_en_dias = dif_en_milisegundos / dias_en_milisegundos;
            var data = "<h2> Vigencia: " + diff_en_dias + " Días restantes </h2>";
            $('#FCorte').html(data);
            if (diff_en_dias >= 6) {
                $("#FCorte").addClass("alert-success");
            }
            if (diff_en_dias <= 5 && diff_en_dias >= 3) {
                $("#FCorte").addClass("alert-warning");
            }
            if (diff_en_dias <= 2) {
                $("#FCorte").addClass("alert-danger");
            }
        },
        error: function(data) {}
    });
}

function AlmacenPred() { //Funcion del boton que selecciona el almacen en el que se trabajara
    var alma = document.getElementById("almacen").value;
    var id = "CambioAlmacen";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            alma: alma
        },
        success: function(data) {
            mostrarAlm("Almacen Seleccionado");
           
        },
        error: function(data) {}
    });
}
