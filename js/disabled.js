document.writeln('<script src="js/jQuery v3.3.1.js"></script>'); //Incluir JQUERY
var src = "http://localhost/Proyectos/SPLocal/";
window.onload = (function() {
    $('#alerta').hide();
    $('#alertaAlm').hide();
    $('.vap1').click(function(){
        mostrar("No tienes acceso a esta función");
    });
    FechaC();
});

function mostrarAlm(mensaje, color) { //Función para mostrar la alerta en pantalla con un mensaje y color verde o rojo
    if (color == "rojo") {
        $("#alertaAlm").removeClass("alert-success");
        $("#alertaAlm").addClass("alert-danger");
    } else {
        $("#alertaAlm").removeClass("alert-danger");
        $("#alertaAlm").addClass("alert-success");
    }
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaAlm').html(data);
    $('#alertaAlm').show('fade');
    setTimeout(function() {
        $('#alertaAlm').hide('fade');
        location.reload();
    }, 1000);
}

function CrearModal() { //Abre el modal para seleccionar el almacén y no podrá cerrarse hasta seleccionarlo
    $('#SelectAlmacen').modal({
        backdrop: 'static',
        keyboard: false
    });
}

function mostrar(mensaje) { //Función para mostrar un mensaje denegando el acceso a alguna función
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alerta').html(data);
    $('#alerta').show('fade');
    setTimeout(function() {
        $('#alerta').hide('fade');
    }, 1000);
}

function FechaC() { //Función para obtener los días restantes del servicio para el cliente y mostrarlo en pantalla
    var id = "corteFecha";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var Corte = new Date(json.Corte);
            var Fecha = new Date(json.Fecha);
            var Dat = json.Corte;
            var day_as_milliseconds = 86400000;
            var diff_in_millisenconds = Corte - Fecha;
            var diff_in_days = diff_in_millisenconds / day_as_milliseconds;
            var data = "<h2> Vigencia: "  + diff_in_days + " Días restantes </h2>";
            $('#FCorte').html(data);
            if (diff_in_days >= 6) {
                $("#FCorte").addClass("alert-success");
            }
            if (diff_in_days <= 5 && diff_in_days >= 3) {
                $("#FCorte").addClass("alert-warning");
            }
            if (diff_in_days <= 2) {
                $("#FCorte").addClass("alert-danger");
            }
        },
        error: function(data) {}
    });
}

function AlmacenPred() { //Funcion del boton que selecciona el almacen en el que se trabajara
    var alma = document.getElementById("almacen").value;
    var id = "CambioAlmacen";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            alma: alma
        },
        success: function(data) {
            mostrarAlm("Almacen Seleccionado");
        },
        error: function(data) {}
    });
}



