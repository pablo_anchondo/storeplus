document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
var Adeudo = 0;
var parcialidad = 0;
var Datos = new Array;

window.onload = (function () {



    buscarDatos();


    $('#caja_busqueda').on('keyup', function() { //Detecta cuando hay un cambio en el buscador
        var valor = $(this).val();
        if (valor != "") {
            buscarDatos(valor);
        } else {
            buscarDatos("");
        }
    }).keyup();

    $('#tarjeta1').hide();
    $('#tarjeta2').hide();

    $('#ppdDisabled').hide();

    $('#importepagado').on('keyup', function() {
        var valor = $(this).val();
        var decallowed = 2;
        if (valor.indexOf('.') == -1) {
            valor += ".";
        }
        dectext = valor.substring(valor.indexOf('.') + 1, valor.length);
        if (dectext.length > decallowed) {
            alert("Maximo 2 decimales");
            valor = valor.substr(0, valor.length - 1);
            document.getElementById("importepagado").value = valor;
        }
    }).keyup();


    $('#monto').on('keyup', function() {
        var valor = $(this).val();
        var decallowed = 2;
        if (valor.indexOf('.') == -1) {
            valor += ".";
        }
        dectext = valor.substring(valor.indexOf('.') + 1, valor.length);
        if (dectext.length > decallowed) {
            alert("Maximo 2 decimales");
            valor = valor.substr(0, valor.length - 1);
            document.getElementById("monto").value = valor;
        }
    }).keyup();


});

function pad2(number) {

    return (number < 10 ? '0' : '') + number

}




function buscarDatos(consult) { // Función que realiza consulta a la base de datos y llena el modal de productos
    var id = "buscarppd";
    var consulta = consult;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta
        },
        success: function(data) {
            $('#TablaPPD').html(data);
        },
        error: function(data) {}
    });
}


function aparecer(forma){
    if (forma.value == "04" || forma.value == "28" || forma.value == "03") {
        $('#tarjeta1').show("fade");
        $('#tarjeta2').show("fade");
        //'fade'
    }else{
        $('#tarjeta1').hide("fade");
        $('#tarjeta2').hide("fade");
    }
}


function getRFC(client){
    if (client.value == 'nulo') {
        document.getElementById("rfce").value = "";
    }else{
        document.getElementById("rfce").value = client.value;
        var id = "getMail";
        var rfcmail = client.value;
        $.ajax({
            url: src + 'valores.php',
            method: 'POST',
            data: {
                id: id,
                rfcmail: rfcmail
            },
            success: function(data) {
                var json = jQuery.parseJSON(data);
                var Mail = json.Mail;
                document.getElementById("mail").value = Mail;
            },
            error: function(data) {}
        });
    }
}


function mostrarOpciones(item){
    if (item.value == 'PUE' || item.value == 'nulo') {
        $('#ppdDisabled').hide("fade");
        document.getElementById("importepagado").value = Adeudo;
        document.getElementById("importepagado").disabled = true;
    }else{
        $('#ppdDisabled').show("fade");
        document.getElementById("importepagado").disabled = false;
        document.getElementById("importepagado").value = "";
        document.getElementById("parcialidad").value = parcialidad;
        document.getElementById("saldoAnterior").value = Adeudo;
        
    }
}



function validarTel(caja, tamano, e) { // Función que valida si la entrada es numérica o no y además limita su tamaño
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        alert("Maximo " + tamano + " caracteres");
        return false;
    }
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validarTamano(caja, tamano) { // Función que valida el tamaño de una cadena y lo limita
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        alert("Maximo " + tamano + " caracteres");
        return false;
    }
}


function valida(e) { // Función que solo acepta números como entrada
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}



function dataSelect(serie, folio){
    var id = "dataSelect";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            serie: serie,
            folio: folio
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var UUID = json.UUID;
            Adeudo = json.Adeudo;
            parcialidad = json.parcialidad;
            parcialidad = parseInt(parcialidad + 1);

            document.getElementById("uuid").value = UUID;
            document.getElementById("serie").value = serie;
            document.getElementById("folio").value = folio;
            document.getElementById("importepagado").value = Adeudo;
            document.getElementById("importepagado").disabled = true;
        },
        error: function(data) {}
    });
}



function insoluto(abono){
    var deuda = document.getElementById("saldoAnterior").value;
    deuda = parseFloat(deuda);
    abono = parseFloat(abono.value);

    if (abono == deuda || abono > deuda) {
        alert("El Importe Pagado debe ser menor que el saldo anterior");
        document.getElementById("importepagado").value = "";
        document.getElementById("Insoluto").value = "";
        return;
    }else{
        var insolut = parseFloat(deuda - abono).toFixed(2);
        
        document.getElementById("Insoluto").value = insolut;
    }

}




function generarComplemento(){
    var id="GenerarComp";
    if (document.getElementById("NombreR").value == 'nulo') {
        alert("Debes Seleccionar un cliente");
        return;
    }else{
        var Receptor = document.getElementById("NombreR");
        var Receptor = Receptor.options[Receptor.selectedIndex].text;
    }

    if (document.getElementById("rfce").value == '') {
        alert("Debes Seleccionar un cliente");
        return;
    }else{
        var rfce = document.getElementById("rfce").value;
    }
   

    if (document.getElementById("fecha").value == '') {
        alert("Debes indicar la fecha");
        return;
    } else {
        var fecha = document.getElementById("fecha").value;
        var fe = new Date();
        var HoraV = pad2(fe.getHours()) + ":" + (pad2(fe.getMinutes())) + ":" + pad2(fe.getSeconds());
        fecha = fecha + "T" + HoraV;
    }

    
    if (document.getElementById("monto").value == '') {
        alert("Debes indicar el monto");
        return;
    } else {
        var monto = document.getElementById("monto").value;
    }

    if (document.getElementById("uuid").value == '') {
        alert("Debes seleccionar una venta");
        return;
    } else {
        var uuid = document.getElementById("uuid").value;
    }

    if (document.getElementById("serie").value == '') {
        alert("Debes seleccionar una venta");
        return;
    } else {
        var serie = document.getElementById("serie").value;
    }

    if (document.getElementById("folio").value == '') {
        alert("Debes seleccionar una venta");
        return;
    } else {
        var folio = document.getElementById("folio").value;
    }

    if (document.getElementById("mp").value == 'nulo') {
        alert("Debes Seleccionar un metodo de pago");
        return;
    }else{
        var mp = document.getElementById("mp").value;
    }

    if (document.getElementById("importepagado").value == 'nulo') {
        alert("Debes indicar el importe a pagar");
        return;
    }else{
        var importepagado = document.getElementById("importepagado").value;
    }

    if (monto != importepagado) {
        alert("El monto " + monto + " y el importe pagado " + importepagado + " no son iguales");
        return;
    }


    if (document.getElementById("mail").value == "") {
        var mail = "";
    }else{
        var mail = document.getElementById("mail").value;
    }

    var moneda = document.getElementById("moneda").value;
    var uso = document.getElementById("uso").value;
    var fp = document.getElementById("fp").value;

    if ((fp == '03' || fp == '04' || fp == '28') && document.getElementById("co").value == '') {
        alert("Debes indicar la cuenta del ordenante");
        return;
    }else{
        var co = document.getElementById("co").value;
        if ((fp == '03' || fp == '04' || fp == '28') && co.length < 16) {
            alert("La cuenta del ordenante no tiene 16 caracteres");
            return;
        }
    }

    if ((fp == '03' || fp == '04' || fp == '28') && document.getElementById("cb").value == '') {
        alert("Debes indicar la cuenta del beneficiario");
        return;
    }else{
        var cb = document.getElementById("cb").value;
        if ((fp == '03' || fp == '04' || fp == '28') && cb.length < 16) {
            alert("La cuenta del beneficiario no tiene 16 caracteres");
            return;
        }
    }

    if ((fp == '03' || fp == '04' || fp == '28') && document.getElementById("rfcco").value == '') {
        alert("Debes indicar el RFC del banco del ordenante");
        return;
    }else{
        var rfcco = document.getElementById("rfcco").value;
    }

    if ((fp == '03' || fp == '04' || fp == '28') && document.getElementById("RfcEmisor").value == '') {
        alert("Debes indicar el RFC del emisor");
        return;
    }else{
        var RfcEmisor = document.getElementById("RfcEmisor").value;
    }

    if (mp == 'PPD' && document.getElementById("Insoluto").value == '') {
        alert("Debes indicar el importe a pagar");
        return;
    }else{
        var Insoluto = document.getElementById("Insoluto").value;
    }


    if (mp == 'PPD' && (fp == '03' || fp == '04' || fp == '28')) {
        Datos = [1, Receptor, rfce, fecha, uuid, serie, folio, mp, importepagado, moneda, uso, fp, co, cb, rfcco, RfcEmisor, Insoluto, parcialidad, Adeudo];
    }else if(mp == 'PPD' && (fp == '01' || fp == '99')){
        Datos = [2, Receptor, rfce, fecha, uuid, serie, folio, mp, importepagado, moneda, uso, fp, Insoluto, parcialidad, Adeudo];
    }else if(mp == 'PUE' && (fp == '03' || fp == '04' || fp == '28')){
        Datos = [3, Receptor, rfce, fecha, uuid, serie, folio, mp, importepagado, moneda, uso, fp, co, cb, rfcco, RfcEmisor];
    }else if(mp == 'PUE' && (fp == '01' || fp == '99')){
        Datos = [4, Receptor, rfce, fecha, uuid, serie, folio, mp, importepagado, moneda, uso, fp];
    }

    var bancoName = document.getElementById("rfcco");
    var bancoName = bancoName.options[bancoName.selectedIndex].text;


    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Datos: Datos,
            bancoName: bancoName,
            mail: mail
        },
        success: function(data) {
            setTimeout(function() {
                var url = src + "generarCompPago.php";
                abrirEnPestana(url);
                location.reload();
            }, 1000);
        },
        error: function(data) {}
    });


}


function abrirEnPestana(url) { // Función que abre una página en una nueva pestaña
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}