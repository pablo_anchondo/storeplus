document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
var articulo = "";
var descrip = "";
var Existens = 0;
var cont = 0;
var array = new Array;
var Partidas = new Array;
var contArray = 0;
var Entrada = 0;
window.onload = (function() {
    $('#alerta').hide();
});

function Consulta() { // Consulta a la tabla de datos productos
    var id = "ConsulSalida";
    var desc = document.getElementById("prod").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            desc: desc
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            Existens = json.existencia;
            LlenaTabla();
        },
        error: function(data) {}
    });
}



function LlenaTabla() { // Llena la tabla con la información del producto seleccionado
    var Producto = document.getElementById("prod").value;
    if (document.getElementById("cant").value <= 0) {
        mostrar("La cantidad debe ser mayor a 0");
        return;
    } else {
        var cant = document.getElementById("cant").value;
    }
    cont++;
    var fila = "<tr><td>" + cont + "</td><td>" + cant + "</td><td>" + Producto + "</td><td>" + Existens + "</td><tr>";
    var btn = document.createElement("TR");
    btn.innerHTML = fila;
    $('#TableBody').append(btn);
    array = [cant, Producto];
    Partidas[contArray] = [array];
    contArray++;
}

function borrar() { // Elimina la última fila de la tabla
    var trs = $("#TableBody tr").length;
    if (trs > 2) {
        if (trs == 3) {
            $("#TableBody tr:last").remove();
            location.reload();
        } else {
            $("#TableBody tr:last").remove();
        }
    }
    Partidas.pop();
    contArray = contArray - 1;
}

function validarTamano(caja, tamano) { // Función que valida el tamaño de una cadena y lo limita
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        return false;
    }
}

function mostrar(mensaje) { // Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alerta').html(data);
    $('#alerta').show('fade');
    setTimeout(function() {
        $('#alerta').hide('fade');
        location.reload();
    }, 1000);
}

function Entradas() { // Agrega las entradas a la DB
    var trs = $("#TableBody tr").length;
    if (trs == 2) {
        mostrar("Debes agregar productos primero");
        return;
    }
    var id = "Entradas";
    var Concepto = document.getElementById("conceptoSalida").value;
    var Comentarios = document.getElementById("Comentarios").value;
    var fe = new Date();
    var FechaV = fe.getFullYear() + "-" + (fe.getMonth() + 1) + "-" + fe.getDate();
    var HoraV = fe.getHours() + ":" + (fe.getMinutes()) + ":" + fe.getSeconds();
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Concepto: Concepto,
            FechaV: FechaV,
            HoraV,
            HoraV,
            Comentarios: Comentarios
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            id = "PartEntrada";
            Entrada = json.Entrada;
            var Descripcion = "";
            var Cantidad = 0;
            for (var i = 0; i < Partidas.length; i++) { // Agrega las partidas de las entradas
                Cantidad = Partidas[i][0][0];
                Descripcion = Partidas[i][0][1];
                $.ajax({
                    url: src + 'valores.php',
                    method: 'POST',
                    data: {
                        id: id,
                        Entrada: Entrada,
                        Descripcion: Descripcion,
                        Cantidad: Cantidad
                    },
                    success: function(data) {},
                    error: function(data) {}
                });
            }
            mostrar("Proceso Realizado");
        },
        error: function(data) {}
    });
}