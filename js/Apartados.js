document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
window.onload = (function () {
    document.getElementById("vendedorF").disabled = true;
    document.getElementById("Adeudo").disabled = true;
    document.getElementById("CobranzaID").disabled = true;
    document.getElementById("CobranzaIDDel").disabled = true;
    document.getElementById("vendedorFDel").disabled = true;
    document.getElementById("AdeudoDel").disabled = true;
    $('#alertaAbono').hide();
    $('#alertaDel').hide();
});

function GetCobr() { //Obtenemos las cobranzas que tiene el vendedor seleccionado
    var id = "GetCobr";
    var Vendedor = document.getElementById("Vendedors").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Vendedor: Vendedor
        },
        success: function (data) {
            $('#datCobr').html(data);
        },
        error: function (data) { }
    });
}

function GetValues() { //Llenamos la tabla con la información de la cobranza seleccionada
    var id = "DatosCobranza";
    var Clientes = document.getElementById("Clientes").value;
    if (Clientes == 'nulo') {
        location.reload();
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Clientes: Clientes
        },
        success: function (data) {
            $('#tablaCobr').html(data);
        },
        error: function (data) { }
    });
}

function DatosAbono(info, adeudo) {
    var cobr = info.id;
    var vendedor = info.name;
    document.getElementById("vendedorF").value = vendedor;
    document.getElementById("Adeudo").value = adeudo;
    document.getElementById("CobranzaID").value = cobr;
}

function Abonar() { //Función Con diferentes opciones (Pagar, Abonar, o Alerta)
    if (document.getElementById("Abono").value == "") {
        alert("Debes indicar el monto del abono");
        return;
    } else {
        var Abono = document.getElementById("Abono").value;
    }
    var Adeudo = document.getElementById("Adeudo").value;
    var Vendedor = document.getElementById("vendedorF").value;
    var CobranzaFinal = document.getElementById("CobranzaID").value;
    Adeudo = parseFloat(Adeudo);
    Abono = parseFloat(Abono);
    if (Abono > Adeudo) { //Si el monto es mayor que el adeudo manda mensaje de error
        mostrar("No es posible cobrar mas que el adeudo");
        return;
    }
    if (Abono == Adeudo) { //Si el abono es igual al adeudo se pagara la cobranza completa
        var id = "PagoCobranza";
        $.ajax({
            url: src + 'valores.php',
            method: 'POST',
            data: {
                id: id,
                CobranzaFinal: CobranzaFinal,
                Vendedor: Vendedor
            },
            success: function (data) {
                alert("Pago Realizado");
                var url = src + "TicketCobranza.php";
                abrirEnPestana(url);
                location.reload();
            },
            error: function (data) { }
        });
    }
    if (Abono < Adeudo) { // Si el abono es mayor al adeudo se abonara el monto al adeudo
        var id = "AbonarCobranza";
        $.ajax({
            url: src + 'valores.php',
            method: 'POST',
            data: {
                id: id,
                Abono: Abono,
                CobranzaFinal: CobranzaFinal,
                Vendedor: Vendedor
            },
            success: function (data) {
                alert("Abono Realizado");
                var url = src + "TicketAbono.php";
                abrirEnPestana(url);
                location.reload();
            },
            error: function (data) { }
        });
    }
}




function abrirEnPestana(url) { // Función que abre una página en una nueva pestaña
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

function mostrar(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaAbono').html(data);
    $('#alertaAbono').show('fade');
    setTimeout(function () {
        $('#alertaAbono').hide('fade');
    }, 1000);
}

function mostrarDel(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaDel').html(data);
    $('#alertaDel').show('fade');
    setTimeout(function () {
        $('#alertaDel').hide('fade');
        location.reload();
    }, 1000);
}

function getDetalle(element) {
    var cobranza = element.id;
    var vendedor = element.name;
    var id = "getDetalle";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            cobranza: cobranza,
            vendedor: vendedor
        },
        success: function (data) {
            $('#contenido').html(data);
        },
        error: function (data) { }
    });
}

function datosDel(info, adeudo) {
    var cobranza = info.id;
    var vendedor = info.name;
    var AdeudoF = adeudo;
    document.getElementById("CobranzaIDDel").value = cobranza;
    document.getElementById("vendedorFDel").value = vendedor;
    document.getElementById("AdeudoDel").value = AdeudoF;
}

function EliminarCobranza() {
    var id = "EliminarCobranza";
    var cobranza = document.getElementById("CobranzaIDDel").value;
    var vendedor = document.getElementById("vendedorFDel").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            cobranza: cobranza,
            vendedor: vendedor
        },
        success: function (data) {
            alert(data);
            mostrarDel("Cobranza Eliminada");
        },
        error: function (data) { }
    });
}