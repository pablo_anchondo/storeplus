document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";

var opcion = false;
var checked = false;

window.onload = (function() {

    $('#alerta').hide();

    $('#caja_busqueda').on('keyup', function() { //Detecta cuando hay un cambio en el buscador
        var valor = $(this).val();
        if (valor != "") {
            buscarDatos(valor);
        } else {
            buscarDatos("");
        }
    }).keyup();

    $('#caja_busquedaUnidad').on('keyup', function() { //Detecta cuando hay un cambio en el buscador
        var valor = $(this).val();
        if (valor != "") {
            buscarDatosUnidad(valor);
        } else {
            buscarDatosUnidad("");
        }
    }).keyup();

});



function buscarDatos(consult) { // Función que realiza consulta a la base de datos y llena el modal de productos
    var id = "prodsSAT";
    var consulta = consult;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta,
            opcion: opcion
        },
        success: function(data) {
            $('#datos').html(data);
        },
        error: function(data) {}
    });
}


function buscarDatosUnidad(consult) { // Función que realiza consulta a la base de datos y llena el modal de productos
    var id = "UnidadSAT";
    var consulta = consult;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta,
            opcion: opcion
        },
        success: function(data) {
            $('#datos').html(data);
        },
        error: function(data) {}
    });
}




function seleccion(){
    var id = "claveProdServ";
    var clave = document.getElementById("claveProd").value;

    $('.micheckbox:checked').each(
        function() {
            var prod =  $(this).attr("id");
            var desc =  $(this).attr("name");
            $.ajax({
                url: src + 'valores.php',
                method: 'POST',
                data: {
                    id: id,
                    prod: prod,
                    desc: desc,
                    clave: clave
                },
                success: function(data) {
                    
                   
                },
                error: function(data) {}
            });
        }
    );
   mostrar("Articulos Mapeados");
}


function seleccionUnidad(){
    var id = "claveUnit";
    var clave = document.getElementById("claveProd").value;

    $('.micheckbox:checked').each(
        function() {
            var unidad =  $(this).attr("id");
            $.ajax({
                url: src + 'valores.php',
                method: 'POST',
                data: {
                    id: id,
                    unidad: unidad,
                    clave: clave
                },
                success: function(data) {
                    
                },
                error: function(data) {}
            });
        }
    );
   mostrar("Unidades Mapeados");
}


function check(){
    if (checked == false) {
        $(".micheckbox").prop('checked', true);
        checked = true;
    }else{
        $(".micheckbox").prop('checked', false);
        checked = false;
    }
    
}


function mostrar(mensaje) {
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alerta').html(data);
    $('#alerta').show('fade');
    setTimeout(function() {
        $('#alerta').hide('fade');
        location.reload();
    }, 1000);
}


function noMapeados(){
    opcion = false;
    buscarDatos("");
}

function siMapeados(){
    opcion = true;
    buscarDatos("");
}
