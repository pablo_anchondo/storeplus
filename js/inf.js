document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
window.onload = (function() {
    $('#alertaUsu').hide();
    $('#alertaPass').hide();
});

function mostrarP(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaPass').html(data);
    $('#alertaPass').show('fade');
    setTimeout(function() {
        $('#alertaPass').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarErrorP(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaPass').html(data);
    $('#alertaPass').show('fade');
    setTimeout(function() {
        $('#alertaPass').hide('fade');
    }, 1000);
}

function mostrar(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaUsu').html(data);
    $('#alertaUsu').show('fade');
    setTimeout(function() {
        $('#alertaUsu').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarError(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaUsu').html(data);
    $('#alertaUsu').show('fade');
    setTimeout(function() {
        $('#alertaUsu').hide('fade');
    }, 1000);
}

function cambioPass() { // Cambio de contraseña del usuario
    var id = "cambioPass";
    if (document.getElementById("pass").value == "") {
        mostrarErrorP("Debes indicar la contraseña actual");
        return;
    } else {
        var pass = document.getElementById('pass').value;
    }
    if (document.getElementById("newpass").value == "") {
        mostrarErrorP("Debes indicar la nueva contraseña");
        return;
    } else {
        var newpass = document.getElementById('newpass').value;
    }
    if (document.getElementById("confpass").value == "") {
        mostrarErrorP("Debes confirmar la nueva contraseña");
        return;
    } else {
        var confpass = document.getElementById('confpass').value;
    }
    if (newpass != confpass) {
        mostrarErrorP("Las contraseñas no son iguales");
        return;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            pass: pass,
            newpass: newpass
        },
        success: function(data) {
            if (data == "error") {
                mostrarErrorP("Contraseña Incorrecta");
            } else {
                mostrarP("Contraseña Actualizada");
            }
        },
        error: function(data) {}
    });
}

function cambioUsu() { // Cambiar nombre de usuario
    var id = "cambioUsu";
    if (document.getElementById("usu").value == "") {
        mostrarError("Debes indicar el usuario actual");
        return;
    } else {
        var usu = document.getElementById('usu').value;
    }
    if (document.getElementById("newusu").value == "") {
        mostrarError("Debes indicar el nuevo usuario");
        return;
    } else {
        var newusu = document.getElementById('newusu').value;
    }
    if (document.getElementById("confusu").value == "") {
        mostrarError("Debes confirmar el nuevo usuario");
        return;
    } else {
        var confusu = document.getElementById('confusu').value;
    }
    if (newusu != confusu) {
        mostrarError("Los nombres no son iguales");
        return;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            usu: usu,
            newusu: newusu
        },
        success: function(data) {
            if (data == "error") {
                mostrarError("Nombre de usuario Incorrecto");
            } else {
                mostrar("Usuario Actualizado");
            }
        },
        error: function(data) {}
    });
}

function buscarDatos() { // Consulta los datos del usuario y los muestra en inputs
    var id = "datosCuenta";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            document.getElementById('clave').value = json.Id_User;
            document.getElementById('Nombre').value = json.Nombre;
            document.getElementById('NombreF').value = json.NFiscal;
            document.getElementById('NombreC').value = json.NComercial;
            document.getElementById('pais').value = json.Pais;
            document.getElementById('cp').value = json.cp;
            document.getElementById('calle').value = json.Calle;
            document.getElementById('Numero').value = json.Numero;
            document.getElementById('Col').value = json.Colonia;
            document.getElementById('Estado').value = json.Estado;
            document.getElementById('City').value = json.Ciudad;
            document.getElementById('loc').value = json.Localidad;
            document.getElementById('tel').value = json.Telefono;
            document.getElementById('rfc').value = json.RFC;
            document.getElementById('mail').value = json.Mail;
        },
        error: function(data) {}
    });
}