document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
var rutaImg = "ProImg/sinImg.jpg";
window.onload = (function() {
    $('#alertaAdd').hide();
    $('#alertaMod').hide();
    $('#alertaDel').hide();
    $('#alertaUni').hide();
    $('#alertaProv').hide();
    $('#alertaImp').hide();
    $('#alertaMax').hide();
    $('#alertaDep').hide();
    $('#alertaErrorAdd').hide();
    $('#alertaErrorProv').hide();
    $('#file').on("change", function() { // Función subir la imagen, mandarla a PHP en insertarla en la DB
        var formData = new FormData($('#formulario')[0]);
        $.ajax({
            url: src + 'imagen.php',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data == "Imagen demasiado grande seleccione otra") {
                    alert(data);
                    rutaImg = rutaImg = "ProImg/sinImg.jpg";
                }
                rutaImg = data;
            },
            error: function(data) {}
        });
    });
    $('#file2').on("change", function() { // Función subir la imagen, mandarla a PHP en insertarla en la DB
        var formData = new FormData($('#formulario2')[0]);
        $.ajax({
            url: src + 'imagen.php',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data == "Imagen demasiado grande seleccione otra") {
                    alert(data);
                    rutaImg = rutaImg = "ProImg/sinImg.jpg";
                }
                rutaImg = data;
            },
            error: function(data) {}
        });
    });
    $('#caja_busqueda').on('keyup', function() { //Detecta cuando hay un cambio en el buscador
        var valor = $(this).val();
        if (valor != "") {
            buscarDatos(valor);
        } else {
            buscarDatos("");
        }
    }).keyup();
});

function AddDpto() { // Agrega un departamento a la DB
    var id = "Department";
    if (document.getElementById("depto").value == "") {
        mostrarDepm("Debes Indicar el nombre del departamento");
        return;
    } else {
        var Dpto = document.getElementById("depto").value;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Dpto: Dpto
        },
        success: function(data) {
            if (data == 'Existe') {
                mostrarDepm("El departamento ya existe");
            } else {
                mostrarDepm("Departamento Agregado");
                document.getElementById("depto").value = "";
            }
        },
        error: function(data) {}
    });
}

function mostrarErrAdd(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaErrorAdd').html(data);
    $('#alertaErrorAdd').show('fade');
    setTimeout(function() {
        $('#alertaErrorAdd').hide('fade');
    }, 1000);
}

function mostrarErrProv(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaErrorProv').html(data);
    $('#alertaErrorProv').show('fade');
    setTimeout(function() {
        $('#alertaErrorProv').hide('fade');
    }, 1000);
}

function mostrarDepm(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaDep').html(data);
    $('#alertaDep').show('fade');
    setTimeout(function() {
        $('#alertaDep').hide('fade');
    }, 1000);
}

function mostrarMax() { //Muestra en pantalla una alerta
    $('#alertaMax').show('fade');
    setTimeout(function() {
        $('#alertaMax').hide('fade');
    }, 1000);
}

function mostrarImp(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaImp').html(data);
    $('#alertaImp').show('fade');
    setTimeout(function() {
        $('#alertaImp').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarProv() { //Muestra en pantalla una alerta
    $('#alertaProv').show('fade');
    setTimeout(function() {
        $('#alertaProv').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarUni(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaUni').html(data);
    $('#alertaUni').show('fade');
    setTimeout(function() {
        $('#alertaUni').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarDel() { //Muestra en pantalla una alerta
    $('#alertaDel').show('fade');
    setTimeout(function() {
        $('#alertaDel').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarAdd() { //Muestra en pantalla una alerta
    $('#alertaAdd').show('fade');
    setTimeout(function() {
        $('#alertaAdd').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarAddE(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaAdd').html(data);
    $('#alertaAdd').show('fade');
    setTimeout(function() {
        $('#alertaAdd').hide('fade');
    }, 1000);
}

function mostrarMod() { //Muestra en pantalla una alerta
    $('#alertaMod').show('fade');
    setTimeout(function() {
        $('#alertaMod').hide('fade');
        location.reload();
    }, 1000);
}

function InsertProv() { // Agrega un proveedor a la DB
    if (document.getElementById("NombreProv").value == "") {
        mostrarErrProv("El campo Nombre es obligatorio");
        return;
    } else {
        var NombreProv = document.getElementById("NombreProv").value;
    }
    if (document.getElementById("CpProv").value == "") {
        mostrarErrProv("El campo Código Postal es obligatorio");
        return;
    } else {
        var CpProv = document.getElementById("CpProv").value;
    }
    if (document.getElementById("RfcProv").value == "") {
        mostrarErrProv("El campo R.F.C es obligatorio");
        return;
    } else {
        var RfcProv = document.getElementById("RfcProv").value;
    }
    if (document.getElementById("LadaP").value == "") {
        mostrarErrProv("El campo Lada es obligatorio");
        return;
    } else {
        var LadaP = document.getElementById("LadaP").value;
    }
    if (document.getElementById("TelProv").value == "") {
        mostrarErrProv("El campo Telefono es obligatorio");
        return;
    } else {
        var TelProv = document.getElementById("TelProv").value;
    }
    var PaisProv = document.getElementById("PaisProv").value;
    var ColProv = document.getElementById("ColProv").value;
    var PobProv = document.getElementById("PobProv").value;
    var CityProv = document.getElementById("CityProv").value;
    var StateProv = document.getElementById("StateProv").value;
    var MailProv = document.getElementById("MailProv").value;
    var WebProv = document.getElementById("WebProv").value;
    var observ = document.getElementById("observ").value;
    $.ajax({
        url: src + 'Proveedor.php',
        method: 'POST',
        data: {
            NombreProv: NombreProv,
            CpProv: CpProv,
            RfcProv: RfcProv,
            PaisProv: PaisProv,
            ColProv: ColProv,
            PobProv: PobProv,
            CityProv: CityProv,
            StateProv: StateProv,
            TelProv: TelProv,
            MailProv: MailProv,
            WebProv: WebProv,
            observ: observ,
            LadaP: LadaP
        },
        success: function(data) {
            mostrarProv();
        },
        error: function(data) {}
    });
}

function Catch(boton) { // Determina el producto a modificar
    var id = boton.id;
    if (id == "update") {
        Update(value);
        return;
    } else {
        if (id == "delete") {
            Delete(value);
            return;
        }
    }
}

function Delete(boton) { // Elimina un producto de la DB
    var valor = boton;
    var id = "Borrar";
    $.ajax({
        url: src + 'AddProds.php',
        method: 'POST',
        data: {
            id: id,
            valor: valor
        },
        success: function(data) {
            mostrarDel();
        },
        error: function(data) {}
    });
}

function Update(boton) { // Modifica la informacion de un producto
    if (document.getElementById("art2").value == "") {
        alert("El campo Clave es obligatorio");
        return;
    } else {
        var articulo = document.getElementById("art2").value;
    }
    if (document.getElementById("desc2").value == "") {
        alert("El campo Descripción es obligatorio");
        return;
    } else {
        var descripcion = document.getElementById("desc2").value;
    }
    if (document.getElementById("cost2").value == "") {
        alert("El campo Costo es obligatorio");
        return;
    } else {
        var costo = document.getElementById("cost2").value;
    }
    if (document.getElementById("Precio2").value == "") {
        alert("El campo Precio es obligatorio");
        return;
    } else {
        var Precio = document.getElementById("Precio2").value;
    }
    var id = "update";
    var valor = boton;
    var unidad = document.getElementById("unit2").value;
    var linea = document.getElementById("Linea2").value;
    var impuesto = document.getElementById("impuesto2").value;
    $.ajax({
        url: src + 'AddProds.php',
        method: 'POST',
        data: {
            id: id,
            valor: valor,
            articulo: articulo,
            descripcion: descripcion,
            unidad: unidad,
            costo: costo,
            linea: linea,
            impuesto: impuesto,
            Precio: Precio,
            rutaImg: rutaImg
        },
        success: function(data) {
            if (data == 'Existe') {
                alert("La clave ya existe");
            } else {
                mostrarMod();
            }
        },
        error: function(data) {}
    });
}

function Add(boton) { // Agrega un producto a la DB
    if (document.getElementById("art").value == "") {
        mostrarErrAdd("El campo Clave es obligatorio");
        return;
    } else {
        var articulo = document.getElementById("art").value;
    }
    if (document.getElementById("desc").value == "") {
        mostrarErrAdd("El campo Descripción es obligatorio");
        return;
    } else {
        var descripcion = document.getElementById("desc").value;
    }
    if (document.getElementById("cost").value == "") {
        mostrarErrAdd("El campo Costo es obligatorio");
        return;
    } else {
        var costo = document.getElementById("cost").value;
    }
    if (document.getElementById("Precio").value == "") {
        mostrarErrAdd("El campo Precio es obligatorio");
        return;
    } else {
        var Precio = document.getElementById("Precio").value;
    }
    var id = boton.id;
    var unidad = document.getElementById("unit").value;
    var linea = document.getElementById("Linea").value;
    var impuesto = document.getElementById("impuesto").value;
    $.ajax({
        url: src + 'AddProds.php',
        method: 'POST',
        data: {
            id: id,
            articulo: articulo,
            descripcion: descripcion,
            unidad: unidad,
            costo: costo,
            linea: linea,
            impuesto: impuesto,
            Precio: Precio,
            rutaImg: rutaImg
        },
        success: function(data) {
            if (data == 'Existe') {
                mostrarAddE("La clave ya existe");
            } else {
                mostrarAdd();
            }
        },
        error: function(data) {}
    });
}

function addUnit() { // Agrega una unidad a la DB
    var id = "unidades";
    if (document.getElementById("unidades").value == "") {
        alert("El campo Unidad es obligatorio");
        return;
    } else {
        var unidad = document.getElementById("unidades").value;
    }
    $.ajax({
        url: src + 'Unidades.php',
        method: 'POST',
        data: {
            id: id,
            unidad: unidad
        },
        success: function(data) {
            if (data == "Existe") {
                mostrarUni("La unidad ya existe");
            } else {
                mostrarUni("Unidad dada de alta");
            }
        },
        error: function(data) {}
    });
}

function addImp() { // Agrega un impuesto a la DB
    var id = "impuestos"
    if (document.getElementById("impu").value == "") {
        alert("El campo Impuesto es obligatorio");
        return;
    } else {
        var impu = document.getElementById("impu").value;
    }
    if (document.getElementById("Impdesc").value == "") {
        alert("El campo Descripción es obligatorio");
        return;
    } else {
        var Impdesc = document.getElementById("Impdesc").value;
    }
    if (document.getElementById("Porcentaje").value == "") {
        alert("El campo Porcentaje es obligatorio");
        return;
    } else {
        var Porcentaje = document.getElementById("Porcentaje").value;
    }
    $.ajax({
        url: src + 'Unidades.php',
        method: 'POST',
        data: {
            id: id,
            impu: impu,
            Impdesc: Impdesc,
            Porcentaje: Porcentaje
        },
        success: function(data) {
            if (data == 'Existe') {
                mostrarImp("El Impuesto ya existe");
            } else {
                mostrarImp("Impuesto Agregado");
            }
        },
        error: function(data) {}
    });
}
var value;
var cont = 0;

function test(boton) { // Obtiene la información del producto y llena el modal de update con ella
    value = boton.id;
    var articulo1 = value;
    $.ajax({
        url: src + 'consulta.php',
        method: 'POST',
        data: {
            articulo1: articulo1
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var art = json.Nombre;
            var desc = json.descripcion;
            var un = json.unidad;
            var cost = json.costo;
            var linea = json.Departamento;
            var precio = json.precio;
            var imp = json.impuesto;
            document.getElementById("art2").value = art;
            document.getElementById("desc2").value = desc;
            document.getElementById("cost2").value = cost;
            document.getElementById("Precio2").value = precio;
            document.getElementById("unit2").value = un;
            document.getElementById("Linea2").value = linea;
            document.getElementById("impuesto2").value = imp;
        },
        error: function(data) {}
    });
}

function ValueDelete(boton) { // Determina el ID del producto a eliminar
    value = boton.id;
}

function buscarDatos(consult) { // Función que realiza consulta a la base de datos y llena el modal de productos
    var id = "busqueda";
    var consulta = consult;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta
        },
        success: function(data) {
            $('#datos').html(data);
        },
        error: function(data) {}
    });
}

function valida(e) { // Función que solo acepta números como entrada
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validaEspacio(e) { // Valida que la entrada no contenga espacios
    key = e.keyCode || e.which;
    teclado = String.fromCharCode(key).toLowerCase();
    letras = "abcdefghijklmnopqrstuvwxyz1234567890/_-";
    especiales = "8-37-38-46-164-45-95";
    teclado_especial = false;
    for (var i in especiales) {
        if (key == especiales[i]) {
            teclado_especial = true;
            break;
        }
    }
    if (letras.indexOf(teclado) == -1 && !teclado_especial) {
        return false;
    }
}

function validarTel(caja, tamano, e) { // Función que valida si la entrada es numérica o no y además limita su tamaño
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        mostrarMax();
        return false;
    }
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validarTamano(caja, tamano) { // Función que valida el tamaño de una cadena y lo limita
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        mostrarMax();
        return false;
    }
}