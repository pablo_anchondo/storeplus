document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
var idCliente = "";
var value = "";
var f = new Date();
var Fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
var Hora = f.getHours() + ":" + (f.getMinutes()) + ":" + f.getSeconds();
var Corte = f.getFullYear() + "-" + (f.getMonth() + 2) + "-" + f.getDate();

window.onload = (function() {
    $('#alerta').hide();
    $('#alertaAdd').hide();
    $('#alertaDel').hide();
    $('#alertaMax13').hide();
    $('#alertaMax13U').hide();
    $('#caja_busqueda2').on('keyup', function() { //Detecta cuando hay un cambio en el buscador
        var valor = $(this).val();
        if (valor != "") {
            buscarDatos(valor);
        } else {
            buscarDatos("");
        }
    }).keyup();


    $('#filecer').on("change", function() { // Función subir la imagen, mandarla a PHP en insertarla en la DB
        var id = "valorUsuario";
        if (document.getElementById("ClienteEmisor").value == "empty") {
            alert("Debes Indicar el cliente");
            return;
        }else{
            var usuario = document.getElementById("ClienteEmisor").value;
        }

        var Almacen = document.getElementById("almacenSelect").value;

        $.ajax({
            url: src + 'valores.php',
            method: 'POST',
            data: {
                id: id,
                usuario: usuario,
                Almacen: Almacen
            },
            success: function(data) {

                var formData = new FormData($('#formulariocer')[0]);

                $.ajax({
                    url: src + 'upload.php',
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        alert(data);
                    },
                    error: function(data) {}
                });
                
            },
            error: function(data) {}
        });

    });






});

function mostrarUpdate() { //Muestra en pantalla una alerta
    $('#alerta').show('fade');
    setTimeout(function() {
        $('#alerta').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarAdd() { //Muestra en pantalla una alerta
    $('#alertaAdd').show('fade');
    setTimeout(function() {
        $('#alertaAdd').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarDel() { //Muestra en pantalla una alerta
    $('#alertaDel').show('fade');
    setTimeout(function() {
        $('#alertaDel').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarMax() { //Muestra en pantalla una alerta
    $('#alertaMax13').show('fade');
    setTimeout(function() {
        $('#alertaMax13').hide('fade');
    }, 1000);
    $('#alertaMax13U').show('fade');
    setTimeout(function() {
        $('#alertaMax13U').hide('fade');
    }, 1000);
}

function Activar(Usuario) { // Función para activar a los usuarios deshabilitados
    var id = "Activar";
    UsuarioA = Usuario.id;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            UsuarioA: UsuarioA
        },
        success: function(data) {
            location.reload();
        },
        error: function(data) {}
    });
}

function buscarDatos(consult) { // Función que realiza consulta a la base de datos y muestra los usuarios en pantalla
    var id = "busquedaUsuario";
    var consulta = consult;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta
        },
        success: function(data) {
            $('#datosUsuario').html(data);
        },
        error: function(data) {}
    });
}

function validarTamano(caja, tamano) { // Función que valida el tamaño de una cadena y lo limita
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        mostrarMax();
        return false;
    }
}

function validarTel(caja, tamano, e) { // Función que valida si la entrada es numérica o no y además limita su tamaño
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        mostrarMax();
        return false;
    }
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validaR(e) { // Función que valida si la entrada es numérica o no y además limita su tamaño
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function AddClient() { // Agrega un usuario a la DB
    var id = "Usuario";
    if (document.getElementById("userU").value == "") {
        alert("El campo Usuario  es obligatorio");
        return;
    } else {
        var Usuario = document.getElementById("userU").value;
    }
    if (document.getElementById("PassW").value == "") {
        alert("El campo Contraseña  es obligatorio");
        return;
    } else {
        var PassW = document.getElementById("PassW").value;
    }
    if (document.getElementById("NombreC").value == "") {
        alert("El campo Nombre  es obligatorio");
        return;
    } else {
        var NombreC = document.getElementById("NombreC").value;
    }
    if (document.getElementById("TelC").value == "") {
        alert("El campo Telefono  es obligatorio");
        return;
    } else {
        var TelC = document.getElementById("TelC").value;
    }
    if (document.getElementById("Lada").value == "") {
        alert("El campo Lada  es obligatorio");
        return;
    } else {
        var Lada = document.getElementById("Lada").value;
    }
    if (document.getElementById("PaisC").value == "") {
        alert("El campo País  es obligatorio");
        return;
    } else {
        var PaisC = document.getElementById("PaisC").value;
    }
    if (document.getElementById("cpC").value == "") {
        alert("El campo C.P  es obligatorio");
        return;
    } else {
        var cpC = document.getElementById("cpC").value;
    }
    if (document.getElementById("CalleC").value == "") {
        alert("El campo Calle es obligatorio");
        return;
    } else {
        var CalleC = document.getElementById("CalleC").value;
    }
    if (document.getElementById("NumeroC").value == "") {
        alert("El campo Numero es obligatorio");
        return;
    } else {
        var NumeroC = document.getElementById("NumeroC").value;
    }
    if (document.getElementById("CityC").value == "") {
        alert("El campo Ciudad es obligatorio");
        return;
    } else {
        var CityC = document.getElementById("CityC").value;
    }
    if (document.getElementById("StateC").value == "") {
        alert("El campo Estado es obligatorio");
        return;
    } else {
        var StateC = document.getElementById("StateC").value;
    }
    if (document.getElementById("ColC").value == "") {
        alert("El campo Colonia es obligatorio");
        return;
    } else {
        var ColC = document.getElementById("ColC").value;
    }
    if (document.getElementById("LocC").value == "") {
        alert("El campo Localidad es obligatorio");
        return;
    } else {
        var LocC = document.getElementById("LocC").value;
    }
    if (document.getElementById("RFCC").value == "") {
        alert("El campo RFC es obligatorio");
        return;
    } else {
        var RFCC = document.getElementById("RFCC").value;
    }
    if (document.getElementById("NombreComercial").value == "") {
        alert("El campo Nombre Comercial es obligatorio");
        return;
    } else {
        var NombreComercial = document.getElementById("NombreComercial").value;
    }
    if (document.getElementById("NombreFiscal").value == "") {
        alert("El campo Nombre Fiscal es obligatorio");
        return;
    } else {
        var NombreFiscal = document.getElementById("NombreFiscal").value;
    }
    if (document.getElementById("EntreCalles").value == "") {
        alert("El campo Entre Calles es obligatorio");
        return;
    } else {
        var EntreCalles = document.getElementById("EntreCalles").value;
    }
    var MailC = document.getElementById("MailC").value;
    var Observ = document.getElementById("observ").value;
    $.ajax({
        url: src + 'insert.php',
        method: 'POST',
        data: {
            id: id,
            Usuario: Usuario,
            PassW: PassW,
            NombreC: NombreC,
            TelC: TelC,
            PaisC: PaisC,
            cpC: cpC,
            CalleC: CalleC,
            NumeroC: NumeroC,
            CityC: CityC,
            StateC: StateC,
            ColC: ColC,
            LocC: LocC,
            RFCC: RFCC,
            MailC: MailC,
            Observ: Observ,
            Fecha: Fecha,
            Corte: Corte,
            NombreComercial: NombreComercial,
            NombreFiscal: NombreFiscal,
            EntreCalles: EntreCalles,
            Lada: Lada
        },
        success: function(data) {
            var id = "Trigger";
            $.ajax({
                url: src + 'valores.php',
                method: 'POST',
                data: {
                    id: id
                },
                success: function(data) {
                    $('#Agregar').modal('toggle');
                    mostrarAdd();
                },
                error: function(data) {}
            });
        },
        error: function(data) {}
    });
}

function deleteClient() { // Elimina un usuario de la DB
    var id = 'deleteUser';
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            value: value
        },
        success: function(data) {
            $('#Eliminar').modal('toggle');
            mostrarDel();
        },
        error: function(data) {}
    });
}

function ValueDelete(boton) { // Determina el valor del usuario a eliminar
    value = boton.id;
}

function DatosCliente(cliente) { // Obtiene los datos del usuario seleccionado y los muestra en el modal de update
    var id = "DatosUsuario";
    var client = cliente.id;
    idCliente = client;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            client: client
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var us = json.Usuario;
            var pas = json.Password;
            var nom = json.Nombre;
            var tel = json.Telefono;
            var pai = json.Pais;
            var cp = json.cp;
            var call = json.Calle;
            var num = json.Numero;
            var cit = json.Ciudad;
            var stat = json.Estado;
            var col = json.Colonia;
            var loc = json.Localidad;
            var rfc = json.RFC;
            var mail = json.Mail;
            var obs = json.Observ;
            var Corte = json.Corte;
            var NFiscal = json.NFiscal;
            var NComercial = json.NComercial;
            var EntreCalles = json.EntreCalles;
            var Lada = json.Lada;
            document.getElementById("usu").value = us;
            document.getElementById("pass").value = pas;
            document.getElementById("NombreCM").value = nom;
            document.getElementById("TelCM").value = tel;
            document.getElementById("PaisCM").value = pai;
            document.getElementById("cpCM").value = cp;
            document.getElementById("CalleCM").value = call;
            document.getElementById("NumeroCM").value = num;
            document.getElementById("CityCM").value = cit;
            document.getElementById("StateCM").value = stat;
            document.getElementById("ColCM").value = col;
            document.getElementById("LocCM").value = loc;
            document.getElementById("RFCCM").value = rfc;
            document.getElementById("MailCM").value = mail;
            document.getElementById("observM").value = obs;
            document.getElementById("fechaCort").value = Corte;
            document.getElementById("NombreFUdt").value = NFiscal;
            document.getElementById("NombreCUdt").value = NComercial;
            document.getElementById("EntreCudt").value = EntreCalles;
            document.getElementById("LadaM").value = Lada;
        },
        error: function(data) {}
    });
}

function UpdateCliente() { // Modifica los datos del usuario
    var id = "UpdateUser";
    var usu = document.getElementById("usu").value;
    var pass = document.getElementById("pass").value;
    var nom = document.getElementById("NombreCM").value;
    var tel = document.getElementById("TelCM").value;
    var pai = document.getElementById("PaisCM").value;
    var cp = document.getElementById("cpCM").value;
    var call = document.getElementById("CalleCM").value;
    var num = document.getElementById("NumeroCM").value;
    var cit = document.getElementById("CityCM").value;
    var stat = document.getElementById("StateCM").value;
    var col = document.getElementById("ColCM").value;
    var loc = document.getElementById("LocCM").value;
    var rfc = document.getElementById("RFCCM").value;
    var mail = document.getElementById("MailCM").value;
    var obs = document.getElementById("observM").value;
    var Corte = document.getElementById("fechaCort").value;
    var NFiscU = document.getElementById("NombreFUdt").value
    var NComU = document.getElementById("NombreCUdt").value;
    var EntreCU = document.getElementById("EntreCudt").value;
    var Lada = document.getElementById("LadaM").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            usu: usu,
            pass: pass,
            idCliente: idCliente,
            nom: nom,
            tel: tel,
            pai: pai,
            cp: cp,
            call: call,
            num: num,
            cit: cit,
            stat: stat,
            col: col,
            loc: loc,
            rfc: rfc,
            mail: mail,
            obs: obs,
            Corte: Corte,
            NFiscU: NFiscU,
            NComU: NComU,
            EntreCU: EntreCU,
            Lada: Lada
        },
        success: function(data) {
            $('#Modificar').modal('toggle');
            mostrarUpdate()
        },
        error: function(data) {}
    });
}

function liberar() { // Elimina las imágenes que no estén relacionadas a algún producto o empresa
    var id = "liberar";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            alert(data);
        },
        error: function(data) {}
    });
}



function almacenes(user){
    var usuario = user.value;
    if (usuario == "empty") {
        return;
    }
    var id = "activaAlmacen";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            usuario: usuario
        },
        success: function(data) {
            $('#almacenEmisor').html(data);
        },
        error: function(data) {}
    });
}


function AltaDatos(){
    var id = "AltaDatos";
    if (document.getElementById("Emisor").value == "") {
        alert("Todos los campos son obligatorios");
        return;
    } else {
        var Emisor = document.getElementById("Emisor").value;
    }

    if (document.getElementById("rfcEmisor").value == "") {
        alert("Todos los campos son obligatorios");
        return;
    } else {
        var rfcEmisor = document.getElementById("rfcEmisor").value;
    }

    if (document.getElementById("NoCert").value == "") {
        alert("Todos los campos son obligatorios");
        return;
    } else {
        var NoCert = document.getElementById("NoCert").value;
    }

    if (document.getElementById("ClienteEmisor").value == "empty") {
        alert("Todos los campos son obligatorios");
        return;
    }else{
        var usuario = document.getElementById("ClienteEmisor").value;
    }

    if (document.getElementById("passkey").value == "") {
        alert("Debes lenar todos los campos");
        return;
    } else {
        var passkey = document.getElementById("passkey").value;
    }

    if (document.getElementById("contrato").value == "") {
        alert("Debes lenar todos los campos");
        return;
    } else {
        var contrato = document.getElementById("contrato").value;
    }

    if (document.getElementById("userpade").value == "") {
        alert("Debes lenar todos los campos");
        return;
    } else {
        var userpade = document.getElementById("userpade").value;
    }

    if (document.getElementById("pasaspade").value == "") {
        alert("Debes lenar todos los campos");
        return;
    } else {
        var pasaspade = document.getElementById("pasaspade").value;
    }



    var almacen = document.getElementById("almacenSelect").value;

    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Emisor: Emisor,
            rfcEmisor: rfcEmisor,
            NoCert: NoCert,
            usuario: usuario,
            almacen: almacen,
            passkey: passkey,
            contrato: contrato,
            userpade: userpade,
            pasaspade: pasaspade
        },
        success: function(data) {
            alert("Datos Registrados");
        },
        error: function(data) {}
    });


}


function cambiar(texto){
    var num = texto.value.length;
    var textof =texto.value;
  
    for ( i = 0; i < num; i++) {
        if (textof.charAt(i) == "\\") {
            textof = textof.replace(textof.charAt(i), "/");
        }
    }

    document.getElementById(texto.id).value = textof+"/";
}


function getLlaves(){

    if (document.getElementById("passkey").value == "") {
        alert("Debes lenar todos los campos");
        return;
    } else {
        var passkey = document.getElementById("passkey").value;
    }
var id = "Step1";
    

    $.ajax({
        url: src + 'getLlaves.php',
        method: 'POST',
        data: {
            id: id,
            passkey: passkey
        },
        success: function(data) {
            alert(data);
        },
        error: function(data) {}
    });


}

function Calcular(){
    var id = "Step2";
    $.ajax({
        url: src + 'getLlaves.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            alert(data);
            var json = jQuery.parseJSON(data);
            var serialnumber = json.serialnumber;
            var rfc = json.rfc;
            var razon = json.razon;
            document.getElementById("Emisor").value = razon;
            document.getElementById("rfcEmisor").value = rfc;
            document.getElementById("NoCert").value = serialnumber;
        },
        error: function(data) {}
    });

}