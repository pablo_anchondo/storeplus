document.writeln('<script src="js/jQuery v3.3.1.js"></script>'); //Incluir JQUERY
var src = "http://localhost/Proyectos/SPLocal/";
window.onload = (function() {
    $('#alerta').hide();
});

function abrirEnPestana(url) { // Función que abre una página en una nueva pestaña
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

function mostrar(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alerta').html(data);
    $('#alerta').show('fade');
    setTimeout(function() {
        $('#alerta').hide('fade');
    }, 1000);
}

function LlenandoClientes() { // Llena el select de clientes en los reportes de crédito
    document.getElementById("CobranzaCliente").disabled = false;
    var Vendedor = document.getElementById("VendedorCobr").value;
    var id = "ValueSelectRA";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Vendedor: Vendedor
        },
        success: function(data) {
            $('#clientesS').html(data);
        },
        error: function(data) {}
    });
}

function UtilidadTiempo() { // Obtiene los input del boton Reporte de ventas
    var id = "Date";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function generarTiempo() { // genera el reporte de ventas
    var id = "SendDate";
    var vendedor = document.getElementById("vendedor").value;
    if (document.getElementById("fecha").value == "") {
        mostrar("Debes indicar Fecha de inicio");
        return;
    } else {
        var FI = document.getElementById("fecha").value;
    }
    if (document.getElementById("fecha2").value == "") {
        mostrar("Debes indicar Fecha Final");
        return;
    } else {
        var FF = document.getElementById("fecha2").value;
    }
    if (FI > FF) {
        mostrar("Intervalo de tiempo incorrecto");
        return;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            FI: FI,
            FF: FF,
            vendedor: vendedor
        },
        success: function(data) {
            var url = src + "UtileriasTiempo.php";
            abrirEnPestana(url);
        },
        error: function(data) {}
    });
}

function CompraSelec(CompraV) { // Inicia la variable de sesión compra
    var compra = CompraV.id;
    id = "ComrpaId";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            compra: compra
        },
        success: function(data) {},
        error: function(data) {}
    });
}

function TicketSelec(CompraV) { // Llena el select de los tickets en operaciones 
    var Venta = CompraV.id;
    var vendedor = document.getElementById("vendedores").value;
    id = "SessionTicket";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Venta: Venta,
            vendedor: vendedor
        },
        success: function(data) {},
        error: function(data) {}
    });
}

function buscarCompras() { // Trae los input del botón Compras en operaciones
    $('#Contenido').html("");
    var id = "Reportes";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function ReporteCompras() { // Trae los input del botón reporte de compras en operaciones en reportes
    var id = "ReportesCompras";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function ReporteClientes() { // Trae los input del botón reporte clientes en reportes
    var id = "ReportesClientes";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function generarRClient() { // Inicia la variable de sesión cliente
    var id = "RCliente";
    var cliente = document.getElementById("Clients").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            cliente: cliente
        },
        success: function(data) {
            var url = src + "VentasCliente.php";
            abrirEnPestana(url);
        },
        error: function(data) {}
    });
}

function generarRC() { // genera el reporte de cliente
    var id = "generarRC";
    var provedor = document.getElementById("Provedores").value;
    if (document.getElementById("fecha").value == "") {
        mostrar("Debes indicar Fecha de inicio");
        return;
    } else {
        var FI = document.getElementById("fecha").value;
    }
    if (document.getElementById("fecha2").value == "") {
        mostrar("Debes indicar Fecha Final");
        return;
    } else {
        var FF = document.getElementById("fecha2").value;
    }
    if (FI > FF) {
        mostrar("Intervalo de tiempo incorrecto");
        return;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            FI: FI,
            FF: FF,
            provedor: provedor
        },
        success: function(data) {
            var url = src + "ReporteVentas.php";
            abrirEnPestana(url);
        },
        error: function(data) {}
    });
}

function buscarTicket() { // Genera los inputs del botón tickets en operaciones
    var id = "Tickets";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function getTickets() { // Trae las ventas realizadas por el vendedor seleccionado
    var vendedor = "";
    var id = "ContenidoTickets";
    if (document.getElementById("vendedores").value == "Nulo") {
        location.reload();
    } else {
        vendedor = document.getElementById("vendedores").value;
    }

    if (document.getElementById("fechaTickI").value == "") {
        mostrar("Debes indicar Fecha de inicio");
        return;
    } else {
        var FI = document.getElementById("fechaTickI").value;
    }

    if (document.getElementById("fechaTickF").value == "") {
        mostrar("Debes indicar Fecha Final");
        return;
    } else {
        var FF = document.getElementById("fechaTickF").value;
    }


    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            vendedor: vendedor,
            FI: FI,
            FF: FF
        },
        success: function(data) {
            $('#Contenido').html(data);
        },
        error: function(data) {}
    });
}

function InventarioPV() { // Trae el contenido del botón inventario precio venta
    var id = "PV";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function InventarioCU() { // Trae el contenido del botón inventario costo ultimo
    var id = "CU";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function UtilidadProd() { // Trae el contenido del botón utilidades
    var id = "UtilidadProd";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function generar() { // Inicializa la variable de sesión Producto
    var id = "ReportePV";
    var product = document.getElementById("Prods").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            product: product
        },
        success: function(data) {},
        error: function(data) {}
    });
}

function generarCU() {  // Inicializa la variable de sesión Producto
    var id = "ReportePV";
    var product = document.getElementById("Prods").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            product: product
        },
        success: function(data) {},
        error: function(data) {}
    });
}

function generarUP() { // Inicializa la variable de sesión ProductoPV
    var id = "ReportePV";
    var product = document.getElementById("Prods").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            product: product
        },
        success: function(data) {},
        error: function(data) {}
    });
}

function UtilieriasVenta() { // Trae el contenido del botón utilidad ventas
    var id = "UtVentas";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function generarUV() { // Genera el reporte de utilidad por venta
    var id = "ReporteUV";
    var vendedor = document.getElementById("vendedor").value;
    if (document.getElementById("fecha").value == "") {
        mostrar("Debes indicar Fecha de inicio");
        return;
    } else {
        var FI = document.getElementById("fecha").value;
    }
    if (document.getElementById("fecha2").value == "") {
        mostrar("Debes indicar Fecha Final");
        return;
    } else {
        var FF = document.getElementById("fecha2").value;
    }
    if (FI > FF) {
        mostrar("Intervalo de tiempo incorrecto");
        return;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            FI: FI,
            FF: FF,
            vendedor: vendedor
        },
        success: function(data) {
            var url = src + "UtileriasVent.php";
            abrirEnPestana(url);
        },
        error: function(data) {}
    });
}

function Credito() { // Trae el contenido del botón reporte crédito
    var id = "ReporteCredito";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function generarRApartado() { // Genera el reporte de apartados
    var claveCliente = document.getElementById("CobranzaCliente").value;
    var Vendedor = document.getElementById("VendedorCobr").value;
    var id = "generarRApartado";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            claveCliente: claveCliente,
            Vendedor: Vendedor
        },
        success: function(data) {
            var url = src + "ReporteApartado.php";
            abrirEnPestana(url);
        },
        error: function(data) {}
    });
}

function buscarCotiza() { // Trae las cotizaciones realizadas por el usuario
    $('#Contenido').html("");
    var id = "BuscarCotiza";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function CotizaSelec(CompraV) { // Genera el reporte de cotización
    var Cotizacion = CompraV.id;
    id = "CotizaId";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Cotizacion: Cotizacion
        },
        success: function(data) {},
        error: function(data) {}
    });
}


function detalleCliente(){
    var id = "DetalleCliente";  
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#Reportes').html(data);
        },
        error: function(data) {}
    });
}

function generarDetalleCliente() { // Inicia la variable de sesión cliente
    var id = "DetCliente";  
    var cliente = document.getElementById("Clients").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            cliente: cliente
        },
        success: function(data) {
            var url = src + "DetalleCliente.php";
            abrirEnPestana(url);
        },
        error: function(data) {}
    });
}