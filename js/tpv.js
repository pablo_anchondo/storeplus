document.writeln('<script src="js/jQuery v3.3.1.js"></script>'); //Incluir JQUERY
var src = "http://localhost/Proyectos/SPLocal/";
var f = new Date();
var Fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
var SubTotal = 0;
var iva = 0;
var Total = 0;
var Partidas = new Array;
var contArray = 0;
var Totales = new Array;
var array = new Array;
var porcentaje = 0;
var Venta = 0;
var cobran = 0;
var habilitarPrecio = false;
window.onload = (function() {
    cargarVenta();
    cargarCobranza();
    document.onkeyup = mostrarInformacionTecla;
    document.getElementById("Sub").disabled = true;
    document.getElementById("iva").disabled = true;
    document.getElementById("tot").disabled = true;
    document.getElementById("cambio").disabled = true;
    document.getElementById("impF").disabled = true;
    document.getElementById("Empresa").disabled = true;
    document.getElementById("TotTick").disabled = true;
    document.getElementById("MPagoTck").disabled = true;
    document.getElementById("Pagotck").disabled = true;
    document.getElementById("CambioTck").disabled = true;
    document.getElementById("priceTMP").disabled = true;
    $('#alerta').hide();
    $('#alertaPagando').hide();
    $('#alertaApartado').hide();
    $('#imagenProd').attr('src', 'ProImg/sinImg.jpg');
    $('#caja_busqueda').on('keyup', function() {
        var valor = $(this).val();
        if (valor != "") {
            buscarDatos(valor);
        } else {
            buscarDatos("");
        }
    }).keyup();
    $('#priceTMP').on('keyup', function() {
        var valor = $(this).val();
        var decallowed = 2;
        if (valor.indexOf('.') == -1) {
            valor += ".";
        }
        dectext = valor.substring(valor.indexOf('.') + 1, valor.length);
        if (dectext.length > decallowed) {
            mostrar("Maximo 2 decimales");
            valor = valor.substr(0, valor.length - 1);
            document.getElementById("priceTMP").value = valor;
        }
    }).keyup();
    document.getElementById("Fecha").value = Fecha;
    document.getElementById("Fecha").disabled = true;
    $('#Llena').click(function() {
        Llena();
    });
    $('#TicketBtn').click(function() {
        var id = "SessionTicket";
        var vendedor = document.getElementById("Vendedo").value;
        $.ajax({
            url: src + 'valores.php',
            method: 'POST',
            data: {
                id: id,
                Venta: Venta,
                vendedor: vendedor
            },
            success: function(data) {
                var url = src + "TicketText.php";
                abrirEnPestana(url);
                location.reload();
            },
            error: function(data) {}
        });
    });
    $('#exit').click(function() {
        window.location = "menu.php";
    });
    $('#pagar').click(function() {
        var Efe = document.getElementById("Efe").value;
        var impF = document.getElementById("impF").value;
        if (parseFloat(Efe) < parseFloat(impF)) {
            mostrarPago("Pago Incompleto");
            return;
        }
        $('#Pagando').modal('toggle');
        var trs = $("#TableBody tr").length;
        if (trs == 1) {
            mostrarPago("Debes agregar una producto");
            return;
        }
        if (document.getElementById("Efe").value == "") {
            mostrarPago("Debes indicar el pago del cliente");
            return;
        } else {
            var Efe = document.getElementById("Efe").value;
        }
        if (document.getElementById("Efe").value == "") {
            mostrarPago("Debes indicar el pago del cliente");
            return;
        } else {
            var cambio = document.getElementById("cambio").value;
        }
        var id = "Venta";
        var cliente = document.getElementById("cliente").value;
        var Empresa = document.getElementById("Empresa").value;
        var Vendedor = document.getElementById("Vendedo").value;
        var Comentarios = document.getElementById("coment").value;
        var Importe = document.getElementById("Sub").value;
        var Impuesto = document.getElementById("iva").value;
        var Total = document.getElementById("tot").value;
        var fe = new Date();
        var FechaV = fe.getFullYear() + "-" + (fe.getMonth() + 1) + "-" + fe.getDate();
        var HoraV = fe.getHours() + ":" + (fe.getMinutes()) + ":" + fe.getSeconds();
        var mpago = document.getElementById("MPago").value;
        $.ajax({
            url: src + 'valores.php',
            method: 'POST',
            data: {
                id: id,
                cliente: cliente,
                Empresa: Empresa,
                Vendedor: Vendedor,
                Comentarios: Comentarios,
                Importe: Importe,
                Impuesto: Impuesto,
                Total: Total,
                FechaV: FechaV,
                HoraV: HoraV,
                Efe: Efe,
                cambio: cambio,
                mpago: mpago,
                Venta: Venta
            },
            success: function(data) {
                var json = jQuery.parseJSON(data);
                var vent = json.Venta;
                id = 'PartVta';
                var Art2 = "";
                var Cant2 = 0;
                var Price2 = 0;
                var porc2 = 0;
                var desc2 = 0;
                var import2 = 0;
                var identific = 0;
                for (var i = 0; i < Partidas.length; i++) {
                    desc2 = Partidas[i][0][0];
                    Art2 = Partidas[i][0][1];
                    Cant2 = Partidas[i][0][2];
                    Price2 = Partidas[i][0][3];
                    porc2 = Partidas[i][0][4];
                    import2 = Partidas[i][0][5];
                    identific = Partidas[i][0][6];
                    $.ajax({
                        url: src + 'valores.php',
                        method: 'POST',
                        data: {
                            id: id,
                            Venta: Venta,
                            Art2: Art2,
                            Cant2: Cant2,
                            Price2: Price2,
                            porc2: porc2,
                            desc2: desc2,
                            import2: import2,
                            identific: identific,
                            Vendedor: Vendedor
                        },
                        success: function(data) {},
                        error: function(data) {}
                    });
                }
                document.getElementById("TotTick").value = "$" + Total;
                document.getElementById("MPagoTck").value = mpago;
                document.getElementById("Pagotck").value = "$" + Efe;
                document.getElementById("CambioTck").value = "$" + cambio;
                $('#Imprimir').modal({
                    backdrop: 'static',
                    keyboard: false
                })
                document.getElementById("impF").value = "";
                document.getElementById("Efe").value = "";
                document.getElementById("cambio").value = "";
            },
            error: function(data) {}
        });
    });
});

function cargarVenta() {
    var id = "cargarVenta";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var vent = json.Venta;
            if (vent == null) {
                Venta = 1;
            } else {
                Venta = vent;
                Venta = parseInt(Venta) + 1;
            }
        },
        error: function(data) {}
    });
}

function mostrarInformacionTecla(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 113) {
        Llena();
    }
}

function Llena() {
    var trs = $("#TableBody tr").length;
    if (trs == 1) {
        mostrar("Debes Seleccionar un producto");
        return;
    }
    document.getElementById("impF").value = document.getElementById("tot").value;
    $('#Pagando').modal('toggle');
}

function Recargar() {
    location.reload();
}

function getPorcentajeAddSelectImg(valor) {
    var id = "porcentajeVenta";
    var val = valor.id;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            val: val
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var por = json.Porcentaje;
            porcentaje = por;
            AddSelect(valor);
        },
        error: function(data) {}
    });
}

function getPorcentajeAddSelect(valor) {
    var id = "porcentajeVenta";
    var val = valor.id;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            val: val
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var por = json.Porcentaje;
            porcentaje = por;
            AddSelectImg(valor);
        },
        error: function(data) {}
    });
}

function getPorcentajeAdd() {
    if (document.getElementById("prod").value == "") {
        mostrar("Debes Seleccionar un producto");
        return;
    } else {
        var val = document.getElementById("prod").value;
    }
    var id = "porcentajeVenta";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            val: val
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var por = json.Porcentaje;
            porcentaje = por;
            Add();
        },
        error: function(data) {}
    });
}

function validar(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 13) {
        getPorcentajeAdd();
    }
}

function mostrarAparta(mensaje) {
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaApartado').html(data);
    $('#alertaApartado').show('fade');
    setTimeout(function() {
        $('#alertaApartado').hide('fade');
    }, 1000);
}

function mostrar(mensaje) {
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alerta').html(data);
    $('#alerta').show('fade');
    setTimeout(function() {
        $('#alerta').hide('fade');
    }, 1000);
}

function mostrarPago(mensaje) {
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaPagando').html(data);
    $('#alertaPagando').show('fade');
    setTimeout(function() {
        $('#alertaPagando').hide('fade');
    }, 1000);
}

function borrar() {
    var trs = $("#TableBody tr").length;
    if (trs > 1) {
        if (trs == 2) {
            $("#TableBody tr:last").remove();
            location.reload();
            document.getElementById("Sub").value = "";
            document.getElementById("iva").value = "";
            document.getElementById("tot").value = "";
        } else {
            $("#TableBody tr:last").remove();
            var tmp = contArray - 2;
            SubTotal = Totales[tmp][0];;
            iva = Totales[tmp][1];
            Total = Totales[tmp][2];
            document.getElementById("Sub").value = Totales[tmp][0];
            document.getElementById("iva").value = Totales[tmp][1];
            document.getElementById("tot").value = Totales[tmp][2];
        }
    }
    Partidas.pop();
    contArray = contArray - 1;
}

function buscarDatos(consult) {
    var id = "busqueda2";
    var consulta = consult;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta
        },
        success: function(data) {
            $('#datos').html(data);
        },
        error: function(data) {}
    });
}

function buscarImg() {
    var id = "busquedaImg";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#datos').html(data);
        },
        error: function(data) {}
    });
}

function buscarImgChange() {
    var id = "busquedaImgChange";
    var depa = document.getElementById("Departamento").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            depa: depa
        },
        success: function(data) {
            $('#datos').html(data);
        },
        error: function(data) {}
    });
}

function Add() {
    if (document.getElementById("cant").value == "") {
        mostrar("Debes indicar una cantidad");
        return;
    } else if (document.getElementById("cant").value == 0) {
        mostrar("La cantidad debe ser mayor a 0");
    } else {
        var cant = document.getElementById("cant").value;
    }
    if (document.getElementById("prod").value == "") {
        mostrar("Debes Seleccionar un producto");
        return;
    } else {
        var producto = document.getElementById("prod").value;
    }
    var id = "LlenaTabla";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            producto: producto,
            cant: cant
        },
        success: function(data) {
            if (data == 0) {
                mostrar("Sin existencia");
                return;
            } else if (data == 1) {
                mostrar("Existencia Insuficiente");
                return;
            }
            var json = jQuery.parseJSON(data);
            var desc = json.descripcion;
            var nom = json.Nombre;
            var img = json.img;
            var existencia = json.existencia;
            var ident = json.articulo;
            if (habilitarPrecio == false) {
                var precio = json.precio;
            } else {
                var precio = document.getElementById("priceTMP").value;
                document.getElementById("priceTMP").value = 0.00;
            }
            var importe = (cant * precio).toFixed(2);
            var fila = "<tr><td>" + cant + "</td><td>" + nom + "</td><td>" + desc + "</td><td>" + precio + "</td><td>" + "$" + importe + "</td></tr>";
            var btn = document.createElement("TR");
            btn.innerHTML = fila;
            $('#TableBody').append(btn);
            $('#imagenProd').attr('src', img);
            $('#imagenProd').css({
                'width': '320px',
                'height': '180px'
            });
            SubTotal = parseFloat(parseFloat(SubTotal) + parseFloat(importe)).toFixed(2);
            document.getElementById("Sub").value = SubTotal;
            iva = parseFloat(parseFloat(iva) + parseFloat(parseFloat(importe) * (porcentaje / 100))).toFixed(2);
            document.getElementById("iva").value = iva;
            Total = parseFloat(parseFloat((parseFloat(SubTotal) + parseFloat(iva)))).toFixed(2);
            document.getElementById("tot").value = Total;
            array = [desc, nom, cant, precio, porcentaje, importe, ident];
            Partidas[contArray] = [array];
            Totales[contArray] = [SubTotal, iva, Total];
            contArray++;
            document.getElementById("prod").value = "";
        },
        error: function(data) {}
    });
}

function AddSelectImg(boton) {
    $('#Buscar').modal('toggle');
    if (document.getElementById("cant").value == "") {
        mostrar("Debes seleccionar una cantidad");
        return;
    } else if (document.getElementById("cant").value == 0) {
        mostrar("La cantidad debe ser mayor a 0");
    } else {
        var cant = document.getElementById("cant").value;
    }
    var producto = boton.id;
    var id = "LlenaTabla";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            producto: producto,
            cant: cant
        },
        success: function(data) {
            if (data == 0) {
                mostrar("Sin Existencia");
                return;
            } else if (data == 1) {
                mostrar("Existencia Insuficiente");
                return;
            }
            var json = jQuery.parseJSON(data);
            var desc = json.descripcion;
            var nom = json.Nombre;
            var img = json.img;
            var existencia = json.existencia;
            var ident = json.articulo;
            if (habilitarPrecio == false) {
                var precio = json.precio;
            } else {
                var precio = document.getElementById("priceTMP").value;
                document.getElementById("priceTMP").value = 0.00;
            }
            var importe = (cant * precio).toFixed(2);
            var fila = "<tr><td>" + cant + "</td><td>" + nom + "</td><td>" + desc + "</td><td>" + precio + "</td><td>" + "$" + importe + "</td></tr>";
            var btn = document.createElement("TR");
            btn.innerHTML = fila;
            $('#TableBody').append(btn);
            $('#imagenProd').attr('src', img);
            $('#imagenProd').css({
                'width': '320px',
                'height': '180px'
            });
            SubTotal = parseFloat(parseFloat(SubTotal) + parseFloat(importe)).toFixed(2);
            document.getElementById("Sub").value = SubTotal;
            iva = parseFloat(parseFloat(iva) + parseFloat(parseFloat(importe) * (porcentaje / 100))).toFixed(2);
            document.getElementById("iva").value = iva;
            Total = parseFloat(parseFloat((parseFloat(SubTotal) + parseFloat(iva)))).toFixed(2);
            document.getElementById("tot").value = Total;
            array = [desc, nom, cant, precio, porcentaje, importe, ident];
            Partidas[contArray] = [array];
            Totales[contArray] = [SubTotal, iva, Total];
            contArray++;
        },
        error: function(data) {}
    });
}

function AddSelect(boton) {
    $('#Buscar').modal('toggle');
    if (document.getElementById("cant").value == "") {
        mostrar("Debes seleccionar una cantidad");
        return;
    } else if (document.getElementById("cant").value == 0) {
        mostrar("La cantidad debe ser mayor a 0");
    } else {
        var cant = document.getElementById("cant").value;
    }
    var producto = boton.id;
    var id = "LlenaTabla";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            producto: producto,
            cant: cant
        },
        success: function(data) {
            if (data == 0) {
                mostrar("Sin Existencia");
                return;
            } else if (data == 1) {
                mostrar("Existencia Insuficiente");
                return;
            }
            var json = jQuery.parseJSON(data);
            var desc = json.descripcion;
            var nom = json.Nombre;
            var img = json.img;
            var existencia = json.existencia;
            var ident = json.articulo;
            if (habilitarPrecio == false) {
                var precio = json.precio;
            } else {
                var precio = document.getElementById("priceTMP").value;
                document.getElementById("priceTMP").value = 0.00;
            }
            var importe = (cant * precio).toFixed(2);
            var fila = "<tr><td>" + cant + "</td><td>" + nom + "</td><td>" + desc + "</td><td>" + precio + "</td><td>" + "$" + importe + "</td></tr>";
            var btn = document.createElement("TR");
            btn.innerHTML = fila;
            $('#TableBody').append(btn);
            $('#imagenProd').attr('src', img);
            $('#imagenProd').css({
                'width': '320px',
                'height': '180px'
            });
            SubTotal = parseFloat(parseFloat(SubTotal) + parseFloat(importe)).toFixed(2);
            document.getElementById("Sub").value = SubTotal;
            iva = parseFloat(parseFloat(iva) + parseFloat(parseFloat(importe) * (porcentaje / 100))).toFixed(2);
            document.getElementById("iva").value = iva;
            Total = parseFloat(parseFloat((parseFloat(SubTotal) + parseFloat(iva)))).toFixed(2);
            document.getElementById("tot").value = Total;
            array = [desc, nom, cant, precio, porcentaje, importe, ident];
            Partidas[contArray] = [array];
            Totales[contArray] = [SubTotal, iva, Total];
            contArray++;
        },
        error: function(data) {}
    });
}

function validarTamano(caja, tamano) {
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        return false;
    }
}

function validaR(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function cambio(valor) {
    var importe = document.getElementById("impF").value;
    var result = (parseFloat(valor.value) - parseFloat(importe)).toFixed(2);
    if (result == "NaN") {
        document.getElementById("cambio").value = "0.00";
        return;
    }
    document.getElementById("cambio").value = result;
}

function recargar() {
    location.reload();
}

function CorteX() {
    var vendedor = document.getElementById("Vendedo").value;
    var fe = new Date();
    var FechaV = fe.getFullYear() + "-" + (fe.getMonth() + 1) + "-" + fe.getDate();
    var HoraV = fe.getHours() + ":" + (fe.getMinutes()) + ":" + fe.getSeconds();
    var id = "CorteX";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            vendedor: vendedor,
            FechaV: FechaV,
            HoraV: HoraV
        },
        success: function(data) {
            var url = src + "CorteX.php";
            abrirEnPestana(url);
        },
        error: function(data) {}
    });
}

function CorteZ() {
    var vendedor = document.getElementById("Vendedo").value;
    var fe = new Date();
    var FechaV = fe.getFullYear() + "-" + (fe.getMonth() + 1) + "-" + fe.getDate();
    var HoraV = fe.getHours() + ":" + (fe.getMinutes()) + ":" + fe.getSeconds();
    var id = "CorteX";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            vendedor: vendedor,
            FechaV: FechaV,
            HoraV: HoraV
        },
        success: function(data) {
            var url = src + "CorteZ.php";
            abrirEnPestana(url);
        },
        error: function(data) {}
    });
}

function getImagen(boton) {
    alert(boton.id);
}

function abrirEnPestana(url) {
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

function cargarCobranza() {
    var id = "cargarCobranza";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var cobr = json.id;
            if (cobr == null) {
                cobran = parseInt(cobran) + 1;
            } else {
                cobran = cobr;
                cobran = parseInt(cobran) + 1;
            }
        },
        error: function(data) {}
    });
}

function cobranzaasdasd() {
    setTimeout(function() {
        alert(cobran);
    }, 1000);
}

function VentanaApartado() {
    var trs = $("#TableBody tr").length;
    if (trs == 1) {
        mostrar("Debes Seleccionar un producto");
        return;
    }
    if (document.getElementById("cliente").value == "PUBLICO EN GENERAL") {
        mostrar("Debes Seleccionar un cliente");
        return;
    }
    document.getElementById("impFA").value = document.getElementById("tot").value;
    $('#Apartar').modal('toggle');
}

function Adeudo(valor) {
    var importe = document.getElementById("impFA").value;
    var result = (parseFloat(importe) - parseFloat(valor.value)).toFixed(2);
    if (result == "NaN") {
        document.getElementById("Adeudo").value = "0.00";
        return;
    }
    var Adeud = parseFloat(document.getElementById("Adeudo").value);
    if (result <= 0) {
        mostrarAparta("Solo ventas a credito");
        document.getElementById("btnApartar").disabled = true;
    } else {
        document.getElementById("btnApartar").disabled = false;
    }
    document.getElementById("Adeudo").value = result;
}

function PagarApartado() {
    if (document.getElementById("fechaVensimiento").value == "") {
        mostrarAparta("Debes indicar la fecha de vencimiento");
        return;
    } else {
        var fechaVens = document.getElementById("fechaVensimiento").value;
    }
    if (document.getElementById("Abono").value == "") {
        mostrarAparta("Debes indicar el pago del cliente");
        return;
    }
    $('#Apartar').modal('toggle');
    var id = "Cobranza";
    var cliente = document.getElementById("cliente").value;
    var Empresa = document.getElementById("Empresa").value;
    var Vendedor = document.getElementById("Vendedo").value;
    var Comentarios = document.getElementById("coment").value;
    var Importe = document.getElementById("Sub").value;
    var Impuesto = document.getElementById("iva").value;
    var Total = document.getElementById("tot").value;
    var Abono = document.getElementById("Abono").value;
    var Adeudo = document.getElementById("Adeudo").value;
    var fe = new Date();
    var FechaV = fe.getFullYear() + "-" + (fe.getMonth() + 1) + "-" + fe.getDate();
    var HoraV = fe.getHours() + ":" + (fe.getMinutes()) + ":" + fe.getSeconds();
    var mpago = document.getElementById("MPagoA").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            cliente: cliente,
            Empresa: Empresa,
            Vendedor: Vendedor,
            Comentarios: Comentarios,
            Importe: Importe,
            Impuesto: Impuesto,
            Total: Total,
            FechaV: FechaV,
            HoraV: HoraV,
            mpago: mpago,
            cobran: cobran,
            Abono: Abono,
            Adeudo: Adeudo,
            fechaVens: fechaVens
        },
        success: function(data) {
            var id = 'PartCob';
            var Art2 = "";
            var Cant2 = 0;
            var Price2 = 0;
            var porc2 = 0;
            var desc2 = 0;
            var import2 = 0;
            var identific = 0;
            for (var i = 0; i < Partidas.length; i++) {
                desc2 = Partidas[i][0][0];
                Art2 = Partidas[i][0][1];
                Cant2 = Partidas[i][0][2];
                Price2 = Partidas[i][0][3];
                porc2 = Partidas[i][0][4];
                import2 = Partidas[i][0][5];
                identific = Partidas[i][0][6];
                $.ajax({
                    url: src + 'valores.php',
                    method: 'POST',
                    data: {
                        id: id,
                        cobran: cobran,
                        Art2: Art2,
                        Cant2: Cant2,
                        Price2: Price2,
                        porc2: porc2,
                        desc2: desc2,
                        import2: import2,
                        identific: identific,
                        Vendedor: Vendedor
                    },
                    success: function(data) {},
                    error: function(data) {}
                });
            }
        },
        error: function(data) {}
    });
    document.getElementById("TotaApa").value = "$" + Total;
    document.getElementById("MPagoAPA").value = mpago;
    document.getElementById("AbonoApa").value = "$" + Abono;
    document.getElementById("AdeudoApa").value = "$" + Adeudo;
    document.getElementById("VencimientoApa").value = fechaVens;
    $('#ImprimirApartado').modal({
        backdrop: 'static',
        keyboard: false
    })
}

function AbrirTktApar() {
    var id = "TicketApa";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            cobran: cobran
        },
        success: function(data) {
            var url = src + "TicketApartado.php";
            abrirEnPestana(url);
            location.reload();
        },
        error: function(data) {}
    });
}

function habilitar() {
    if (habilitarPrecio == false) {
        habilitarPrecio = true;
    } else {
        habilitarPrecio = false;
    }
    if (habilitarPrecio == false) {
        document.getElementById("priceTMP").disabled = true;
    } else {
        document.getElementById("priceTMP").disabled = false;
    }
}
