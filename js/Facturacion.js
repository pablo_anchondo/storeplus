document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
var checked = false;

window.onload = (function() {

    document.getElementById("rfcr").disabled = true;

});

function getTableFactura(prov){
    var proveedor = prov.value;
    if (proveedor == "empty") {
        return;
    }
    var id = "getTableFactura";

    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            proveedor: proveedor
        },
        success: function(data) {
            $('#TablaVentasFactura').html(data);
        },
        error: function(data) {}
    });

}


function facturaVenta(vendedor, venta){
    var id = "sessionTimbrarVenta";
    var uso = document.getElementById("usocfdi").value;
    var formaPago = document.getElementById("fp").value;
    var MetodoPago = document.getElementById("mp").value;
    var Serie = document.getElementById("Serie").value;
    
    if (document.getElementById("nr").value == 'nulo') {
        alert("Debes llenar todos los campos");
        return;
    }else{
        var Receptor = document.getElementById("nr");
        var Receptor = Receptor.options[Receptor.selectedIndex].text;
    }

    if (document.getElementById("rfcr").value == '') {
        alert("Debes llenar todos los campos");
        return;
    }else{
        var rfcr = document.getElementById("rfcr").value;
        
    }

    if (document.getElementById("ordencompra").value == "") {
        var ordencompra = "";
    }else{
        var ordencompra = document.getElementById("ordencompra").value;
    }

    if (document.getElementById("mail").value == "") {
        var mail = "";
    }else{
        var mail = document.getElementById("mail").value;
    }

   
    
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            vendedor: vendedor,
            venta: venta,
            uso: uso,
            formaPago: formaPago,
            MetodoPago: MetodoPago,
            Receptor: Receptor,
            rfcr: rfcr,
            Serie: Serie,
            ordencompra: ordencompra,
            mail: mail
        },
        success: function(data) {
            setTimeout(function() {
                var url = src + "generaFactura.php";
                abrirEnPestana(url);
                location.reload();
            }, 1000);
        },
        error: function(data) {

        }
    });
}

function validarTamano(caja, tamano) { //Función que valida el tamaño de una cadena y lo limita
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        alert("Maximo "+tamano+" caracteres");
        return false;
    }
}


function abrirEnPestana(url) { // Función que abre una página en una nueva pestaña
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}


function check(){

    if (checked == false) {
        $(".micheckbox").prop('checked', true);
        checked = true;
    }else{
        $(".micheckbox").prop('checked', false);
        checked = false;
    }
    
}


function Seleccion(){
    var id = "SeleccionVentas";
    var uso = document.getElementById("usocfdi").value;
    var formaPago = document.getElementById("fp").value;
    var MetodoPago = document.getElementById("mp").value;
    var Serie = document.getElementById("Serie").value;
    
    if (document.getElementById("nr").value == '') {
        alert("Debes llenar todos los campos");
        return;
    }else{
        var Receptor = document.getElementById("nr");
        var Receptor = Receptor.options[Receptor.selectedIndex].text;
    }


    if (document.getElementById("rfcr").value == '') {
        alert("Debes llenar todos los campos");
        return;
    }else{
        var rfcr = document.getElementById("rfcr").value;
        
    }

    if (document.getElementById("ordencompra").value == "") {
        var ordencompra = "";
    }else{
        var ordencompra = document.getElementById("ordencompra").value;
    }

    if (document.getElementById("mail").value == "") {
        var mail = "";
    }else{
        var mail = document.getElementById("mail").value;
    }

    var vendedor = document.getElementById("vendedor").value;
    var Ventas = new Array;
    var cont = 0;
    $('.micheckbox:checked').each(
        function() {
            var venta =  $(this).attr("id");
            Ventas[cont] = venta;
            cont = cont + 1;
        }
    );

    if (Ventas.length == 0) {
        alert("Debes seleccionar las ventas");
        return;
    }else{

        $.ajax({
            url: src + 'valores.php',
            method: 'POST',
            data: {
                id: id,
                vendedor: vendedor,
                Ventas: Ventas,
                uso: uso,
                formaPago: formaPago,
                MetodoPago: MetodoPago,
                Receptor: Receptor,
                rfcr: rfcr,
                Serie: Serie,
                ordencompra: ordencompra,
                mail: mail
            },
            success: function(data) {
                setTimeout(function() {
                    var url = src + "FacturaMasiva.php";
                    abrirEnPestana(url);
                    location.reload();
                }, 1000);
    
            },
            error: function(data) {
    
            }
        });


        
    }
  

    

}


function getRFC(client){
    if (client.value == 'nulo') {
        document.getElementById("rfcr").value = "";
    }else{
        document.getElementById("rfcr").value = client.value;
       
        var id = "getMail";
        var rfcmail = client.value;
        $.ajax({
            url: src + 'valores.php',
            method: 'POST',
            data: {
                id: id,
                rfcmail: rfcmail
            },
            success: function(data) {
                var json = jQuery.parseJSON(data);
                var Mail = json.Mail;
                document.getElementById("mail").value = Mail;
            },
            error: function(data) {}
        });
    }
}