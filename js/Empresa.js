document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
var rutaImg = "ProImg/sinImg.jpg";
window.onload = (function () {
    $('#excel').on("change", function () { // Función subir el Excel y mandarlo a PHP
        var formData = new FormData($('#formularioExcel')[0]);
        $.ajax({
            url: src + 'Excel.php',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                mostrar(data, "verde");
            },
            error: function (data) { }
        });
    });
    $('#file').on("change", function () { // Función subir la imagen, mandarla a PHP en insertarla en la DB
        var formData2 = new FormData($('#formulario')[0]);
        $.ajax({
            url: src + 'imagen.php',
            type: "POST",
            data: formData2,
            contentType: false,
            processData: false,
            success: function (data) {
                alert(data);
                if (data == "Imagen demasiado grande seleccione otra") {
                    alert("data");
                    rutaImg = rutaImg = "ProImg/sinImg.jpg";
                } else {
                    rutaImg = data;
                }
                var id = "ImgEmpresa";
                $.ajax({
                    url: src + 'valores.php',
                    method: 'POST',
                    data: {
                        id: id,
                        rutaImg: rutaImg
                    },
                    success: function (data) { },
                    error: function (data) { }
                });
            },
            error: function (data) { }
        });
    });
    $('#alerta').hide();
    $('#alertaVend').hide();
    $('#alertaAlm').hide();
    $('#alertaModAlm').hide();
    document.getElementById("AlmacenAct").disabled = true;
});
/**
  Muestra en pantalla una alerta 
**/ 
function mostrarModAlm(mensaje, color) {
    if (color == "rojo") {
        $("#alertaModAlm").removeClass("alert-success");
        $("#alertaModAlm").addClass("alert-danger");
    } else {
        $("#alertaModAlm").removeClass("alert-danger");
        $("#alertaModAlm").addClass("alert-success");
    }
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaModAlm').html(data);
    $('#alertaModAlm').show('fade');
    setTimeout(function () {
        $('#alertaModAlm').hide('fade');
        location.reload();
    }, 1000);
}

function saliendo() {
    alert("asasd");
}

function mostrarAlm(mensaje, color) { //Muestra en pantalla una alerta
    if (color == "rojo") {
        $("#alertaAlm").removeClass("alert-success");
        $("#alertaAlm").addClass("alert-danger");
    } else {
        $("#alertaAlm").removeClass("alert-danger");
        $("#alertaAlm").addClass("alert-success");
    }
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaAlm').html(data);
    $('#alertaAlm').show('fade');
    setTimeout(function () {
        $('#alertaAlm').hide('fade');
    }, 1000);
}

function mostrar(mensaje, color) { //Muestra en pantalla una alerta
    if (color == "rojo") {
        $("#alerta").removeClass("alert-success");
        $("#alerta").addClass("alert-danger");
    } else {
        $("#alerta").removeClass("alert-danger");
        $("#alerta").addClass("alert-success");
    }
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alerta').html(data);
    $('#alerta').show('fade');
    setTimeout(function () {
        $('#alerta').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarNoAct(mensaje, color) { //Muestra en pantalla una alerta
    if (color == "rojo") {
        $("#alerta").removeClass("alert-success");
        $("#alerta").addClass("alert-danger");
    } else {
        $("#alerta").removeClass("alert-danger");
        $("#alerta").addClass("alert-success");
    }
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alerta').html(data);
    $('#alerta').show('fade');
    setTimeout(function () {
        $('#alerta').hide('fade');
    }, 1000);
}

function mostrarVend(mensaje, color) { //Muestra en pantalla una alerta
    if (color == "rojo") {
        $("#alertaVend").removeClass("alert-success");
        $("#alertaVend").addClass("alert-danger");
    } else {
        $("#alertaVend").removeClass("alert-danger");
        $("#alertaVend").addClass("alert-success");
    }
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaVend').html(data);
    $('#alertaVend').show('fade');
    setTimeout(function () {
        $('#alertaVend').hide('fade');
    }, 1000);
}

function AddVend() { // Agrega un vendedor a la DB
    var id = "Vendedor";
    if (document.getElementById("NameProv").value == "") {
        mostrarVend("Nombre obligatorio", "rojo");
        return;
    } else {
        var NameProv = document.getElementById('NameProv').value;
    }
    if (document.getElementById("UsuVend").value == "") {
        mostrarVend("Usuario obligatorio", "rojo");
        return;
    } else {
        var UsuVend = document.getElementById('UsuVend').value;
    }
    if (document.getElementById("PassVend").value == "") {
        mostrarVend("Contraseña obligatoria", "rojo");
        return;
    } else {
        var PassVend = document.getElementById('PassVend').value;
    }

    if (document.getElementById("Inventario").checked == true) {
        var Inventario = "TRUE";
    } else {
        var Inventario = "FALSE";
    }

    if (document.getElementById("Compras").checked == true) {
        var Compras = "TRUE";
    } else {
        var Compras = "FALSE";
    }

    if (document.getElementById("Control").checked == true) {
        var Control = "TRUE";
    } else {
        var Control = "FALSE";
    }

    if (document.getElementById("Reportes").checked == true) {
        var Reportes = "TRUE";
    } else {
        var Reportes = "FALSE";
    }

    if (document.getElementById("Operaciones").checked == true) {
        var Operaciones = "TRUE";
    } else {
        var Operaciones = "FALSE";
    }


    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            NameProv: NameProv,
            UsuVend: UsuVend,
            PassVend: PassVend,
            Inventario: Inventario,
            Compras: Compras,
            Control: Control,
            Reportes: Reportes,
            Operaciones: Operaciones
        },
        success: function (data) {
            mostrarVend(data, "verde");
            document.getElementById('NameProv').value = "";
            document.getElementById('UsuVend').value = "";
            document.getElementById('PassVend').value = "";
            document.getElementById("Inventario").checked = false;
            document.getElementById("Compras").checked = false;
            document.getElementById("Control").checked = false;
            document.getElementById("Reportes").checked = false;
            document.getElementById("Operaciones").checked = false;
        },
        error: function (data) { }
    });
}

function Datos() { // Carga los datos de la empresa y los muestra en un input
    var id = "DatosEmpresa";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function (data) {
            var json = jQuery.parseJSON(data);
            var Nombre = json.Nombre;
            var Direccion = json.Direccion;
            var cp = json.cp;
            var RFC = json.RFC;
            var Telefono = json.Telefono;
            var Mail = json.Mail;
            var Web = json.Web;
            var Lada = json.Lada;
            var tick = json.TextoTicket;
            document.getElementById('Name').value = Nombre;
            document.getElementById('dir').value = Direccion;
            document.getElementById('cp').value = cp;
            document.getElementById('rfc').value = RFC;
            document.getElementById('tel').value = Telefono;
            document.getElementById('mail').value = Mail;
            document.getElementById('web').value = Web;
            document.getElementById('Lada').value = Lada;
            document.getElementById('Ticket').value = tick;
        },
        error: function (data) { }
    });
}

function modificar() { // Modifica los datos de la empresa
    var id = "Empresa";
    if (document.getElementById("Name").value == "") {
        alert("El Nombre es obligatorio");
        return;
    } else {
        var Name = document.getElementById('Name').value;
    }
    if (document.getElementById("dir").value == "") {
        alert("La direccion es obligatoria");
        return;
    } else {
        var dire = document.getElementById('dir').value;
    }
    if (document.getElementById("cp").value == "") {
        alert("C.P Obligatorio");
        return;
    } else {
        var cp = document.getElementById('cp').value;
    }
    if (document.getElementById("rfc").value == "") {
        alert("R.F.C Obligatorio");
        return;
    } else {
        var rfc = document.getElementById('rfc').value;
    }
    var tel = document.getElementById('tel').value;
    var Lada = document.getElementById('Lada').value;
    var mail = document.getElementById('mail').value;
    var web = document.getElementById('web').value;
    var ticket = document.getElementById('Ticket').value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Name: Name,
            dire: dire,
            cp: cp,
            rfc: rfc,
            tel: tel,
            mail: mail,
            web: web,
            Lada: Lada,
            ticket: ticket
        },
        success: function (data) {
            mostrar(data, "verde");
        },
        error: function (data) { }
    });
}

function valida(e) { // Función que solo acepta números como entrada
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validaEspacio(e) { // Valida que la entrada no contenga espacios
    key = e.keyCode || e.which;
    teclado = String.fromCharCode(key).toLowerCase();
    letras = "abcdefghijklmnopqrstuvwxyz1234567890/_-";
    especiales = "8-37-38-46-164-45-95";
    teclado_especial = false;
    for (var i in especiales) {
        if (key == especiales[i]) {
            teclado_especial = true;
            break;
        }
    }
    if (letras.indexOf(teclado) == -1 && !teclado_especial) {
        return false;
    }
}

function validarTel(caja, tamano, e) { // Función que valida si la entrada es numérica o no y además limita su tamaño
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        mostrarNoAct("Numero de caracteres invalido", "rojo");
        return false;
    }
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validarTamano(caja, tamano) { // Función que valida el tamaño de una cadena y lo limita
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        mostrarNoAct("Numero de caracteres invalido", "rojo");
        return false;
    }
}

function Almacen() { // Agrega almacenes a la DB
    var id = "almacen";
    var Nombre = document.getElementById("NameAlm").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Nombre: Nombre
        },
        success: function (data) {
            if (data == 'Existe') {
                mostrarAlm("El Almacen ya existe");
            } else if (data == 'Maximo') {
                mostrarAlm("Ya tienes 2 almacenes", "verde");
            } else {
                mostrarAlm("Almacen Agregado", "verde");
            }
        },
        error: function (data) { }
    });
}

function AlmacenPred() { // Cambia la variable de session de almacén
    var alma = document.getElementById("almacen").value;
    var id = "CambioAlmacen";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            alma: alma
        },
        success: function (data) {
            mostrar("Almacen Seleccionado");
        },
        error: function (data) { }
    });
}

function ModAlmacen() { // Modifica el nombre del almacén
    var NuevoAlm = document.getElementById("NameModAlm").value;
    var id = "ModAlm";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            NuevoAlm: NuevoAlm
        },
        success: function (data) {
            if (data == 'Existe') {
                mostrarModAlm("El Almacen ya existe");
            } else {
                mostrarModAlm("Almacen Modificado");
            }
        },
        error: function (data) { }
    });
}

