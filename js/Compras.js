document.writeln('<script src="js/jQuery v3.3.1.js"></script>'); //Incluir JQUERY
var src = "https://storeplus.com.mx/";
var cont = 0;
var contArray = 0;
var producto = "";
var porcentaje = 0;
var array = new Array;
var Partidas = new Array;
var Totales = new Array;
var f = new Date();
var Fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
var Hora = f.getHours() + ":" + (f.getMinutes()) + ":" + f.getSeconds();
var compra = 0;
var descFinal = "";
var CompraF = 0; // Id de la compra a cancelar
$(document).ready(function() {
    cargarCompra();
    $('#caja_busqueda').on('keyup', function() { //Detecta cuando hay un cambio en el buscador
        var valor = $(this).val();
        if (valor != "") {
            buscarDatos(valor);
        } else {
            buscarDatos("");
        }
    }).keyup();
    $('#alerta').hide();
    $('#alertaCompras').hide();
    $('#alertaDev').hide();
    document.getElementById("importe").disabled = true;
    document.getElementById("impuest").disabled = true;
    document.getElementById("tot").disabled = true;
    $('#llenaTabla').click(function() { //Listener de botón llenar tabla
        consultaDesc();
    });
    $('#Del').click(function() { // Elimina la ultima fila de la tabla y el ultimo valor del array
        var trs = $("#TableBody tr").length;
        if (trs > 1) {
            if (trs == 3) {
                $("#TableBody tr:last").remove();
                location.reload();
                document.getElementById("importe").value = 0;
                document.getElementById("impuest").value = 0;
                document.getElementById("tot").value = 0;
            } else {
                $("#TableBody tr:last").remove();
                var tmp = contArray - 2;
                document.getElementById("importe").value = Totales[tmp][0];
                document.getElementById("impuest").value = Totales[tmp][1];
                document.getElementById("tot").value = Totales[tmp][2];
            }
        }
        Partidas.pop();
        contArray = contArray - 1;
    });
});

function mostrarCancel(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaDev').html(data);
    $('#alertaDev').show('fade');
    setTimeout(function() {
        $('#alertaDev').hide('fade');
        location.reload();
    }, 1000);
}

function cargarCompra() { // Carga la compra en la que se va a trabajar
    var id = "cargarCompra";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var comp = json.Compra;
            if (comp == null) {
                compra = 1;
            } else {
                compra = comp;
                compra = parseInt(compra) + 1;
            }
            
        },
        error: function(data) {}
    });

    
}

function mostrarComp(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaCompras').html(data);
    $('#alertaCompras').show('fade');
    setTimeout(function() {
        $('#alertaCompras').hide('fade');
    }, 1000);
}

function mostrar() { //Muestra en pantalla una alerta
    $('#alerta').show('fade');
    setTimeout(function() {
        $('#alerta').hide('fade');
        location.reload();
    }, 1000);
}

function agregar() { // Agrega una fila a la tabla y a los array
    if (document.getElementById("prod").value == "Producto") {
        mostrarComp("Debes seleccionar un producto");
        return;
    } else {
        var desc = document.getElementById("prod").value;
    }
    if (document.getElementById("prov").value == "Proveedor") {
        mostrarComp("Debes seleccionar un Proveedor");
        return;
    } else {
        var prov = document.getElementById("prov").value;
    }
    if (document.getElementById("cant").value == "") {
        mostrarComp("Debes indicar una cantidad");
        return;
    } else {
        var cant = document.getElementById("cant").value;
    }
    if (document.getElementById("price").value == "") {
        mostrarComp("Debes indicar un precio");
        return;
    } else {
        var price = document.getElementById("price").value;
    }
    var importe = (cant * price).toFixed(2);
    cont++;
    var fila = "<tr id = " + "id_tr" + "><td>" + cont + "</td><td>" + producto + "</td><td>" + cant + "</td><td>$" + price + "</td><td>" + porcentaje + "%" + "</td><td>" + descFinal + "</td><td>$" + importe + "</td></tr>";
    var btn = document.createElement("TR");
    btn.innerHTML = fila;
    $('#TableBody').append(btn);
    if (document.getElementById("importe").value == "") {
        SubTotal = 0;
    } else {
        SubTotal = document.getElementById("importe").value;
    }
    if (document.getElementById("impuest").value == "") {
        iva = 0;
    } else {
        iva = document.getElementById("impuest").value;
    }
    if (document.getElementById("tot").value == "") {
        Total = 0;
    } else {
        Total = document.getElementById("tot").value;
    }
    SubTotal = parseFloat(parseFloat(SubTotal) + parseFloat(importe)).toFixed(2);
    document.getElementById("importe").value = SubTotal;
    iva = parseFloat(parseFloat(iva) + parseFloat(parseFloat(importe) * (porcentaje / 100))).toFixed(2);
    document.getElementById("impuest").value = iva;
    Total = parseFloat(parseFloat((parseFloat(SubTotal) + parseFloat(iva)))).toFixed(2);
    document.getElementById("tot").value = Total;
    array = [producto, cant, price, porcentaje, descFinal, importe];
    Partidas[contArray] = [array];
    Totales[contArray] = [SubTotal, iva, Total];
    contArray++;
    document.getElementById("prod").value = "";
    document.getElementById("cant").value = 1;
    document.getElementById("price").value = "";
}

function consultaDesc() { //Obtiene el porcentaje de impuesto y la descripción del artículo seleccionado
    if (document.getElementById("prod").value == "") {
        mostrarComp("Debes seleccionar un producto");
        return;
    } else {
        var descrip = document.getElementById("prod").value;
    }
    if (document.getElementById("imp").value == "Impuesto") {
        mostrarComp("Debes seleccionar un Impuesto");
        return;
    } else {
        var impu = document.getElementById("imp").value;
    }
    var id = "artic";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            descrip: descrip
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var art = json.Nombre;
            descFinal = json.descripcion;
            producto = art;
            var id = "porcentaje";
            $.ajax({
                url: src + 'valores.php',
                method: 'POST',
                data: {
                    id: id,
                    impu: impu
                },
                success: function(data) {
                    var json = jQuery.parseJSON(data);
                    var por = json.Porcentaje;
                    porcentaje = por;
                    agregar();
                },
                error: function(data) {}
            });
        },
        error: function(data) {}
    });
}

function cerrarModal() {
    $('#Buscar').modal('hide');
}

function llenarCamposTabla(combo) { //Obtiene información del producto y la muestra en un input se llama con el botón seleccionar
    cerrarModal();
    var descrip = combo.id;
    document.getElementById("prod").value = descrip;
    var id = "artic";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            descrip: descrip
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var precio = json.costo;
            var imp = json.impuesto;
            document.getElementById("price").value = precio;
            document.getElementById("imp").value = imp;
        },
        error: function(data) {}
    });
}

function llenarCampos(combo) { //Obtiene información del producto y la muestra en un input se llama al presioanr enter
    var descrip = combo.value;
    var id = "artic";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            descrip: descrip
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var precio = json.costo;
            var imp = json.impuesto;
            document.getElementById("price").value = precio;
            document.getElementById("imp").value = imp;
        },
        error: function(data) {}
    });
}

function AddCompra() { // Agrega la compra a la base de datos
    var trs = $("#TableBody tr").length;
    if (trs == 2) {
        mostrarComp("Debes agregar una producto");
        return;
    }
    var id = "Compra";
    var Proveedor = document.getElementById("prov").value;
    var Importe = document.getElementById("importe").value;
    var Impuesto = document.getElementById("impuest").value;
    var Total = document.getElementById("tot").value;
    $.ajax({
        url: src + 'insert.php',
        method: 'POST',
        data: {
            id: id,
            Fecha: Fecha,
            Hora: Hora,
            Proveedor: Proveedor,
            Importe: Importe,
            Impuesto: Impuesto,
            Total: Total,
            compra: compra
        },
        success: function(data) {

            var json = jQuery.parseJSON(data);
            var comp = compra;
            id = 'PartCompra';
            var Art2 = "";
            var Cant2 = 0;
            var Price2 = 0;
            var porc2 = 0;
            var desc2 = 0;
            var import2 = 0;
            for (var i = 0; i < Partidas.length; i++) { //Agrega las partidas de la compra
                Art2 = Partidas[i][0][0];
                Cant2 = Partidas[i][0][1];
                Price2 = Partidas[i][0][2];
                porc2 = Partidas[i][0][3];
                desc2 = Partidas[i][0][4];
                import2 = Partidas[i][0][5];
                $.ajax({
                    url: src + 'insert.php',
                    method: 'POST',
                    data: {
                        id: id,
                        compra: compra,
                        Art2: Art2,
                        Cant2: Cant2,
                        Price2: Price2,
                        porc2: porc2,
                        desc2: desc2,
                        import2: import2
                    },
                    success: function(data) {},
                    error: function(data) {}
                });
            }
            id = "ComrpaId";
            $.ajax({
                url: src + 'valores.php',
                method: 'POST',
                data: {
                    id: id,
                    compra: compra
                },
                success: function(data) {
                    var url = src + "ordenC.php";
                    abrirEnPestana(url);
                    location.reload();
                },
                error: function(data) {}
            });
        },
        error: function(data) {}
    });
}

function abrirEnPestana(url) { // Función que abre una página en una nueva pestaña
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

function valida(e) { //Función que solo acepta números como entrada
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validar(e, prod) { //Función que detecta la tecla enter
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 13) {
        llenarCampos(prod);
    }
}

function buscarDatos(consult) { // Función que realiza consulta a la base de datos y llena el modal de productos
    var id = "busquedaCompras";
    var consulta = consult;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta
        },
        success: function(data) {
            $('#datos').html(data);
        },
        error: function(data) {}
    });
}

function DatosTabla() { // Llena la tabla con la compra a cancelar
    var id = "CancelCompra";
    if (document.getElementById("compra").value == "nulo") {
        location.reload();
    } else {
        var compra = document.getElementById("compra").value;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            compra: compra
        },
        success: function(data) {
            $('#TablaDevoluciones').html(data);
        },
        error: function(data) {}
    });
}

function DevTotal(btn) { // Cambia el id de la compra a cancelar
    CompraF = btn.id;
}

function DevolucionT() { // Cancelacion de la compra
    var id = "CancelTotal";
    var compra = CompraF;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            compra: compra
        },
        success: function(data) {
            mostrarCancel("Cancelación Realizada");
        },
        error: function(data) {}
    });
}