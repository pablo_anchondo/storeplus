document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";

window.onload = (function() {

    buscarDatos();

    $('#caja_busqueda').on('keyup', function() { //Detecta cuando hay un cambio en el buscador
        var valor = $(this).val();
        if (valor != "") {
            buscarDatos(valor);
        } else {
            buscarDatos("");
        }
    }).keyup();

});


function buscarDatos(consult) { // Función que realiza consulta a la base de datos y llena el modal de productos
    var id = "buscarCFDIS";
    var consulta = consult;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta
        },
        success: function(data) {
            $('#TablaCFDIS').html(data);
        },
        error: function(data) {}
    });
}


function cancelar(uuid){
    var id = "uuidCancelar";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            uuid: uuid
        },
        success: function(data) {
            setTimeout(function() {
                var url = src + "cancelarCFDI.php";
                abrirEnPestana(url);
                location.reload();
            }, 1000);
        },
        error: function(data) {}
    });

}



function abrirEnPestana(url) { // Función que abre una página en una nueva pestaña
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}


function cargarcfdi(getUUID){
    var id = "reutilizarCFDI";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            getUUID: getUUID
        },
        success: function(data) {
            window.location="ganeraCFDI.php";
        },
        error: function(data) {}
    });
}