document.writeln('<script src="js/jQuery v3.3.1.js"></script>'); //Incluir JQUERY
var src = "http://localhost/Proyectos/SPLocal/";
var ventaT = "";
var devP = "";
window.onload = (function() {
    $('#alertaDev').hide();
    $('#alertaDevP').hide();
    document.getElementById("venta").disabled = true;
});

function getVents() { // Obtiene las ventas del vendedor y almacén seleccionado y las muestra en un select
    var id = "getVents";
    var Vendedor = document.getElementById("Vendedors").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Vendedor: Vendedor
        },
        success: function(data) {
            $('#datosVenta').html(data);
        },
        error: function(data) {}
    });
}

function mostrarPart(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaDevP').html(data);
    $('#alertaDevP').show('fade');
    setTimeout(function() {
        $('#alertaDevP').hide('fade');
        location.reload();
    }, 1000);
}

function mostrar(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaDev').html(data);
    $('#alertaDev').show('fade');
    setTimeout(function() {
        $('#alertaDev').hide('fade');
        location.reload();
    }, 1000);
}

function DatosTabla() { // Llena la tabla con los datos de la venta seleccionada
    var id = "Devol";
    var Vendedor = document.getElementById("Vendedors").value;
    if (document.getElementById("venta").value == "nulo") {
        location.reload();
    } else {
        var venta = document.getElementById("venta").value;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            venta: venta,
            Vendedor: Vendedor
        },
        success: function(data) {
            $('#TablaDevoluciones').html(data);
        },
        error: function(data) {}
    });
}

function DevPartial(btn) { // Establece el id de la partida a devolver
    devP = btn.id;
}

function DevolucionPartial() { // Realiza la devolución de un articulo
    var id = "devPartial";
    var partida = devP;
    var venta = document.getElementById("venta").value;
    var Vendedor = document.getElementById("Vendedors").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            venta: venta,
            partida: partida,
            Vendedor: Vendedor
        },
        success: function(data) {
            mostrarPart("Producto devuelto con exito");
        },
        error: function(data) {}
    });
}

function DevolucionT() { // Realiza la devolución total de la venta
    var id = "devTotal";
    var venta = ventaT;
    var Vendedor = document.getElementById("Vendedors").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            venta: venta,
            Vendedor: Vendedor
        },
        success: function(data) {
            mostrar("Venta devuelta con exito");
        },
        error: function(data) {}
    });
}

function DevTotal(btn) { // Establece el id de la venta a devolver
    ventaT = btn.id;
}


