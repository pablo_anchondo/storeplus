document.writeln('<script src="js/jQuery v3.3.1.js"></script>'); //Incluir JQUERY
var src = "http://localhost/Proyectos/SPLocal/";
var cont = 0;
var contArray = 0;
var producto = "";
var porcentaje = 0;
var array = new Array;
var Partidas = new Array;
var Totales = new Array;
var f = new Date();
var Fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
var Hora = f.getHours() + ":" + (f.getMinutes()) + ":" + f.getSeconds();
var descFinal = "";
var usuario = 0;
var Cotizacion = 0; // id de la cotizacion
window.onload = (function() {
    $('#alerta').hide();
    buscarDatos();
    buscarUsuario();
    cargarCotiza();
    $('#llenaTabla').click(function() { //Listener de botón llenar tabla
        consultaDesc();
    });
    $('#caja_busqueda').on('keyup', function() { //Detecta cuando hay un cambio en el buscador
        var valor = $(this).val();
        if (valor != "") {
            buscarDatos(valor);
        } else {
            buscarDatos("");
        }
    }).keyup();
    $('#Del').click(function() { // Elimina la ultima fila de la tabla y el ultimo valor del array
        var trs = $("#TableBody tr").length;
        if (trs > 1) {
            if (trs == 3) {
                $("#TableBody tr:last").remove();
                location.reload();
                document.getElementById("importe").value = 0;
                document.getElementById("impuest").value = 0;
                document.getElementById("tot").value = 0;
            } else {
                $("#TableBody tr:last").remove();
                var tmp = contArray - 2;
                document.getElementById("importe").value = Totales[tmp][0];
                document.getElementById("impuest").value = Totales[tmp][1];
                document.getElementById("tot").value = Totales[tmp][2];
            }
        }
        Partidas.pop();
        contArray = contArray - 1;
    });
    document.getElementById("importe").disabled = true;
    document.getElementById("impuest").disabled = true;
    document.getElementById("tot").disabled = true;
});

function mostrar(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alerta').html(data);
    $('#alerta').show('fade');
    setTimeout(function() {
        $('#alerta').hide('fade');
    }, 1000);
}

function buscarDatos(consult) { // Función que realiza consulta a la base de datos y llena el modal de productos
    var id = "busquedaCotiza";
    var consulta = consult;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta
        },
        success: function(data) {
            $('#datos').html(data);
        },
        error: function(data) {}
    });
}

function valida(e) { //Función que solo acepta números como entrada
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validar(e, prod) { //Función que detecta la tecla enter
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 13) {
        llenarCampos(prod);
    }
}

function llenarCampos(combo) { //Obtiene información del producto y la muestra en un input se llama al presioanr enter
    var descrip = combo.value;
    var id = "artic";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            descrip: descrip
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var precio = json.precio;
            var imp = json.impuesto;
            document.getElementById("price").value = precio;
            document.getElementById("imp").value = imp;
        },
        error: function(data) {}
    });
}

function cerrarModal() { //Cierra el modal de productos
    $('#Buscar').modal('hide');
}

function llenarCamposTabla(combo) { //Obtiene información del producto y la muestra en un input se llama con el botón seleccionar
    cerrarModal();
    var descrip = combo.id;
    document.getElementById("prod").value = descrip;
    var id = "artic";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            descrip: descrip
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var precio = json.precio;
            var imp = json.impuesto;
            document.getElementById("price").value = precio;
            document.getElementById("imp").value = imp;
        },
        error: function(data) {}
    });
}

function consultaDesc() { //Obtiene el porcentaje de impuesto y la descripción del artículo seleccionado
    if (document.getElementById("prod").value == "") {
        mostrar("Debes seleccionar un producto");
        return;
    } else {
        var descrip = document.getElementById("prod").value;
    }
    if (document.getElementById("imp").value == "Impuesto") {
        mostrar("Debes seleccionar un Impuesto");
        return;
    } else {
        var impu = document.getElementById("imp").value;
    }
    var id = "artic";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            descrip: descrip
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var art = json.Nombre;
            descFinal = json.descripcion;
            producto = art;
            var id = "porcentaje";
            $.ajax({
                url: src + 'valores.php',
                method: 'POST',
                data: {
                    id: id,
                    impu: impu
                },
                success: function(data) {
                    var json = jQuery.parseJSON(data);
                    var por = json.Porcentaje;
                    porcentaje = por;
                    agregar();
                },
                error: function(data) {}
            });
        },
        error: function(data) {}
    });
}

function agregar() { // Agrega una fila a la tabla y a los array
    if (document.getElementById("prod").value == "Producto") {
        mostrar("Debes seleccionar un producto");
        return;
    } else {
        var desc = document.getElementById("prod").value;
    }
    if (document.getElementById("cant").value == "") {
        mostrar("Debes indicar una cantidad");
        return;
    } else {
        var cant = document.getElementById("cant").value;
    }
    if (document.getElementById("price").value == "") {
        mostrar("Debes indicar un precio");
        return;
    } else {
        var price = document.getElementById("price").value;
    }
    var importe = (cant * price).toFixed(2);
    cont++;
    var fila = "<tr><td>" + cont + "</td><td>" + producto + "</td><td>" + cant + "</td><td>$" + price + "</td><td>" + porcentaje + "%" + "</td><td>" + descFinal + "</td><td>$" + importe + "</td></tr>";
    var btn = document.createElement("TR");
    btn.innerHTML = fila;
    $('#TableBody').append(btn);
    if (document.getElementById("importe").value == "") {
        SubTotal = 0;
    } else {
        SubTotal = document.getElementById("importe").value;
    }
    if (document.getElementById("impuest").value == "") {
        iva = 0;
    } else {
        iva = document.getElementById("impuest").value;
    }
    if (document.getElementById("tot").value == "") {
        Total = 0;
    } else {
        Total = document.getElementById("tot").value;
    }
    SubTotal = parseFloat(parseFloat(SubTotal) + parseFloat(importe)).toFixed(2);
    document.getElementById("importe").value = SubTotal;
    iva = parseFloat(parseFloat(iva) + parseFloat(parseFloat(importe) * (porcentaje / 100))).toFixed(2);
    document.getElementById("impuest").value = iva;
    Total = parseFloat(parseFloat((parseFloat(SubTotal) + parseFloat(iva)))).toFixed(2);
    document.getElementById("tot").value = Total;
    array = [producto, cant, price, porcentaje, descFinal, importe];
    Partidas[contArray] = [array];
    Totales[contArray] = [SubTotal, iva, Total];
    contArray++;
    document.getElementById("prod").value = "";
    document.getElementById("cant").value = 1;
    document.getElementById("price").value = "";
}

function abrirEnPestana(url) { // Función que abre una página en una nueva pestaña
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

function buscarUsuario() { // Busca el id del usuario actual
    var id = "getUser";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            usuario = data;
        },
        error: function(data) {}
    });
}

function Cotizando() { // Agrega la cotización a la base de datos
    var trs = $("#TableBody tr").length;
    
    if (trs == 2) {
        
        mostrar("Debes agregar un producto");
        return;
    }
  
    var id = "Cotizando";
    if (document.getElementById("client").value == 'Cliente') {
        mostrar("Debes seleccionar un cliente");
        return;
    } else {
        var Cliente = document.getElementById("client").value;
    }
    var Importe = document.getElementById("importe").value;
    var Impuesto = document.getElementById("impuest").value;
    var Total = document.getElementById("tot").value;
    $.ajax({
        url: src + 'insert.php',
        method: 'POST',
        data: {
            id: id,
            Fecha: Fecha,
            Hora: Hora,
            Cliente: Cliente,
            Importe: Importe,
            Impuesto: Impuesto,
            Total: Total,
            Cotizacion: Cotizacion
        },
        success: function(data) {
            alert(data);
            id = 'PartCotiza';
            var Art2 = "";
            var Cant2 = 0;
            var Price2 = 0;
            var porc2 = 0;
            var desc2 = 0;
            var import2 = 0;
            for (var i = 0; i < Partidas.length; i++) { //Agrega las partidas de la cotización
                Art2 = Partidas[i][0][0];
                Cant2 = Partidas[i][0][1];
                Price2 = Partidas[i][0][2];
                porc2 = Partidas[i][0][3];
                desc2 = Partidas[i][0][4];
                import2 = Partidas[i][0][5];
                $.ajax({
                    url: src + 'insert.php',
                    method: 'POST',
                    data: {
                        id: id,
                        Cotizacion: Cotizacion,
                        Art2: Art2,
                        Cant2: Cant2,
                        Price2: Price2,
                        porc2: porc2,
                        desc2: desc2,
                        import2: import2
                    },
                    success: function(data) {},
                    error: function(data) {}
                });
            }
            id = "CotizaId";
            $.ajax({
                url: src + 'valores.php',
                method: 'POST',
                data: {
                    id: id,
                    Cotizacion: Cotizacion
                },
                success: function(data) {
                    var url = src + "reporteCotiza.php";
                    abrirEnPestana(url); // Abre el reporte de cotización
                    location.reload();
                },
                error: function(data) {}
            });
        },
        error: function(data) {}
    });
}

function cargarCotiza() { // Carga la cotización en la que se va a trabajar
    var id = "cargarCotiza";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var cot = json.Cotizacion;
            if (cot == null) {
                Cotizacion = 1;
            } else {
                Cotizacion = cot;
                Cotizacion = parseInt(Cotizacion) + 1;
            }
        },
        error: function(data) {}
    });
}