document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
var option = true; //true UPDATE false ADD

window.onload = (function () {

    cargarOpciones();

});


function Guardar(){
    if (option == true) {
        update();
    }else{
        alert("No tienes acceso a la facturación")
    }
}


function update(){
    var id = "updateDatosFactura";


    if (document.getElementById("expeditionPlace").value == "") {
        alert("Debes llenar todos los campos");
        return;
    } else {
        var expeditionPlace = document.getElementById('expeditionPlace').value;
    }


    var regimenFiscal = document.getElementById('regimenFiscal').value;


    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            expeditionPlace: expeditionPlace,
            regimenFiscal: regimenFiscal
        },
        success: function(data) {
            alert("Datos Modificados");
        },
        error: function(data) {
        }
    });



}



function getData(){
    var id = "dataFacturacion";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var RegimenFiscal = json.RegimenFiscal;
            var ExpeditionPlace = json.ExpeditionPlace;

            document.getElementById("regimenFiscal").value = RegimenFiscal;
            document.getElementById("expeditionPlace").value = ExpeditionPlace;

        },
        error: function(data) {}
    });
}


function cargarOpciones() {
    var id = "cargarOpciones";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {

            if (data == 1) {
                option = true;
                getData();
            } else {
                option = false;
            }
        },
        error: function(data) {}
    });
}


function AddSerie(){
    var id = "AddSerie";
    if (document.getElementById("serie").value == "") {
        alert("Debes indicar la serie");
        return;
    } else {
        var Serie = document.getElementById("serie").value;
    }

    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Serie: Serie
        },
        success: function(data) {
            if (data == 1) {
                alert("La Serie Ya Existe");
            }else{
                alert("Serie Agregada");
                location.reload();
            }
            
        },
        error: function(data) {
        }
    });
}

