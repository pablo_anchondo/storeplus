document.writeln('<script src="js/jQuery v3.3.1.js"></script>');
var src = "http://localhost/Proyectos/SPLocal/";
var idCliente = "";
var value = ""; //id del cliente a eliminar
var prov = 0; //id del proveedor a eliminar
var vend = 0;
var f = new Date();
var Fecha = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
var Hora = f.getHours() + ":" + (f.getMinutes()) + ":" + f.getSeconds();
window.onload = (function() {
    $('#alerta').hide();
    $('#alertaAdd').hide();
    $('#alertaDel').hide();
    $('#alertaMax13').hide();
    $('#alertaMax13U').hide();
    $('#alertaProv').hide();
    $('#alertaMax').hide();
    $('#alertaErrorProv').hide();
    $('#alertaDel2').hide();
    $('#aletrMP').hide();
    $('#alertaVend').hide();
    $('#caja_busqueda2').on('keyup', function() { //Detecta cuando hay un cambio en el buscador
        var valor = $(this).val();
        if (valor != "") {
            buscarDatos(valor);
        } else {
            buscarDatos("");
        }
    }).keyup();
});

function NotificaVendedor(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaVend').html(data);
    $('#alertaVend').show('fade');
    setTimeout(function() {
        $('#alertaVend').hide('fade');
    }, 1000);
}


function Notifica(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#aletrMP').html(data);
    $('#aletrMP').show('fade');
    setTimeout(function() {
        $('#aletrMP').hide('fade');
    }, 1000);
}

function mostrarErrProv(mensaje) { //Muestra en pantalla una alerta
    var data = "<strong>" + mensaje + "<strong/>"
    $('#alertaErrorProv').html(data);
    $('#alertaErrorProv').show('fade');
    setTimeout(function() {
        $('#alertaErrorProv').hide('fade');
    }, 1000);
}

function mostrarMax() { //Muestra en pantalla una alerta
    $('#alertaMax').show('fade');
    setTimeout(function() {
        $('#alertaMax').hide('fade');
    }, 1000);
}

function mostrarProv2() { //Muestra en pantalla una alerta
    $('#alertaProv').show('fade');
    setTimeout(function() {
        $('#alertaProv').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarUpdate() { //Muestra en pantalla una alerta
    $('#alerta').show('fade');
    setTimeout(function() {
        $('#alerta').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarProvdel() { //Muestra en pantalla una alerta
    $('#alertaDel2').show('fade');
    setTimeout(function() {
        $('#alertaDel2').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarAdd() { //Muestra en pantalla una alerta
    $('#alertaAdd').show('fade');
    setTimeout(function() {
        $('#alertaAdd').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarDel() { //Muestra en pantalla una alerta
    $('#alertaDel').show('fade');
    setTimeout(function() {
        $('#alertaDel').hide('fade');
        location.reload();
    }, 1000);
}

function mostrarMax() { //Muestra en pantalla una alerta
    $('#alertaMax13').show('fade');
    setTimeout(function() {
        $('#alertaMax13').hide('fade');
    }, 1000);
    $('#alertaMax13U').show('fade');
    setTimeout(function() {
        $('#alertaMax13U').hide('fade');
    }, 1000);
}

function buscarDatos(consult) { //Función que realiza consulta a la base de datos de los clientes y llena la tabla en base a ella
    var id = "busquedaCliente";
    var consulta = consult;
    var empieza = 0;

    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            consulta: consulta,
            empieza: empieza,
        },
        success: function(data) {
            $('#datosCliente').html(data);
            padre = document.getElementById("1").parentNode;
            padre.classList.add('active'); 
        },
        error: function(data) {}
    });
}

function buscarProv() { // Función que realiza consulta a la base de datos de los proveedores y llena la tabla en base a ella
    var id = "busquedaProv";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#datosCliente').html(data);

        },
        error: function(data) {}
    });
}

function validarTamano(caja, tamano) { //Función que valida el tamaño de una cadena y lo limita
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        mostrarMax();
        return false;
    }
}

function DatosProv(boton) { // Función que llena los campos de la ventana modal de los proveedores 
    var datProv = boton.id;
    var id = "getProv";
    prov = datProv;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            datProv: datProv
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var Nombre = json.Nombre;
            var Pais = json.Pais;
            var CP = json.CP;
            var Colonia = json.Colonia;
            var Poblacion = json.Poblacion;
            var Ciudad = json.Ciudad;
            var Estado = json.Estado;
            var Lada = json.Lada;
            var Telefono = json.Telefono;
            var RFC = json.RFC;
            var Mail = json.Mail;
            var Web = json.Web;
            var Observ = json.Observ;
            document.getElementById("NombreProv").value = Nombre;
            document.getElementById("PaisProv").value = Pais;
            document.getElementById("LadaP").value = Lada;
            document.getElementById("TelProv").value = Telefono;
            document.getElementById("StateProv").value = Estado;
            document.getElementById("CityProv").value = Ciudad;
            document.getElementById("ColProv").value = Colonia;
            document.getElementById("PobProv").value = Poblacion;
            document.getElementById("CpProv").value = CP;
            document.getElementById("RfcProv").value = RFC;
            document.getElementById("MailProv").value = Mail;
            document.getElementById("WebProv").value = Web;
            document.getElementById("observ").value = Observ;
        },
        error: function(data) {}
    });
}

function deleteProv(bto) { // Función que cambia el id del proveedor a eliminar se llama cuando se da clic en el botón borrar
    var id = "delProv";
    prov = bto.id;
}

function Borrar() { // Función para eliminar un proveedor de la base de datos
    var id = "BorrarProv";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            prov: prov
        },
        success: function(data) {
            mostrarProvdel();
        },
        error: function(data) {}
    });
}

function ModUni() { //Función que modifica los datos de las unidades en la base de datos
    if (document.getElementById("newUni").value == "") {
        alert("El campo Nuevo Nombre Unidad es obligatorio");
        return;
    } else {
        var newUni = document.getElementById("newUni").value;
    }
    var unidad = document.getElementById("unidad").value;
    var id = "ModUnidad";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            unidad: unidad,
            newUni: newUni
        },
        success: function(data) {
            alert("Unidad Modificada");
            location.reload();
        },
        error: function(data) {}
    });
}

function ModDpto() { // Función que modifica los datos de los departamentos en la base de datos
    if (document.getElementById("newdepto").value == "") {
        alert("El campo Nuevo Nombre  es obligatorio");
        return;
    } else {
        var NewName = document.getElementById("newdepto").value;
    }
    var depa = document.getElementById("dpt").value;
    var id = "ModDpto";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            depa: depa,
            NewName: NewName
        },
        success: function(data) {
            alert("Departamento Modificado");
            location.reload();
        },
        error: function(data) {}
    });
}

function ModImp() { // Función que modifica los datos de los impuestos en la base de datos
    if (document.getElementById("newImp").value == "") {
        alert("El campo Nombre  es obligatorio");
        return;
    } else {
        var newImp = document.getElementById("newImp").value;
    }
    if (document.getElementById("newPor").value == "") {
        alert("El campo Porcentaje  es obligatorio");
        return;
    } else {
        var newPor = document.getElementById("newPor").value;
    }
    var Imp = document.getElementById("impu").value;
    var id = "ModImp";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            newImp: newImp,
            newPor: newPor,
            Imp: Imp
        },
        success: function(data) {
            alert("Impuesto Modificado");
            location.reload();
        },
        error: function(data) {}
    });
}

function UpdtProv() { // Función que modifica los datos de los proveedores en la base de datos
    var id = "UpdtProv";
    var Nombre = document.getElementById("NombreProv").value;
    var Pais = document.getElementById("PaisProv").value;
    var Lada = document.getElementById("LadaP").value;
    var Telefono = document.getElementById("TelProv").value;
    var Estado = document.getElementById("StateProv").value;
    var Ciudad = document.getElementById("CityProv").value;
    var Colonia = document.getElementById("ColProv").value;
    var Poblacion = document.getElementById("PobProv").value;
    var CP = document.getElementById("CpProv").value;
    var RFC = document.getElementById("RfcProv").value;
    var Mail = document.getElementById("MailProv").value;
    var Web = document.getElementById("WebProv").value;
    var Observ = document.getElementById("observ").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Nombre: Nombre,
            Pais: Pais,
            Lada: Lada,
            Telefono: Telefono,
            Estado: Estado,
            Ciudad: Ciudad,
            Colonia: Colonia,
            Poblacion: Poblacion,
            CP: CP,
            RFC: RFC,
            Mail: Mail,
            Web: Web,
            Observ: Observ,
            prov: prov
        },
        success: function(data) {
            mostrarProv2("Proveedor Modifcado");
        },
        error: function(data) {}
    });
}

function validarTel(caja, tamano, e) { //Función que valida si la entrada es numérica o no y además limita su tamaño
    if (document.getElementById(caja.id).value.length > tamano - 1) {
        mostrarMax();
        return false;
    }
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta números 
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validaR(e) { //Función que solo acepta números como entrada
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    if (tecla == 46) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function deleteClient() { //Función que elimina un cliente de la base de datos
    var id = 'deleteClient';
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            value: value
        },
        success: function(data) {
            $('#Eliminar').modal('toggle');
            mostrarDel();
        },
        error: function(data) {}
    });
}

function ValueDelete(boton) { //  Función que cambia el id del cliente a eliminar se llama cuando se da clic en el botón borrar
    value = boton.id;
}

function UpdateCliente() { // Función que modifica los datos de los clientes en la base de datos
    var id = "UpdateCliente";
    var nom = document.getElementById("NombreCM").value;
    var tel = document.getElementById("TelCM").value;
    var pai = document.getElementById("PaisCM").value;
    var cp = document.getElementById("cpCM").value;
    var call = document.getElementById("CalleCM").value;
    var num = document.getElementById("NumeroCM").value;
    var cit = document.getElementById("CityCM").value;
    var stat = document.getElementById("StateCM").value;
    var col = document.getElementById("ColCM").value;
    var loc = document.getElementById("LocCM").value;
    var rfc = document.getElementById("RFCCM").value;
    var mail = document.getElementById("MailCM").value;
    var obs = document.getElementById("observM").value;
    var Lada = document.getElementById("LadaCM").value;
    var ClaveI = document.getElementById("ClaveIM").value;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            idCliente: idCliente,
            nom: nom,
            tel: tel,
            pai: pai,
            cp: cp,
            call: call,
            num: num,
            cit: cit,
            stat: stat,
            col: col,
            loc: loc,
            rfc: rfc,
            mail: mail,
            obs: obs,
            Lada: Lada,
            ClaveI: ClaveI
        },
        success: function(data) {
            $('#Modificar').modal('toggle');
            mostrarUpdate()
        },
        error: function(data) {}
    });
}

function DatosCliente(cliente) { // Función que llena los campos de la ventana modal de los clientes 
    var id = "DatosCliente";
    var client = cliente.id;
    idCliente = client;
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            client: client
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var nom = json.Nombre;
            var tel = json.Telefono;
            var pai = json.Pais;
            var cp = json.cp;
            var call = json.Calle;
            var num = json.Numero;
            var cit = json.Ciudad;
            var stat = json.Estado;
            var col = json.Colonia;
            var loc = json.Localidad;
            var rfc = json.RFC;
            var mail = json.Mail;
            var obs = json.Observ;
            var Lada = json.Lada;
            var ClaveInterna = json.ClaveInterna;
            document.getElementById("NombreCM").value = nom;
            document.getElementById("TelCM").value = tel;
            document.getElementById("PaisCM").value = pai;
            document.getElementById("cpCM").value = cp;
            document.getElementById("CalleCM").value = call;
            document.getElementById("NumeroCM").value = num;
            document.getElementById("CityCM").value = cit;
            document.getElementById("StateCM").value = stat;
            document.getElementById("ColCM").value = col;
            document.getElementById("LocCM").value = loc;
            document.getElementById("RFCCM").value = rfc;
            document.getElementById("MailCM").value = mail;
            document.getElementById("observM").value = obs;
            document.getElementById("LadaCM").value = Lada;
            document.getElementById("ClaveIM").value = ClaveInterna;
        },
        error: function(data) {}
    });
}

function AddClient() { // Agrega un cliente a la base de datos
    var id = "cliente";
    if (document.getElementById("NombreC").value == "") {
        alert("El campo Nombre  es obligatorio");
        return;
    } else {
        var NombreC = document.getElementById("NombreC").value;
    }
    if (document.getElementById("TelC").value == "") {
        alert("El campo Telefono  es obligatorio");
        return;
    } else {
        var TelC = document.getElementById("TelC").value;
    }
    if (document.getElementById("LadaC").value == "") {
        alert("El campo Lada  es obligatorio");
        return;
    } else {
        var LadaC = document.getElementById("LadaC").value;
    }
    if (document.getElementById("PaisC").value == "") {
        alert("El campo País  es obligatorio");
        return;
    } else {
        var PaisC = document.getElementById("PaisC").value;
    }
    if (document.getElementById("cpC").value == "") {
        alert("El campo C.P  es obligatorio");
        return;
    } else {
        var cpC = document.getElementById("cpC").value;
    }
    if (document.getElementById("CalleC").value == "") {
        alert("El campo Calle es obligatorio");
        return;
    } else {
        var CalleC = document.getElementById("CalleC").value;
    }
    if (document.getElementById("NumeroC").value == "") {
        alert("El campo Numero es obligatorio");
        return;
    } else {
        var NumeroC = document.getElementById("NumeroC").value;
    }
    if (document.getElementById("CityC").value == "") {
        alert("El campo Ciudad es obligatorio");
        return;
    } else {
        var CityC = document.getElementById("CityC").value;
    }
    if (document.getElementById("StateC").value == "") {
        alert("El campo Estado es obligatorio");
        return;
    } else {
        var StateC = document.getElementById("StateC").value;
    }
    if (document.getElementById("ColC").value == "") {
        alert("El campo Colonia es obligatorio");
        return;
    } else {
        var ColC = document.getElementById("ColC").value;
    }
    if (document.getElementById("LocC").value == "") {
        alert("El campo Localidad es obligatorio");
        return;
    } else {
        var LocC = document.getElementById("LocC").value;
    }
    if (document.getElementById("RFCC").value == "") {
        alert("El campo RFC es obligatorio");
        return;
    } else {
        var RFCC = document.getElementById("RFCC").value;
    }
    var MailC = document.getElementById("MailC").value;
    var Observ = document.getElementById("observ").value;
    var ClaveI = document.getElementById("ClaveI").value;
    $.ajax({
        url: src + 'insert.php',
        method: 'POST',
        data: {
            id: id,
            NombreC: NombreC,
            TelC: TelC,
            PaisC: PaisC,
            cpC: cpC,
            CalleC: CalleC,
            NumeroC: NumeroC,
            CityC: CityC,
            StateC: StateC,
            ColC: ColC,
            LocC: LocC,
            RFCC: RFCC,
            MailC: MailC,
            Observ: Observ,
            Fecha: Fecha,
            LadaC: LadaC,
            ClaveI: ClaveI
        },
        success: function(data) {
            $('#Agregar').modal('toggle');
            mostrarAdd()
        },
        error: function(data) {}
    });
}

function AddMpago() { // Agrega un método de pago a la base de datos
    var id = "AddMpagp";
    if (document.getElementById("ValorMpago").value == "") {
        Notifica("Debes indicar el metodo de pago");
        return;
    } else {
        var ValorMpago = document.getElementById("ValorMpago").value;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            ValorMpago: ValorMpago
        },
        success: function(data) {
            Notifica("Proceso Realizado");
        },
        error: function(data) {}
    });
}

function ModMpago() { // Modifica el método de pago
    var id = "ModMpagp";
    if (document.getElementById("ValorMpago").value == "") {
        Notifica("Debes indicar el metodo de pago");
        return;
    } else {
        var ValorMpago = document.getElementById("ValorMpago").value;
    }
    if (document.getElementById("mPago").value == "") {
        Notifica("Debes seleccionar un metodo de pago");
        return;
    } else {
        var Select = document.getElementById("mPago").value;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            ValorMpago: ValorMpago,
            Select: Select
        },
        success: function(data) {
            Notifica("Proceso Realizado");
        },
        error: function(data) {}
    });
}

function DelMpago() { // Elimina el método de pago
    var id = "DelMpago";
    if (document.getElementById("mPago").value == "") {
        Notifica("Debes seleccionar un metodo de pago");
        return;
    } else {
        var Select = document.getElementById("mPago").value;
    }
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Select: Select
        },
        success: function(data) {
            Notifica("Proceso Realizado");
        },
        error: function(data) {}
    });
}

function existecliente(cliente){
    var clienteexist = cliente.value;
    var id = "existecliente";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            clienteexist: clienteexist
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var Nombre = json.Nombre;
            if(typeof Nombre === 'undefined'){

            }else{
                alert("El cliente ya existe");
                document.getElementById("NombreC").value = "";
            }

        },
        error: function(data) {}
    });

}


function pagination(pagina){
    var id = "busquedaCliente";
    var numpagina = pagina.id;
    var clase = pagina.className;
    if (numpagina == 1) {
        var empieza = ((numpagina-1)*20);
    }else{
        var empieza = ((numpagina-1)*20)+1;
    }
    
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            empieza: empieza,
        },
        success: function(data) {
            $('#datosCliente').html(data);
            
            padre = document.getElementById(numpagina).parentNode;
            padre.classList.add('active');              

        },
        error: function(data) {}
    });

}


function buscarVendedor() { // Función que realiza consulta a la base de datos de los proveedores y llena la tabla en base a ella
    var id = "buscarVendedor";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id
        },
        success: function(data) {
            $('#datosCliente').html(data);

        },
        error: function(data) {}
    });
}

function idVend(boton){
    vend = boton.id;
}


function getPrivileges(boton) { // Función que llena los campos de la ventana modal de los proveedores 
    var datVend = boton.id;
    vend = datVend;
    var id = "getPrivileges";
   
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            datVend: datVend
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var Inventario = json.Inventario;
            var Compras = json.Compras;
            var Control = json.Control;
            var Reportes = json.Reportes;
            var Operaciones = json.Operaciones;
      
            if (Inventario == 'TRUE') {
                document.getElementById("Inventario").checked = true;
            }else{
                document.getElementById("Inventario").checked = false;
            }

            if (Compras == 'TRUE') {
                document.getElementById("Compras").checked = true;
            }else{
                document.getElementById("Compras").checked = false;
            }

            if (Control == 'TRUE') {
                document.getElementById("Control").checked = true;
            }else{
                document.getElementById("Control").checked = false;
            }

            if (Reportes == 'TRUE') {
                document.getElementById("Reportes").checked = true;
            }else{
                document.getElementById("Reportes").checked = false;
            }

            if (Operaciones == 'TRUE') {
                document.getElementById("Operaciones").checked = true;
            }else{
                document.getElementById("Operaciones").checked = false;
            }
         
            

        },
        error: function(data) {}
    });
}


function ModVendedor(){
    var id = "ModVendedor";
    var Inventario = "FALSE";
    var Compras = "FALSE";
    var Control = "FALSE";
    var Reportes = "FALSE";
    var Operaciones = "FALSE";

    if (document.getElementById("Inventario").checked == true) {
        Inventario = "TRUE";
    }

    if (document.getElementById("Compras").checked == true) {
        Compras = "TRUE";
    }

    if (document.getElementById("Control").checked == true) {
        Control = "TRUE";
    }

    if (document.getElementById("Reportes").checked == true) {
        Reportes = "TRUE";
    }

    if (document.getElementById("Operaciones").checked == true) {
        Operaciones = "TRUE";
    }

    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Inventario: Inventario,
            Compras: Compras,
            Control: Control,
            Reportes: Reportes,
            Operaciones: Operaciones,
            vend: vend
        },
        success: function(data) {
            NotificaVendedor("Privilegios Modificados");
        },
        error: function(data) {}
    });
}


function deleteVendedor(){
    var id="deleteVendedor";
    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            vend: vend
        },
        success: function(data) {
            alert("Vendedor Eliminado");
            location.reload();
        },
        error: function(data) {}
    });
}