document.writeln('<script src="js/jQuery v3.3.1.js"></script>'); //Incluir JQUERY
var src = "http://localhost/Proyectos/SPLocal/";
var ImporteF = 0;
var impuTotalF = 0;
var TotalF = 0;
var contador = 0;
var TempImpor = 0;
var TempImp = 0;
var TempTot = 0;
var Contenedor = new Array;
var Partidas = new Array;
var contArray = 0;
window.onload = (function() {

    document.getElementById("SubT").disabled = true;
    document.getElementById("impue").disabled = true;
    document.getElementById("Tot").disabled = true;
    document.getElementById("rfce").disabled = true;

    $('#precioU').on('keyup', function() {
        var valor = $(this).val();
        var decallowed = 2;
        if (valor.indexOf('.') == -1) {
            valor += ".";
        }
        dectext = valor.substring(valor.indexOf('.') + 1, valor.length);
        if (dectext.length > decallowed) {
            alert("Maximo 2 decimales");
            valor = valor.substr(0, valor.length - 1);
            document.getElementById("precioU").value = valor;
        }
    }).keyup();


    $('#cantidad').on('keyup', function() {
        var valor = $(this).val();
        var decallowed = 2;
        if (valor.indexOf('.') == -1) {
            valor += ".";
        }
        dectext = valor.substring(valor.indexOf('.') + 1, valor.length);
        if (dectext.length > decallowed) {
            alert("Maximo 2 decimales");
            valor = valor.substr(0, valor.length - 1);
            document.getElementById("cantidad").value = valor;
        }
    }).keyup();

});

function Agregar(){
    if (document.getElementById("prodServ").value == '') {
        alert("Debes Seleccionar un producto");
        return;
    }else{
        var Producto = document.getElementById("prodServ").value;
    }

    if (document.getElementById("precioU").value == '' || document.getElementById("precioU").value == '0.00') {
        alert("Debes Indicar un precio");
    }else{
        var precio = document.getElementById("precioU").value;
    }

    if (document.getElementById("cantidad").value == '' || document.getElementById("cantidad").value == '0') {
        alert("Debes Indicar una cantidad");
    }else{
        var cantidad = document.getElementById("cantidad").value;
    }

        var Impuesto = parseFloat(document.getElementById("imp").value);
        cantidad = parseFloat(cantidad);
        precio = parseFloat(precio);
        var Importe = (cantidad * precio).toFixed(2);
        var impuTotal = (Importe*(Impuesto/100)).toFixed(2);
        var Total = parseFloat(parseFloat(Importe) + parseFloat(impuTotal)).toFixed(2);
       

        ImporteF = parseFloat(parseFloat(ImporteF) + parseFloat(Importe)).toFixed(2);
        impuTotalF = parseFloat(parseFloat(impuTotalF) + parseFloat(impuTotal)).toFixed(2);
        TotalF = parseFloat(parseFloat(TotalF) + parseFloat(Total)).toFixed(2);



    var id = "articDesc";

    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Producto: Producto
        },
        success: function(data) {
            var json = jQuery.parseJSON(data);
            var descripcion = json.descripcion;
            
           
            var fila = "<tr><td class='tdd'>" + descripcion + "</td><td class='tdd'>" + cantidad + "</td><td class='tdd'>" + precio + "</td><td class='tdd'>" + Importe + "</td><td class='tdd'>"+ impuTotal +"</td><td class='tdd'>"+Impuesto+"</td><td class='tdd'>"+Total+"</td><td><button class='btn btn-danger borrar col-lg-12'>Eliminar</button></td></td></tr>";
            var btn = document.createElement("TR");
            btn.innerHTML = fila;
            $('#tablita').append(btn);


            document.getElementById("SubT").value = ImporteF;
            document.getElementById("impue").value = impuTotalF;
            document.getElementById("Tot").value = TotalF;

        },
        error: function(data) {}
    });


    

}


function getConcepto(articulo){
   var id = "getConcepto";
    if (articulo.value == '') {
        document.getElementById("cantidad").value = 1;
        document.getElementById("precioU").value = "0.00";
        return;
    }

    var Sarticulo = articulo.value;

    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            id: id,
            Sarticulo: Sarticulo
        },
        success: function(data) {

            var json = jQuery.parseJSON(data);
            var impuesto = json.Porcentaje;
            var precio = json.precio;
           
            document.getElementById("precioU").value = precio;
            document.getElementById("imp").value = impuesto;

        },
        error: function(data) {}
    });

}



function GenerarCFDI(){
    var id = "generarcfdi";

    if (document.getElementById("NombreE").value == "nulo") {
        alert("Falta Nombre o Razon Social");
        return;
    }else{
        var NombreE = document.getElementById("NombreE");
        var NombreE = NombreE.options[NombreE.selectedIndex].text;
    }


    if (document.getElementById("rfce").value == "") {
        alert("Falta RFC");
        return;
    }else{
        var rfce = document.getElementById("rfce").value;
    }

    if (document.getElementById("condiciones").value == "") {
        var condiciones = "CONDICIONES";
    }else{
        var condiciones = document.getElementById("condiciones").value;
    }

    if (document.getElementById("ordencompra").value == "") {
        var ordencompra = "";
    }else{
        var ordencompra = document.getElementById("ordencompra").value;
    }

    if (document.getElementById("mail").value == "") {
        var mail = "";
    }else{
        var mail = document.getElementById("mail").value;
    }

    

    var Serie = document.getElementById("Serie").value;
    var uso = document.getElementById("usocfdi").value;
    var formapago = document.getElementById("fp").value;
    var metodopago = document.getElementById("mp").value;
    var tipocomprobante = document.getElementById("tipoComprobante").value;
    var TotalCFDI = document.getElementById("Tot").value;
    var SubTotalCFDI = document.getElementById("SubT").value;
    var ImpuestoCFDI = document.getElementById("impue").value;
    var Descrip = "";
    var Cantidad = 0;
    var Precio = 0;
    var Importet = 0;
    var Impuestot = 0;
    var Totalt = 0;
    var porcen = 0;

    var control = 0;

    $(".tdd").each(function(){

        if (control == 0) {
            Descrip = $(this).text();
        }
        
        if (control == 1) {
            Cantidad = parseFloat($(this).text()).toFixed(2);
        }

        if (control == 2) {
            Precio = parseFloat($(this).text()).toFixed(2);
        }

        if (control == 3) {
            Importet = parseFloat($(this).text()).toFixed(2);
        }

        if (control == 4) {
            Impuestot = parseFloat($(this).text()).toFixed(2);
        }

        if (control == 5) {
            porcen = parseFloat($(this).text()).toFixed(2);
        }

        if (control == 6) {
            Totalt = parseFloat($(this).text()).toFixed(2);
            Partidas = [Descrip, Cantidad ,Precio, Importet, Impuestot, porcen, Totalt];
            Contenedor[contArray] = [Partidas];
            contArray++;
            control = 0;
        }else{
            control = control +1;
        }
        
    });


    $.ajax({
        url: src + 'valores.php',
        method: 'POST',
        data: {
            NombreE: NombreE,
            Contenedor: Contenedor,
            rfce: rfce,
            uso: uso,
            formapago: formapago,
            metodopago: metodopago,
            tipocomprobante: tipocomprobante,
            TotalCFDI: TotalCFDI,
            SubTotalCFDI: SubTotalCFDI,
            ImpuestoCFDI: ImpuestoCFDI,
            id: id,
            Serie: Serie,
            condiciones: condiciones,
            ordencompra: ordencompra,
            mail: mail
        },
        success: function(data) {
            setTimeout(function() {
                var url = src + "CFDI.php";
                abrirEnPestana(url);
                location.reload();
            }, 1000);
            
        },
        error: function(data) {}
    });




}



function abrirEnPestana(url) { // Función que abre una página en una nueva pestaña
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}


function getRFC(client){
    if (client.value == 'nulo') {
        document.getElementById("rfce").value = "";
    }else{
        document.getElementById("rfce").value = client.value;
        var id = "getMail";
        var rfcmail = client.value;
        $.ajax({
            url: src + 'valores.php',
            method: 'POST',
            data: {
                id: id,
                rfcmail: rfcmail
            },
            success: function(data) {
                var json = jQuery.parseJSON(data);
                var Mail = json.Mail;
                document.getElementById("mail").value = Mail;
            },
            error: function(data) {}
        });
    }

}

function SetFP(item){
    if (item.value == 'PPD') {
        document.getElementById("fp").value = "99";
        document.getElementById("fp").disabled = true;
    }else{
        document.getElementById("fp").value = "01";
        document.getElementById("fp").disabled = false;
    }
}


function confirm(){
    var metodopago = document.getElementById("mp").value;
    var metodo = "";
    if (metodopago == "PUE") {
        metodo = "a contado";
    }else{
        metodo = "a credito";
    }
    var chain = "<h3>Seguro que la venta es "+ metodo+"</h3>";
    $('#content').html(chain);
}

function marcar(){
    if (document.getElementById("NombreE").value == "") {
        alert("Falta Nombre o Razon Social");
        return;
    }else{
       // var NombreE = document.getElementById("NombreE").value;
    }

    if (document.getElementById("rfce").value == "") {
        alert("Falta RFC");
        return;
    }else{
        //var rfce = document.getElementById("rfce").value;
    }

    $("#mp").css('border-color', '#C80000');  
    $('body, html').animate({
        scrollTop: '0px'
    });
 
 
}


function loadCFDI(uuidLoad){
    
    $.ajax({
        url: src + 'leer.php',
        method: 'POST',
        data: {
            uuidLoad: uuidLoad
        },
        success: function(data) {
            
            var json = jQuery.parseJSON(data);
            console.log(json);
            FormaPago = json[0].FormaPago[0];
            MetodoPago = json[0].MetodoPago[0];
            TipoDeComprobante = json[0].TipoDeComprobante[0];
            Serie = json[0].Serie[0];
            if (typeof json[0].CondicionesDePago  == "undefined" || json[0].CondicionesDePago  == null) {
                
            }else{
                CondicionesDePago = json[0].CondicionesDePago[0];
                document.getElementById("condiciones").value = CondicionesDePago;
            }
            
            SubTotal = json[0].SubTotal[0];
            TotalImpuestosTrasladados = json[0].TotalImpuestosTrasladados[0];
            Total = json[0].Total[0];
            Nombre = json[0].Nombre[0];
            Rfc = json[0].Rfc[0];
            UsoCFDI = json[0].UsoCFDI[0];

            document.getElementById("fp").value = FormaPago;
            document.getElementById("mp").value = MetodoPago;
            document.getElementById("tipoComprobante").value = TipoDeComprobante;
            document.getElementById("Serie").value = Serie;
            
            document.getElementById("SubT").value = SubTotal;
            document.getElementById("Tot").value = Total;
            document.getElementById("impue").value = TotalImpuestosTrasladados;
            document.getElementById("NombreE").value = Rfc;
            document.getElementById("rfce").value = Rfc;
            document.getElementById("usocfdi").value = UsoCFDI;

            ImporteF = SubTotal;
            impuTotalF = TotalImpuestosTrasladados;
            TotalF = Total;
            var tasa = 0;

            

            for (index = 0; index < json[1].length; index++) {
                if (json[1][index].TasaOCuota[0] == "0.160000") {
                    tasa = 16;
                }else if (json[1][index].TasaOCuota[0] == "0.000000") {
                    tasa = 0;
                }
                var fila = "<tr><td class='tdd'>" + json[1][index].Descripcion[0] + "</td><td class='tdd'>" + json[1][index].Cantidad[0] + "</td><td class='tdd'>" + json[1][index].ValorUnitario[0]  + "</td><td class='tdd'>" + json[1][index].Importe[0] + "</td><td class='tdd'>"+ json[1][index].ImporteImp[0] +"</td><td class='tdd'>"+tasa+"</td><td class='tdd'>"+json[1][index].Total+"</td><td><button class='btn btn-danger borrar col-lg-12'>Eliminar</button></td></td></tr>";
               
                var btn = document.createElement("TR");
                btn.innerHTML = fila;
                $('#tablita').append(btn);
                
            }
            
        },
        error: function(data) {
            
        }
    });
}



