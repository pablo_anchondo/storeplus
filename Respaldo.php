<?php
    // Se incluye la librería de Excel
    require 'Classes/PHPExcel.php';
    session_start();
    include("conexion.php");
    if (isset($_SESSION['user'])) {
        echo "";
    } //isset($_SESSION['user'])
    else {
        echo '<script> window.location="index.php"; </script>';
    }
    // Inicializamos variables de sesión
    $Identificador = $_SESSION["Id_User"];
    $querito2      = 'select * from productos where Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"];
    $result2       = $cbd->query($querito2);
    // Se carga el Excel
    $objPHPExcel   = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("STORE-PLUS")->setDescription("Respaldo de Productos");
    // Determina el numero de hoja
    $objPHPExcel->setActiveSheetIndex(0);
    // Se crean lo encabezados de las columnas
    $objPHPExcel->getActiveSheet()->setTitle("Productos");
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'CLAVE');
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'DESCRIPCION');
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'UNIDAD');
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'COSTO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'DEPARTAMENTO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'IMPUESTO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'PRECIO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'EXISTENCIA');
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'USUARIO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'RUTA IMAGEN');
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'COSTO ULTIMO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'ALMACEN');
    $fila = 2;
    while ($fila2 = mysqli_fetch_array($result2)) {
        // Se llenan las partidas
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $fila, $fila2['articulo']);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila, $fila2['Nombre']);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $fila, $fila2['descripcion']);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $fila, $fila2['unidad']);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $fila, $fila2['costo']);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $fila, $fila2['Departamento']);
        $objPHPExcel->getActiveSheet()->setCellValue('G' . $fila, $fila2['impuesto']);
        $objPHPExcel->getActiveSheet()->setCellValue('H' . $fila, $fila2['precio']);
        $objPHPExcel->getActiveSheet()->setCellValue('I' . $fila, $fila2['existencia']);
        $objPHPExcel->getActiveSheet()->setCellValue('J' . $fila, $fila2['Id_User']);
        $objPHPExcel->getActiveSheet()->setCellValue('K' . $fila, $fila2['img']);
        $objPHPExcel->getActiveSheet()->setCellValue('L' . $fila, $fila2['costoU']);
        $objPHPExcel->getActiveSheet()->setCellValue('M' . $fila, $fila2['Almacen']);
        $fila++;
    } //$fila2 = mysqli_fetch_array($result2)
    // Se cambia de hoja
    $objPHPExcel->createSheet(1);
    $objPHPExcel->setActiveSheetIndex(1);
    $objPHPExcel->getActiveSheet()->setTitle("Clientes");
    $queryClientes  = 'select * from clients where Id_User = ' . $Identificador;
    $resultClientes = $cbd->query($queryClientes);
    // Se crean lo encabezados de las columnas
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Clave Cliente');
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Nombre');
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Pais');
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'C.P');
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CALLE');
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'NUMERO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'COLONIA');
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'ESTADO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'CIUDAD');
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'LOCALIDAD');
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'TELEFONO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'RFC');
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'E-MAIL');
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('N1', 'OBSERVACIONES');
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('O1', 'FECHA INGRESO');
    $fila = 2;
    while ($fila3 = mysqli_fetch_array($resultClientes)) {
        // Se llenan las partidas
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $fila, $fila3['ClaveCliente']);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila, $fila3['Nombre']);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $fila, $fila3['Pais']);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $fila, $fila3['cp']);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $fila, $fila3['Calle']);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $fila, $fila3['Numero']);
        $objPHPExcel->getActiveSheet()->setCellValue('G' . $fila, $fila3['Colonia']);
        $objPHPExcel->getActiveSheet()->setCellValue('H' . $fila, $fila3['Estado']);
        $objPHPExcel->getActiveSheet()->setCellValue('I' . $fila, $fila3['Ciudad']);
        $objPHPExcel->getActiveSheet()->setCellValue('J' . $fila, $fila3['Localidad']);
        $objPHPExcel->getActiveSheet()->setCellValue('K' . $fila, $fila3['Telefono']);
        $objPHPExcel->getActiveSheet()->setCellValue('L' . $fila, $fila3['RFC']);
        $objPHPExcel->getActiveSheet()->setCellValue('M' . $fila, $fila3['Mail']);
        $objPHPExcel->getActiveSheet()->setCellValue('N' . $fila, $fila3['Observ']);
        $objPHPExcel->getActiveSheet()->setCellValue('O' . $fila, $fila3['Ingreso']);
        $fila++;
    } //$fila3 = mysqli_fetch_array($resultClientes)
    $objPHPExcel->createSheet(2);
    // Se cambia de hoja
    $objPHPExcel->setActiveSheetIndex(2);
    $objPHPExcel->getActiveSheet()->setTitle("Ventas");
    $queryVentas  = 'select * from ventas where Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"];
    $resultVentas = $cbd->query($queryVentas);
    // Se crean lo encabezados de las columnas
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Venta');
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Fecha');
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Hora');
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Cliente');
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Importe');
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Impuesto');
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Total');
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Vendedor');
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Empresa');
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Comentarios');
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Pago');
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Cambio');
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Tipo de pago');
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Almacen');
    $fila = 2;
    while ($fila4 = mysqli_fetch_array($resultVentas)) {
        // Se llenan las partidas
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $fila, $fila4['Venta']);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila, $fila4['Fecha']);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $fila, $fila4['Hora']);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $fila, $fila4['Cliente']);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $fila, $fila4['Importe']);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $fila, $fila4['Impuesto']);
        $objPHPExcel->getActiveSheet()->setCellValue('G' . $fila, $fila4['Total']);
        $objPHPExcel->getActiveSheet()->setCellValue('H' . $fila, $fila4['Vendedor']);
        $objPHPExcel->getActiveSheet()->setCellValue('I' . $fila, $fila4['Empresa']);
        $objPHPExcel->getActiveSheet()->setCellValue('J' . $fila, $fila4['Comentarios']);
        $objPHPExcel->getActiveSheet()->setCellValue('K' . $fila, $fila4['Pago']);
        $objPHPExcel->getActiveSheet()->setCellValue('L' . $fila, $fila4['Cambio']);
        $objPHPExcel->getActiveSheet()->setCellValue('M' . $fila, $fila4['TPago']);
        $objPHPExcel->getActiveSheet()->setCellValue('N' . $fila, $fila4['Almacen']);
        $fila++;
    } //$fila4 = mysqli_fetch_array($resultVentas)
    $objPHPExcel->createSheet(3);
    // Se cambia de hoja
    $objPHPExcel->setActiveSheetIndex(3);
    $objPHPExcel->getActiveSheet()->setTitle("Compras");
    $queryCompras  = 'select * from compras where Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"];
    $resultCompras = $cbd->query($queryCompras);
    // Se crean lo encabezados de las columnas
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Compra');
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Fecha');
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Hora');
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Proveedor');
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Importe');
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Impuesto');
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Total');
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Almacen');
    $fila = 2;
    while ($fila5 = mysqli_fetch_array($resultCompras)) {
        // Se llenan las partidas
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $fila, $fila5['Compra']);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila, $fila5['Fecha']);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $fila, $fila5['Hora']);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $fila, $fila5['Proveedor']);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $fila, $fila5['Importe']);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $fila, $fila5['Impuesto']);
        $objPHPExcel->getActiveSheet()->setCellValue('G' . $fila, $fila5['Total']);
        $objPHPExcel->getActiveSheet()->setCellValue('H' . $fila, $fila5['Almacen']);
        $fila++;
    } //$fila5 = mysqli_fetch_array($resultCompras)
    $objPHPExcel->createSheet(4);
    // Se cambia de hoja
    $objPHPExcel->setActiveSheetIndex(4);
    $objPHPExcel->getActiveSheet()->setTitle("Departamentos");
    $queryDep  = 'select * from departamentos where Id_User = ' . $Identificador;
    $resultDep = $cbd->query($queryDep);
    // Se crean lo encabezados de las columnas
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID DEPARTAMENTO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'DEPARTAMENTO');
    $fila = 2;
    while ($fila6 = mysqli_fetch_array($resultDep)) {
        // Se llenan las partidas
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $fila, $fila6['id_Dept']);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila, $fila6['Departamento']);
        $fila++;
    } //$fila6 = mysqli_fetch_array($resultDep)
    $objPHPExcel->createSheet(5);
    // Se cambia de hoja
    $objPHPExcel->setActiveSheetIndex(5);
    $objPHPExcel->getActiveSheet()->setTitle("Almacenes");
    $queryAlm  = 'select * from almacen where Id_User = ' . $Identificador;
    $resultAlm = $cbd->query($queryAlm);
    // Se crean lo encabezados de las columnas
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Almacen');
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Nombre');
    $fila = 2;
    while ($fila7 = mysqli_fetch_array($resultAlm)) {
        // Se llenan las partidas
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $fila, $fila7['Almacen']);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila, $fila7['Nombre']);
        $fila++;
    } //$fila7 = mysqli_fetch_array($resultAlm)
    $objPHPExcel->createSheet(6);
    // Se cambia de hoja
    $objPHPExcel->setActiveSheetIndex(6);
    $objPHPExcel->getActiveSheet()->setTitle("Impuestos");
    $queryImp  = 'select * from impuestos where Id_User = ' . $Identificador;
    $resultImp = $cbd->query($queryImp);
    // Se crean lo encabezados de las columnas
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'IMPUESTO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'DESCRIPCION');
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'PORCENTAJE');
    $fila = 2;
    while ($fila8 = mysqli_fetch_array($resultImp)) {
        // Se llenan las partidas
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $fila, $fila8['id']);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila, $fila8['Impuesto']);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $fila, $fila8['Descripcion']);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $fila, $fila8['Porcentaje']);
        $fila++;
    } //$fila8 = mysqli_fetch_array($resultImp)
    $objPHPExcel->createSheet(7);
    // Se cambia de hoja
    $objPHPExcel->setActiveSheetIndex(7);
    $objPHPExcel->getActiveSheet()->setTitle("Unidades");
    $queryUni  = 'select * from unidades where Id_User = ' . $Identificador;
    $resultUni = $cbd->query($queryUni);
    // Se crean lo encabezados de las columnas
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'CODIGO');
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'UNIDAD');
    $fila = 2;
    while ($fila9 = mysqli_fetch_array($resultUni)) {
        // Se llenan las partidas
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $fila, $fila9['Codigo']);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila, $fila9['Unidad']);
        $fila++;
    } //$fila9 = mysqli_fetch_array($resultUni)
    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header('Content-Disposition: attachment;filename="Productos.xlsx"');
    header('Cache-Control: max-age=0');
    // Se crea el Excel y se descarga
    $writer->save('php://output');
?>