<?php

session_start();
include("conexion.php");
date_default_timezone_set('America/Mexico_City');

$profile       = $_SESSION['user'];
$Identificador = $_SESSION["Id_User"];
$dominio       = $_SESSION["dominio"];

$Almacen = $_SESSION['Almacen'];
$Serie = $_SESSION["Serie"];
$Subtotal = 0;
$Total = 0;
$fecha = date('Y-m-d');

$id = 0;


$fol = "SELECT MAX(id) as ident from facturas where Id_User = $Identificador AND Almacen = $Almacen AND Serie = '$Serie'";
$resultFol  = $cbd->query($fol);
$ResId = mysqli_fetch_array($resultFol);
if ($ResId['ident'] == NULL) {
    $id = 1;
}else{
    $id = $ResId['ident'] + 1;
}



$folio = Crear($id);


$queryDatos = "SELECT * FROM datosfactura WHERE Id_User = $Identificador AND Almacen = $Almacen";
$resDatos  = $cbd->query($queryDatos);
$datosFact    = mysqli_fetch_array($resDatos);

$Ruta = $datosFact['src'];
$cer = $Ruta.$datosFact['cert'];
$key = $Ruta.$datosFact['keyy'];
$cerpem = $Ruta."cer.pem";
$keypem = $Ruta."key.pem";


$contrato = $datosFact['Contrato'];
$userpade = $datosFact['Usuario'];
$passpade = $datosFact['Pass'];


$cfdi_sellado = sellarXML($folio, $cer, $keypem);

$client = new SoapClient("https://timbrado.pade.mx/servicio/Timbrado3.3?wsdl");

$opciones[0] = "GENERAR_PDF";
$cont = 1;

if (isset($_SESSION['ordencompra'])) {
    $ordencompra = $_SESSION['ordencompra'];
    $addenda2 = base64_encode($ordencompra);
    $opciones[$cont] = "OBSERVACIONES:".$addenda2;
    $cont = $cont + 1;
}else{

}

if (isset($_SESSION['mail'])) {
    $mail = $_SESSION['mail'];
    $opciones[$cont] = "ENVIAR_AMBOS:".$mail;
    $cont = $cont + 1;
}else{

}

$params = array(
    "contrato" => $contrato,
    "usuario" => $userpade,
    "passwd" => $passpade,
    "cfdiXml" => $cfdi_sellado,
    "opciones" => $opciones,
);


$response = $client->__soapCall("timbrado", array($params));

$xmlres = $response->return;

$xml_doc = new DOMDocument();
$xml_doc->loadXML($xmlres);

$UUID = $xml_doc->getElementsByTagName('UUID'); 

if (isset($UUID->item(0)->nodeValue)) {
    $idSat = $xml_doc->getElementsByTagName('id'); 

    //echo $idSat->item(0)->nodeValue;

    //echo $UUID->item(0)->nodeValue;
  
    $uid = (string)$UUID->item(0)->nodeValue;

    $msj = $xml_doc->getElementsByTagName('mensaje'); 

    //echo $msj->item(0)->nodeValue;
  
    $selloSAT = $xml_doc->getElementsByTagName('selloSAT'); 

   //echo $selloSAT->item(0)->nodeValue;

    $xmlBase64 = $xml_doc->getElementsByTagName('xmlBase64'); 

    //echo $xmlBase64->item(0)->nodeValue;

    $pdf64 = $xml_doc->getElementsByTagName('pdfBase64'); 
    $pdfFactura = (string)$pdf64->item(0)->nodeValue;

    $pdfFactura = base64_decode($pdfFactura);

    $rutaPDF = "Facturacion/PDF/Facturas/".$uid.".pdf";
    $PdfName = $uid.".pdf";
    $RutaPdfRaiz = $dominio."Facturacion/PDF/Facturas/";

    $RutaXMLRaiz = $dominio."Facturacion/XML/Facturas/";
    $XMLName = $uid.".xml";

    if($archivo = fopen($rutaPDF, "w+")){
        fwrite($archivo, $pdfFactura);
        fclose($archivo);
    }


    $timbrado = base64_decode($xmlBase64->item(0)->nodeValue);
    $cfdi_final = new DOMDocument();
    $cfdi_final->loadXML($timbrado);
    $RrutaXML = "Facturacion/XML/Facturas/".$uid.".xml";
    $cfdi_final->save($RrutaXML);

    $Total = $_SESSION['TotalCFDI'];
    $Subtotal = $_SESSION['SubTotalCFDI'];

    $Receptor = $_SESSION['NombreE'];
    $rfcr = $_SESSION['rfce'];

    $MPago = $_SESSION["metodopago"];
    $Estado = "";
    
    if ($MPago == 'PUE') {
        $Adeudo = 0;
        $Estado = "PAGADO";
    }elseif ($MPago == 'PPD') {
        $Adeudo = $Total;
        $Estado = "PENDIENTE";
    }
    

    $sql = "INSERT INTO facturas VALUES ($id, '$RutaPdfRaiz', '$PdfName', '$RutaXMLRaiz', '$XMLName','$Receptor', '$rfcr', '$Serie', '$id', $Subtotal, $Total, 0.00,
    $Adeudo, 'FACTURA', '$fecha', '$Estado', '$uid', '0', '$MPago', 0, 'VIGENTE', 'VIGENTE', 'VIGENTE', $Identificador, $Almacen)";
    $cbd->query($sql);

    unset($_SESSION["NombreE"]);
    unset($_SESSION["Contenedor"]);
    unset($_SESSION["rfce"]);
    unset($_SESSION["uso"]);
    unset($_SESSION["formapago"]);
    unset($_SESSION["metodopago"]);
    unset($_SESSION["tipocomprobante"]);
    unset($_SESSION["TotalCFDI"]);
    unset($_SESSION["SubTotalCFDI"]);
    unset($_SESSION["ImpuestoCFDI"]);
    unset($_SESSION["Serie"]);
    unset($_SESSION["ordencompra"]);
    unset($_SESSION["mail"]);
    unset($_SESSION["condiciones"]);
    header("Location: ".$dominio."Facturacion/PDF/Facturas/".$uid.".pdf");

    
}else{
    $msj = $xml_doc->getElementsByTagName('mensaje'); 
    echo "Error";
    echo "<br>";
    echo utf8_decode($msj->item(0)->nodeValue);
}


function sellarXML($cfdi, $archivo_cer, $archivo_pem) {
    $private = openssl_pkey_get_private(file_get_contents($archivo_pem));
    $certificado = str_replace(array('\n', '\r'), '', base64_encode(file_get_contents($archivo_cer)));

    $xdoc = new DomDocument();
    $xdoc->loadXML($cfdi) or die("XML invalido");

    $c = $xdoc->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/3', 'Comprobante')->item(0); 
    $c->setAttribute('Certificado', $certificado);


    $XSL = new DOMDocument();
    $XSL->load('XSLT/cadenaoriginal_3_3.xslt');
    
    $proc = new XSLTProcessor;
    $proc->importStyleSheet($XSL);

    $cadena_original = $proc->transformToXML($xdoc);

    openssl_sign($cadena_original, $sig, $private, OPENSSL_ALGO_SHA256);
    $sello = base64_encode($sig);
    $c->setAttribute('Sello', $sello);
    
    return $xdoc->saveXML();
}


function Crear($Folio){
    include("conexion.php");

    $profile         = $_SESSION['user'];
    $Identificador   = $_SESSION["Id_User"];
    $dominio         = $_SESSION["dominio"];
    $Almacen         = $_SESSION['Almacen'];
    $NombreE         = $_SESSION['NombreE'];
    $Contenedor      = $_SESSION['Contenedor'];
    $rfce            = $_SESSION['rfce'];
    $uso             = $_SESSION['uso'];
    $formapago       = $_SESSION['formapago'];
    $metodopago      = $_SESSION['metodopago'];
    $tipocomprobante = $_SESSION['tipocomprobante'];
    $TotalCFDI       = $_SESSION['TotalCFDI'];
    $SubTotalCFDI    = $_SESSION['SubTotalCFDI'];
    $ImpuestoCFDI    = $_SESSION['ImpuestoCFDI'];
    $Serie           = $_SESSION["Serie"];
    $condiciones     = $_SESSION['condiciones'];
    $Impuestos[0]    = 0;
    $cont            = 0; 

    $fecha_actual = str_replace(' ', 'T', date('Y-m-d H:i:s', (strtotime ("-1 Hours"))));


    $queryDatos = "SELECT * FROM datosfactura WHERE Id_User = $Identificador AND Almacen = $Almacen";
    $resDatos  = $cbd->query($queryDatos);
    $datosFact    = mysqli_fetch_array($resDatos);

    $queryImpuestos = "SELECT Porcentaje FROM impuestos WHERE Id_User = $Identificador";
    $resImpuestos  = $cbd->query($queryImpuestos);
    

    while ($datosImpuestos = mysqli_fetch_array($resImpuestos)) {
        $Impuestos[$cont] = $datosImpuestos['Porcentaje'];
        $cont = $cont + 1;
    }

    $xml = new DomDocument('1.0', 'UTF-8');
 
    $cfdi_Comprobante = $xml->createElement('cfdi:Comprobante');
    $cfdi_Comprobante = $xml->appendChild($cfdi_Comprobante);
    $cfdi_Comprobante->setAttribute('Certificado', '');
    $cfdi_Comprobante->setAttribute('Fecha', $fecha_actual);
    $cfdi_Comprobante->setAttribute('Folio', $Folio);
    $cfdi_Comprobante->setAttribute('FormaPago', $formapago);
    $cfdi_Comprobante->setAttribute('LugarExpedicion', $datosFact['ExpeditionPlace']);
    $cfdi_Comprobante->setAttribute('MetodoPago', $metodopago);
    $cfdi_Comprobante->setAttribute('Moneda', 'MXN');
    $cfdi_Comprobante->setAttribute('NoCertificado', $datosFact['NumCertificado']); 
    $cfdi_Comprobante->setAttribute('CondicionesDePago', $condiciones);
    $cfdi_Comprobante->setAttribute('Sello', '');
    $cfdi_Comprobante->setAttribute('Serie', $Serie);
    $cfdi_Comprobante->setAttribute('SubTotal', $SubTotalCFDI);
    $cfdi_Comprobante->setAttribute('TipoDeComprobante', $tipocomprobante);
    $cfdi_Comprobante->setAttribute('Total', $TotalCFDI);
    $cfdi_Comprobante->setAttribute('Version', '3.3');
    $cfdi_Comprobante->setAttribute('xmlns:cfdi', 'http://www.sat.gob.mx/cfd/3');
    $cfdi_Comprobante->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    $cfdi_Comprobante->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd');


     $cfdi_Emisor = $xml->createElement('cfdi:Emisor');
     $cfdi_Emisor = $cfdi_Comprobante->appendChild($cfdi_Emisor);
     $cfdi_Emisor->setAttribute('Nombre', utf8_encode ($datosFact['Emisor']));
     $cfdi_Emisor->setAttribute('RegimenFiscal', $datosFact['RegimenFiscal']);
     $cfdi_Emisor->setAttribute('Rfc', $datosFact['RFC']);

     $cfdi_Receptor = $xml->createElement('cfdi:Receptor');
     $cfdi_Receptor = $cfdi_Comprobante->appendChild($cfdi_Receptor);
     $cfdi_Receptor->setAttribute('Nombre', $NombreE);
     $cfdi_Receptor->setAttribute('Rfc', $rfce);
     $cfdi_Receptor->setAttribute('UsoCFDI', $uso);

     $cfdi_Conceptos = $xml->createElement('cfdi:Conceptos');
     $cfdi_Conceptos = $cfdi_Comprobante->appendChild($cfdi_Conceptos);

     for ($i=0; $i < count($Contenedor); $i++) { 
         

        $int = intval($Contenedor[$i][0][5]);
        $int = sprintf("%02d", $int);
        $final = '0.'.$int.'0000';

        $queryProd = "SELECT articulo, unidad, claveProdServ, claveUnidad FROM productos WHERE Id_User = $Identificador AND Almacen = $Almacen  AND descripcion = ". "'".$Contenedor[$i][0][0]."'";
        $resProd  = $cbd->query($queryProd);
        $datosProd = mysqli_fetch_array($resProd);
        
        $cfdi_Concepto = $xml->createElement('cfdi:Concepto');
        $cfdi_Concepto = $cfdi_Conceptos->appendChild($cfdi_Concepto);
        $cfdi_Concepto->setAttribute('Cantidad', $Contenedor[$i][0][1]);
        $cfdi_Concepto->setAttribute('ClaveProdServ', $datosProd['claveProdServ']);
        $cfdi_Concepto->setAttribute('ClaveUnidad', $datosProd['claveUnidad']);
        $cfdi_Concepto->setAttribute('Descripcion', $Contenedor[$i][0][0]);
        $cfdi_Concepto->setAttribute('Importe', $Contenedor[$i][0][3]);
        $cfdi_Concepto->setAttribute('NoIdentificacion', $datosProd['articulo']);
        $cfdi_Concepto->setAttribute('Unidad', $datosProd['unidad']);
        $cfdi_Concepto->setAttribute('ValorUnitario',$Contenedor[$i][0][2]);
    
        $cfdi_Impuestos = $xml->createElement('cfdi:Impuestos');
        $cfdi_Impuestos = $cfdi_Concepto->appendChild($cfdi_Impuestos);
    
        $cfdi_Traslados = $xml->createElement('cfdi:Traslados');
        $cfdi_Traslados = $cfdi_Impuestos->appendChild($cfdi_Traslados);
    
        $cfdi_Traslado = $xml->createElement('cfdi:Traslado');
        $cfdi_Traslado = $cfdi_Traslados->appendChild($cfdi_Traslado);
        $cfdi_Traslado->setAttribute('Base', $Contenedor[$i][0][3]);
        $cfdi_Traslado->setAttribute('Importe', $Contenedor[$i][0][4]);
        $cfdi_Traslado->setAttribute('Impuesto', '002');
        $cfdi_Traslado->setAttribute('TasaOCuota', $final);
        $cfdi_Traslado->setAttribute('TipoFactor', 'Tasa');

     }


     $cfdi_Impuestos = $xml->createElement('cfdi:Impuestos');
     $cfdi_Impuestos = $cfdi_Comprobante->appendChild($cfdi_Impuestos);
     $cfdi_Impuestos->setAttribute('TotalImpuestosTrasladados', $ImpuestoCFDI);


     $cfdi_Traslados = $xml->createElement('cfdi:Traslados');
     $cfdi_Traslados = $cfdi_Impuestos->appendChild($cfdi_Traslados);
     $cont = 0;
     $TotalImpuesto = 0;
     while ($cont < count($Impuestos)) {
        for ($i=0; $i < count($Contenedor); $i++) { 
            if ($Contenedor[$i][0][5] == $Impuestos[$cont]) {
                $TotalImpuesto = $TotalImpuesto + $Contenedor[$i][0][4];
                $TotalImpuesto = round($TotalImpuesto, 2);
            }
        }
        

        if ($TotalImpuesto == 0 and $Impuestos[$cont] != 0.00) {

        }else{
            $int = intval($Impuestos[$cont]);
            $int = sprintf("%02d", $int);
            $final = '0.'.$int.'0000';
            $cfdi_Traslado = $xml->createElement('cfdi:Traslado');
            $cfdi_Traslado = $cfdi_Traslados->appendChild($cfdi_Traslado);
            $cfdi_Traslado->setAttribute('Importe', sprintf('%0.2f', $TotalImpuesto));
            $cfdi_Traslado->setAttribute('Impuesto', '002');
            $cfdi_Traslado->setAttribute('TasaOCuota',$final);
            $cfdi_Traslado->setAttribute('TipoFactor', 'Tasa');         
        }

        $TotalImpuesto = 0;
        $cont = $cont + 1;


     }


     return $xml->saveXML();

}

?>