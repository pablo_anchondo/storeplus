<?php
    require('fpdf/fpdf.php');
    session_start();
    include("conexion.php");
    date_default_timezone_set('America/Mexico_City');
    $uuid = $_SESSION['uuid'];
    $profile       = $_SESSION['user'];
    $Identificador = $_SESSION["Id_User"];
    $dominio       = $_SESSION["dominio"];
    $Almacen = $_SESSION['Almacen'];

    $queryDatos = "SELECT * FROM datosfactura WHERE Id_User = $Identificador AND Almacen = $Almacen";
    $resDatos  = $cbd->query($queryDatos);
    $datosFact    = mysqli_fetch_array($resDatos);

    $Ruta = $datosFact['src'];
    $cer = $Ruta.$datosFact['cert'];
    $key = $Ruta.$datosFact['keyy'];
    $cerpem = file_get_contents($Ruta."cer.pem");
    $keypem = file_get_contents($Ruta."key.pem");
    $keypass = $datosFact['passkey'];

    $rfcEmisor = $datosFact['RFC'];

    $contrato = $datosFact['Contrato'];
    $userpade = $datosFact['Usuario'];
    $passpade = $datosFact['Pass'];
    

    $params = array(
        "contrato" => $contrato,
        "usuario" => $userpade,
        "passwd" => $passpade,
        "rfcEmisor" => $rfcEmisor,
        "arregloUUID" => $uuid,
        "cert" => $cerpem,
        "key" => $keypem,
        "keyPass" => $keypass,
        "opciones" => "GENERAR_PDF",
      );

      $client = new SoapClient("https://timbrado.pade.mx/servicio/Timbrado3.3?wsdl");
      
      $response = $client->__soapCall("cancelar", array($params));
      
      $xmlres = $response->return;
    
      
      $xml_doc = new DOMDocument();
      $xml_doc->loadXML($xmlres);

      $UUIDt = $xml_doc->getElementsByTagName('uuid'); 

      if (isset($UUIDt->item(0)->nodeValue)) {

        $acuse = $xml_doc->getElementsByTagName('acuseCancelBase64'); 
        $acusef =  $acuse->item(0)->nodeValue;
        $acusedecode = base64_decode($acusef);
        
        $xml_acuse = new DOMDocument();
        $xml_acuse->loadXML($acusedecode);

        $fec = $xml_acuse->getElementsByTagName("Acuse");
        foreach( $fec as $fec ) {
            $fechaCancel = $fec->getAttribute('Fecha'); 
        } 

        $Signature = $xml_acuse->getElementsByTagName('SignatureValue'); 
        $SignatureValue =  $Signature->item(0)->nodeValue;

        $Estado = $xml_acuse->getElementsByTagName('EstatusUUID'); 
        $Estatus =  $Estado->item(0)->nodeValue;

        $xml_acuse->save("Facturacion/XML/Cancelaciones/".$uuid.".xml");


        $pdf = new FPDF();
        // Agrega nueva página
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Image("img/shcp.png", 12, 10, 45, 48);
        $pdf->SetY(25);
        $pdf->SetX(60);
        $pdf->SetFont('Arial', 'B', 19);
        $pdf->Cell(135, 10, utf8_decode("Servicio de Administración Tributaria"), 0, 0, 'C');
        $pdf->ln(15);
        $pdf->SetX(60);
        $pdf->Cell(135, 10, utf8_decode("Acuse de Cancelación de CFDI"), 0, 0, 'C');
        $pdf->ln(28);
        $pdf->SetX(12);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(50, 10, utf8_decode("RFC Emisor:"), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(70, 10, utf8_decode($rfcEmisor), 0, 0, 'L');
        $pdf->ln(8);
        $pdf->SetX(12);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(50, 10, utf8_decode("Fecha Cancelación:"), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(70, 10, utf8_decode($fechaCancel), 0, 0, 'L');
        $pdf->ln(20);
        $pdf->SetX(50);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(90, 6, utf8_decode("UUID"), 1, 0, 'C');
        $pdf->Cell(20, 6, utf8_decode("Estatus"), 1, 0, 'C');
        $pdf->ln(6);
        $pdf->SetX(50);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(90, 6, utf8_decode($uuid), 1, 0, 'C');
        $pdf->Cell(20, 6, utf8_decode($Estatus), 1, 0, 'C');
        $pdf->ln(25);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(50, 10, utf8_decode("Sello Digital del SAT:"), 0, 0, 'L');

        $pdf->SetFont('Arial', '', 10);
        $pdf->MultiCell(130, 8, utf8_decode($SignatureValue), 0, 'L', 0);

        $pdf->ln(25);
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetTextColor(97,97,97);
        $pdf->Cell(100, 10, utf8_decode("*"), 0, 0, 'L');
        $pdf->ln(5);
        $pdf->Cell(100, 10, utf8_decode("Estatus 201 = Documento Cancelado"), 0, 0, 'L');
        $pdf->ln(5);
        $pdf->Cell(100, 10, utf8_decode("Estatus 202 = Cancelado Previamente"), 0, 0, 'L');
        $pdf->ln(78);

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(190, 3, utf8_decode('"Este comprobante es una Acuse de Cancelación Oficial del SAT"'), 0, 0, 'C');
        $pdf->ln(8);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(190, 3, utf8_decode("Acuse de Cancelación generado con StorPlus / https://storeplus.com.mx/"), 0, 0, 'C');
        $pdf->ln(8);
        $pdf->Cell(190, 3, utf8_decode("Una División de CompuPlus / http://compu-plus.mx/"), 0, 0, 'C');
        $pdf->Output("F", "Facturacion/PDF/Cancelaciones/".$uuid.".pdf");
        $pdf->Output();

        $AcusePDF = $dominio."Facturacion/PDF/Cancelaciones/".$uuid.".pdf";
        $AcuseXML = $dominio."Facturacion/XML/Cancelaciones/".$uuid.".xml";

        $sql = "UPDATE facturas SET Cancelado = 'CANCELADO', AcusePDF = '$AcusePDF', AcuseXML = '$AcuseXML' WHERE Id_User = $Identificador AND Almacen = $Almacen AND UUID = '$uuid'";
        $cbd->query($sql);

      }else{
        $msj = $xml_doc->getElementsByTagName('mensaje'); 
        echo utf8_decode($msj->item(0)->nodeValue);
      }


    
?>