<?php
    // Incluimos la librería de PDF
	require('fpdf/fpdf.php');
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión
	if (isset($_SESSION['user'])) {
	} //isset($_SESSION['user'])
	else {
		echo '<script> window.location="index.php"; </script>';
	}

	
	// Inicializamos variables de sesión
	$Identificador = $_SESSION["Id_User"];
	$Cliente       = $_SESSION['cliente'];
	$Nombre        = "";
	if ($Cliente == "Todo") {
		$Nombre = "Todos";
	} //$Cliente == "Todo"
	else {
		$queryCl = 'select Nombre from clients where Id_User = ' . $Identificador . ' AND ClaveCliente = ' . $Cliente;
		$ResCl   = $cbd->query($queryCl);
		$filaCl  = mysqli_fetch_array($ResCl);
		$Nombre  = $filaCl['Nombre'];
	}
	class PDF extends FPDF
	{
		// Cabecera de página
		function Header()
		{
			include("conexion.php");
			$Identificador = $_SESSION["Id_User"];
			$Cliente       = $_SESSION['cliente'];
			$Nombre        = "";
			if ($Cliente == "Todo") {
				$Nombre = "Todos";
			} //$Cliente == "Todo"
			else {
				$queryCl = 'select Nombre from clients where Id_User = ' . $Identificador . ' AND ClaveCliente = ' . $Cliente;
				$ResCl   = $cbd->query($queryCl);
				$filaCl  = mysqli_fetch_array($ResCl);
				$Nombre  = $filaCl['Nombre'];
			}
			$Almacen = $_SESSION["Almacen"];
			$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
			$ResEmp   = $cbd->query($queryEmp);
			$filaEmp  = mysqli_fetch_array($ResEmp);
			$this->SetFont('Arial', 'B', 13);
			if ($filaEmp['img'] == "ProImg/sinImg.jpg") {
				$this->SetY(15);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
				$this->SetX(55);
			} //$filaEmp['img'] == "ProImg/sinImg.jpg"
			else {
				$this->Image($filaEmp['img'], 10, 7, 60, 28);
				$this->SetY(15);
				$this->SetX(75);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
			}
			$this->SetFont('Arial', 'B', 13);
			$this->Cell(51, 10, utf8_decode($filaEmp['Nombre']), 0, 1, 'C');
			$this->Ln(9);
			$this->Line(10, 35, 281, 35);
			$this->SetFont('Arial', 'B', 10);
			$this->Cell(100, 10, utf8_decode('Reporte por Cliente'), 0, 0, 'L');
			$this->Ln(5);
			$this->Cell(100, 10, utf8_decode('Cliente ' . $Nombre), 0, 0, 'L');
			$this->Ln(15);
			$this->SetFont('Arial', '', 9);
			$this->Cell(50, 6, utf8_decode('Nombre'), 1, 0, 'C');
			$this->Cell(30, 6, utf8_decode('Fecha'), 1, 0, 'C');
			$this->Cell(15, 6, utf8_decode('Venta'), 1, 0, 'C');
			$this->Cell(50, 6, utf8_decode('Vendedor'), 1, 0, 'C');
			$this->Cell(22, 6, 'Importe', 1, 0, 'C');
			$this->Cell(22, 6, 'Impuesto.', 1, 0, 'C');
			$this->Cell(22, 6, 'Total', 1, 0, 'C');
			$this->Cell(60, 6, 'Comentarios', 1, 1, 'C');
		}
	}
	// Se crea el PDF
	$pdf = new PDF('L');
	// Agrega nueva página
	$pdf->AddPage();
	$pdf->SetFont('Arial', '', 9);
	if ($Cliente == 'Todo') {
		$queryProds = 'SELECT Comentarios, Fecha, Cliente, Venta as Compras, Vendedor, Importe as Importe, Impuesto as Impuesto,  Total as Total FROM ventas WHERE Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"];
	} //$Cliente == 'Todo'
	else {
		$queryProds = 'SELECT Comentarios, Fecha, Cliente, Venta as Compras, Vendedor, Importe as Importe, Impuesto as Impuesto,  Total as Total FROM ventas WHERE Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND Cliente = ' . "'" . $Nombre . "'";
	}
	$ResProds = $cbd->query($queryProds);
	$pdf->SetFont('Arial', '', 8);
	while ($filaProds = mysqli_fetch_array($ResProds)) {
		// Se llenan las partidas
		$pdf->Cell(50, 6, utf8_decode($filaProds['Cliente']), 0, 0, 'C');
		$pdf->Cell(30, 6, utf8_decode($filaProds['Fecha']), 0, 0, 'C');
		$pdf->Cell(15, 6, utf8_decode($filaProds['Compras']), 0, 0, 'C');
		$pdf->Cell(50, 6, utf8_decode($filaProds['Vendedor']), 0, 0, 'C');
		$pdf->Cell(22, 6, "$" . $filaProds['Importe'], 0, 0, 'C');
		$pdf->Cell(22, 6, "$" . $filaProds['Impuesto'], 0, 0, 'C');
		$pdf->Cell(22, 6, "$" . $filaProds['Total'], 0, 0, 'C');
		$pdf->MultiCell(60, 6,  utf8_decode($filaProds['Comentarios']), 0, 'C');
	} //$filaProds = mysqli_fetch_array($ResProds)
	$pdf->Ln(8);
	if ($Cliente == 'Todo') {
		$queryTot = 'SELECT  Sum(Importe) as Importe, Sum(Impuesto) as Impuesto,  Sum(Total) as Total FROM ventas WHERE Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"];
	} //$Cliente == 'Todo'
	else {
		$queryTot = 'SELECT  Sum(Importe) as Importe, Sum(Impuesto) as Impuesto,  Sum(Total) as Total FROM ventas WHERE Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND Cliente = ' . "'" . $Nombre . "'";
	}
	$ResTot  = $cbd->query($queryTot);
	$filaTot = mysqli_fetch_array($ResTot);
	$pdf->Cell(50, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(30, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(15, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(50, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(22, 6, "$" . $filaTot['Importe'], 1, 0, 'C');
	$pdf->Cell(22, 6, "$" . $filaTot['Impuesto'], 1, 0, 'C');
	$pdf->Cell(22, 6, "$" . $filaTot['Total'], 1, 1, 'C');
	// Se muestra el PDF en pantalla
	$pdf->Output();
?>