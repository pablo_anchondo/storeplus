<?php  
   session_start();
   include("conexion.php");
   if (isset($_SESSION['user'])) { // Determina si se ha iniciado sesión 
   	echo "";
   }else{
   	echo '<script> window.location="index.php"; </script>';
   }   
   Crear();


   function Crear(){
    include("conexion.php");
    
    $profile = $_SESSION['user'];
    $Identificador = $_SESSION["Id_User"];
    $dominio = $_SESSION["dominio"];
    $Almacen = $_SESSION['Almacen'];  
    
    $queryVenta = "SELECT * FROM ventas  WHERE Id_User = $Identificador AND Vendedor = 'VENDEDOR GENERICO' AND Almacen = " . $_SESSION["Almacen"] . " AND  Venta = 16 ";
    $resVenta  = $cbd->query($queryVenta);
    $datosVenta    = mysqli_fetch_array($resVenta);

    $queryImp= "SELECT  SUM(TotImp) as Total , Impuesto  FROM partventa  WHERE Id_User = $Identificador AND Vendedor = 'VENDEDOR GENERICO' AND Almacen = " . $_SESSION["Almacen"] . " AND  Venta = 16 GROUP BY Impuesto ";
    $resImp  = $cbd->query($queryImp);

    $TotalImp = "SELECT  SUM(TotImp) as Total FROM partventa  WHERE Id_User = $Identificador AND Vendedor = 'VENDEDOR GENERICO' AND Almacen = " . $_SESSION["Almacen"] . " AND  Venta = 16";
    $resTot  = $cbd->query($TotalImp);
    $TotalImpuestos    = mysqli_fetch_array($resTot);
 
    $queryPart = "SELECT * FROM partventa WHERE Id_User = $Identificador AND Vendedor = 'VENDEDOR GENERICO' AND Almacen = " . $_SESSION["Almacen"] . " AND  Venta = 16 ";
    $resPart  = $cbd->query($queryPart);
 
 
    $xml = new DomDocument('1.0', 'UTF-8');
 
     $cfdi_Comprobante = $xml->createElement('cfdi:Comprobante');
     $cfdi_Comprobante = $xml->appendChild($cfdi_Comprobante);
     $cfdi_Comprobante->setAttribute('Certificado', '');
     $cfdi_Comprobante->setAttribute('Fecha', '');
     $cfdi_Comprobante->setAttribute('Folio', '');
     $cfdi_Comprobante->setAttribute('FormaPago', '');
     $cfdi_Comprobante->setAttribute('LugarExpedicion', '');
     $cfdi_Comprobante->setAttribute('MetodoPago', '');
     $cfdi_Comprobante->setAttribute('Moneda', 'MXN');
     $cfdi_Comprobante->setAttribute('NoCertificado', '');
     $cfdi_Comprobante->setAttribute('Sello', '');
     $cfdi_Comprobante->setAttribute('Serie', '');
     $cfdi_Comprobante->setAttribute('SubTotal', $datosVenta['Importe']);
     $cfdi_Comprobante->setAttribute('TipoDeComprobante', '');
     $cfdi_Comprobante->setAttribute('Total', $datosVenta['Total']);
     $cfdi_Comprobante->setAttribute('Version', '3.3');
     $cfdi_Comprobante->setAttribute('xmlns:cfdi', 'http://www.sat.gob.mx/cfd/3');
     $cfdi_Comprobante->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
     $cfdi_Comprobante->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd');

     $cfdi_CfdiRelacionados = $xml->createElement('cfdi:CfdiRelacionados');
     $cfdi_CfdiRelacionados = $cfdi_Comprobante->appendChild($cfdi_CfdiRelacionados);
     $cfdi_CfdiRelacionados->setAttribute('TipoRelacion', '01');

     $cfdi_Emisor = $xml->createElement('cfdi:Emisor');
     $cfdi_Emisor = $cfdi_Comprobante->appendChild($cfdi_Emisor);
     $cfdi_Emisor->setAttribute('Nombre', '');
     $cfdi_Emisor->setAttribute('RegimenFiscal', '');
     $cfdi_Emisor->setAttribute('Rfc', '');

     $cfdi_Receptor = $xml->createElement('cfdi:Receptor');
     $cfdi_Receptor = $cfdi_Comprobante->appendChild($cfdi_Receptor);
     $cfdi_Receptor->setAttribute('Nombre', '');
     $cfdi_Receptor->setAttribute('Rfc', '');
     $cfdi_Receptor->setAttribute('UsoCFDI', '');

     $cfdi_Conceptos = $xml->createElement('cfdi:Conceptos');
     $cfdi_Conceptos = $cfdi_Comprobante->appendChild($cfdi_Conceptos);


     while ($datosPart = mysqli_fetch_array($resPart)) {

        $int = intval($datosPart['Impuesto']);
        $int = sprintf("%02d", $int);
        $final = '0.'.$int.'0000';
        
        $queryProd = "SELECT unidad, claveProdServ, claveUnidad FROM productos WHERE Id_User = $Identificador AND Almacen = " . $_SESSION['Almacen'] . " AND  Nombre = " .$datosPart['Articulo']. " AND descripcion = ". "'".$datosPart['Descripcion']."'";
        $resProd  = $cbd->query($queryProd);
        $datosProd    = mysqli_fetch_array($resProd);

        $cfdi_Concepto = $xml->createElement('cfdi:Concepto');
        $cfdi_Concepto = $cfdi_Conceptos->appendChild($cfdi_Concepto);
        $cfdi_Concepto->setAttribute('Cantidad', $datosPart['Cantidad']);
        $cfdi_Concepto->setAttribute('ClaveProdServ', $datosProd['claveProdServ']);
        $cfdi_Concepto->setAttribute('ClaveUnidad', $datosProd['claveUnidad']);
        $cfdi_Concepto->setAttribute('Descripcion', $datosPart['Descripcion']);
        $cfdi_Concepto->setAttribute('Importe', $datosPart['Importe']);
        $cfdi_Concepto->setAttribute('NoIdentificacion', $datosPart['Articulo']);
        $cfdi_Concepto->setAttribute('Unidad', $datosProd['unidad']);
        $cfdi_Concepto->setAttribute('ValorUnitario', $datosPart['Precio']);
    
        $cfdi_Impuestos = $xml->createElement('cfdi:Impuestos');
        $cfdi_Impuestos = $cfdi_Concepto->appendChild($cfdi_Impuestos);
    
        $cfdi_Traslados = $xml->createElement('cfdi:Traslados');
        $cfdi_Traslados = $cfdi_Impuestos->appendChild($cfdi_Traslados);
    
        $cfdi_Traslado = $xml->createElement('cfdi:Traslado');
        $cfdi_Traslado = $cfdi_Traslados->appendChild($cfdi_Traslado);
        $cfdi_Traslado->setAttribute('Base', $datosPart['Importe']);
        $cfdi_Traslado->setAttribute('Importe', $datosPart['TotImp']);
        $cfdi_Traslado->setAttribute('Impuesto', '002');
        $cfdi_Traslado->setAttribute('TasaOCuota', $final);
        $cfdi_Traslado->setAttribute('TipoFactor', 'Tasa');

     }


     $cfdi_Impuestos = $xml->createElement('cfdi:Impuestos');
     $cfdi_Impuestos = $cfdi_Comprobante->appendChild($cfdi_Impuestos);
     $cfdi_Impuestos->setAttribute('TotalImpuestosTrasladados', $TotalImpuestos['Total']);


     $cfdi_Traslados = $xml->createElement('cfdi:Traslados');
     $cfdi_Traslados = $cfdi_Impuestos->appendChild($cfdi_Traslados);

     while ($datosImp = mysqli_fetch_array($resImp)) {
     
        $int = intval($datosImp['Impuesto']);
        $int = sprintf("%02d", $int);
        $final = '0.'.$int.'0000';
        $cfdi_Traslado = $xml->createElement('cfdi:Traslado');
        $cfdi_Traslado = $cfdi_Traslados->appendChild($cfdi_Traslado);
        $cfdi_Traslado->setAttribute('Impuesto', '002');
        $cfdi_Traslado->setAttribute('TipoFactor', 'Tasa');
        $cfdi_Traslado->setAttribute('TasaOCuota',$final );
        $cfdi_Traslado->setAttribute('Importe', $datosImp['Total']);

     }

     $xml->formatOutput = true;
     $el_xml = $xml->saveXML();
     $xml->save('XML/Test.xml');
 









   }







?>

