<?php
	// Incluimos la librería de PDF
	require('fpdf/fpdf.php');
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión 
	if (isset($_SESSION['user'])) {
		echo "";
	} //isset($_SESSION['user'])
	else {
		echo '<script> window.location="index.php"; </script>';
	}
	// Inicializamos variables de sesión
	$Identificador = $_SESSION["Id_User"];
	$Producto      = $_SESSION['ProductoPV'];
	// Creamos la clase PDF
	class PDF extends FPDF
	{
		// Cabecera de página
		function Header()
		{
			include("conexion.php");
			$Identificador = $_SESSION["Id_User"];
			$Almacen = $_SESSION["Almacen"];
			$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
			$ResEmp        = $cbd->query($queryEmp);
			$filaEmp       = mysqli_fetch_array($ResEmp);
			$this->SetFont('Arial', 'B', 13);
			if ($filaEmp['img'] == "ProImg/sinImg.jpg") {
				$this->SetY(15);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
				$this->SetX(55);
			} //$filaEmp['img'] == "ProImg/sinImg.jpg"
			else {
				$this->Image($filaEmp['img'], 10, 7, 40, 28);
				$this->SetY(15);
				$this->SetX(55);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
			}
			$this->Cell(51, 10, utf8_decode($filaEmp['Nombre']), 0, 1, 'C');
			$this->Ln(9);
			$this->Line(10, 35, 199, 35);
			$this->SetFont('Arial', 'B', 10);
			$this->Cell(100, 10, utf8_decode('Valuación de inventario por costo ultimo'), 0, 0, 'L');
			$this->Ln(15);
			$this->SetFont('Arial', '', 9);
			$this->Cell(30, 6, utf8_decode('Articulo'), 1, 0, 'C');
			$this->Cell(90, 6, utf8_decode('Descripción'), 1, 0, 'C');
			$this->Cell(25, 6, 'Existencia', 1, 0, 'C');
			$this->Cell(18, 6, 'Costo.', 1, 0, 'C');
			$this->Cell(25, 6, 'Valor', 1, 1, 'C');
		}
	}
	// Se crea el PDF
	$pdf = new PDF();
	// Agrega nueva página
	$pdf->AddPage();
	// Se determina si son todos los productos o solo uno
	if ($Producto == 'Todo') {
		$queryProds = 'select * from productos where Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"];
	} //$Producto == 'Todo'
	else {
		$queryProds = 'select * from productos where Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND articulo = ' . $Producto;
	}
	$ResProds = $cbd->query($queryProds);
	// Se llenan las partidas
	while ($filaProds = mysqli_fetch_array($ResProds)) {
		$pdf->Cell(30, 6, utf8_decode($filaProds['Nombre']), 0, 0, 'L');
		$pdf->Cell(90, 6, utf8_decode($filaProds['descripcion']), 0, 0, 'L');
		$pdf->Cell(25, 6, $filaProds['existencia'], 0, 0, 'C');
		$pdf->Cell(18, 6, "$" . $filaProds['costo'], 0, 0, 'C');
		$Valor = $filaProds['costo'] * $filaProds['existencia'];
		$pdf->Cell(25, 6, "$" . round($Valor, 2), 0, 1, 'C');
	} //$filaProds = mysqli_fetch_array($ResProds)
	// Se muestra el PDF en pantalla
	$pdf->Output();
?>