<?php
session_start();

include("conexion.php");
// Determina si se ha iniciado sesión 
if (isset($_SESSION['user'])) {
    echo "";
} else {
    echo '<script> window.location="index.php"; </script>';
}

if (isset($_SESSION['FacturaActiva'])) {
    echo "";
} else {
    echo '<script> window.location="index.php"; </script>';
}


$profile       = $_SESSION['user'];
$Identificador = $_SESSION["Id_User"];
$dominio       = $_SESSION["dominio"];
$Almacen = $_SESSION["Almacen"];

?>

<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/select2-bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/select2.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/Facturacion.js" ></script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
      <?php
         $Vendedores    = 'select Vendedor from vendedores where Id_User = ' . $Identificador . ' order by Vendedor ASC ';
         $resultVend    = $cbd->query($Vendedores);
         
         $UsoCFDI    = 'select * from sat_usocfdi';
         $resultUso    = $cbd->query($UsoCFDI);
         
         $MPago    = 'select * from sat_mpago';
         $resultMP    = $cbd->query($MPago);
         
         $FPago    = 'select * from sat_formapago';
         $resultFP    = $cbd->query($FPago);
         
         $querySerie    = "SELECT Serie FROM seriefactura  WHERE Id_User = $Identificador AND Almacen = $Almacen";
         $resultSF     = $cbd->query($querySerie);

         
         $queryClients    = "SELECT RFC, Nombre FROM clients  WHERE Id_User = $Identificador";
         $resultClients     = $cbd->query($queryClients);
         
         
      ?>
      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>          
                  <li class="active"><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
                  
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
         <div class="bs-example" align="center">
            <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="dropdown active col-xs-12 col-lg-12" align="center">
               <a href="#" class="dropdown-toggle" id="drop4" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Desplegar Opciones <span class="caret"></span> </a>
               <ul class="dropdown-menu col-xs-12 col-lg-12" align="center" id="menu1" aria-labelledby="drop4">
                  <li><a href="Mapeo.php">Mapeo Productos</a></li>
                  <li><a href="MapeoUnidad.php">Mapeo Unidades</a></li>
                  <li><a href="datosFactura.php">Datos Factura</a></li>
                  <li><a href="ganeraCFDI.php">Generar CFDI</a></li>
                  <li><a href="consultarCFDI.php">Consultar CFDI</a></li>
                  <li><a href="ComplementosPago.php">Complementos de pago</a></li>
                  
               </ul>
            </li>
         </div>
      <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Configuración</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-address-book"></i> Uso de CFDI:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <select class="form-control" id="usocfdi">
                           <?php while ($filaUso = mysqli_fetch_array($resultUso)){ ?>
                           <option value="<?php echo $filaUso['uso'];?>"><?php echo $filaUso['descripcion'];?></option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Metodo de pago:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                           <select class="form-control" id="mp">
                              <?php while ($filaMP = mysqli_fetch_array($resultMP)){ ?>
                              <option value="<?php echo $filaMP['Metodo'];?>"><?php echo $filaMP['Descripcion'];?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-credit-card"></i> Forma de Pago:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <select class="form-control" id="fp">
                           <?php while ($filaFP = mysqli_fetch_array($resultFP)){ ?>
                           <option value="<?php echo $filaFP['forma'];?>"><?php echo $filaFP['descripcion'];?></option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-user-tie"></i> Nombre Receptor:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                           <select id="nr" class="form-control" onchange="getRFC(this)">
                            <option value="nulo">SELECCIONAR CLIENTE</option>
                            <?php while ($filaclients = mysqli_fetch_array($resultClients)){ ?>
                            <option value="<?php echo $filaclients['RFC'];?>"><?php echo $filaclients['Nombre'];?></option>
                            <?php } ?>
                            </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-barcode"></i> RFC Receptor:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <input type="text" class="form-control textColor" id="rfcr"  onkeypress="return validarTamano(this, 13)" >
                     </div>
                     <div class="form-group">
                        <label class="control-label  col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-credit-card"></i> Serie Factura:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                           <select class="form-control" id="Serie">
                              <?php while ($filaSF = mysqli_fetch_array($resultSF)){ ?>
                              <option value="<?php echo $filaSF['Serie'];?>"><?php echo $filaSF['Serie'];?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong>Orden Compra / Observaciones:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <textarea type="text" class="form-control textColor" id="ordencompra"></textarea>
                     </div>
                     <div class="form-group">
                        <label class="control-label  col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-envelop"></i> E-Mail:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                            <input type="mail" id="mail" class="form-control" >
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Ventas por facturar</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
               <select class="form-control" id="vendedor" onchange="getTableFactura(this)">
                  <option value="empty">SELECCIONAR VENDEDOR</option>
                  <?php while ($filaVend = mysqli_fetch_array($resultVend)){ ?>
                  <option value="<?php echo $filaVend['Vendedor'];?>"><?php echo $filaVend['Vendedor'];?></option>
                  <?php } ?>
               </select>
               <div id="TablaVentasFactura" class="table-responsive">
                  <table class="table table-striped table-bordered">
                     <tr>
                        <td align="center" class="TituloVerde" COLSPAN="3">Ventas</td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/select2.min.js" ></script>
      <script type="text/javascript" src="js/select2_locale_es.js" ></script>
      <script>
          $(document).ready(function() { 
              $("#nr").select2({
                placeholder: "SELECCIONA UN PRODUCTO",
                allowClear: true
              });
            });
       </script>
   </body>
</html>