<?php
    // Incluimos la librería de PDF
	require('fpdf/fpdf.php');
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión
	if (isset($_SESSION['user'])) {
	} //isset($_SESSION['user'])
	else {
		echo '<script> window.location="index.php"; </script>';
	}
	// Inicializamos variables de sesión
	$Identificador = $_SESSION["Id_User"];
	$Cliente       = $_SESSION['cliente'];
	$Nombre        = "";
	if ($Cliente == "Todo") {
		$Nombre = "Todos";
	} //$Cliente == "Todo"
	else {
		$queryCl = 'select Nombre from clients where Id_User = ' . $Identificador . ' AND ClaveCliente = ' . $Cliente;
		$ResCl   = $cbd->query($queryCl);
		$filaCl  = mysqli_fetch_array($ResCl);
		$Nombre  = $filaCl['Nombre'];
	}
	class PDF extends FPDF
	{
		// Cabecera de página
		function Header()
		{
			include("conexion.php");
			$Identificador = $_SESSION["Id_User"];
			$Cliente       = $_SESSION['cliente'];
			$Nombre        = "";
			if ($Cliente == "Todo") {
				$Nombre = "Todos";
			} //$Cliente == "Todo"
			else {
				$queryCl = 'select Nombre from clients where Id_User = ' . $Identificador . ' AND ClaveCliente = ' . $Cliente;
				$ResCl   = $cbd->query($queryCl);
				$filaCl  = mysqli_fetch_array($ResCl);
				$Nombre  = $filaCl['Nombre'];
			}
			$Almacen = $_SESSION["Almacen"];
			$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
			$ResEmp   = $cbd->query($queryEmp);
			$filaEmp  = mysqli_fetch_array($ResEmp);
			$this->SetFont('Arial', 'B', 13);
			if ($filaEmp['img'] == "ProImg/sinImg.jpg") {
				$this->SetY(15);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
				$this->SetX(55);
			} //$filaEmp['img'] == "ProImg/sinImg.jpg"
			else {
				$this->Image($filaEmp['img'], 10, 7, 40, 28);
				$this->SetY(15);
				$this->SetX(55);
				$this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
			}
			$this->SetFont('Arial', 'B', 13);
			$this->Cell(51, 10, utf8_decode($filaEmp['Nombre']), 0, 1, 'C');
			$this->Ln(9);
			$this->Line(10, 35, 199, 35);
			$this->SetFont('Arial', 'B', 10);
			$this->Cell(100, 10, utf8_decode('Reporte Detallado por Cliente'), 0, 0, 'L');
			$this->Ln(5);
			$this->Cell(100, 10, utf8_decode('Cliente ' . $Nombre), 0, 0, 'L');
			$this->Ln(15);
			$this->SetFont('Arial', '', 9);

		}
	}
	// Se crea el PDF
	$pdf = new PDF();
	// Agrega nueva página
	$pdf->AddPage();
	$pdf->SetFont('Arial', '', 9);
	if ($Cliente == 'Todo') {
		$queryProds = 'SELECT Comentarios, Fecha, Cliente, Venta as Compras, Importe as Importe, Impuesto as Impuesto,  Total as Total, Vendedor FROM ventas WHERE Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"];
	} //$Cliente == 'Todo'
	else {
		$queryProds = 'SELECT Comentarios, Fecha, Cliente, Venta as Compras, Importe as Importe, Impuesto as Impuesto,  Total as Total, Vendedor FROM ventas WHERE Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND Cliente = ' . "'" . $Nombre . "'";
	}
	$ResProds = $cbd->query($queryProds);
	
	while ($filaProds = mysqli_fetch_array($ResProds)) {
        $pdf->SetFont('Arial', 'B', 8);
        // Se llenan las partidas
        $pdf->Cell(50, 6, utf8_decode('Nombre'), 1, 0, 'C');
        $pdf->Cell(30, 6, utf8_decode('Fecha'), 1, 0, 'C');
        $pdf->Cell(15, 6, utf8_decode('Venta'), 1, 0, 'C');
        $pdf->Cell(39, 6, 'Vendedor', 1, 0, 'C');
        $pdf->Cell(22, 6, 'Total', 1, 0, 'C');
        $pdf->Cell(34, 6, 'Comentarios', 1, 1, 'C');
		$pdf->Cell(50, 6, utf8_decode($filaProds['Cliente']), 1, 0, 'C');
		$pdf->Cell(30, 6, utf8_decode($filaProds['Fecha']), 1, 0, 'C');
		$pdf->Cell(15, 6, utf8_decode($filaProds['Compras']), 1, 0, 'C');
		$pdf->Cell(39, 6, utf8_decode($filaProds['Vendedor']), 1, 0, 'C');
		$pdf->Cell(22, 6, "$" . $filaProds['Total'], 1, 0, 'C');
        $pdf->MultiCell(34, 6,  utf8_decode($filaProds['Comentarios']), 1, 'C');
        
        $queryPart = 'SELECT * FROM partventa WHERE Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND Venta = '. $filaProds['Compras']. ' AND Vendedor = '. "'" . $filaProds['Vendedor'] . "'";
        //$pdf->Cell(6, 6, $queryPart, 0, 0, 'C');
        $ResPart = $cbd->query($queryPart);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(25, 6,"Cantidad", 1, 0, 'C');
        $pdf->Cell(25, 6,"Importe", 1, 0, 'C');
        $pdf->Cell(25, 6,"Impuesto", 1, 0, 'C');
        $pdf->Cell(25, 6,"Total", 1, 0, 'C');
        $pdf->Cell(90, 6,"Descripcion", 1, 0, 'C');
        $pdf->Ln();
        while ($filaPart = mysqli_fetch_array($ResPart)) {
            
            $pdf->Cell(25, 6,$filaPart['Cantidad'], 1, 0, 'C');
            $pdf->Cell(25, 6,$filaPart['Importe'], 1, 0, 'C');
            $pdf->Cell(25, 6,$filaPart['Impuesto'], 1, 0, 'C');
            $pdf->Cell(25, 6,$filaPart['Total'], 1, 0, 'C');
            $pdf->MultiCell(90, 6,  utf8_decode($filaPart['Descripcion']), 1, 'C');
        }
        $pdf->Ln(5);


	} //$filaProds = mysqli_fetch_array($ResProds)
	$pdf->Ln(8);
	if ($Cliente == 'Todo') {
		$queryTot = 'SELECT  Sum(Importe) as Importe, Sum(Impuesto) as Impuesto,  Sum(Total) as Total FROM ventas WHERE Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"];
	} //$Cliente == 'Todo'
	else {
		$queryTot = 'SELECT  Sum(Importe) as Importe, Sum(Impuesto) as Impuesto,  Sum(Total) as Total FROM ventas WHERE Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND Cliente = ' . "'" . $Nombre . "'";
	}
	$ResTot  = $cbd->query($queryTot);
	$filaTot = mysqli_fetch_array($ResTot);
	$pdf->Cell(50, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(30, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(15, 6, utf8_decode(""), 0, 0, 'C');
	$pdf->Cell(22, 6, "$" . $filaTot['Importe'], 1, 0, 'C');
	$pdf->Cell(22, 6, "$" . $filaTot['Impuesto'], 1, 0, 'C');
	$pdf->Cell(22, 6, "$" . $filaTot['Total'], 1, 1, 'C');
	// Se muestra el PDF en pantalla
	$pdf->Output();
?>