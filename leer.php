<?php

$uid = $_POST['uuidLoad'];

$xml = simplexml_load_file("Facturacion/XML/Facturas/".$uid.".xml");
   
$namespaces = $xml->getNameSpaces(true);
$media = $xml->children($namespaces['cfdi']);

$imptras = $media->Impuestos;
$atributos = $imptras->attributes();


$Receptor = $media->Receptor;
$atributosR = $Receptor->attributes();

$params = array(
    "FormaPago" => $xml['FormaPago'],
    "MetodoPago" => $xml['MetodoPago'],
    "TipoDeComprobante" => $xml['TipoDeComprobante'],
    "Serie" => $xml['Serie'],
    "CondicionesDePago" => $xml['CondicionesDePago'],
    "SubTotal" => $xml['SubTotal'],
    "TotalImpuestosTrasladados" => $atributos['TotalImpuestosTrasladados'],
    "Total" => $xml['Total'],
    "Nombre" => $atributosR['Nombre'],
    "Rfc" => $atributosR['Rfc'],
    "UsoCFDI" => $atributosR['UsoCFDI'],
);


$Partidas[0] = 0;

$conceptos = $media->Conceptos;
//var_dump($conceptos);

$concepto = $conceptos->Concepto;
//var_dump($concepto);

$cont = 0;
foreach ($concepto as $nodo) 
{
$atributosp = $nodo->attributes();
$impuestos = $nodo->Impuestos->Traslados->Traslado->attributes();
//var_dump($impuestos);


$Importe = floatval($impuestos['Importe']);
$Base = floatval($impuestos['Base']);
$Total = floatval($Importe + $Base);


$Partidas[$cont] = array(
    "Descripcion" => $atributosp['Descripcion'],
    "Cantidad" => $atributosp['Cantidad'],
    "ValorUnitario" => $atributosp['ValorUnitario'],
    "Importe" => $atributosp['Importe'],
    "ImporteImp" => $impuestos['Importe'],
    "TasaOCuota" => $impuestos['TasaOCuota'],
    "Total" => $Total,
);

$cont = $cont + 1;
}



$Contenedor[0] = $params;
$Contenedor[1] = $Partidas;

echo json_encode($Contenedor);



?>