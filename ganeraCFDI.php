<?php

session_start();
include("conexion.php");

if (isset($_SESSION['user'])) {
    echo "";
} else {
    echo '<script> window.location="index.php"; </script>';
}

if (isset($_SESSION['FacturaActiva'])) {
    echo "";
} else {
    echo '<script> window.location="index.php"; </script>';
}

$profile = $_SESSION['user'];
$Identificador = $_SESSION["Id_User"];
$dominio = $_SESSION["dominio"];
$Almacen = $_SESSION["Almacen"];

?>

<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/select2-bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/select2.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/GenerarCFDI.js" ></script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
      <?php
         $UsoCFDI    = 'select * from sat_usocfdi';
         $resultUso    = $cbd->query($UsoCFDI);
         
         $MPago    = 'select * from sat_mpago';
         $resultMP    = $cbd->query($MPago);
         
         $FPago    = 'select * from sat_formapago';
         $resultFP    = $cbd->query($FPago);
         
         $Productos    = "SELECT articulo, Nombre, descripcion FROM productos WHERE Id_User = $Identificador AND Almacen = $Almacen";
         $resultProds    = $cbd->query($Productos);
         
         $Impuestos    = "SELECT Impuesto, Porcentaje FROM impuestos WHERE Id_User = $Identificador";
         $resultImp    = $cbd->query($Impuestos);
         
         $querySerie    = "SELECT Serie FROM seriefactura  WHERE Id_User = $Identificador AND Almacen = $Almacen";
         $resultSF     = $cbd->query($querySerie);

         $queryClients    = "SELECT RFC, Nombre FROM clients  WHERE Id_User = $Identificador";
         $resultClients     = $cbd->query($queryClients);
         
         ?>

      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
                  <li class="active"><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
         <div class="bs-example" align="center">
            <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="dropdown active col-xs-12 col-lg-12" align="center">
               <a href="#" class="dropdown-toggle" id="drop4" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Desplegar Opciones <span class="caret"></span> </a>
               <ul class="dropdown-menu col-xs-12 col-lg-12" align="center" id="menu1" aria-labelledby="drop4">
                  <li><a href="Mapeo.php">Mapeo Productos</a></li>
                  <li><a href="MapeoUnidad.php">Mapeo Unidades</a></li>
                  <li><a href="datosFactura.php">Datos Factura</a></li>
                  <li><a href="ganeraCFDI.php">Generar CFDI</a></li>
                  <li><a href="consultarCFDI.php">Consultar CFDI</a></li>
                  <li><a href="ComplementosPago.php">Complementos de pago</a></li>
               </ul>
            </li>
         </div>
         
      <div class="container-fluid">
          
         <div class="cabezera" align="center">
            <h3 class="Titulo">Datos de Factura</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-credit-card"></i> Forma de Pago:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <select class="form-control" id="fp">
                           <?php while ($filaFP = mysqli_fetch_array($resultFP)){ ?>
                           <option value="<?php echo $filaFP['forma'];?>"><?php echo $filaFP['descripcion'];?></option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Metodo de pago:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                           <select class="form-control" id="mp" onchange="SetFP(this)">
                              <?php while ($filaMP = mysqli_fetch_array($resultMP)){ ?>
                              <option value="<?php echo $filaMP['Metodo'];?>"><?php echo $filaMP['Descripcion'];?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-clipboard"></i> Tipo de Comprobante:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <select class="form-control" id="tipoComprobante">
                           <option value="I">I - Ingreso</option>
                           <option value="E">E - Egreso</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label  col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-credit-card"></i> Serie Factura:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                           <select class="form-control" id="Serie">
                              <?php while ($filaSF = mysqli_fetch_array($resultSF)){ ?>
                              <option value="<?php echo $filaSF['Serie'];?>"><?php echo $filaSF['Serie'];?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-book"></i> Condiciones de Pago:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <input type="text" id="condiciones" class="form-control">
                     </div>
                     <div class="form-group">
                        <label class="control-label  col-lg-2">
                            <h4 class="textoBlack"><strong>Orden de Compra / Observaciones:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                        <textarea  type="text" id="ordencompra" class="form-control">
                        </textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Receptor</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-user"></i> Nombre:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                     <select id="NombreE" class="form-control " onchange="getRFC(this)">
                            <option value="nulo">SELECCIONAR CLIENTE</option>
                            <?php while ($filaclients = mysqli_fetch_array($resultClients)){ ?>
                            <option value="<?php echo $filaclients['RFC'];?>"><?php echo $filaclients['Nombre'];?></option>
                            <?php } ?>
                     </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-key"></i> RFC:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                           <input type="text" id="rfce" class="form-control" >
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-address-book"></i> Uso de CFDI:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <select class="form-control" id="usocfdi">
                           <?php while ($filaUso = mysqli_fetch_array($resultUso)){ ?>
                           <option value="<?php echo $filaUso['uso'];?>"><?php echo $filaUso['descripcion'];?></option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-envelop"></i> E-Mail:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <input type="mail" id="mail" class="form-control" >
                     </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Agregar Conceptos</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-cart"></i> Producto o Servicio:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <select class="form-control" id="prodServ" onchange="getConcepto(this)">
                           <option></option>
                           <?php while ($filaProds = mysqli_fetch_array($resultProds)){ ?>
                           <option value="<?php echo $filaProds['articulo'];?>"><?php echo $filaProds['Nombre']." - ".$filaProds['descripcion'] ;?></option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-dice"></i> Cantidad:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                           <input type="number" id="cantidad" class="form-control" value="1">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Precio Unitario:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <input type="number" id="precioU" class="form-control" value="0.00">
                     </div>
                     <div class="form-group">
                        <label class="control-label  col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Impuesto:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                           <select id="imp" class="form-control">
                              <?php while ($filaImp = mysqli_fetch_array($resultImp)){ ?>
                              <option value="<?php echo $filaImp['Porcentaje'];?>"><?php echo $filaImp['Impuesto'];?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid">
                  <div class="form-group">
                     <div class="form-group">
                        <label class="control-label  col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-plus"></i> Agregar:</strong></h4>
                        </label>
                        <div class="col-lg-4">
                           <button class="btn btn-success col-xs-12 col-lg-12" onclick="Agregar()"><i class="icon-plus"></i></button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="contenido">
            <div class="tablita table-responsive  contenido">
               <table id="tablita" class="table table-bordered">
                  <tr>
                     <td align="center" class="TituloAzul" COLSPAN="8">Tabla Conceptos</td>
                  </tr>
                  <tr>
                     <th class="headVerde">Descripción</th>
                     <th class="headVerde">Cantidad</th>
                     <th class="headVerde">Precio</th>
                     <th class="headVerde">Importe</th>
                     <th class="headVerde">Impuesto</th>
                     <th class="headVerde">%</th>
                     <th class="headVerde">Total</th>
                     <th class="headVerde">Borrar</th>
                  </tr>
               </table>
               <div class="col-lg-5 col-lg-offset-7">
                  <table class="table table-bordered">
                     <tr>
                        <td align="center" class="TituloVerde" COLSPAN="6">Totales</td>
                     </tr>
                     <tr>
                        <td class="headAzul">Subtotal</td>
                        <td>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="icon-coin-dollar"></i></span>
                              <input id="SubT" class="form-control"  >
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td class="headAzul">Iva</td>
                        <td>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="icon-coin-dollar"></i></span>
                              <input id="impue" class="form-control" >
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td class="headAzul">Total</td>
                        <td>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="icon-coin-dollar"></i></span>
                              <input id="Tot" class="form-control"  >
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td class="headAzul">Generar CFDI</td>
                        <td>
                           <button onclick="confirm()" href="#Confirmar" data-toggle="modal" class="btn btn-info col-lg-12" >Generar</button>
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="modal fade " id="Confirmar">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Confirmar Datos</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                    <div id="content">
                        <h3>CONFIRMACION DE DATOS</h3>
                    </div>
                     

                     <table class="table">
                        <tr>
                           <td><button onclick="GenerarCFDI()" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button onclick="marcar()" class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/select2.min.js" ></script>
      <script type="text/javascript" src="js/select2_locale_es.js" ></script>
      <script>
          $(document).ready(function() { 
              $("#prodServ").select2({
                placeholder: "SELECCIONA UN PRODUCTO",
                allowClear: true
              });
            
              $("#NombreE").select2({
                placeholder: "SELECCIONA UN PRODUCTO",
                allowClear: true
              });
            

             $("#usocfdi").select2();


                 $(document).on('click', '.borrar', function (event) {
                    event.preventDefault();
                    $(this).closest('tr').remove();
                    $(".tdd").each(function(){

                        if (contador == 3) {
                            TempImpor = (parseFloat(TempImpor) + parseFloat($(this).text())).toFixed(2);
                        }

                        if (contador == 4) {
                            TempImp = (parseFloat(TempImp) + parseFloat($(this).text())).toFixed(2);
                        }

                        if (contador == 6) {
                            TempTot = (parseFloat(TempTot) + parseFloat($(this).text())).toFixed(2);
                            contador = 0;
                        }else{
                            contador = contador +1;
                        }

                        
                    });

                    ImporteF = TempImpor;
                    impuTotalF = TempImp;
                    TotalF = TempTot;

                    TempImpor = 0;
                    TempImp = 0;
                    TempTot = 0;
                    
                    document.getElementById("SubT").value = ImporteF;
                    document.getElementById("impue").value = impuTotalF;
                    document.getElementById("Tot").value = TotalF;
                });
              
           });

      </script>


      <?php
        if (isset($_SESSION['getUUID'])) {
            echo '<script> loadCFDI('."'".$_SESSION['getUUID']."'".'); </script>';
            unset($_SESSION["getUUID"]);
        } else {
            
        }
      ?>

    
</body>
</html>