<?php
    session_start();
    include("conexion.php");
    // Determina si se ha iniciado sesión 
    if (isset($_SESSION['user'])) {
        echo '<script> window.location="menu.php"; </script>';
    } //isset($_SESSION['user'])
?>
<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="UTF-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/login.css">
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
      <!-- Contenedor proncipal -->
      <div class="container form" align="center">
      <img src="img/storeletras2.png" class="img-responsive">
      <div class="form-group">
      <!-- Formulario -->
      <form method="POST" action="validar.php">
         <input type="text" name="clave" class="form-control cajas" placeholder="Clave cliente" required>
         <br>
         <input type="text" name="user" class="form-control cajas" placeholder="Usuario" required>
         <br>
         <input type="password" name="pass" class="form-control cajas" placeholder="Contraseña" required>
         <div class="checkbox" align="left" >
            <label>
               <input type="checkbox" value="Admin" name="Admin">
               <p class="adm">Administrador</p>
            </label>
         </div>
         <input type="submit" name="login" value="Acceder"  class="btn col-xs-12 col-lg-12 boton"></input>
         <br>
         <br>
         <br>
         <br>
        
      </form>
        <a href="info/info.html" target="_blank" class="btn col-xs-12 col-lg-12 bnt btn-lg masinfo">Mas Información</a>
      <br><br><br><br><br>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>