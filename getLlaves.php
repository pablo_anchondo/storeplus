<?php
    session_start();
    include("conexion.php");
    $Identificador = $_SESSION["usuario"];
    $Almacen = $_SESSION['Almacenuser'] ;
    $dominio       = $_SESSION["dominio"];

    if (($_POST['id']) == 'Step1') {


        $rutacer = "Archivos/".$Identificador."/".$Almacen."/";
        $Nombrecer = "cer.cer";
        $Nombrekey = "key.key";
        $passkey = $_POST['passkey'];
    
        shell_exec("openssl x509 -inform der -in ". $rutacer.$Nombrecer. " -out ". $rutacer."cer.pem");
        shell_exec("openssl pkcs8 -inform DER -in ". $rutacer.$Nombrekey. " -out ". $rutacer."key.pem -passin pass:".$passkey);

        //openssl pkcs8 -inform DER -in key.key -out key.pem -passin pass:SMC080314993
        //openssl x509 -inform der -in cer.crt -out cer.pem
        //openssl x509 -in C:\Users\Particular\Desktop\Certificados\cer.pem -serial -noout
        echo "Archivos PEM Generados en ". $rutacer;

    }
    elseif (($_POST['id']) == 'Step2') {
        $rutacer = "Archivos/".$Identificador."/".$Almacen."/";
        $output = shell_exec("openssl x509 -in ".$rutacer."cer.pem -serial -noout");
        $parse_res = str_replace('serial=','',$output);
        $serialNumber = '';
        for($i=1;$i<strlen($parse_res);$i=$i+2){
            $serialNumber = $serialNumber.''.$parse_res[$i];
        }
        $file = file_get_contents($dominio.$rutacer.'cer.pem');
        $Certificado = openssl_x509_parse($file);
        $rfc =  $Certificado['subject']['x500UniqueIdentifier'];
        $razon = $Certificado['subject']['name'];
        $rfc = iconv_substr ($rfc, 0, 12);
    
        $array = [
            "serialnumber" => $serialNumber,
            "rfc" => $rfc,
            "razon" => $razon,
        ];
    
        echo json_encode($array);
    }


?>