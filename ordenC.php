<?php
    // Incluimos la librería de PDF
	require('fpdf/fpdf.php');
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión
	if (isset($_SESSION['user'])) {
		echo "";
	} //isset($_SESSION['user'])
	else {
		echo '<script> window.location="index.php"; </script>';
	}
	// Determina si se ha iniciado la compra
	if (isset($_SESSION['Compra'])) {
		echo "";
	} //isset($_SESSION['Compra'])
	else {
		echo '<script> window.location="index.php"; </script>';
	}
	// Inicializamos variables de sesión
	$Identificador = $_SESSION["Id_User"];
	$Compra        = $_SESSION["Compra"];
	class PDF extends FPDF
	{
		// Cabecera de página
		function Header()
		{
			include("conexion.php");
			if (isset($_SESSION['user'])) {
				echo "";
			} //isset($_SESSION['user'])
			else {
				echo '<script> window.location="index.php"; </script>';
			}
			if (isset($_SESSION['Compra'])) {
				echo "";
			} //isset($_SESSION['Compra'])
			else {
				echo '<script> window.location="index.php"; </script>';
			}
			$profile       = $_SESSION['user'];
			$Identificador = $_SESSION["Id_User"];
			$Compra        = $_SESSION["Compra"];
			$queryCompra   = 'select * from compras where Id_User = ' . $Identificador . ' AND Compra = ' . $Compra;
			$ResCompra     = $cbd->query($queryCompra);
			$filaCompra    = mysqli_fetch_array($ResCompra);
			$Almacen = $_SESSION["Almacen"];
			$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
			$ResEmp        = $cbd->query($queryEmp);
			$filaEmp       = mysqli_fetch_array($ResEmp);
			if ($filaEmp['img'] == "ProImg/sinImg.jpg") {
			} //$filaEmp['img'] == "ProImg/sinImg.jpg"
			else {
				$this->Image($filaEmp['img'], 10, 7, 40, 28);
				$this->SetX(55);
			}
			$this->SetFont('Arial', 'B', 13);
			$this->Cell(80, 25, $filaEmp['Nombre'], 0, 0, 'L');
			$this->SetX(150);
			$this->SetFont('Arial', '', 10);
			$this->Cell(20, 10, 'Fecha:', 0, 0, 'L');
			$this->Cell(20, 10, $filaCompra['Fecha'], 0, 0, 'L');
			$this->SetY(20);
			$this->SetX(150);
			$this->Cell(20, 10, 'Id. Compra:', 0, 0, 'L');
			$this->Cell(20, 10, $filaCompra['Compra'], 0, 0, 'L');
			$this->Ln(15);
			$this->Cell(20, 10, utf8_decode('Dirección'));
			$this->Cell(50, 10, utf8_decode($filaEmp['Direccion']));
			$this->Cell(10, 10, 'C.P');
			$this->Cell(15, 10, $filaEmp['cp']);
			$this->SetX(150);
			$this->Cell(10, 10, 'Tel.');
			$this->Cell(20, 10, $filaEmp['Telefono']);
			$this->Ln(10);
			$this->Cell(30, 10, 'Proveedor');
			$this->Cell(50, 10, $filaCompra['Proveedor']);
			$this->Ln(15);
			$this->SetFont('Arial', '', 9);
			$this->Cell(38, 6, 'Nombre', 1, 0, 'C');
			$this->Cell(23, 6, 'Costo', 1, 0, 'C');
			$this->Cell(13, 6, 'Cant.', 1, 0, 'C');
			$this->Cell(23, 6, 'Subtotal', 1, 0, 'C');
			$this->Cell(13, 6, 'IVA', 1, 0, 'C');
			$this->Cell(23, 6, 'Importe', 1, 0, 'C');
			$this->Cell(51, 6, utf8_decode('Descripción'), 1, 1, 'C');
		}
	}
	// Creación del objeto de la clase heredada
	$queryCompra = 'select * from compras where Id_User = ' . $Identificador . ' AND Compra = ' . $Compra;
	$ResCompra   = $cbd->query($queryCompra);
	$filaCompra  = mysqli_fetch_array($ResCompra);
	$queryPart   = 'select * from partcompra where Id_User = ' . $Identificador . ' AND Compra = ' . $Compra;
	$ResPart     = $cbd->query($queryPart);
	// Se crea el PDF
	$pdf         = new PDF();
	// Agrega nueva página
	$pdf->AddPage();
	$pdf->SetAutoPageBreak('auto', 40);
	// Se cambia la fuente y el tamaño de la letra
	$pdf->SetFont('Arial', '', 9);
	// Se llenan las partidas
	while ($filaPart = mysqli_fetch_array($ResPart)) {
		$pdf->Cell(38, 8, $filaPart['Articulo'], 0, 0, 'C');
		$pdf->Cell(23, 8, $filaPart['Precio'], 0, 0, 'C');
		$pdf->Cell(13, 8, $filaPart['Cantidad'], 0, 0, 'C');
		$pdf->Cell(23, 8, $filaPart['Importe'], 0, 0, 'C');
		$pdf->Cell(13, 8, $filaPart['Impuesto'], 0, 0, 'C');
		$Iva   = $filaPart['Importe'] * ($filaPart['Impuesto'] / 100);
		$Total = $filaPart['Importe'] + $Iva;
		$pdf->Cell(23, 8, round($Total, 2), 0, 0, 'C');
		$pdf->MultiCell(51, 8, utf8_decode($filaPart['Descripcion']), 0, 'C', 0);
	} //$filaPart = mysqli_fetch_array($ResPart)
	$pdf->SetY(-70);
	$pdf->SetX(130);
	$pdf->SetFont('Arial', '', 12);
	$pdf->Cell(30, 6, 'Subtotal', 1, 0, 'C');
	$pdf->Cell(40, 6, $filaCompra['Importe'], 1, 0, 'R');
	$pdf->Ln(10);
	$pdf->SetX(130);
	$pdf->Cell(30, 6, 'Impuesto', 1, 0, 'C');
	$pdf->Cell(40, 6, $filaCompra['Impuesto'], 1, 0, 'R');
	$pdf->Ln(10);
	$pdf->SetX(130);
	$pdf->Cell(30, 6, 'Total', 1, 0, 'C');
	$pdf->Cell(40, 6, $filaCompra['Total'], 1, 0, 'R');
	$pdf->Ln(10);
	// Se muestra el PDF en pantalla
	$pdf->Output();
?>