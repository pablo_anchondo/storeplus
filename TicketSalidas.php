<?php
require('fpdf/fpdf.php');
    session_start();
    include("conexion.php");
    // Determina si se ha iniciado sesión
    if (isset($_SESSION['user'])) {
    } //isset($_SESSION['user'])
    else {
        echo '<script> window.location="index.php"; </script>';
    }




    class PDF_JavaScript extends FPDF
	{
		protected $javascript;
		protected $n_js;
		function IncludeJS($script, $isUTF8 = false)
		{
			if (!$isUTF8)
				$script = utf8_encode($script);
			$this->javascript = $script;
		}
		function _putjavascript()
		{
			$this->_newobj();
			$this->n_js = $this->n;
			$this->_put('<<');
			$this->_put('/Names [(EmbeddedJS) ' . ($this->n + 1) . ' 0 R]');
			$this->_put('>>');
			$this->_put('endobj');
			$this->_newobj();
			$this->_put('<<');
			$this->_put('/S /JavaScript');
			$this->_put('/JS ' . $this->_textstring($this->javascript));
			$this->_put('>>');
			$this->_put('endobj');
		}
		function _putresources()
		{
			parent::_putresources();
			if (!empty($this->javascript)) {
				$this->_putjavascript();
			} //!empty($this->javascript)
		}
		function _putcatalog()
		{
			parent::_putcatalog();
			if (!empty($this->javascript)) {
				$this->_put('/Names <</JavaScript ' . ($this->n_js) . ' 0 R>>');
			} //!empty($this->javascript)
		}
	}
	class PDF_AutoPrint extends PDF_JavaScript
	{
		function AutoPrint($printer = '')
		{
			// Open the print dialog
			if ($printer) {
				$printer = str_replace('\\', '\\\\', $printer);
				$script  = "var pp = getPrintParams();";
				$script .= "pp.interactive = pp.constants.interactionLevel.full;";
				$script .= "pp.printerName = '$printer'";
				$script .= "print(pp);";
			} //$printer
			else
				$script = 'print(true);';
			$this->IncludeJS($script);
		}
    }
    $Salida = $_SESSION['Salida'];
	$profile       = $_SESSION['user'];
	$Identificador = $_SESSION["Id_User"];
	$Almacen       = $_SESSION["Almacen"];
	$queryPart     = "select * from partsalidas where Id_User = " . $Identificador . " AND Salida = $Salida AND Almacen = $Almacen";
	$ResPart       = $cbd->query($queryPart);
	$queryVenta    = "select * from salidas where Id_User = " . $Identificador . " AND Salida = $Salida AND Almacen = $Almacen";
	;
	$ResVenta  = $cbd->query($queryVenta);
	$filaVenta = mysqli_fetch_array($ResVenta);
	$Almacen = $_SESSION["Almacen"];
	$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
	$ResEmp    = $cbd->query($queryEmp);
	$filaEmp   = mysqli_fetch_array($ResEmp);
	// Se crea el PDF
	$pdf           = new PDF_AutoPrint();
	// Agrega nueva página
	$pdf->AddPage();
	$pdf->SetFont('Arial', 'B', 6);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Empresa ' . utf8_decode($filaEmp['Nombre']));
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Direccion ' . utf8_decode($filaEmp['Direccion']));
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Salida No: ' . $Salida);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Fecha: ' . $filaVenta['Fecha']);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Hora: ' . $filaVenta['Hora']);
    $pdf->Ln(4);
	$pdf->SetX(2);
	$pdf->Cell(4, 6, 'Cant', 0, 0, 'C');
	$pdf->Cell(28, 6, 'Descripcion', 0, 1, 'C');
	while ($filaPart = mysqli_fetch_array($ResPart)) {
		$pdf->SetX(2);
        $pdf->Cell(4, 6, $filaPart['Cantidad'], 0, 0, 'C');
        $pdf->MultiCell(28, 6, utf8_decode($filaPart['Articulo']), 0, 'C', 0);
	} //$filaPart = mysqli_fetch_array($ResPart)

	$pdf->Ln(3);

	$pdf->SetX(1);
	$pdf->Cell(20, 10, 'Comentarios');
	$pdf->Ln(6);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, $filaVenta['Comentarios']);
	$pdf->Ln(5);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, $filaEmp['TextoTicket']);
	$pdf->Ln(5);
	// Se llama la función para imprimir
	$pdf->AutoPrint();
	// Se muestra el PDF en pantalla
	$pdf->Output();
    
?>