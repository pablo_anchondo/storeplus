<?php
	include("conexion.php");
	session_start();
	// Inicializamos variables de sesión
	$Identificador = $_SESSION["Id_User"];
	// Se evalúa si la variable esta vacía o no
	if (isset($_FILES["file"])) {
		$file             = $_FILES["file"];
		$fecha            = "";
		$tipo             = $file["type"];
		$resultado        = substr($tipo, 6);
		// Se le asigna un nombre a la imagen con el usuario la fecha y la hora
		$fecha            = $Identificador . '-' . date("d-m-y") . '-' . date("h-i-s") . '.' . $resultado;
		$nombre           = $fecha;
		$ruta_provicional = $file["tmp_name"];
		$size             = $file["size"];
		$dimensiones      = getimagesize($ruta_provicional);
		$width            = $dimensiones[0];
		$height           = $dimensiones[1];
		// Ruta de destino 
		$carpeta          = "ProImg/";
		// Se evalúa si el archivo es imagen o no
		if ($tipo != 'image/jpg' && $tipo != 'image/jpeg' && $tipo != 'image/png') {
			echo "El archivo no es una imagen";
		} //$tipo != 'image/jpg' && $tipo != 'image/jpeg' && $tipo != 'image/png'
		else {
			$src = $carpeta . $nombre;
			echo $src;
			// Se sube la imagen al servidor
			move_uploaded_file($ruta_provicional, $src);
		}
	} //isset($_FILES["file"])
	else {
		echo "Imagen demasiado grande seleccione otra";
	}
?>