<?php
    session_start();
    include("conexion.php");
    // Determina si se ha iniciado sesión 
    if (isset($_SESSION['user'])) {
        echo "";
    } else {
        echo '<script> window.location="index.php"; </script>';
    }
    // Determina si es administrador o vende
    if (isset($_SESSION['Vendedor'])) {
        echo '<script> window.location="index.php"; </script>';
    } else {
        echo "";
    }
    // Inicializamos variables de sesión
    $profile       = $_SESSION['user'];
    $Identificador = $_SESSION["Id_User"];
    $dominio       = $_SESSION["dominio"];
?>
<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="img/favicon.ico">
   <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="fonts/style.css">
   <link rel="stylesheet" type="text/css" href="css/paneles.css">
   <link rel="stylesheet" type="text/css" href="css/navbar.css">
   <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
   <link rel="stylesheet" type="text/css" href="css/estilos.css">
   <link rel="stylesheet" type="text/css" href="css/Tablas.css">
   <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
   <script type="text/javascript" src="js/Cotiza.js" ></script>
   <title>Store-Plus</title>
</head>
<body>
   <?php 
      // Consultas para llenar los select
        $Clientes = 'select * from clients where Id_User = '.$Identificador;
        $result2 = $cbd->query($Clientes);
        
        $imp = 'select Impuesto from impuestos where Id_User = '.$Identificador;
        $result3 = $cbd->query($imp);
    ?>
   <!--// Navigation bar -->   
   <nav class="navbar navbar-default navbar-fixed-static navcolor">
      <div class="container-fluid">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="menu.php"><img src="img/favicon.ico"></a>
         </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
               <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
               <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
               <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
               <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
               <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
               <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
               <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
               <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
               <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
               <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li><a href="logout.php">Cerrar Sesión</a></li>
                  </ul>
               </li>
            </ul>
         </div>
      </div>
   </nav>
   <!-- Contenedor proncipal -->	
   <div class="container-fluid">
      <div class="cabezera" align="center">
         <h3 class="Titulo">Cotización</h3>
      </div>
      <div class="contenido">
         <div class="tablita table-responsive table-bordered contenido">
            <br>
            <div class="container-fluid">
               <div class="form-group col-xs-12 col-md-12 col-lg-12">
                  <select class="form-control Sarticulo" id="client" name="name[]">
                     <option value="Cliente">Cliente</option>
                     <?php while ($fila2 = mysqli_fetch_array($result2)){ ?>
                     <option value="<?php echo $fila2['Nombre'];?>"><?php echo $fila2['Nombre'];?></option>
                     <?php } ?>
                  </select>
               </div>
            </div>
            <!-- Se crea la tabla -->
            <div class="table-responsive container-fluid tabla">
               <table id="TableBody" class="table table-striped table-bordered TextBlack">
                  <tr>
                     <td align="center" class="TituloVerde" COLSPAN="7">Detalles de Cotización</td>
                  </tr>
                  <tr>
                     <th class="headAzul">N°</th>
                     <th class="headAzul">Articulo</th>
                     <th class="headAzul">Cantidad</th>
                     <th class="headAzul">Precio</th>
                     <th class="headAzul">Impuesto</th>
                     <th class="headAzul">Descripción</th>
                     <th class="headAzul">Importe</th>
                  </tr>
               </table>
            </div>
            <div class="table-responsive container-fluid">
               <table class="table table-striped table-bordered">
                  <tr>
                     <td align="center" class="TituloAzul" COLSPAN="6">Detalles de Producto o Servicio</td>
                  </tr>
                  <tr>
                     <th class="headVerde">Producto</th>
                     <th class="headVerde" >Cantidad</th>
                     <th class="headVerde" >Precio</th>
                     <th class="headVerde">Impuesto</th>
                     <th class="headVerde">Agregar</th>
                     <th class="headVerde">Borrar</th>
                  </tr>
                  <tr>
                     <td class="col-xs-12 col-lg-2">
                        <div class="input-group">
                           <input type="text" id="prod"  onkeypress="validar(event, this)" class="form-control textColor" placeholder="Productos" autofocus>
                           <span class="input-group-addon  color btn" id="find" href="#Buscar" data-toggle="modal"><i class="icon-search"></i></span>
                        </div>
                     </td>
                     <td class="col-xs-12 col-lg-2">
                        <input class="form-control " onkeypress="return valida(event)" placeholder="Cantidad" id="cant" name="name[] " value=1>
                     </td>
                     <td class="col-xs-12 col-lg-2">
                        <input class="form-control " placeholder="Precio" id="price" name="name[]">
                     </td>
                     <td class="col-xs-12 col-lg-2">
                        <select class="form-control Sarticulo" id="imp" name="name[]">
                           <option value="Impuesto">Impuesto</option>
                           <?php while ($fila3 = mysqli_fetch_array($result3)){ ?>
                           <option value="<?php echo $fila3['Impuesto'];?>"><?php echo $fila3['Impuesto'];?></option>
                           <?php } ?>
                        </select>
                     </td>
                     <td class="col-xs-12 col-lg-2">
                        <button id="llenaTabla" class="btn btn-primary col-xs-12 col-lg-12"><span class="icon-plus"></span></button>
                     </td>
                     <td class="col-xs-12 col-lg-2">
                        <button id="Del" class="btn btn-warning col-xs-12 col-lg-12"><span class='icon-bin' ></span></button>
                     </td>
                  </tr>
               </table>
            </div>
            <div class="alert alert-danger alert-dismissible" id="alerta" align="center">
            </div>
            <div class="col-lg-5 col-lg-offset-7">
               <table class="table table-bordered">
                  <tr>
                     <td align="center" class="TituloVerde" COLSPAN="6">Totales</td>
                  </tr>
                  <tr>
                     <td class="headAzul">Subtotal</td>
                     <td>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="icon-coin-dollar"></i></span>
                           <input id="importe" class="form-control" placeholder="0.00">
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td class="headAzul">Iva</td>
                     <td>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="icon-coin-dollar"></i></span>
                           <input id="impuest" class="form-control" placeholder="0.00">
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td class="headAzul">Total</td>
                     <td>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="icon-coin-dollar"></i></span>
                           <input id="tot" class="form-control" placeholder="0.00">
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td class="headAzul" valign="middle">Opciones</td>
                     <td>
                        <a class="btn btn-success col-xs-12 col-lg-6 btnCompra" onclick="Cotizando()">Cotizar</a>
                        <a href="<?php echo $dominio;?>menu.php" class="btn btn-danger col-xs-12 col-lg-6 btnCompra">Cancelar</a>
                     </td>
                  </tr>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- Modal de Buscar Producto -->
   <div class="container" align="center">
      <div class="modal  " id="Buscar" align="center">
         <div class="modal-dialog tamno-modal" align="center">
            <div class="modal-content">
               <div class="modal-header panel-header HeadPanel">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title" align="center">Buscar Producto</h2>
               </div>
               <div class="modal-body cuerpoTabla" align="center">
                  <div class="container-fluid">
                     <input class="col-xs-12 col-lg-12 form-control" placeholder="Buscar" id="caja_busqueda" name="caja_busqueda">
                  </div>
                  <br>
                  <div class="container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                     <div class="table-responsive tablita">
                        <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" id="datos">
                        </div>
                     </div>
                  </div>
                  <br>
                  <br>
                  <br>
                  <br>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script src="js/jquery.js"></script>
   <script src="js/bootstrap.min.js"></script>
</body>
</html>