<?php
    // Incluimos la librería de PDF
    require('fpdf/fpdf.php');
    session_start();
    include("conexion.php");
    // Determina si se ha iniciado sesión
    if (isset($_SESSION['user'])) {
        echo "";
    } //isset($_SESSION['user'])
    else {
        echo '<script> window.location="index.php"; </script>';
    }
    // Determina si se ha iniciado la fecha de inicio
    if (isset($_SESSION["FI"])) {
        echo "";
    } //isset($_SESSION["FI"])
    else {
        echo '<script> window.location="Reportes.php"; </script>';
    }
    // Determina si se ha iniciado la fecha final
    if (isset($_SESSION["FF"])) {
        echo "";
    } //isset($_SESSION["FF"])
    else {
        echo '<script> window.location="Reportes.php"; </script>';
    }
    $Identificador = $_SESSION["Id_User"];
    class PDF extends FPDF
    {
        // Cabecera de página
        function Header()
        {
            include("conexion.php");
            if (isset($_SESSION['user'])) {
                echo "";
            } //isset($_SESSION['user'])
            else {
                echo '<script> window.location="index.php"; </script>';
            }
            if (isset($_SESSION["FI"])) {
                echo "";
            } //isset($_SESSION["FI"])
            else {
                echo '<script> window.location="Reportes.php"; </script>';
            }
            if (isset($_SESSION["FF"])) {
                echo "";
            } //isset($_SESSION["FF"])
            else {
                echo '<script> window.location="Reportes.php"; </script>';
            }
            $profile       = $_SESSION['user'];
            $Identificador = $_SESSION["Id_User"];
            $FI            = $_SESSION["FI"];
            $FF            = $_SESSION["FF"];
            $prov          = $_SESSION["provedor"];
            $Almacen = $_SESSION["Almacen"];
			$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
            $ResEmp        = $cbd->query($queryEmp);
            $filaEmp       = mysqli_fetch_array($ResEmp);
            if ($filaEmp['img'] == "ProImg/sinImg.jpg") {
            } //$filaEmp['img'] == "ProImg/sinImg.jpg"
            else {
                $this->Image($filaEmp['img'], 10, 7, 40, 28);
                $this->SetX(55);
            }
            $this->SetFont('Arial', 'B', 13);
            $this->Cell(80, 25, $filaEmp['Nombre'], 0, 0, 'L');
            $this->SetX(150);
            $this->SetFont('Arial', '', 10);
            $this->SetY(20);
            $this->Ln(15);
            $this->Cell(70, 10, utf8_decode('Reporte de Compras del ' . $FI . ' al  ' . $FF));
            $this->ln(8);
            $this->Cell(70, 10, utf8_decode('Proveedor ' . $prov));
            $this->Ln(15);
            $this->SetFont('Arial', '', 9);
            $this->Cell(18, 6, 'Compra', 1, 0, 'C');
            $this->Cell(28, 6, 'Fecha', 1, 0, 'C');
            $this->Cell(28, 6, 'Hora.', 1, 0, 'C');
            $this->Cell(38, 6, utf8_decode('Proveedor'), 1, 0, 'C');
            $this->Cell(28, 6, 'Importe', 1, 0, 'C');
            $this->Cell(28, 6, 'Impuesto', 1, 0, 'C');
            $this->Cell(28, 6, 'Total', 1, 1, 'C');
        }
    }
    // Se crea el PDF
    $pdf = new PDF();
    // Agrega nueva página
    $pdf->AddPage();
    $pdf->SetAutoPageBreak('auto', 40);
    $pdf->SetFont('Arial', '', 9);
    // Inicializamos variables de sesión
    $profile       = $_SESSION['user'];
    $Identificador = $_SESSION["Id_User"];
    $FI            = $_SESSION["FI"];
    $FF            = $_SESSION["FF"];
    $prov          = $_SESSION["provedor"];
    // Se determina si son todos los proveedores o solo uno
    if ($prov == "todos") {
        $queryVent = 'select * from compras where Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND (Fecha BETWEEN ' . "'" . $FI . "'" . ' AND ' . "'" . $FF . "'" . ')';
    } //$prov == "todos"
    else {
        $queryVent = 'select * from compras where Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND (Fecha BETWEEN ' . "'" . $FI . "'" . ' AND ' . "'" . $FF . "'" . ') AND     Proveedor = (SELECT Nombre FROM proveedores WHERE Proveedor = ' . $prov . ') ';
    }
    $ResVent = $cbd->query($queryVent);
    while ($filaVent = mysqli_fetch_array($ResVent)) {
        // Se llenan las partidas
        $pdf->Cell(18, 6, $filaVent['Compra'], 0, 0, 'C');
        $pdf->Cell(28, 6, $filaVent['Fecha'], 0, 0, 'C');
        $pdf->Cell(28, 6, $filaVent['Hora'], 0, 0, 'C');
        $pdf->Cell(38, 6, utf8_decode($filaVent['Proveedor']), 0, 0, 'C');
        $pdf->Cell(28, 6, "$" . $filaVent['Importe'], 0, 0, 'C');
        $pdf->Cell(28, 6, "$" . $filaVent['Impuesto'], 0, 0, 'C');
        $pdf->Cell(28, 6, "$" . $filaVent['Total'], 0, 1, 'C');
    } //$filaVent = mysqli_fetch_array($ResVent)
    if ($prov == "todos") {
        $querySum = 'select SUM(Importe) as Importe, SUM(Impuesto) as Impuesto, SUM(Total) as Total  from compras where Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND (Fecha BETWEEN ' . "'" . $FI . "'" . ' AND ' . "'" . $FF . "'" . ')';
    } //$prov == "todos"
    else {
        $querySum = 'select SUM(Importe) as Importe, SUM(Impuesto) as Impuesto, SUM(Total) as Total  from compras where Id_User = ' . $Identificador . ' AND Almacen = ' . $_SESSION["Almacen"] . ' AND (Fecha BETWEEN ' . "'" . $FI . "'" . ' AND ' . "'" . $FF . "'" . ') AND Proveedor = (SELECT Nombre FROM proveedores WHERE Proveedor = ' . $prov . ') ';
    }
    $ResSum  = $cbd->query($querySum);
    $filaSum = mysqli_fetch_array($ResSum);
    $pdf->SetY(-70);
    $pdf->SetX(130);
    $pdf->SetFont('Arial', '', 12);
    $pdf->Cell(30, 6, 'Subtotal', 1, 0, 'C');
    $pdf->Cell(40, 6, "$" . $filaSum['Importe'], 1, 0, 'R');
    $pdf->Ln(10);
    $pdf->SetX(130);
    $pdf->Cell(30, 6, 'Impuesto', 1, 0, 'C');
    $pdf->Cell(40, 6, "$" . $filaSum['Impuesto'], 1, 0, 'R');
    $pdf->Ln(10);
    $pdf->SetX(130);
    $pdf->Cell(30, 6, 'Total', 1, 0, 'C');
    $pdf->Cell(40, 6, "$" . $filaSum['Total'], 1, 0, 'R');
    $pdf->Ln(10);
    // Se muestra el PDF en pantalla
    $pdf->Output();
?>
