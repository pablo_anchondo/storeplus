<?php
	require('fpdf/fpdf.php');
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión 
	if (isset($_SESSION['user'])) {
		echo "";
	} //isset($_SESSION['user'])
	else {
		echo '<script> window.location="index.php"; </script>';
	}
	// Inicializamos variables de sesión
	$Identificador = $_SESSION["Id_User"];
	$vendedor      = $_SESSION["vendedor"];
	$Almacen = $_SESSION["Almacen"];
	// Consultas base de datos
	$queryEmp      = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
	$ResEmp        = $cbd->query($queryEmp);
	$filaEmp       = mysqli_fetch_array($ResEmp);
	$queryTotal    = "select SUM(Total) as Total from flujo where Id_User = " . $Identificador . " AND Almacen = " . $_SESSION["Almacen"] . " AND Vendedor = '$vendedor' ";
	$ResTotal      = $cbd->query($queryTotal);
	$filaTot       = mysqli_fetch_array($ResTotal);
	// Se crea el PDF
	$pdf           = new FPDF();
	// Agrega nueva página
	$pdf->AddPage();
	// Se cambia la fuente y el tamaño
	$pdf->SetFont('Arial', 'B', 6);
	// Se llena el PDF
	$pdf->Cell(30, 10, '****Corte Total X****', 0, 0, 'C');
	$pdf->Ln(5);
	$pdf->Cell(10, 10, $filaEmp['Nombre']);
	$pdf->Ln(5);
	$pdf->Cell(12, 10, 'Vendedor:');
	$pdf->SetX(21);
	$pdf->Cell(15, 10, $_SESSION["vendedor"]);
	$pdf->Ln(5);
	$pdf->Cell(15, 10, $filaEmp['Direccion']);
	$pdf->Ln(5);
	$pdf->Cell(15, 10, 'Fecha: ' . $_SESSION["FechaV"]);
	$pdf->Ln(5);
	$pdf->Cell(15, 10, 'Hora: ' . $_SESSION["HoraV"]);
	$pdf->Ln(10);
	$pdf->Cell(30, 10, '***Ingresos***', 0, 0, 'C');
	$pdf->Ln(9);
	$pdf->Cell(15, 10, 'Total de Ingresos:');
	$pdf->SetX(30);
	$pdf->Cell(15, 10, "$" . $filaTot['Total']);
	$pdf->Ln(10);
	$queryImp = "select SUM(Importe) as Importe, SUM(Impuesto) as Impuesto , Porcentaje from flujo where Id_User = " . $Identificador . " AND Almacen = " . $_SESSION["Almacen"] . " AND Vendedor = '$vendedor' GROUP BY Porcentaje";
	$ResImp   = $cbd->query($queryImp);
	$queryApa = "select SUM(Abono) AS Abono, Cobranza from flujocobranza where Id_User = " . $Identificador . " AND Vendedor = '$vendedor' AND Almacen = " . $_SESSION["Almacen"]. " GROUP BY Cobranza";
	$ResApa   = $cbd->query($queryApa);
	$pdf->Cell(30, 10, '****Ventas del corte****', 0, 0, 'C');
	$pdf->Ln(9);
	// Se llenan las partidas
	while ($filaImp = mysqli_fetch_array($ResImp)) {
		$pdf->Cell(15, 10, 'Ventas: ' . $filaImp['Porcentaje'] . '%');
		$pdf->SetX(30);
		$pdf->Cell(15, 10, "$" . $filaImp['Importe']);
		$pdf->Ln(5);
		$pdf->Cell(15, 10, 'Impuesto:');
		$pdf->SetX(30);
		$pdf->Cell(15, 10, "$" . $filaImp['Impuesto']);
		$pdf->Ln(5);
	} //$filaImp = mysqli_fetch_array($ResImp)
	$pdf->Ln(10);
	$pdf->Cell(30, 10, '****Abonos****', 0, 0, 'C');
	$pdf->Ln(9);
	while ($filaApa = mysqli_fetch_array($ResApa)) {

		$pdf->Cell(13, 10, 'Cobranza: ' . $filaApa['Cobranza']);
		$pdf->Cell(13, 10, 'Abono: ' . $filaApa['Abono']);
		$pdf->Ln(5);
	} //$filaApa = mysqli_fetch_array($ResApa)
	$queryTicket = "select SUM(Total) as Total, Venta from flujo where Id_User = " . $Identificador . " AND Almacen = " . $_SESSION["Almacen"] . " AND Vendedor = '$vendedor' GROUP BY Venta";
	$ResTick     = $cbd->query($queryTicket);
	$pdf->Ln(10);
	$pdf->Cell(30, 10, '***Ventas por Ticket***', 0, 0, 'C');
	$pdf->Ln(10);
	while ($filaTicket = mysqli_fetch_array($ResTick)) {
		$pdf->Cell(13, 10, 'Venta: ' . $filaTicket['Venta']);
		$pdf->Cell(13, 10, 'Total: ' . $filaTicket['Total']);
		$pdf->Ln(5);
	} //$filaTicket = mysqli_fetch_array($ResTick)
	$sql = "DELETE FROM  flujo WHERE Id_User = $Identificador AND Vendedor = '$vendedor' AND Almacen = " . $_SESSION["Almacen"];
	$cbd->query($sql);

	$sql = "DELETE FROM flujocobranza WHERE Id_User = $Identificador AND Vendedor = '$vendedor' AND Almacen = " . $_SESSION["Almacen"];
	$cbd->query($sql);
	// Se muestra el PDF en pantalla
	$pdf->Output();
?>