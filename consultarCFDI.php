<?php
    session_start();
    include("conexion.php");
    // Determina si se ha iniciado sesión 
    if (isset($_SESSION['user'])) {
        echo "";
    } else {
        echo '<script> window.location="index.php"; </script>';
    }

    if (isset($_SESSION['FacturaActiva'])) {
        echo "";
    } else {
        echo '<script> window.location="index.php"; </script>';
    }

    $profile       = $_SESSION['user'];
    $Identificador = $_SESSION["Id_User"];
    $dominio       = $_SESSION["dominio"];
    $Almacen       = $_SESSION["Almacen"];
?>

<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/consultarCFDI.js" ></script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
                  <li class="active"><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Consultar CFDI</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
                <br>
         <div class="bs-example" align="center">
            <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="dropdown active col-xs-12 col-lg-12" align="center">
               <a href="#" class="dropdown-toggle" id="drop4" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Desplegar Opciones <span class="caret"></span> </a>
               <ul class="dropdown-menu col-xs-12 col-lg-12" align="center" id="menu1" aria-labelledby="drop4">
                  <li><a href="Mapeo.php">Mapeo Productos</a></li>
                  <li><a href="MapeoUnidad.php">Mapeo Unidades</a></li>
                  <li><a href="datosFactura.php">Datos Factura</a></li>
                  <li><a href="ganeraCFDI.php">Generar CFDI</a></li>
                  <li><a href="consultarCFDI.php">Consultar CFDI</a></li>
                  <li><a href="ComplementosPago.php">Complementos de pago</a></li>
               </ul>
            </li>
         </div>

               <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-8 col-lg-offset-8">
                  <br>
                  <div class="input-group">
                     <input class="col-xs-6 col-lg-6 form-control" placeholder="Buscar" id="caja_busqueda" name="caja_busqueda">
                     <span class="input-group-addon  color" data-toggle="modal"><i class="icon-search"></i></span>
                  </div>
                  <br>
               </div>
               <div id="TablaCFDIS" >
               </div>
            </div>
         </div>
      </div>

      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>