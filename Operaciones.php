<?php
  session_start();
  include("conexion.php");
  // Determina si se ha iniciado sesión
  if (isset($_SESSION['user'])) {
      echo "";
  } //isset($_SESSION['user'])
  else {
      echo '<script> window.location="index.php"; </script>';
  }
  // Determina si es administrador o vendedor
  if (isset($_SESSION['Vendedor'])) {
      echo '<script> window.location="index.php"; </script>';
  } //isset($_SESSION['Vendedor'])
  else {
      echo "";
  }

  if (isset($_SESSION['ValOperaciones'])) {
    echo '<script> window.location="index.php"; </script>';
    } //isset($_SESSION['Vendedor'])
    else {
        echo "";
    }

  // Inicializamos variables de sesión
  $profile       = $_SESSION['user'];
  $Identificador = $_SESSION["Id_User"];
  $dominio       = $_SESSION["dominio"];
?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/Reportes.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/Reportes.js"></script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
      <!--// Navigation bar -->
      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li class="active"><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
                  <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- Contenedor proncipal -->
      <div class="container-fluid">
            <div class="cabezera" align="center">
                   <h3 class="Titulo">Operaciones</h3>
            </div>
            <div class="contenido">

                <div class="tablita table-responsive table-bordered contenido">
                    <br>
                    <button onclick="buscarCompras()" class="color-boton btn bg col-xs-12  col-sm-12 col-md-4 col-lg-4">Compras</button>
                    <button onclick="buscarTicket()" class="color-boton btn bg col-xs-12  col-sm-12 col-md-4 col-lg-4">Tickets</button>
                    <button onclick="buscarCotiza()" class="color-boton btn bg col-xs-12  col-sm-12 col-md-4 col-lg-4">Cotizaciones</button>
                    <br>
                    <br>
                    <br>
                    <div class="alert alert-danger alert-dismissible" id="alerta" align="center">
                    </div>
                    <div class="col-xs-12 col-lg-12 container" id="Reportes" >
                    </div>
                    <br><br><br><br>
                    <div class="col-xs-12 col-lg-12 " id="Contenido" align="center">
                    </div>
                </div>

            </div>
       </div>



      <div class="container-fluid" align="center">
         <img src="img/storeletras.png" class="img-responsive">
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>