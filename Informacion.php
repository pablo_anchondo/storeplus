<?php
    session_start();
    include("conexion.php");
    // Determina si se ha iniciado sesión 
    if (isset($_SESSION['user'])) {
        echo "";
    } //isset($_SESSION['user'])
    else {
        echo '<script> window.location="index.php"; </script>';
    }
    // Determina si es administrador o vendedor
    if (isset($_SESSION['Vendedor'])) {
        echo '<script> window.location="index.php"; </script>';
    } //isset($_SESSION['Vendedor'])
    else {
        echo "";
    }
    // Inicializamos variables de sesión
    $profile       = $_SESSION['user'];
    $Identificador = $_SESSION["Id_User"];
    $dominio       = $_SESSION["dominio"];
?>
<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="img/favicon.ico">
   <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="fonts/style.css">
   <link rel="stylesheet" type="text/css" href="css/paneles.css">
   <link rel="stylesheet" type="text/css" href="css/navbar.css">
   <link rel="stylesheet" type="text/css" href="css/estilos.css">
   <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
   <link rel="stylesheet" type="text/css" href="css/Tablas.css">
   <script type="text/javascript" src="js/inf.js" ></script>
   <script type="text/javascript">buscarDatos()</script>
   <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
   <title>Store-Plus</title>
</head>
<body>
   <!--// Navigation bar -->
   <nav class="navbar navbar-default navbar-fixed-static navcolor">
      <div class="container-fluid">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="menu.php"><img src="img/favicon.ico"></a>
         </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
               <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
               <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
               <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
               <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
               <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
               <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
               <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
               <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
               <li class="active"><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>
               <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li><a href="logout.php">Cerrar Sesión</a></li>
                  </ul>
               </li>
            </ul>
         </div>
      </div>
   </nav>
   <!-- Contenedor proncipal -->
   <div class="container-fluid">
            <div class="cabezera" align="center">
                   <h3 class="Titulo">Información de la cuenta</h3>
            </div>
            <div class="contenido">

                <div class="tablita table-responsive table-bordered contenido">

                <div class="container-fluid">

                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-key2"></i> Clave Cliente:</strong></h4>
                        <input class="form-control" id="clave" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-user"></i> Nombre:</strong></h4>
                        <input class="form-control" id="Nombre" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-user-tie"></i> Nombre Fiscal:</strong></h4>
                        <input class="form-control" id="NombreF" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-user-tie"></i> Nombre Comercial:</strong></h4>
                        <input class="form-control" id="NombreC" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-earth"></i> País:</strong></h4>
                        <input class="form-control" id="pais" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-compass2"></i> Codigo Postal:</strong></h4>
                        <input class="form-control" id="cp" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-location"></i> Calle:</strong></h4>
                        <input class="form-control" id="calle" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-location2"></i> Numero:</strong></h4>
                        <input class="form-control" id="Numero" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-map2"></i> Colonia:</strong></h4>
                        <input class="form-control" id="Col" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-map"></i> Estado:</strong></h4>
                        <input class="form-control" id="Estado" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-office"></i> Ciudad:</strong></h4>
                        <input class="form-control" id="City" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-location"></i> Localidad:</strong></h4>
                        <input class="form-control" id="loc" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-phone"></i> Telefono:</strong></h4>
                        <input class="form-control" id="tel" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-key2"></i> R.F.C:</strong></h4>
                        <input class="form-control" id="rfc" readonly>
                    </div>
                    <div class="form-group col-lg-4" >
                        <h4 class="textoBlack"><strong><i class="icon-envelop"></i> E-Mail:</strong></h4>
                        <input class="form-control" id="mail" readonly>
                    </div>

                </div>


            <br>
            <div class="container-fluid">
               <button  href="#Usuario" data-toggle="modal" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 btn-success btn">Cambiar Nombre de usuario</button>
               <button  href="#Pass" data-toggle="modal" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 btn-warning btn">Cambiar contraseña</button>
            </div>
                </div>


   <!-- Modal de nombre de usuario -->
   <div class="container">
      <div class="modal fade " id="Usuario">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header panel-header HeadPanel">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title" align="center">Cambiar nombre de usuario</h2>
               </div>
               <div class="modal-body cuerpoM" align="center">
                  <br>
                  <div class="alert alert-success alert-dismissible" id="alertaUsu" align="center">
                  </div>
                  <table class="table">
                     <tr>
                        <td class="col-lg-6"><label class="text3" > Usuario actual</label></td>
                        <td class="col-lg-6"><input class="form-control" id="usu" ></td>
                     </tr>
                     <tr>
                        <td><label class="text3" > Nuevo Usuario</label></td>
                        <td><input class="form-control" id="newusu" ></td>
                     </tr>
                     <tr>
                        <td><label class="text3" > Confirmar Usuario</label></td>
                        <td><input class="form-control" id="confusu" ></td>
                     </tr>
                     <tr>
                        <td><button onclick="cambioUsu()" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                        <td><button class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Modal de cambio de contraseña -->
   <div class="container">
      <div class="modal fade" id="Pass">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header panel-header HeadPanel">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title" align="center">Cambiar contraseña</h2>
               </div>
               <div class="modal-body cuerpoM" align="center">
                  <br>
                  <div class="alert alert-success alert-dismissible" id="alertaPass" align="center">
                  </div>
                  <table class="table">
                     <tr>
                        <td class="col-lg-6"><label class="text3" > Contraseña actual</label></td>
                        <td class="col-lg-6"><input class="form-control" id="pass" type="password" ></td>
                     </tr>
                     <tr>
                        <td><label class="text3" > Nueva contraseña</label></td>
                        <td><input class="form-control" id="newpass" type="password"></td>
                     </tr>
                     <tr>
                        <td><label class="text3" > Confirmar contraseña</label></td>
                        <td><input class="form-control" id="confpass" type="password"></td>
                     </tr>
                     <tr>
                        <td><button onclick="cambioPass()" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                        <td><button class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script src="js/jquery.js"></script>
   <script src="js/bootstrap.min.js"></script>    
</body>
</html>