<?php
    // Incluimos la librería de PDF
    require('fpdf/fpdf.php');
    session_start();
    include("conexion.php");
    // Determina si se ha iniciado sesión
    if (isset($_SESSION['user'])) {
        echo "";
    } //isset($_SESSION['user'])
    else {
        echo '<script> window.location="index.php"; </script>';
    }
    // Inicializamos variables de sesión
    $Identificador = $_SESSION["Id_User"];
    $FI            = $_SESSION["FI"];
    $FF            = $_SESSION["FF"];
    $vendedor      = $_SESSION["vendedorT"];
    class PDF extends FPDF
    {
        // Cabecera de página
        function Header()
        {
            include("conexion.php");
            $Identificador = $_SESSION["Id_User"];
            $FI            = $_SESSION["FI"];
            $FF            = $_SESSION["FF"];
            $vendedor      = $_SESSION["vendedorT"];
            $Almacen = $_SESSION["Almacen"];
			$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
            $ResEmp        = $cbd->query($queryEmp);
            $filaEmp       = mysqli_fetch_array($ResEmp);
            $queryV        = "select * from ventas where Id_User = " . $Identificador . " AND Vendedor = '$vendedor' AND Almacen = " . $_SESSION["Almacen"] . " AND (Fecha BETWEEN " . $FI . " AND " . $FF . ")";
            $ResV          = $cbd->query($queryV);
            $filaV         = mysqli_fetch_array($ResV);
            $this->SetFont('Arial', 'B', 13);
            if ($filaEmp['img'] == "ProImg/sinImg.jpg") {
                $this->SetY(15);
                $this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
                $this->SetX(55);
            } //$filaEmp['img'] == "ProImg/sinImg.jpg"
            else {
                $this->Image($filaEmp['img'], 10, 7, 40, 28);
                $this->SetY(15);
                $this->SetX(55);
                $this->Cell(15, 10, 'Empresa:', 0, 0, 'L');
            }
            $this->Cell(51, 10, utf8_decode($filaEmp['Nombre']), 0, 1, 'C');
            $this->Ln(9);
            $this->Line(10, 35, 199, 35);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(100, 10, utf8_decode('Utilidad de ventas del ' . $FI . ' al ' . $FF), 0, 0, 'L');
            $this->Ln(15);
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(31.6, 6, utf8_decode('Venta'), 1, 0, 'C');
            $this->Cell(31.6, 6, utf8_decode('Fecha'), 1, 0, 'C');
            $this->Cell(31.6, 6, 'Importe', 1, 0, 'C');
            $this->Cell(31.6, 6, 'Costo.', 1, 0, 'C');
            $this->Cell(31.6, 6, 'Utilidad', 1, 0, 'C');
            $this->Cell(31.6, 6, '%', 1, 1, 'C');
        }
    }
    // Se crea el PDF
	$pdf = new PDF();
	// Agrega nueva página
    $pdf->AddPage();
    $pdf->SetFont('Arial', '', 9);
    $queryV        = "select * from ventas where Id_User = " . $Identificador . " AND Vendedor = '$vendedor' AND Almacen = " . $_SESSION["Almacen"] . " AND (Fecha BETWEEN " . "'" . $FI . "'" . " AND " . "'" . $FF . "'" . ")";
    $ResV          = $cbd->query($queryV);
    $TotalImp      = 0;
    $TotalCosto    = 0;
    $UtilidadFinal = 0;
    $costo         = 0;
    $costoPart     = 0;
    $Utilidad      = 0;
    $porcentaje    = 0;
    while ($filaV = mysqli_fetch_array($ResV)) {
        $queryPart = "select * from partventa where Id_User = " . $Identificador . " AND Vendedor = '$vendedor'  AND Almacen = " . $_SESSION["Almacen"] . " AND Venta = " . $filaV['Venta'];
        $ResPart   = $cbd->query($queryPart);
        while ($filaPart = mysqli_fetch_array($ResPart)) {
            $queryProd = 'select * from productos where Id_User = ' . $Identificador . ' AND articulo = ' . "'" . $filaPart['idProd'] . "'";
            $ResProd   = $cbd->query($queryProd);
            $filaProd  = mysqli_fetch_array($ResProd);
            $costoPart = $filaPart['Cantidad'] * $filaPart['CostoU'];
            $costo     = $costo + $costoPart;
        } //$filaPart = mysqli_fetch_array($ResPart)
        $Utilidad   = $filaV['Importe'] - $costo;
        $porcentaje = (round($Utilidad, 2) * 100) / $costo;
        $pdf->Cell(31.6, 6, utf8_decode($filaV['Venta']), 0, 0, 'C');
        $pdf->Cell(31.6, 6, utf8_decode($filaV['Fecha']), 0, 0, 'C');
        $pdf->Cell(31.6, 6, '$' . $filaV['Importe'], 0, 0, 'C');
        $pdf->Cell(31.6, 6, '$' . round($costo, 2), 0, 0, 'C');
        $pdf->Cell(31.6, 6, '$' . round($Utilidad, 2), 0, 0, 'C');
        $pdf->Cell(31.6, 6, round($porcentaje, 2) . '%', 0, 1, 'C');
        $TotalImp      = $TotalImp + $filaV['Importe'];
        $TotalCosto    = $TotalCosto + $costo;
        $UtilidadFinal = $UtilidadFinal + $Utilidad;
        $costo         = 0;
        $Utilidad      = 0;
        $porcentaje    = 0;
    } //$filaV = mysqli_fetch_array($ResV)
    $pdf->SetX(136.4);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(31.6, 6, 'Importe Final', 1, 0, 'C');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(31.6, 6, '$' . round($TotalImp, 2), 1, 1, 'C');
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetX(136.4);
    $pdf->Cell(31.6, 6, 'Costo Final', 1, 0, 'C');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(31.6, 6, '$' . round($TotalCosto, 2), 1, 1, 'C');
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetX(136.4);
    $pdf->Cell(31.6, 6, 'Utilidad Final', 1, 0, 'C');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(31.6, 6, '$' . round($UtilidadFinal, 2), 1, 1, 'C');
    // Se muestra el PDF en pantalla
    $pdf->Output();
?>