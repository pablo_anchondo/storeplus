<?php
  session_start();
  include("conexion.php");
  // Determina si se ha iniciado sesión
  if (isset($_SESSION['user'])) {
      echo "";
  } //isset($_SESSION['user'])
  else {
      echo '<script> window.location="index.php"; </script>';
  }

  if (isset($_SESSION['Almacen'])) {
    echo "";
} //isset($_SESSION['user'])
else {
    echo '<script> window.location="VentanaAlmacen.php"; </script>';
}
  // Determina si es administrador o vendedor
  if (isset($_SESSION["Vendedor"])) {
      echo '<script type="text/javascript" src="js/disabled.js" ></script>';
  } //isset($_SESSION["Vendedor"])
  else {
      echo '<script type="text/javascript" src="js/menu.js" ></script>';
  }
  // Inicializamos variables de sesión
  $profile       = $_SESSION['user'];
  $Identificador = $_SESSION["Id_User"];
  $dominio       = $_SESSION["dominio"];


?>
<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Store-Plus</title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/custom.css" rel="stylesheet">
      <link href="css/menu.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <script type="text/javascript" href="js/menu.js"></script>
   </head>
   <header>
   </header>
   <body id="cuerpo">
      <?php
         $querito = 'select * from almacen where Id_User = '.$Identificador.' order by Nombre ASC ';
         $result = $cbd->query($querito);
         
         $imgEmp = 'select img from empresa where Id_User = '.$Identificador;
         $resultImgEmp = $cbd->query($imgEmp);

         $fila48 = mysqli_fetch_array($resultImgEmp);

         $queryFac   = "SELECT Emisor from datosfactura where Id_User = $Identificador  AND Almacen = " . $_SESSION["Almacen"];
         $resultFac  = $cbd->query($queryFac);
         $filaFac    = mysqli_fetch_array($resultFac);
         $Emisor     = $filaFac['Emisor'];
         if (isset($Emisor)) {
             $_SESSION['FacturaActiva'] = "TRUE";
         } 
         else {

         }

         if (isset($_SESSION["Vendedor"])) {
            $queritoVendedor = 'select * from vendedores where Vendedor = '. "'".$profile."'" .' AND Id_User = '.$Identificador;
            $resultVend = $cbd->query($queritoVendedor);
            $filaVend = mysqli_fetch_array($resultVend);
            if ($filaVend['Inventario'] == 'TRUE') {
                
            }else{
                $_SESSION['ValInventario'] = TRUE;
            }

            if ($filaVend['Compras'] == 'TRUE') {
                
            }else{
                $_SESSION['ValCompras'] = TRUE;
            }

            if ($filaVend['Control'] == 'TRUE') {
                
            }else{
                $_SESSION['ValControl'] = TRUE;
            }

            if ($filaVend['Reportes'] == 'TRUE') {
                
            }else{
                $_SESSION['ValReportes'] = TRUE;
            }

            if ($filaVend['Operaciones'] == 'TRUE') {
                
            }else{
                $_SESSION['ValOperaciones'] = TRUE;
            }


        } 
        else {
            
        }
         
                 
         ?>
      <!-- Encabezado -->
      <section id="cabecera">
         <div class="container" class="row">
            <div class="row" align="center">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h1>Bienvenido: <?php echo $profile;?></h1>
               </div>
            </div>
         </div>
         <br>
      </section>
      <div  align="center" id="FCorte">
      </div>


      <!-- Panel-->
      <section id="top">
         <div class="container">
            <div class="row" align="center">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h1 class="blanco"><strong>Panel De Control</strong></h1>
               </div>
            </div>
         </div>
      </section>
      <br>
      <section id="top">
         <div class="alert alert-danger alert-dismissible" id="alerta" align="center">
         </div>

         <!-- Menu -->

        <div class="container-fluid col-xs-12 col-sm-12 col-md-7 col-lg-7" align="center">

          <div id="menu" class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <ul>

               <li ><a href="<?php echo $dominio;?>tpv.php">Punto de Venta</a></li>
               
               <?php 
                     if (isset($_SESSION["Vendedor"])) {
                     
                     }else{?>
                        
                        <li><a href="<?php echo $dominio;?>Empresa.php">Empresa</a></li> <?php
                     }
                ?> 

                
                <?php 
                     if (isset($_SESSION["Vendedor"])) {
                     
                     }else{?>
                        
                        <li><a href="<?php echo $dominio;?>Informacion.php">Información</a></li> <?php
                     }
                ?>                      
                 

                    <li><a href="<?php echo $dominio;?>cotizacion.php">Cotización</a></li>
                    <li><a href="<?php echo $dominio;?>Apartados.php">Apartados</a></li>
                    <?php
                        if (isset($_SESSION['FacturaActiva'])) {?>
                            <li id="special"><a id="special" href="<?php echo $dominio;?>Facturacion.php" >Facturación</a></li>
                    <?php } else {?>
                            
                    <?php }
                    ?>
                    <!-- <li>Facturación</li> -->
                </ul>
                </div>
           
                <div id="menu" class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <ul>
                    

                    <?php  
                        if (isset($_SESSION['ValInventario'])) {
                            
                        }else{
                            ?><li ><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li> <?php 
                        }
                    ?>

                    <?php  
                        if (isset($_SESSION['ValCompras'])) {
                            
                        }else{
                            ?><li ><a href="<?php echo $dominio;?>compras.php">Compras</a></li> <?php 
                        }
                    ?>

                    <?php  
                        if (isset($_SESSION['ValControl'])) {
                            
                        }else{
                            ?><li ><a href="<?php echo $dominio;?>clients.php">Control</a></li> <?php 
                        }
                    ?>

                    <?php  
                        if (isset($_SESSION['ValReportes'])) {
                            
                        }else{
                            ?><li ><a href="<?php echo $dominio;?>Reportes.php">Reportes</a></li> <?php 
                        }
                    ?>

                    <?php  
                        if (isset($_SESSION['ValOperaciones'])) {
                            
                        }else{
                            ?><li ><a href="<?php echo $dominio;?>Operaciones.php">Operaciones</a></li> <?php 
                        }
                    ?>
                </ul>
                </div>      
        
        </div>              
           
        <div class="container col-xs-12 col-sm-12 col-md-4 col-lg-4" align="center">

            <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php
                     if ($fila48['img'] == 'ProImg/sinImg.jpg' ) {
                         echo "<img src='img/slogan.png' class='img-responsive'>";
                     } else {
                         echo "<img src='".$fila48['img']."' class='img-responsive'>";
                     }
                ?>
               
               <h4 class="blanco"><strong>V:2.0</strong></h4>
                <img src="img/facturacion.png" class="img-responsive"><br>
               <div class="row" align="center">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <input class="botonsito" type="button" onclick="location.href='logout.php' " value="Cerrar Sesión" name="boton" /> 
                  </div>
               </div>
               <br>
               <br>
               <br>
            </div>
        </div>
       
      </section>
      <br>
    
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>

</html>