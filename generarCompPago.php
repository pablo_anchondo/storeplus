<?php
    session_start();
    include("conexion.php");
    date_default_timezone_set('America/Mexico_City');
    $profile       = $_SESSION['user'];
    $Identificador = $_SESSION["Id_User"];
    $dominio       = $_SESSION["dominio"];
    $Almacen = $_SESSION['Almacen'];
    $BancoName = $_SESSION['bancoName'];
    $Datos = $_SESSION['Datos'];
    if ($Datos[0] == 1) {
        $Receptor = $Datos[1];
        $rfce = $Datos[2];
        $fecha = $Datos[3];
        $uuid = $Datos[4];
        $serie = $Datos[5];
        $folio = $Datos[6];
        $mp = $Datos[7];
        $importepagado = $Datos[8];
        $moneda = $Datos[9];
        $uso = $Datos[10];
        $fp = $Datos[11];
        $co = $Datos[12];
        $cb = $Datos[13];
        $rfcco = $Datos[14];
        $RfcEmisor = $Datos[15];
        $Insoluto = $Datos[16];
        $parcialidad = $Datos[17];
        $Adeudo = $Datos[18];
    }elseif ($Datos[0] == 2) {
        $Receptor = $Datos[1];
        $rfce = $Datos[2];
        $fecha = $Datos[3];
        $uuid = $Datos[4];
        $serie = $Datos[5];
        $folio = $Datos[6];
        $mp = $Datos[7];
        $importepagado = $Datos[8];
        $moneda = $Datos[9];
        $uso = $Datos[10];
        $fp = $Datos[11];
        $Insoluto = $Datos[12];
        $parcialidad = $Datos[13];
        $Adeudo = $Datos[14];
    }elseif ($Datos[0] == 3) {
        $Receptor = $Datos[1];
        $rfce = $Datos[2];
        $fecha = $Datos[3];
        $uuid = $Datos[4];
        $serie = $Datos[5];
        $folio = $Datos[6];
        $mp = $Datos[7];
        $importepagado = $Datos[8];
        $moneda = $Datos[9];
        $uso = $Datos[10];
        $fp = $Datos[11];
        $co = $Datos[12];
        $cb = $Datos[13];
        $rfcco = $Datos[14];
        $RfcEmisor = $Datos[15];
    }elseif ($Datos[0] == 4) {
        $Receptor = $Datos[1];
        $rfce = $Datos[2];
        $fecha = $Datos[3];
        $uuid = $Datos[4];
        $serie = $Datos[5];
        $folio = $Datos[6];
        $mp = $Datos[7];
        $importepagado = $Datos[8];
        $moneda = $Datos[9];
        $uso = $Datos[10];
        $fp = $Datos[11];
    }

    $id = 0;

    $fol = "SELECT MAX(id) as ident from facturas where Id_User = $Identificador AND Almacen = $Almacen AND Serie = '$serie'";
    $resultFol  = $cbd->query($fol);
    $ResId = mysqli_fetch_array($resultFol);
    if ($ResId['ident'] == NULL) {
        $id = 1;
    }else{
        $id = $ResId['ident'] + 1;
    }

    $folio = Crear($id);
    $xml_doc = new DOMDocument();
    $xml_doc->loadXML($folio);

    $queryDatos = "SELECT * FROM datosfactura WHERE Id_User = $Identificador AND Almacen = $Almacen";
    $resDatos  = $cbd->query($queryDatos);
    $datosFact    = mysqli_fetch_array($resDatos);
    
    $Ruta = $datosFact['src'];
    $cer = $Ruta.$datosFact['cert'];
    $key = $Ruta.$datosFact['keyy'];
    $cerpem = $Ruta."cer.pem";
    $keypem = $Ruta."key.pem";

    $contrato = $datosFact['Contrato'];
    $userpade = $datosFact['Usuario'];
    $passpade = $datosFact['Pass'];


    $cfdi_sellado = sellarXML($folio, $cer, $keypem);

    $client = new SoapClient("https://timbrado.pade.mx/servicio/Timbrado3.3?wsdl");

    $opciones[0] = "GENERAR_PDF";
    $cont = 1;
    
    if (isset($_SESSION['ordencompra'])) {
        $ordencompra = $_SESSION['ordencompra'];
        $addenda2 = base64_encode($ordencompra);
        $opciones[$cont] = "OBSERVACIONES:".$addenda2;
        $cont = $cont + 1;
    }else{
    
    }
    
    if (isset($_SESSION['mail'])) {
        $mail = $_SESSION['mail'];
        $opciones[$cont] = "ENVIAR_AMBOS:".$mail;
        $cont = $cont + 1;
    }else{
    
    }
    
    $params = array(
        "contrato" => $contrato,
        "usuario" => $userpade,
        "passwd" => $passpade,
        "cfdiXml" => $cfdi_sellado,
        "opciones" => $opciones,
    );

    $response = $client->__soapCall("timbrado", array($params));

    unset($_SESSION["mail"]);

    $xmlres = $response->return;

    $xml_doc = new DOMDocument();
    $xml_doc->loadXML($xmlres);

    $UUID = $xml_doc->getElementsByTagName('UUID'); 

    if (isset($UUID->item(0)->nodeValue)) {
        $idSat = $xml_doc->getElementsByTagName('id'); 
        $uid2 = (string)$UUID->item(0)->nodeValue;
        $msj = $xml_doc->getElementsByTagName('mensaje');       
        $selloSAT = $xml_doc->getElementsByTagName('selloSAT'); 
        $xmlBase64 = $xml_doc->getElementsByTagName('xmlBase64');
            
        $pdf64 = $xml_doc->getElementsByTagName('pdfBase64'); 
        $pdfFactura = (string)$pdf64->item(0)->nodeValue;

        $pdfFactura = base64_decode($pdfFactura);

        $rutaPDF = "Facturacion/PDF/Pagos/".$uid2.".pdf";
        $PdfName = $uid2.".pdf";
        $RutaPdfRaiz = $dominio."Facturacion/PDF/Pagos/";

        $RutaXMLRaiz = $dominio."Facturacion/XML/Pagos/";
        $XMLName = $uid2.".xml"; 

        if($archivo = fopen($rutaPDF, "w+")){
            fwrite($archivo, $pdfFactura);
            fclose($archivo);
        }

        $timbrado = base64_decode($xmlBase64->item(0)->nodeValue);
        $cfdi_final = new DOMDocument();
        $cfdi_final->loadXML($timbrado);
        $RrutaXML = "Facturacion/XML/Pagos/".$uid2.".xml";
        $cfdi_final->save($RrutaXML);

        $sql = "INSERT INTO facturas VALUES ($id, '$RutaPdfRaiz', '$PdfName', '$RutaXMLRaiz', '$XMLName','$Receptor', '$rfce', '$serie', '$id', 0.00, $importepagado , 0.00, 0.00, 'PAGO', '$fecha', 'PAGADO', '$uid2', '$uuid', '$mp', 0, 'VIGENTE', 'VIGENTE', 'VIGENTE', $Identificador, $Almacen)";
        $cbd->query($sql);


        $sql = "UPDATE facturas SET Abono = Abono + $importepagado, Adeudo = Adeudo - $importepagado, parcialidad = parcialidad + 1 WHERE Id_User = $Identificador AND Almacen = $Almacen AND UUID = '$uuid'";
        $cbd->query($sql);

        if ($mp == "PUE") {
            $sql = "UPDATE facturas SET Estado = 'PAGADO' WHERE Id_User = $Identificador AND Almacen = $Almacen AND UUID = '$uuid'";
            $cbd->query($sql);
        }
    
        unset($_SESSION["Datos"]);

        header("Location: ".$dominio."Facturacion/PDF/Pagos/".$uid2.".pdf");


        


    }else{
        $msj = $xml_doc->getElementsByTagName('mensaje'); 
        echo "Error";
        echo "<br>";
        echo utf8_decode($msj->item(0)->nodeValue);
    }




    function sellarXML($cfdi, $archivo_cer, $archivo_pem) {
        $private = openssl_pkey_get_private(file_get_contents($archivo_pem));
        $certificado = str_replace(array('\n', '\r'), '', base64_encode(file_get_contents($archivo_cer)));
    
        $xdoc = new DomDocument();
        $xdoc->loadXML($cfdi) or die("XML invalido");
    
        $c = $xdoc->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/3', 'Comprobante')->item(0); 
        $c->setAttribute('Certificado', $certificado);
    
    
        $XSL = new DOMDocument();
        $XSL->load('XSLT/cadenaoriginal_3_3.xslt');
        
        $proc = new XSLTProcessor;
        $proc->importStyleSheet($XSL);
    
        $cadena_original = $proc->transformToXML($xdoc);
    
        openssl_sign($cadena_original, $sig, $private, OPENSSL_ALGO_SHA256);
        $sello = base64_encode($sig);
        $c->setAttribute('Sello', $sello);
        
        return $xdoc->saveXML();
    }





    function Crear($Folioid){
        include("conexion.php");
        $BancoName = $_SESSION['bancoName'];
        $profile       = $_SESSION['user'];
        $Identificador = $_SESSION["Id_User"];
        $dominio       = $_SESSION["dominio"];
        $Almacen = $_SESSION['Almacen'];

        $Datos = $_SESSION['Datos'];
    if ($Datos[0] == 1) {
        $Receptor = $Datos[1];
        $rfce = $Datos[2];
        $fecha = $Datos[3];
        $uuid = $Datos[4];
        $serie = $Datos[5];
        $folio = $Datos[6];
        $mp = $Datos[7];
        $importepagado = $Datos[8];
        $moneda = $Datos[9];
        $uso = $Datos[10];
        $fp = $Datos[11];
        $co = $Datos[12];
        $cb = $Datos[13];
        $rfcco = $Datos[14];
        $RfcEmisor = $Datos[15];
        $Insoluto = $Datos[16];
        $parcialidad = $Datos[17];
        $Adeudo = $Datos[18];
    }elseif ($Datos[0] == 2) {
        $Receptor = $Datos[1];
        $rfce = $Datos[2];
        $fecha = $Datos[3];
        $uuid = $Datos[4];
        $serie = $Datos[5];
        $folio = $Datos[6];
        $mp = $Datos[7];
        $importepagado = $Datos[8];
        $moneda = $Datos[9];
        $uso = $Datos[10];
        $fp = $Datos[11];
        $Insoluto = $Datos[12];
        $parcialidad = $Datos[13];
        $Adeudo = $Datos[14];
    }elseif ($Datos[0] == 3) {
        $Receptor = $Datos[1];
        $rfce = $Datos[2];
        $fecha = $Datos[3];
        $uuid = $Datos[4];
        $serie = $Datos[5];
        $folio = $Datos[6];
        $mp = $Datos[7];
        $importepagado = $Datos[8];
        $moneda = $Datos[9];
        $uso = $Datos[10];
        $fp = $Datos[11];
        $co = $Datos[12];
        $cb = $Datos[13];
        $rfcco = $Datos[14];
        $RfcEmisor = $Datos[15];
    }elseif ($Datos[0] == 4) {
        $Receptor = $Datos[1];
        $rfce = $Datos[2];
        $fecha = $Datos[3];
        $uuid = $Datos[4];
        $serie = $Datos[5];
        $folio = $Datos[6];
        $mp = $Datos[7];
        $importepagado = $Datos[8];
        $moneda = $Datos[9];
        $uso = $Datos[10];
        $fp = $Datos[11];
    }
    
     
    
        $fecha_actual = str_replace(' ', 'T', date('Y-m-d H:i:s', (strtotime ("-1 Hours"))));
    
    
        $queryDatos = "SELECT * FROM datosfactura WHERE Id_User = $Identificador AND Almacen = $Almacen";
        $resDatos  = $cbd->query($queryDatos);
        $datosFact    = mysqli_fetch_array($resDatos);
    
    
        $xml = new DomDocument('1.0', 'UTF-8');
     
         $cfdi_Comprobante = $xml->createElement('cfdi:Comprobante');
         $cfdi_Comprobante = $xml->appendChild($cfdi_Comprobante);
         $cfdi_Comprobante->setAttribute('Certificado', '');
         $cfdi_Comprobante->setAttribute('Fecha', $fecha_actual);
         $cfdi_Comprobante->setAttribute('Folio', $Folioid);
         $cfdi_Comprobante->setAttribute('LugarExpedicion', $datosFact['ExpeditionPlace']);
         $cfdi_Comprobante->setAttribute('Moneda', 'XXX');
         $cfdi_Comprobante->setAttribute('NoCertificado', $datosFact['NumCertificado']);
         $cfdi_Comprobante->setAttribute('Sello', '');
         $cfdi_Comprobante->setAttribute('Serie', $serie);
         $cfdi_Comprobante->setAttribute('SubTotal', "0");
         $cfdi_Comprobante->setAttribute('TipoDeComprobante', "P");
         $cfdi_Comprobante->setAttribute('Total', "0");
         $cfdi_Comprobante->setAttribute('Version', '3.3');
         $cfdi_Comprobante->setAttribute('xmlns:cfdi', 'http://www.sat.gob.mx/cfd/3');
         $cfdi_Comprobante->setAttribute('xmlns:pago10', 'http://www.sat.gob.mx/pagos');
         $cfdi_Comprobante->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
         $cfdi_Comprobante->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd');
    
    
         $cfdi_Emisor = $xml->createElement('cfdi:Emisor');
         $cfdi_Emisor = $cfdi_Comprobante->appendChild($cfdi_Emisor);
         $cfdi_Emisor->setAttribute('Nombre', utf8_encode ($datosFact['Emisor']));
         $cfdi_Emisor->setAttribute('RegimenFiscal', $datosFact['RegimenFiscal']);
         $cfdi_Emisor->setAttribute('Rfc', $datosFact['RFC']);
    
         $cfdi_Receptor = $xml->createElement('cfdi:Receptor');
         $cfdi_Receptor = $cfdi_Comprobante->appendChild($cfdi_Receptor);
         $cfdi_Receptor->setAttribute('Nombre', $Receptor);
         $cfdi_Receptor->setAttribute('Rfc', $rfce);
         $cfdi_Receptor->setAttribute('UsoCFDI', $uso);
    
         $cfdi_Conceptos = $xml->createElement('cfdi:Conceptos');
         $cfdi_Conceptos = $cfdi_Comprobante->appendChild($cfdi_Conceptos);
    
            $cfdi_Concepto = $xml->createElement('cfdi:Concepto');
            $cfdi_Concepto = $cfdi_Conceptos->appendChild($cfdi_Concepto);
            $cfdi_Concepto->setAttribute('Cantidad', "1");
            $cfdi_Concepto->setAttribute('ClaveProdServ', "84111506");
            $cfdi_Concepto->setAttribute('ClaveUnidad', "ACT");
            $cfdi_Concepto->setAttribute('Descripcion', "Pago");
            $cfdi_Concepto->setAttribute('Importe', "0");
            $cfdi_Concepto->setAttribute('ValorUnitario',"0");

    
        $cfdi_Complemento = $xml->createElement('cfdi:Complemento');
        $cfdi_Complemento = $cfdi_Comprobante->appendChild($cfdi_Complemento);

        $pago10_Pagos = $xml->createElement('pago10:Pagos');
        $pago10_Pagos = $cfdi_Complemento->appendChild($pago10_Pagos);
        $pago10_Pagos->setAttribute('Version', "1.0");
        $pago10_Pagos->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $pago10_Pagos->setAttribute('xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');
        $pago10_Pagos->setAttribute('xmlns:pago10', 'http://www.sat.gob.mx/Pagos');

        $pago10_Pago = $xml->createElement('pago10:Pago');
        $pago10_Pago = $pago10_Pagos->appendChild($pago10_Pago);
        $pago10_Pago->setAttribute('FechaPago', $fecha);
        $pago10_Pago->setAttribute('FormaDePagoP', $fp);
        $pago10_Pago->setAttribute('MonedaP', "MXN");
        $int = intval($importepagado);
        $int = sprintf("%02d", $int);
        $final = $int.'.00';
        $pago10_Pago->setAttribute('Monto', number_format($importepagado, 2));
        
        if ($Datos[0] == 1 || $Datos[0] == 2) {
            $pago10_Pago->setAttribute('NumOperacion', $parcialidad);
        }
        if ($Datos[0] == 1 || $Datos[0] == 3) {
            $pago10_Pago->setAttribute('RfcEmisorCtaOrd', $rfcco);
            $pago10_Pago->setAttribute('CtaOrdenante', $co);
            $pago10_Pago->setAttribute('RfcEmisorCtaBen', $rfce);
            $pago10_Pago->setAttribute('CtaBeneficiario', $cb);
            $pago10_Pago->setAttribute('NomBancoOrdExt', $BancoName);

        }
        

        $pago10_DoctoRelacionado = $xml->createElement('pago10:DoctoRelacionado');
        $pago10_DoctoRelacionado = $pago10_Pago->appendChild($pago10_DoctoRelacionado);
        $pago10_DoctoRelacionado->setAttribute('IdDocumento', $uuid);
        $pago10_DoctoRelacionado->setAttribute('Serie', $serie);
        $pago10_DoctoRelacionado->setAttribute('Folio', $folio);
        $pago10_DoctoRelacionado->setAttribute('MonedaDR', "MXN");
        $pago10_DoctoRelacionado->setAttribute('MetodoDePagoDR', $mp);
        $pago10_DoctoRelacionado->setAttribute('ImpPagado', number_format($importepagado, 2));
        if ($Datos[0] == 1 || $Datos[0] == 2) {
            $pago10_DoctoRelacionado->setAttribute('NumParcialidad', $parcialidad);
            $pago10_DoctoRelacionado->setAttribute('ImpSaldoAnt', $Adeudo);
            $pago10_DoctoRelacionado->setAttribute('ImpSaldoInsoluto', $Insoluto);
        }

     return $xml->saveXML();
    
    }


?>