<?php
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión
	// if (isset($_SESSION['user'])) {
	// 	echo "";
	// } //isset($_SESSION['user'])
	// else {
	// 	echo '<script> window.location="index.php"; </script>';
	// }
	// // Determina si es administrador o vendedor
	// if (isset($_SESSION['ValInventario'])) {
	// 	echo '<script> window.location="index.php"; </script>';
	// } //isset($_SESSION['Vendedor'])
	// else {
	// 	echo "";
   //  }
    
   //  if (isset($_SESSION['Almacen'])) {
   //      echo "";
   //  } //isset($_SESSION['user'])
   //  else {
   //      echo '<script> window.location="VentanaAlmacen.php"; </script>';
   //  }
	// Inicializamos variables de sesión
	$profile       = $_SESSION['user'];
	$Identificador = $_SESSION["Id_User"];
	$dominio       = $_SESSION["dominio"];
?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/Productos.js"></script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
       <!--// Ejecuta lña función buscarDatos -->
      <script> buscarDatos()</script>
	   <!--// Navigation bar -->
      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li class="active"><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
                  <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
	  <?php
	    // Consultas para llenar los select
		include("conexion.php");
		$tabla      = 'productos';
		$cont       = 0;
		$querito    = 'select * from productos where Id_User = ' . $Identificador . ' order by descripcion ASC ';
		$result     = $cbd->query($querito);
		$querito2   = 'select * from unidades where Id_User = ' . $Identificador . '';
		$result2    = $cbd->query($querito2);
		$result3    = $cbd->query($querito2);
		$querito3   = 'select Impuesto from impuestos where Id_User = ' . $Identificador . '';
		$result4    = $cbd->query($querito3);
		$result5    = $cbd->query($querito3);
		$queryDep   = 'select * from departamentos where Id_User = ' . $Identificador . '';
		$resultDep  = $cbd->query($queryDep);
		$resultDepU = $cbd->query($queryDep);
	  ?>





    
    <div class="container-fluid">
            <div class="cabezera" align="center">
                   <h3 class="Titulo">Inventario</h3>
            </div>
            <div class="contenido">

                <div class="tablita table-responsive table-bordered contenido">
                <br>
                      
     

          <div class="bs-example" align="center">
          <ul class="nav nav-pills" role="tablist">
          <li role="presentation" class="dropdown active col-xs-12 col-lg-12" align="center"> <a href="#" class="dropdown-toggle" id="drop4" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Desplegar Opciones <span class="caret"></span> </a>
            <ul class="dropdown-menu col-xs-12 col-lg-12" align="center" id="menu1" aria-labelledby="drop4">
                <li><a href="#Unidades" data-toggle="modal" ><span class="icon-pencil"></span> Unidades</a></li>
                <li><a href="#Proveedor" data-toggle="modal" ><span class="icon-user-plus"></span>  Alta Proveedores</a></li>
                <li><a href="#Dpto" data-toggle="modal" ><span class="icon-books"></span>  Departamentos</a></li>
                <li><a href="<?php echo $dominio;?>Entradas.php" data-toggle="modal" ><span class="icon-redo2"></span>  Entradas</a></li>
                <li><a href="<?php echo $dominio;?>Salidas.php" data-toggle="modal" ><span class="icon-undo2"></span>  Salidas</a></li>
            </ul>
          </li>

          </div>

                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-8 col-lg-offset-8">
                        <br>
                        <div class="input-group">
                            <input class="col-xs-6 col-lg-6 form-control" placeholder="Buscar" id="caja_busqueda" name="caja_busqueda">
                            <span class="input-group-addon  color" data-toggle="modal"><i class="icon-search"></i></span>
                        </div>
                        <br>

                    </div>
                    
                    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" id="datos">
                  </div>

                </div>

            </div>
    </div>

    </div>

	  <!-- Modal de Agregar un producto -->
      <div class="container">
         <div class="modal fade" id="Agregar">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Agregar un producto</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaAdd" align="center">
                        <strong>Producto dado de alta</strong>
                     </div>
                     <div class="alert alert-danger alert-dismissible" id="alertaErrorAdd" align="center">
                     </div>
                     <table class="table table-striped">
                        <tr>
                           <th class="text3">Información del producto</th>
                           <th></th>
                        </tr>
                        <tr>
                           <td class="col-xs-6 col-lg-6"><label class="text3" > Clave </label></td>
                           <td class="col-xs-6 col-lg-6"><input placeholder="No permite espacios" class="form-control" id="art" onkeypress="return validaEspacio(event)"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Descripción </label></td>
                           <td><input class="form-control" id="desc" ></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Unidad </label></td>
                           <td>
                              <select class="form-control selecta col-xs-12 col-lg-12" id="unit">
                                 <?php while ($fila2 = mysqli_fetch_array($result2)){ ?>
                                 <option value="<?php echo $fila2['Unidad'];?>"><?php echo $fila2['Unidad'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Costo </label></td>
                           <td><input  placeholder="Máximo dos decimales " class="form-control" id="cost" onkeypress="return valida(event)"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Departamento </label></td>
                           <td>
                              <select class="form-control selecta col-xs-12 col-lg-12" id="Linea">
                                 <?php while ($filaDep = mysqli_fetch_array($resultDep)){ ?>
                                 <option value="<?php echo $filaDep['Departamento'];?>"><?php echo $filaDep['Departamento'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Precio </label></td>
                           <td><input placeholder="Máximo dos decimales " class="form-control" id="Precio" onkeypress="return valida(event)"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Impuesto </label></td>
                           <td>
                              <select class="form-control selecta col-xs-12 col-lg-12" id="impuesto" required>
                                 <?php while ($fila3 = mysqli_fetch_array($result4)){ ?>
                                 <option value="<?php echo $fila3['Impuesto'];?>"><?php echo $fila3['Impuesto'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Imagen </label></td>
                           </td>
                           <td>
                              <form method="POST" id="formulario" enctype="multipart/form-data">
                                 <input class="form-control" id="file" name="file" type="file" />
                              </form>
                           </td>
                        </tr>
                        <tr>
                           <td><button id="add" class="btn btn-success col-xs-12 col-lg-12" onclick="Add(this)">Agregar</button></td>
                           <td><button class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Salir</button></td>
                        </tr>
                     </table>
                     <br>
                     <br>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Modificar un producto -->
      <div class="container">
         <div class="modal fade" id="Modificar">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Modificar un producto</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaMod" align="center">
                        <strong>Producto Actualizado</strong>
                     </div>
                     <table class="table table-striped">
                        <tr>
                           <th class="text3 col-lg-6">Información del producto</th>
                           <th></th>
                        </tr>
                        <tr>
                           <td><label class="text3" > Clave </label></td>
                           <td><input class="form-control" id="art2" ></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Descripción </label></td>
                           <td><input class="form-control" id="desc2"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Unidad </label></td>
                           <td>
                              <select class="form-control selecta col-xs-12 col-lg-12" id="unit2">
                                 <?php while ($fila3 = mysqli_fetch_array($result3)){ ?>
                                 <option value="<?php echo $fila3['Unidad'];?>"><?php echo $fila3['Unidad'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Costo </label></td>
                           <td><input class="form-control" id="cost2" onkeypress="return valida(event)"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Departamento </label></td>
                           <td>
                              <select class="form-control selecta col-xs-12 col-lg-12" id="Linea2">
                                 <?php while ($filaDepU = mysqli_fetch_array($resultDepU)){ ?>
                                 <option value="<?php echo $filaDepU['Departamento'];?>"><?php echo $filaDepU['Departamento'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Precio </label></td>
                           <td><input  class="form-control" id="Precio2"  onkeypress="return valida(event)"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Impuesto </label></td>
                           <td>
                              <select class="form-control selecta col-xs-12 col-lg-12" id="impuesto2" required>
                                 <?php while ($fila5 = mysqli_fetch_array($result5)){ ?>
                                 <option value="<?php echo $fila5['Impuesto'];?>"><?php echo $fila5['Impuesto'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Imagen </label></td>
                           </td>
                           <td>
                              <form method="POST" id="formulario2" enctype="multipart/form-data">
                                 <input class="form-control" id="file2" name="file" type="file" />
                              </form>
                           </td>
                        </tr>
                        <tr>
                           <td><button id="update" name="update" class="btn btn-success col-xs-12 col-lg-12" onclick="Catch(this)">Actualizar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Salir</button></td>
                        </tr>
                     </table>
                     <br>
                     <br>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Eliminar un producto -->
      <div class="container">
         <div class="modal fade " id="Eliminar">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Eliminar un producto</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <h3>Seguro que desea eliminar</h3>
                     <br>
                     <div class="alert alert-success alert-dismissible" id="alertaDel" align="center">
                        <strong>Producto Eliminado</strong>
                     </div>
                     <table class="table">
                        <tr>
                           <td><button onclick="Catch(this)" id="delete" name="delete" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Unidad -->
      <div class="container">
         <div class="modal fade " id="Unidades">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Unidad</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaUni" align="center">
                        <strong>Unidad Agregada</strong>
                     </div>
                     <br>
                     <table class="table">
                        <tr>
                           <td><label class="text3"> Unidad </label></td>
                           <td><input class="form-control" id="unidades"></td>
                        </tr>
                        <tr>
                           <td class="col-xs-6 col-lg-6" onclick="addUnit()"><button class="btn btn-success col-xs-12 col-lg-12" >Agregar</button></td>
                           <td class="col-xs-6 col-lg-6"><button data-dismiss="modal" class="btn btn-danger col-xs-12 col-lg-12" >Cancelar</button></td>
                        </tr>
                     </table>

                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Proveedores -->
      <div class="container" align="center">
         <div class="modal fade " id="Proveedor">
            <div class="modal-dialog tamno-modal">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Proveedores</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaProv" align="center">
                        <strong>Proveedor Agregado</strong>
                     </div>
                     <div class="alert alert-danger alert-dismissible" id="alertaErrorProv" align="center">
                     </div>
                     <div class="alert alert-danger alert-dismissible" id="alertaMax" align="center">
                        <strong>Numero de caracteres invalido</strong>
                     </div>
                     <table class="table table-striped">
                        <tr>
                           <td><label class="text3"> Nombre </label></td>
                           <td><input class="form-control" id="NombreProv"></td>
                           <td><label class="text3"> País </label></td>
                           <td><input class="form-control" id="PaisProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Lada </label></td>
                           <td><input onkeypress="return validarTel(this, 3, event)" class="form-control" id="LadaP"></td>
                           <td><label class="text3"> Telefono </label></td>
                           <td><input onkeypress="return validarTel(this, 7, event)" class="form-control" id="TelProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Estado </label></td>
                           <td><input class="form-control" id="StateProv"></td>
                           <td><label class="text3"> Ciudad </label></td>
                           <td><input class="form-control" id="CityProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Colonia </label></td>
                           <td><input class="form-control" id="ColProv"></td>
                           <td><label class="text3"> Localidad </label></td>
                           <td><input class="form-control" id="PobProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Código Postal </label></td>
                           <td><input onkeypress="return valida(event)" class="form-control" id="CpProv"></td>
                           <td><label class="text3"> R.F.C </label></td>
                           <td><input onkeypress="return validarTamano(this, 13)" class="form-control" id="RfcProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> E-Mail </label></td>
                           <td><input class="form-control" id="MailProv"></td>
                           <td><label class="text3"> Sitio web  </label></td>
                           <td><input class="form-control" id="WebProv"></td>
                        </tr>
                        <tr>
                           <td><label class="text3 pull-left"> Observaciónes </label></td>
                           <td><textarea name="observ" id="observ" class="form-control" placeholder="Observaciónes"></textarea></td>
                        </tr>
                     </table>
                     <br>
                     <button onclick="InsertProv()" class="btn-success btn col-xs-5 col-lg-5 pull-left">Agregar</button>
                     <button data-dismiss="modal" class="btn-danger btn col-xs-5 col-lg-5 pull-right">Cancelar</button>
                     <br>
                     <br>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Impuestos -->
      <div class="container">
         <div class="modal fade " id="Impuestos">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Impuestos</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaImp" align="center">
                        <strong>Impuesto Agregado</strong>
                     </div>
                     <br>
                     <table class="table">
                        <tr>
                           <td><label class="text3"> Impuesto </label></td>
                           <td><input class="form-control" id="impu"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Descripción </label></td>
                           <td><input class="form-control" id="Impdesc"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Porcentaje % </label></td>
                           <td><input onkeypress="return valida(event)" class="form-control" id="Porcentaje"></td>
                        </tr>
                        <tr>
                           <td class="col-xs-6 col-lg-6"><button onclick="addImp()" class="btn btn-success col-xs-12 col-lg-12" >Agregar</button></td>
                           <td class="col-xs-6 col-lg-6"><button data-dismiss="modal" class="btn btn-danger col-xs-12 col-lg-12" >Cancelar</button></td>
                        </tr>
                     </table>
                     <br>
                     <br>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Departamentos -->
      <div class="container">
         <div class="modal fade " id="Dpto">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Departamentos</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaDep" align="center">
                        <strong>Departamento Agregado</strong>
                     </div>
                     <br>
                     <table class="table">
                        <tr>
                           <td><label class="text3"> Nombre del Departamento </label></td>
                           <td><input class="form-control" id="depto"></td>
                        </tr>
                        <tr>
                           <td class="col-xs-6 col-lg-6" ><button class="btn btn-success col-xs-12 col-lg-12" onclick="AddDpto()" >Agregar</button></td>
                           <td class="col-xs-6 col-lg-6"><button data-dismiss="modal" class="btn btn-danger col-xs-12 col-lg-12" >Cancelar</button></td>
                        </tr>
                     </table>

                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>