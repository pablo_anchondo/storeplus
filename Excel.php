<?php
    // Se incluye la librería de Excel
    require 'Classes/PHPExcel/IOFactory.php';
    include("conexion.php");
    session_start();
    // Inicializamos variables de sesión
    $Identificador = $_SESSION["Id_User"];
    $almacen       = $_SESSION["Almacen"];
    // Se evalúa si la variable esta vacía o no
    if (isset($_FILES["excel"])) {
        $file             = $_FILES["excel"];
        $tipo             = $file["type"];
        $ruta_provicional = $file["tmp_name"];
        $Nombre           = $file["name"];
        $carpeta          = "ProImg/";
        $queritoFecha     = 'SELECT CURDATE() AS Ingreso';
        $resultFecha      = $cbd->query($queritoFecha);
        $filaFecha        = mysqli_fetch_array($resultFecha);
        $Ingreso          = $filaFecha['Ingreso'];
        $bandera          = 1;
        // Se evalúa si el archivo es un Excel o no
        if ($tipo == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            $src = $carpeta . $Nombre;
            // Se crea un archivo temporal en el servidor
            move_uploaded_file($ruta_provicional, $src);
            $nombreArchivo = $src;
            // Se carga el Excel
            $objPHPExcel   = PHPExcel_IOFactory::load($nombreArchivo);
            $objPHPExcel->setActiveSheetIndex(0);
            $numRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $prueba  = $objPHPExcel->getActiveSheet()->getCell('A1')->getCalculatedValue();
            // Evaluamos si es el formato correcto
            if ($prueba == "CODIGO DE BARRAS") {
                // Determinamos si el archivo tiene elementos o no
                if ($numRows == 1) {
                    echo "Datos Importados";
                } else {
                    // Recorre las filas del Excel y las inserta en la tabla de productos
                    for ($i = 2; $i <= $numRows; $i++) {
                        $Nombrea      = $objPHPExcel->getActiveSheet()->getCell('A' . $i)->getCalculatedValue();
                        $Nombrea      = htmlspecialchars($Nombrea, ENT_QUOTES);
                        $descripcion  = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getCalculatedValue();
                        $descripcion  = htmlspecialchars($descripcion, ENT_QUOTES);
                        $unidad       = $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getCalculatedValue();
                        $costo        = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getCalculatedValue();
                        $Departamento = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getCalculatedValue();
                        $precio       = $objPHPExcel->getActiveSheet()->getCell('F' . $i)->getCalculatedValue();
                        $impuesto     = $objPHPExcel->getActiveSheet()->getCell('G' . $i)->getCalculatedValue();
                        $Existencia   = $objPHPExcel->getActiveSheet()->getCell('H' . $i)->getCalculatedValue();
                        $ProdSat      = $objPHPExcel->getActiveSheet()->getCell('I' . $i)->getCalculatedValue();
                        $UnidadSat    = $objPHPExcel->getActiveSheet()->getCell('J' . $i)->getCalculatedValue();
                        $sql          = "INSERT INTO productos (Nombre, descripcion, unidad, costo, Departamento, impuesto, precio, existencia, 
                                        Id_User, img, costoU, Almacen, claveProdServ, claveUnidad ) VALUES ('$Nombrea', '$descripcion', '$unidad', $costo, '$Departamento',
                                        '$impuesto', $precio, $Existencia, $Identificador, 'ProImg/sinImg.jpg', $costo, $almacen, '$ProdSat', '$UnidadSat')";
                        $cbd->query($sql);
                    }
                }
                // Pasamos a otra hojas para los clientes
                $objPHPExcel->setActiveSheetIndex(1);
                $numRows = $objPHPExcel->setActiveSheetIndex(1)->getHighestRow();
                if ($numRows == 1) {
                    echo "Datos Importados";
                } else {
                    for ($i = 2; $i <= $numRows; $i++) {
                        $Nombrec   = $objPHPExcel->getActiveSheet()->getCell('A' . $i)->getCalculatedValue();
                        $Pais      = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getCalculatedValue();
                        $cp        = $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getCalculatedValue();
                        $Calle     = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getCalculatedValue();
                        $Numero    = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getCalculatedValue();
                        $Colonia   = $objPHPExcel->getActiveSheet()->getCell('F' . $i)->getCalculatedValue();
                        $Estado    = $objPHPExcel->getActiveSheet()->getCell('G' . $i)->getCalculatedValue();
                        $Ciudad    = $objPHPExcel->getActiveSheet()->getCell('H' . $i)->getCalculatedValue();
                        $Localidad = $objPHPExcel->getActiveSheet()->getCell('I' . $i)->getCalculatedValue();
                        $Lada      = $objPHPExcel->getActiveSheet()->getCell('J' . $i)->getCalculatedValue();
                        $Telefono  = $objPHPExcel->getActiveSheet()->getCell('K' . $i)->getCalculatedValue();
                        $RFC       = $objPHPExcel->getActiveSheet()->getCell('L' . $i)->getCalculatedValue();
                        $Mail      = $objPHPExcel->getActiveSheet()->getCell('M' . $i)->getCalculatedValue();
                        $Observ    = $objPHPExcel->getActiveSheet()->getCell('N' . $i)->getCalculatedValue();
                        $sql       = "INSERT INTO clients (Nombre, Pais, cp, Calle, Numero, Colonia, Estado, Ciudad, 
                                     Localidad, Lada, Telefono, RFC, Mail, Observ, Ingreso, Id_User ) VALUES ('$Nombrec',
                                     '$Pais', $cp , '$Calle', '$Numero', '$Colonia', '$Estado', '$Ciudad',
                                     '$Localidad', $Lada, $Telefono, '$RFC', '$Mail', '$Observ', '$Ingreso', $Identificador)";
                        $cbd->query($sql);
                    }
                }
                echo "Datos Importados";
            } else {
                echo "Formato No Valido";
            }
            // Eliminamos el archivo temporal
            unlink($carpeta . $Nombre);
        } else {
            echo "No es un excel";
        }
    }
?>