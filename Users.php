<?php
	session_start();
	include("conexion.php");
	// Determina si es Administrador o no
	// if (isset($_SESSION['Bandera'])) {
	// 	echo "";
	// } //isset($_SESSION['Bandera'])
	// else {
	// 	echo '<script> window.location="index.php"; </script>';
	// }
	// // Determina si es vendedor o no
	// if (isset($_SESSION['Vendedor'])) {
	// 	echo '<script> window.location="index.php"; </script>';
	// } //isset($_SESSION['Vendedor'])
	// else {
	// 	echo "";
	// }
	// Inicializamos variables de sesión
	$profile       = $_SESSION['user'];
	$Identificador = $_SESSION["Id_User"];
	$dominio       = $_SESSION["dominio"];
?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/Users.js"></script>
      <script> buscarDatos()</script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>

    <?php
        $usu = "SELECT Id_User, Nombre FROM usuarios WHERE EstadoPV = 'TRUE' AND Admin = 'FALSE' ORDER BY Id_User";
        $resultusu = $cbd->query($usu);
    ?>


      <!--// Navigation bar -->
      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand">Store-Plus</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-right">
                  <li><a class="btn" href="#Factura" data-toggle="modal">Alta Facturación</a></li>
                  <li><a class="btn" onclick="liberar()">Liberar Espacio</a></li>
                  <li><a href="logout.php">Salir</a></li>
               </ul>
            </div>
         </div>
      </nav>


            <div class="alert alert-success alert-dismissible" id="alerta" align="center">
         <strong>Datos del cliente modificados</strong>
      </div>
      <div class="alert alert-success alert-dismissible" id="alertaAdd" align="center">
         <strong>Cliente Dado de alta</strong>
      </div>
      <div class="alert alert-success alert-dismissible" id="alertaDel" align="center">
         <strong>Cliente Dado de baja</strong>
      </div>

    <div class="container-fluid">
        <div class="cabezera" align="center">
            <h3 class="Titulo">Control de Usuarios</h3>
        </div>
        <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
                <br>
            <div class="container-fluid">
                  <div class="col-xs-10 col-lg-10" align="left">
                     <input class="col-xs-6 col-lg-6 form-control" placeholder="Buscar" id="caja_busqueda2" name="caja_busqueda">
                  </div>
                  <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 "> 
                     <a href="#Agregar" data-toggle="modal" class="btn btn-success col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-right"><span class="icon-plus"></span></a>
                  </div>
               </div>
               <br>
               <div class="container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                  <div class="table-responsive tabProds">
                     <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" id="datosUsuario">
                     </div>
                  </div>
                  <br><br>
               </div>
               
            </div>
        </div>
    </div>




	  <!-- Modal de Agregar -->
      <div class="container">
      <div class="modal fade " id="Agregar">
         <div class="modal-dialog tamno-modal">
            <div class="modal-content">
               <div class="modal-header panel-header color">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title" align="center">Agregar Usuario</h2>
               </div>
               <div class="modal-body cuerpoM" align="center">
                  <div class="modal-body" align="center">
                     <div class="alert alert-danger alert-dismissible" id="alertaMax13" align="center">
                        <strong>Numero de caracteres invalido</strong>
                     </div>
                     <table class="table table-striped">
                        <tr>
                           <td>Usuario</td>
                           <td><input class="form-control" id="userU"></td>
                           <td>Contraseña</td>
                           <td><input type="password" class="form-control" id="PassW"></td>
                        </tr>
                        <tr>
                           <td>Lada</td>
                           <td><input onkeypress="return validarTel(this, 3, event)" class="form-control" id="Lada"></td>
                           <td>Telefono</td>
                           <td><input onkeypress="return validarTel(this, 7, event)" class="form-control" id="TelC"></td>
                        </tr>
                        <tr>
                           <td>Nombre</td>
                           <td><input class="form-control" id="NombreC"></td>
                           <td>Nombre Fiscal</td>
                           <td><input class="form-control" id="NombreFiscal"></td>
                        </tr>
                        <tr>
                           <td>Nombre Comercial</td>
                           <td><input  class="form-control" id="NombreComercial"></td>
                           <td>País</td>
                           <td><input class="form-control" id="PaisC"></td>
                        </tr>
                        <tr>
                           <td>C.P</td>
                           <td><input onkeypress="return validaR(event)" class="form-control" id="cpC"></td>
                           <td>Numero</td>
                           <td><input onkeypress="return validaR(event)" class="form-control" id="NumeroC"></td>
                        </tr>
                        <tr>
                           <td>Calle</td>
                           <td><input class="form-control" id="CalleC"></td>
                           <td>Estado</td>
                           <td><input class="form-control" id="StateC"></td>
                        </tr>
                        <tr>
                           <td>Ciudad.</td>
                           <td><input class="form-control" id="CityC"></td>
                           <td>Localidad</td>
                           <td><input class="form-control" id="LocC"></td>
                        </tr>
                        <tr>
                           <td>Col.</td>
                           <td><input class="form-control" id="ColC"></td>
                           <td>RFC.</td>
                           <td><input onkeypress="return validarTamano(this, 13)" class="form-control" id="RFCC"></td>
                        </tr>
                        <tr>
                           <td>E-Mail</td>
                           <td><input class="form-control" id="MailC"></td>
                           <td>Entre Calles</td>
                           <td><input class="form-control" id="EntreCalles"></td>
                        </tr>
                     </table>
                     <label class="text3 pull-left"> Observaciónes </label>
                     <textarea name="observ" id="observ" class="form-control" placeholder="Observaciónes"></textarea>
                     <table class="table">
                        <tr>
                           <td><button onclick="AddClient()" id="add" name="add" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Modificar -->
      <div class="container">
         <div class="modal fade " id="Modificar">
            <div class="modal-dialog tamno-modal">
               <div class="modal-content">
                  <div class="modal-header panel-header color">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Modificar Usuario</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <div class="alert alert-danger alert-dismissible" id="alertaMax13U" align="center">
                        <strong>Numero de caracteres invalido</strong>
                     </div>
                     <table class="table table-striped">
                        <tr>
                           <td>Usuario</td>
                           <td><input class="form-control" id="usu"></td>
                           <td>Contraseña</td>
                           <td><input type="password" class="form-control" id="pass"></td>
                        </tr>
                        <tr>
                           <td>Lada</td>
                           <td><input  onkeypress="return validarTel(this, 3, event)" class="form-control" id="LadaM"></td>
                           <td>Telefono</td>
                           <td><input  onkeypress="return validarTel(this, 7, event)" class="form-control" id="TelCM"></td>
                        </tr>
                        <tr>
                           <td>Nombre</td>
                           <td><input class="form-control" id="NombreCM"></td>
                           <td>Nombre Fiscal</td>
                           <td><input class="form-control" id="NombreFUdt"></td>
                        </tr>
                        <tr>
                           <td>Nombre Comercial</td>
                           <td><input  class="form-control" id="NombreCUdt"></td>
                           <td>País</td>
                           <td><input class="form-control" id="PaisCM"></td>
                        </tr>
                        <tr>
                           <td>C.P</td>
                           <td><input onkeypress="return validaR(event)" class="form-control" id="cpCM"></td>
                           <td>Calle</td>
                           <td><input class="form-control" id="CalleCM"></td>
                        </tr>
                        <tr>
                           <td>Numero</td>
                           <td><input onkeypress="return validaR(event)" class="form-control" id="NumeroCM"></td>
                           <td>Ciudad.</td>
                           <td><input class="form-control" id="CityCM"></td>
                        </tr>
                        <tr>
                           <td>Estado</td>
                           <td><input class="form-control" id="StateCM"></td>
                           <td>Localidad</td>
                           <td><input class="form-control" id="LocCM"></td>
                        </tr>
                        <tr>
                           <td>Col.</td>
                           <td><input class="form-control" id="ColCM"></td>
                           <td>RFC.</td>
                           <td><input onkeypress="return validarTamano(this, 13)" class="form-control" id="RFCCM"></td>
                        </tr>
                        <tr>
                           <td>E-Mail</td>
                           <td><input class="form-control" id="MailCM"></td>
                           <td>Entre Calles</td>
                           <td><input class="form-control" id="EntreCudt"></td>
                        </tr>
                        <tr>
                           <td>Fecha Corte.</td>
                           <td><input class='form-control' type='Date' id='fechaCort'></td>
                        </tr>
                     </table>
                     <label class="text3 pull-left"> Observaciónes </label>
                     <textarea name="observM" id="observM" class="form-control" placeholder="Observaciónes"></textarea>
                     <table class="table">
                        <tr>
                           <td><button id="ModificarC" onclick="UpdateCliente()" name="mod" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Eliminar -->
      <div class="container">
         <div class="modal fade " id="Eliminar">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header color">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Baja Usuario</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <h3>Seguro que desea dar de baja el cliente</h3>
                     <br>
                     <table class="table">
                        <tr>
                           <td><button  id="delete" onclick="deleteClient()" name="delete" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>

    <div class="container">
         <div class="modal fade " id="Factura">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header color">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Alta Facturación</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">

                     <table class="table">
                        <tr>
                           <td class="col-lg-6">Cliente:</td>
                           <td>
                                <select class="form-control" id="ClienteEmisor" onchange="almacenes(this)">
                                    <option value="empty">Seleccionar Cliente</option>
                                    <?php while ($usuarios = mysqli_fetch_array($resultusu)){ ?>
                                        <option value="<?php echo $usuarios['Id_User'];?>"><?php echo $usuarios['Nombre'];?></option>
                                    <?php } ?>
                                </select>
                           </td>
                        </tr>
                                        
                         <tr>
                            <td>Almacen:</td>
                            <td>
                                <div id="almacenEmisor">
                                    <input trpe="text" class="form-control">
                                </div>
                            </td>
                         </tr>               

                        <tr>
                            <td class="col-lg-6">Archivos CER y KEY:</td>
                            <td>
                                <form method="POST" id="formulariocer" enctype="multipart/form-data">
                                    <input class="form-control" id="filecer" name="filecer" type="file" />
                                </form>
                            </td>
                        </tr>

                        <tr>
                           <td class="col-lg-6">KEY Password:</td>
                           <td><input class="form-control" id="passkey"></td>
                        </tr>

                        <tr>
                           <td class="col-lg-6">Emisor:</td>
                           <td><input class="form-control" id="Emisor"></td>
                        </tr>

                        <tr>
                           <td class="col-lg-6">RFC:</td>
                           <td><input class="form-control" id="rfcEmisor"></td>
                        </tr>

                        <tr>
                           <td class="col-lg-6">Numero Certificado:</td>
                           <td><input class="form-control" id="NoCert"></td>
                        </tr>

                        <tr>
                           <td class="col-lg-6">Contrato PADE:</td>
                           <td><input class="form-control" id="contrato"></td>
                        </tr>

                        <tr>
                           <td class="col-lg-6">Usuario PADE:</td>
                           <td><input class="form-control" id="userpade"></td>
                        </tr>

                        <tr>
                           <td class="col-lg-6">Contraseña PADE:</td>
                           <td><input class="form-control" id="pasaspade"></td>
                        </tr>

                        <tr>
                           <td><button  id="delete" onclick="AltaDatos()" name="delete" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-warning col-xs-12 col-lg-12" onclick="getLlaves()">generarPEM</button></td>
                        </tr>
                        
                        <tr>
                            <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                            <td><button  class="btn btn-info col-xs-12 col-lg-12" onclick="Calcular()">Calcular</button></td>

                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>