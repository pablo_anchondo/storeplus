<?php
session_start();
include("conexion.php");
// Determina si se ha iniciado sesión 
if (isset($_SESSION['user'])) {
    echo "";
} else {
    echo '<script> window.location="index.php"; </script>';
}

if (isset($_SESSION['FacturaActiva'])) {
    echo "";
} else {
    echo '<script> window.location="index.php"; </script>';
}

date_default_timezone_set('America/Mexico_City');


$profile       = $_SESSION['user'];
$Identificador = $_SESSION["Id_User"];
$dominio       = $_SESSION["dominio"];
$Almacen       = $_SESSION["Almacen"];

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/select2-bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/select2.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/ComplementosPago.js" ></script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
</head>
<body>

     <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>          
                  <li class="active"><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>


    <?php

         $queryClients    = "SELECT RFC, Nombre FROM clients  WHERE Id_User = $Identificador";
         $resultClients     = $cbd->query($queryClients);

         $queryCRFC    = "SELECT RFC FROM datosfactura  WHERE Id_User = $Identificador AND Almacen = $Almacen";
         $resultRFC    = $cbd->query($queryCRFC);
         $filaRFC = mysqli_fetch_array($resultRFC);

         $FPago    = 'select * from sat_formapago';
         $resultFP    = $cbd->query($FPago);

         $MPago    = 'select * from sat_mpago';
         $resultMP    = $cbd->query($MPago);

         $Bancos    = 'select * from bancos';
         $resultbanco    = $cbd->query($Bancos);

    ?>
         <div class="bs-example" align="center">
            <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="dropdown active col-xs-12 col-lg-12" align="center">
               <a href="#" class="dropdown-toggle" id="drop4" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Desplegar Opciones <span class="caret"></span> </a>
               <ul class="dropdown-menu col-xs-12 col-lg-12" align="center" id="menu1" aria-labelledby="drop4">
                  <li><a href="Mapeo.php">Mapeo Productos</a></li>
                  <li><a href="MapeoUnidad.php">Mapeo Unidades</a></li>
                  <li><a href="datosFactura.php">Datos Factura</a></li>
                  <li><a href="ganeraCFDI.php">Generar CFDI</a></li>
                  <li><a href="consultarCFDI.php">Consultar CFDI</a></li>
                  <li><a href="ComplementosPago.php">Complementos de pago</a></li>
               </ul>
            </li>
         </div>
      

    <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Tabla Productos</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-8 col-lg-offset-8">
                    <br>
                    <div class="input-group">
                        <input class="col-xs-6 col-lg-6 form-control" placeholder="Buscar" id="caja_busqueda" name="caja_busqueda">
                        <span class="input-group-addon  color" data-toggle="modal"><i class="icon-search"></i></span>
                    </div>
                    <br>
                </div>
                <div id="TablaPPD" >
                </div>
            </div>
        </div>
    </div>


      <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Receptor</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
                    <br>


                    <div class="container-fluid">
                        <div class="form-group">
                            <label class="control-label  col-lg-2">
                                <h4 class="textoBlack"><strong><i class="icon-user-tie"></i> Receptor:</strong></h4>
                            </label>
                            <div class="col-lg-4">
                                <select id="NombreR" class="form-control" onchange="getRFC(this)">
                                    <option value="nulo">SELECCIONAR CLIENTE</option>
                                    <?php while ($filaclients = mysqli_fetch_array($resultClients)){ ?>
                                        <option value="<?php echo $filaclients['RFC'];?>"><?php echo $filaclients['Nombre'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">
                                <h4 class="textoBlack"><strong><i class="icon-key"></i> RFC:</strong></h4>
                                </label>
                                <div class="col-lg-4">
                                    <input type="text" id="rfce" class="form-control" disabled >
                                </div>
                            </div>
                        </div>
                    </div>
               
                    <div class="container-fluid">
                        <div class="form-group">
                            <label class="control-label  col-lg-2">
                                <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Moneda:</strong></h4>
                            </label>
                            <div class="col-lg-4">
                                <input type="text" id="moneda" class="form-control" value="XXX" disabled>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">
                                <h4 class="textoBlack"><strong><i class="icon-address-book"></i> Uso de CFDI:</strong></h4>
                                </label>
                                <div class="col-lg-4">
                                    <input type="text" id="uso" class="form-control" value="P01" disabled>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="container-fluid">
                        <div class="form-group">
                            <label class="control-label  col-lg-2">
                                <h4 class="textoBlack"><strong><i class="icon-envelop"></i> E-Mail:</strong></h4>
                            </label>
                            <div class="col-lg-4">
                                <input type="mail" id="mail" class="form-control" >
                            </div>
                            <div class="form-group">

                            </div>
                        </div>
                    </div>


                </div>
            </div>
      </div>


<div class="container-fluid">
    <div class="cabezera" align="center">
        <h3 class="Titulo">Complementos de pago</h3>
    </div>
    <div class="contenido">
        <div class="tablita table-responsive table-bordered contenido">
            <br>
            <div class="container-fluid">
                        <div class="form-group">
                            <label class="control-label  col-lg-2">
                                <h4 class="textoBlack"><strong><i class="icon-calendar"></i> Fecha:</strong></h4>
                            </label>
                            <div class="col-lg-4">
                                	
                                <input type="date" id="fecha" class="form-control" step="1" value="<?php echo date("Y-m-d");?>">
                        
                            </div>
                            <div class="form-group">
                            <label class="control-label  col-lg-2">
                                <h4 class="textoBlack"><strong><i class="icon-credit-card"></i> Forma de Pago:</strong></h4>
                            </label>
                            <div class="col-lg-4">
                                <select class="form-control" id="fp" onchange="aparecer(this)">
                                    <?php while ($filaFP = mysqli_fetch_array($resultFP)){ ?>
                                    <option value="<?php echo $filaFP['forma'];?>"><?php echo $filaFP['descripcion'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            </div>
                        </div>
                    </div>


                    <div class="container-fluid">
                        <div class="form-group">

                            <div class="form-group">
                                <label class="control-label col-lg-2">
                                <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Monto:</strong></h4>
                                </label>
                                <div class="col-lg-4">
                                    <input type="text" id="monto" class="form-control" onkeypress="return valida(event)">
                                </div>
                            </div>
                        </div>
                    </div>
                   

                    <div class="container-fluid" id="tarjeta1">

                    <div class="form-group col-lg-3" >
                        <h4 class="textoBlack"><strong> Cuenta Ordenante:</strong></h4>
                        <input type="text" id="co" class="form-control"  onkeypress="return validarTel(this, 16, event)" placeholder="Cuenta de 16 caracteres">
                    </div>

                    <div class="form-group col-lg-3" >
                        <h4 class="textoBlack"><strong> RFC Banco Ordenante:</strong></h4>
                        <select id="rfcco" class="form-control">
                                    <?php while ($filabanco = mysqli_fetch_array($resultbanco)){ ?>
                                        <option value="<?php echo $filabanco['RFC'];?>"><?php echo $filabanco['Banco'];?></option>
                                    <?php } ?>
                        </select>

                    </div>
                            
    

            
                   

                    <div class="container-fluid" id="tarjeta2">

                    <div class="form-group col-lg-3" >
                        <h4 class="textoBlack"><strong> RFC Emisor:</strong></h4>
                        <input type="text" id="RfcEmisor" class="form-control" onkeypress="return validarTamano(this, 13)" placeholder="RFC Emisor" value="<?php echo $filaRFC['RFC']; ?>" disabled>
                    </div>
                    
                    <div class="form-group col-lg-3" >
                        <h4 class="textoBlack"><strong> Cuenta Beneficiario:</strong></h4>
                        <input type="text" id="cb" class="form-control" onkeypress="return validarTel(this, 16, event)" placeholder="Cuenta Beneficiario">
                    </div>

                          </div>
    </div>  
    
        </div>
    </div>
</div>




<div class="container-fluid">
    <div class="cabezera" align="center">
        <h3 class="Titulo">Relación de Pagos</h3>
    </div>
    <div class="contenido">
        <div class="tablita table-responsive table-bordered contenido">
            <br>
            <div class="container-fluid">
                <div class="form-group col-lg-4" >
                    <h4 class="textoBlack"><strong><i class="icon-key2"></i> Id Documento:</strong></h4>
                    <input type="text" class="form-control" id="uuid" placeholder="Id Documento" disabled>
                </div>
                <div class="form-group col-lg-2" >
                    <h4 class="textoBlack"><strong><i class="icon-stack"></i> Serie:</strong></h4>
                    <input type="text" class="form-control" id="serie" placeholder="Serie" disabled>
                </div>
                <div class="form-group col-lg-2" >
                    <h4 class="textoBlack"><strong><i class="icon-list-numbered"></i> Folio:</strong></h4>
                    <input type="text" class="form-control" id="folio" placeholder="Folio" disabled>
                </div>
                <div class="form-group col-lg-4" >
                    <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Metodo de Pago:</strong></h4>
                    <select class="form-control" id="mp" onchange="mostrarOpciones(this)">
                        <option value="nulo">Seleccionar Metodo de Pago</option>
                        <?php while ($filaMP = mysqli_fetch_array($resultMP)){ ?>
                        <option value="<?php echo $filaMP['Metodo'];?>"><?php echo $filaMP['Descripcion'];?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="container-fluid">
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-coin-dollar"></i> Importe Pagado:</strong></h4>
                    <input type="text" class="form-control" id="importepagado"  onblur="insoluto(this)" onkeypress="return valida(event)" placeholder="Monto a Pagar">
                </div>

                <div id="ppdDisabled">
                    <div class="form-group col-lg-3" >
                        <h4 class="textoBlack"><strong><i class="icon-meter"></i> Numero Parcialidad:</strong></h4>
                        <input type="text" class="form-control" id="parcialidad" disabled>
                    </div>

                    <div class="form-group col-lg-3" >
                        <h4 class="textoBlack"><strong><i class="icon-credit-card"></i> Saldo Anterior:</strong></h4>
                        <input type="text" class="form-control" id="saldoAnterior" disabled>
                    </div>

                    <div class="form-group col-lg-3" >
                        <h4 class="textoBlack"><strong><i class="icon-sort-amount-desc"></i> Saldo Insoluto:</strong></h4>
                        <input type="text" class="form-control" id="Insoluto" disabled>
                    </div>
                    
                </div>


            </div>
            
        </div>
    </div>
</div>
<div class="container">
    <button class="btn btn-lg bg col-xs-12 col-lg-4 col-lg-offset-4" onclick="generarComplemento()"><i class="icon-file-text"></i> Generar documento de Pago</button>
</div>


<br>
<br>
<br>
<br>






      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/select2.min.js" ></script>
      <script type="text/javascript" src="js/select2_locale_es.js" ></script>
      <script>
            $(document).ready(function() { 
              $("#rfcco").select2({

              });

            $("#NombreR").select2({
                
              });
            });
      </script>
</body>
</html>