<?php
    // Incluimos la librería de PDF
	require('fpdf/fpdf.php');
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión
	if (isset($_SESSION['user'])) {
	} //isset($_SESSION['user'])
	else {
		echo '<script> window.location="index.php"; </script>';
	}
	// Determina si se ha iniciado la cobranza
	if (isset($_SESSION['Cobranza'])) {
	} //isset($_SESSION['Cobranza'])
	else {
		echo '<script> window.location="menu.php"; </script>';
	}
	class PDF_JavaScript extends FPDF
	{
		protected $javascript;
		protected $n_js;
		function IncludeJS($script, $isUTF8 = false)
		{
			if (!$isUTF8)
				$script = utf8_encode($script);
			$this->javascript = $script;
		}
		function _putjavascript()
		{
			$this->_newobj();
			$this->n_js = $this->n;
			$this->_put('<<');
			$this->_put('/Names [(EmbeddedJS) ' . ($this->n + 1) . ' 0 R]');
			$this->_put('>>');
			$this->_put('endobj');
			$this->_newobj();
			$this->_put('<<');
			$this->_put('/S /JavaScript');
			$this->_put('/JS ' . $this->_textstring($this->javascript));
			$this->_put('>>');
			$this->_put('endobj');
		}
		function _putresources()
		{
			parent::_putresources();
			if (!empty($this->javascript)) {
				$this->_putjavascript();
			} //!empty($this->javascript)
		}
		function _putcatalog()
		{
			parent::_putcatalog();
			if (!empty($this->javascript)) {
				$this->_put('/Names <</JavaScript ' . ($this->n_js) . ' 0 R>>');
			} //!empty($this->javascript)
		}
	}
	class PDF_AutoPrint extends PDF_JavaScript
	{
		function AutoPrint($printer = '')
		{
			// Open the print dialog
			if ($printer) {
				$printer = str_replace('\\', '\\\\', $printer);
				$script  = "var pp = getPrintParams();";
				$script .= "pp.interactive = pp.constants.interactionLevel.full;";
				$script .= "pp.printerName = '$printer'";
				$script .= "print(pp);";
			} //$printer
			else
				$script = 'print(true);';
			$this->IncludeJS($script);
		}
	}
	// Inicializamos variables de sesión
	$profile       = $_SESSION['user'];
	$Identificador = $_SESSION["Id_User"];
	$Cobranza      = $_SESSION['Cobranza'];
	$queryPart     = 'select * from partcobranza where Id_User = ' . $Identificador . ' AND Cobranza = ' . $Cobranza;
	$ResPart       = $cbd->query($queryPart);
	$queryVenta    = 'select * from cobranza where Id_User = ' . $Identificador . ' AND id = ' . $Cobranza;
	$ResVenta      = $cbd->query($queryVenta);
	$filaVenta     = mysqli_fetch_array($ResVenta);
	$Almacen = $_SESSION["Almacen"];
	$queryEmp = 'select * from empresa where Id_User = ' . $Identificador. ' AND Almacen = '. $Almacen;
	$ResEmp        = $cbd->query($queryEmp);
	$filaEmp       = mysqli_fetch_array($ResEmp);
	// Se crea el PDF
	$pdf           = new PDF_AutoPrint();
	// Agrega nueva página
	$pdf->AddPage();
	$pdf->SetFont('Arial', 'B', 6);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Empresa ' . utf8_decode($filaEmp['Nombre']));
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Direccion ' . utf8_decode($filaEmp['Direccion']));
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Cobranza No: ' . $Cobranza);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Fecha: ' . $filaVenta['Fecha']);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Hora: ' . $filaVenta['Hora']);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, 'Cliente ' . utf8_decode($filaVenta['Cliente']));
	$pdf->Ln(10);
	$pdf->SetX(2);
	$pdf->Cell(4, 6, 'Cant', 0, 0, 'C');
	$pdf->Cell(9, 6, 'Precio', 0, 0, 'C');
	$pdf->Cell(9, 6, 'Total', 0, 0, 'C');
	$pdf->Cell(22, 6, 'Descripcion', 0, 1, 'C');
	while ($filaPart = mysqli_fetch_array($ResPart)) {
		// Se llenan las partidas
		$pdf->SetX(2);
		$pdf->Cell(4, 6, $filaPart['Cantidad'], 0, 0, 'C');
		$pdf->Cell(9, 6, $filaPart['Precio'], 0, 0, 'C');
		$pdf->Cell(9, 6, $filaPart['Importe'], 0, 0, 'C');
		$pdf->MultiCell(22, 6, utf8_decode($filaPart['Descripcion']), 0, 'C', 0);
	} //$filaPart = mysqli_fetch_array($ResPart)
	$pdf->SetX(2);
	$pdf->Cell(20, 10, 'Importe');
	$pdf->Cell(20, 10, '$' . $filaVenta['Importe']);
	$pdf->Ln(5);
	$pdf->SetX(2);
	$pdf->Cell(20, 10, 'Impuesto');
	$pdf->Cell(20, 10, '$' . $filaVenta['Impuesto']);
	$pdf->Ln(5);
	$pdf->SetX(2);
	$pdf->Cell(20, 10, 'Total');
	$pdf->Cell(20, 10, '$' . $filaVenta['Total']);
	$pdf->Ln(5);
	$pdf->SetX(2);
	$pdf->Cell(20, 10, 'Abono');
	$pdf->Cell(20, 10, '$' . $filaVenta['Abono']);
	$pdf->Ln(5);
	$pdf->SetX(2);
	$pdf->Cell(20, 10, 'Adeudo');
	$pdf->Cell(20, 10, '$' . $filaVenta['Adeudo']);
	$pdf->Ln(5);
	$pdf->SetX(2);
	$pdf->Cell(20, 10, 'Vencimiento');
	$pdf->Cell(20, 10, $filaVenta['Vencimiento']);
	$pdf->Ln(10);
	$pdf->SetX(2);
	$pdf->Cell(12, 10, 'Metodo');
	$pdf->Cell(20, 10, $filaVenta['MPago']);
	$pdf->Ln(18);
	$pdf->SetX(2);
	$pdf->Cell(15, 10, '----------------------------------------------');
	$pdf->Ln(7);
	$pdf->Cell(38, 5, 'Firma Cliente', 0, 'C');
	$pdf->Ln(15);
	$pdf->SetX(1);
	$pdf->MultiCell(38, 5, $filaEmp['TextoTicket'], 0, 'C');
	$pdf->Ln(5);
	// Se llama la función para imprimir
	$pdf->AutoPrint();
	// Se muestra el PDF en pantalla
	$pdf->Output();
?>