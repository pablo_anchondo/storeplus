<?php  
   session_start();
   include("conexion.php");
   // Determina si se ha iniciado sesión
   if (isset($_SESSION['user'])) {  
   	echo "";
   }else{
   	echo '<script> window.location="index.php"; </script>';
   }
   // Determina si es administrador o vendedor
   if (isset($_SESSION['Vendedor'])) { 
   	echo '<script> window.location="index.php"; </script>';
   }else{
   	echo "";
   }
   // Inicializamos variables de sesión
   $profile = $_SESSION['user'];
   $Identificador = $_SESSION["Id_User"];
   $dominio = $_SESSION["dominio"];
   
   ?>
<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="img/favicon.ico">
   <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="css/estilos.css">
   <link rel="stylesheet" type="text/css" href="fonts/style.css">
   <link rel="stylesheet" type="text/css" href="css/paneles.css">
   <link rel="stylesheet" type="text/css" href="css/navbar.css">
   <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
   <link rel="stylesheet" type="text/css" href="css/Tablas.css">
   <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
   <script src="js/jQuery v3.3.1.js"></script>
   <script src="js/jquery.js"></script>
   <script type="text/javascript" src="js/Compras.js" ></script>
   <title>Store-Plus</title>
</head>
<body>
     <?php 
        // Consultas para llenar los select
            $QueryCompras = 'select * from compras where Id_User = '.$Identificador.' AND Almacen = '.$_SESSION["Almacen"].' order by Compra ASC ';
            $resultCompras = $cbd->query($QueryCompras);
      ?>
   <!--// Navigation bar -->
   <nav class="navbar navbar-default navbar-fixed-static navcolor">
      <div class="container-fluid">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="menu.php"><img src="img/favicon.ico"></a>
         </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
               <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
               <li ><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
               <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
               <li class="active"><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
               <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
               <li ><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
               <li><a href="<?php echo $dominio;?>clients.php" > Clientes</a></li>
               <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
               <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
               <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li><a href="logout.php">Cerrar Sesión</a></li>
                  </ul>
               </li>
            </ul>
         </div>
      </div>
   </nav>
   <!-- Contenedor proncipal -->
   <div class="container-fluid">
      <div class="cabezera" align="center">
         <h3 class="Titulo">Cancelación de Compras</h3>
      </div>
      <div class="contenido">
         <div class="container-fluid">
            <div class="form-group">
               <label class="control-label  col-lg-2">
                  <h4 class="textoBlack"><strong><i class="icon-list-numbered"></i> Numero de Compra:</strong></h4>
               </label>
               <div class="col-lg-10">
                  <select class="form-control" id="compra" onchange="DatosTabla()">
                     <option value="nulo">Seleccionar Compra</option>
                     <?php while ($filaCompra = mysqli_fetch_array($resultCompras)){ ?>
                     <option value="<?php echo $filaCompra['Compra'];?>"><?php echo $filaCompra['Compra'];?></option>
                     <?php } ?>
                  </select>
               </div>
               <div class="form-group">
               </div>
            </div>
         </div>
         <br>
         <div class="table-responsive" id="TablaDevoluciones">
            <!-- Se crea la tabla -->
            <table class="table table-striped table-bordered">
               <tr>
                  <td align="center" class="TituloAzul" COLSPAN="6">Tabla De Cancelacion De Compras</td>
               </tr>
               <tr>
                  <th class="headVerde">Producto</th>
                  <th class="headVerde">Cantidad</th>
                  <th class="headVerde">Precio</th>
                  <th class="headVerde">Importe</th>
                  <th class="headVerde">Impuesto</th>
                  <th class="headVerde">Total</th>
               </tr>
               <tr>
                  <td class="celda"><br> </td>
                  <td class="celda"> </td>
                  <td class="celda"> </td>
                  <td class="celda"> </td>
                  <td class="celda"> </td>
                  <td class="celda"> </td>
               </tr>
            </table>
         </div>
      </div>
   </div>
   </div>
   <!-- Modal de confirmación -->
   <div class="container">
      <div class="modal fade " id="DTotal">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header panel-header HeadPanel">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title" align="center">Cancelación</h2>
               </div>
               <div class="modal-body" align="center">
                  <h3>Seguro que desea Cancelar</h3>
                  <br>
                  <div class="alert alert-success alert-dismissible" id="alertaDev" align="center">
                  </div>
                  <table class="table">
                     <tr>
                        <td><button onclick="DevolucionT()" id="delete" name="delete" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                        <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script src="js/jquery.js"></script>
   <script src="js/bootstrap.min.js"></script>
</body>
</html>