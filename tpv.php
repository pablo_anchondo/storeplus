<?php
	session_start();
	include("conexion.php");
	// Determina si se ha iniciado sesión
	if (isset($_SESSION['user'])) {
		echo "";
	} //isset($_SESSION['user'])
	else {
		echo '<script> window.location="index.php"; </script>';
      }
      
      if (isset($_SESSION['Almacen'])) {
            echo "";
        } //isset($_SESSION['user'])
        else {
            echo '<script> window.location="VentanaAlmacen.php"; </script>';
        }
	// Inicializamos variables de sesión
	$profile       = $_SESSION['user'];
	$Identificador = $_SESSION["Id_User"];
	$dominio       = $_SESSION["dominio"];
?>
<!DOCTYPE html>
<html>
   <head>
      <head>
         <meta charset="UTF-8">
         <link rel="shortcut icon" href="img/favicon.ico">
         <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
         <link rel="stylesheet" type="text/css" href="css/estilos.css">
         <link rel="stylesheet" type="text/css" href="fonts/style.css">
         <link rel="stylesheet" type="text/css" href="css/paneles.css">
         <link rel="stylesheet" type="text/css" href="css/tpv.css">
         <link rel="stylesheet" type="text/css" href="css/Tablas.css">
         <script type="text/javascript" src="js/tpv.js" ></script>
         <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
         <title>Store-Plus</title>
   </head>
   </head>
   <body>
      <script> buscarDatos();</script>
	  <?php
	            // Consultas para llenar los select
			include("conexion.php");
			$Identificador = $_SESSION["Id_User"];
			$Cliente       = 'select * from clients where Id_User = ' . $Identificador . ' AND Nombre != "PUBLICO EN GENERAL" order by Nombre ASC ';
                  $resultClient  = $cbd->query($Cliente);
                  $Almacen = $_SESSION["Almacen"];
			$Empresa       = 'select * from empresa where Id_User = ' . $Identificador . ' AND Almacen = '. $Almacen. ' order by Nombre ASC ';
			$resultEmp     = $cbd->query($Empresa);
			$filaEmp       = mysqli_fetch_array($resultEmp);
			$Vendedores    = 'select Vendedor from vendedores where Id_User = ' . $Identificador . ' order by Vendedor ASC ';
			$resultVend    = $cbd->query($Vendedores);
			$queryUni      = 'select * from MetPago where Id_User = ' . $Identificador;
			$result4       = $cbd->query($queryUni);
			$result5       = $cbd->query($queryUni);
	  ?>
      <div class="container-fluid">
         <div class="input-group">
            <span class="input-group-addon color" ><i class="icon-office"> </i> Empresa</span>
            <input  type="text" id="Empresa" class="form-control textColor" placeholder="Empresa" value="<?php echo($filaEmp['Nombre']); ?>">
            <span class="input-group-addon color"><i class="icon-calendar"> </i> Fecha</span>
            <input type="text" id="Fecha" class="form-control textColor">
            <span class="input-group-addon exit btn" id="exit" i  ><span class="icon-exit" ></span>  </i>Salir</span>
         </div>
      </div>
      <br>
      <div class="container-fluid" align="center">
         <div class="col-xs-12 col-lg-2 img">
            <img id="imagenProd" class="img-responsive" src="img/sinImg.jpg">
         </div>
         <div class="col-lg-10">
            <div class="input-group">
               <span class="input-group-addon color" ><i class="icon-user-tie"> </i>Vendedor</span>
               <?php 
                  if (isset($_SESSION["Vendedor"])) {?>
               <input type="text" id="Vendedo" class="form-control" disabled value="<?php echo $_SESSION["user"] ; ?>">
               <?php }else{?>
               <input type="text" id="Vendedo" class="form-control" disabled value="VENDEDOR GENERICO">
               <?php
                  }
                  ?>					
               <span class="input-group-addon color" ><i class="icon-user"> </i>Cliente</span>
               <select class="form-control textColor" id="cliente">
               <option value="PUBLICO EN GENERAL">PUBLICO EN GENERAL</option>
                  <?php while ($fila = mysqli_fetch_array($resultClient)){ ?>
                  <option value="<?php echo $fila['Nombre'];?>"><?php echo $fila['Nombre'];?></option>
                  <?php } ?>
               </select>
            </div>
            <br>
            <div class="input-group">
               <span class="input-group-addon color"><i class="icon-dice"> </i>Cantidad</span>
               <input onkeypress="return validarTamano(this, 3)" type="number" id="cant" class="form-control textColor" min="1" max="999" value="1">
               <span class="input-group-addon btn-primary textoblanco btn" onclick="habilitar()" id="habilitarP" ><span class="icon-ticket" ></span> Precio </i></span>
               <input type="text"  id="priceTMP" class="form-control textColor" value="0.00">
               <span class="input-group-addon color "><i class="icon-price-tags"></i> Producto</span>
               <input type="text" id="prod"  onkeypress="validar(event)" class="form-control textColor" placeholder="Productos" autofocus>
               <span class="input-group-addon  color btn" id="find" href="#Buscar" data-toggle="modal"><i class="icon-search"></i></span>
            </div>
            <br>
            <div class="input-group">
               <span class="input-group-addon color"><i class="icon-bubble"> </i>Comentarios</span>
               <input type="text" id="coment" class="form-control textColor" placeholder="Comentarios">
            </div>
            <div class="input-group separar pull-left">
               <button class="btn btn-success" onclick="getPorcentajeAdd()"><span class="icon-plus"></span></button>
               <button class="btn btn-danger " onclick="borrar()"><span class="icon-bin"></span></button>
               <button href="#Opciones" data-toggle="modal" class="btn btn-info " ><span class="icon-cogs"></span></button>
               <a href="<?php echo $dominio;?>tpvImg.php"  class="btn btn-warning " ><span class="icon-images"></span> Vista con Imágenes</a>
               
            </div>
         </div>
      </div>
      <br>
      <br>
      <div class="alert alert-danger alert-dismissible" id="alerta" align="center">
      </div>
      <div class="table-responsive container-fluid tabla">
         <table id="TableBody" class="table table-striped table-bordered">
            <tr>
               <th class="color">Cantidad</th>
               <th class="color">Articulo</th>
               <th class="color">Descripción</th>
               <th class="color">Precio</th>
               <th class="color">Importe</th>
            </tr>
         </table>
      </div>
      <br>
      <div class="container-fluid">
         <div class="input-group">
            <span class="input-group-addon color"><i class="icon-coin-dollar"> SubTotal</i></span>
            <input type="text" id="Sub" class="form-control textColor" placeholder="$0.00">
            <span class="input-group-addon color"><i class="icon-coin-dollar"> IVA</i></span>
            <input type="text" id="iva" class="form-control textColor" placeholder="$0.00">
            <span class="input-group-addon color"><i class="icon-coin-dollar"> Total</i></span>
            <input type="text" id="tot" class="form-control textColor" placeholder="$0.00">
         </div>
      </div>
      <div class="container-fluid separar">
         <button onclick="Recargar()" class="btn btn-danger col-xs-6 col-lg-4" ><span class="icon-cross"></span> Cancelar</button>
         <button onclick="VentanaApartado()" class="btn btn-info col-xs-6 col-lg-4" ><span class="icon-cart"></span> Apartado</button>
         <button id="Llena" class="btn btn-success col-xs-6 col-lg-4" ><span class="icon-credit-card"></span> Pagar <span class="icon-arrow-right2"></span> F2</button>	
      </div>
	  <!-- Modal de Buscar -->
      <div class="container" align="center">
         <div class="modal fade " id="Buscar" align="center">
            <div class="modal-dialog tamno-modal" align="center">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Buscar Producto</h2>
                  </div>
                  <div class="modal-body cuerpoTabla" align="center">
                     <div class="container-fluid">
                        <input class="col-xs-12 col-lg-12 form-control" placeholder="Buscar" id="caja_busqueda" name="caja_busqueda">
                     </div>
                     <br>
                     <div class="container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive tablita">
                           <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12" id="datos">
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <br>
                     <br>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Pago de Ticket -->
      <div class="container">
         <div class="modal fade " id="Pagando">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Pago de Ticket</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="alert alert-danger alert-dismissible" id="alertaPagando" align="center">
                     </div>
                     <table class="table">
                        <tr>
                           <td class="col-lg-6"><label class="text3"> Importe </label></td>
                           <td class="col-lg-6"><input class="form-control" id="impF"></td>
                        </tr>
                        <tr>
                           <td class="col-lg-6"><label class="text3"> Metodo de pago </label></td>
                           <td class="col-lg-6">
                              <select class="form-control" id="MPago">
                                 <?php while ($fila4 = mysqli_fetch_array($result4)){ ?>
                                 <option value="<?php echo $fila4['Mpago'];?>"><?php echo $fila4['Mpago'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Efectivo </label></td>
                           <td><input class="form-control" id="Efe" onblur="cambio(this)" onkeypress="return validaR(event)"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Cambio </label></td>
                           <td><input class="form-control" id="cambio" value="0.00"></td>
                        </tr>
                        <tr>
                           <td><button id="pagar" name="delete" class="btn btn-success col-xs-12 col-lg-12">Pagar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Imprimir -->
      <div class="container">
         <div class="modal fade " id="Imprimir">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <h2 class="modal-title" align="center">Deseas Imprimir el Ticket</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="container" id="detalles">
                     </div>
                     <table class="table">
                        <tr>
                           <td class="col-lg-6"><label class="text3"> Total </label></td>
                           <td class="col-lg-6"><input class="form-control" id="TotTick"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Metodo de pago </label></td>
                           <td><input class="form-control" id="MPagoTck"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Pago </label></td>
                           <td><input class="form-control" id="Pagotck"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Cambio </label></td>
                           <td><input class="form-control" id="CambioTck"></td>
                        </tr>
                        <tr>
                           <td class="col-lg-6"><button  id="TicketBtn" name="delete" class="btn btn-success col-xs-12 col-lg-12">Imprimir</button></td>
                           <td class="col-lg-6"><button  onclick="recargar()" class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Salir</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Imprimir Apartado -->
      <div class="container">
         <div class="modal fade " id="ImprimirApartado">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <h2 class="modal-title" align="center">Deseas Imprimir el Ticket</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="container" id="detalles">
                     </div>
                     <table class="table">
                        <tr>
                           <td class="col-lg-6"><label class="text3"> Total </label></td>
                           <td class="col-lg-6"><input class="form-control" id="TotaApa"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Metodo de pago </label></td>
                           <td><input class="form-control" id="MPagoAPA"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Abono </label></td>
                           <td><input class="form-control" id="AbonoApa"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Adeudo </label></td>
                           <td><input class="form-control" id="AdeudoApa"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Vencimiento </label></td>
                           <td><input class="form-control" id="VencimientoApa"></td>
                        </tr>
                        <tr>
                           <td class="col-lg-6"><button onclick="AbrirTktApar()" id="TicketBtnApa" name="delete" class="btn btn-success col-xs-12 col-lg-12">Imprimir</button></td>
                           <td class="col-lg-6"><button onclick="recargar()" class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Salir</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
	<!-- Modal de Opciones -->
      <div class="container">
         <div class="modal fade " id="Opciones">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Opciones</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <button class="btn-info  options btn col-xs-12 col-lg-12" onclick="CorteX()">Corte Parcial X</button>
                     <button class="btn-info options btn col-xs-12 col-lg-12" onclick="CorteZ()">Corte Total Z</button>
                     <a href="<?php echo $dominio;?>Devoluciones.php" class="options btn-info btn col-xs-12 col-lg-12" >Devolución de producto</a>
                     <br>
                     <br>
                     <br>
                     <br>
                     <br>
                     <br>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- Modal de Apartado -->
      <div class="container">
         <div class="modal fade " id="Apartar">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Venta Apartado</h2>
                  </div>
                  <div class="modal-body cuerpo" align="center">
                     <div class="alert alert-danger alert-dismissible" id="alertaApartado" align="center">
                     </div>
                     <table class="table">
                        <tr>
                           <td class="col-lg-6"><label class="text3"> Importe </label></td>
                           <td class="col-lg-6"><input class="form-control" id="impFA"></td>
                        </tr>
                        <tr>
                           <td class="col-lg-6"><label class="text3"> Metodo de pago </label></td>
                           <td class="col-lg-6">
                              <select class="form-control" id="MPagoA">
                                 <?php while ($fila5 = mysqli_fetch_array($result5)){ ?>
                                 <option value="<?php echo $fila5['Mpago'];?>"><?php echo $fila5['Mpago'];?></option>
                                 <?php } ?>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Vencimiento </label></td>
                           <td>
                              <input class='form-control' type='Date' id='fechaVensimiento'>
                           </td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Abono </label></td>
                           <td><input class="form-control" id="Abono" onblur="Adeudo(this)" onkeypress="return validaR(event)"></td>
                        </tr>
                        <tr>
                           <td><label class="text3"> Adeudo </label></td>
                           <td><input class="form-control" id="Adeudo" value="0.00"></td>
                        </tr>
                        <tr>
                           <td><button  id="btnApartar" onclick="PagarApartado()"  name="delete" class="btn btn-success col-xs-12 col-lg-12">Apartar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>