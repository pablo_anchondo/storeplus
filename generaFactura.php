<?php
session_start();
include("conexion.php");
date_default_timezone_set('America/Mexico_City');
// Determina si se ha iniciado sesión 
if (isset($_SESSION['user'])) {
    echo "";
} else {
    echo '<script> window.location="index.php"; </script>';
}

if (isset($_SESSION['vendedor'])) {
    echo "";
} else {
    echo '<script> window.location="Facturacion.php"; </script>';
}


if (isset($_SESSION['venta'])) {
    echo "";
} else {
    echo '<script> window.location="Facturacion.php"; </script>';
}

if (isset($_SESSION['Receptor'])) {
    echo "";
} else {
    echo '<script> window.location="Facturacion.php"; </script>';
}

if (isset($_SESSION['rfcr'])) {
    echo "";
} else {
    echo '<script> window.location="Facturacion.php"; </script>';
}

if (isset($_SESSION['uso'])) {
    echo "";
} else {
    echo '<script> window.location="Facturacion.php"; </script>';
}

if (isset($_SESSION['formaPago'])) {
    echo "";
} else {
    echo '<script> window.location="Facturacion.php"; </script>';
}

if (isset($_SESSION['MetodoPago'])) {
    echo "";
} else {
    echo '<script> window.location="Facturacion.php"; </script>';
}


$profile       = $_SESSION['user'];
$Identificador = $_SESSION["Id_User"];
$dominio       = $_SESSION["dominio"];
$Almacen = $_SESSION['Almacen']; 
$vendedor = $_SESSION["vendedor"];
$venta = $_SESSION["venta"];
$Receptor = $_SESSION["Receptor"];
$rfcr = $_SESSION["rfcr"];
$Serie = $_SESSION["Serie"];
$id = 0;
$Subtotal = 0;
$Total = 0;
$fecha = date('Y-m-d');

$fol = "SELECT MAX(id) as ident from facturas where Id_User = $Identificador AND Almacen = $Almacen AND Serie = '$Serie'";
$resultFol  = $cbd->query($fol);
$ResId = mysqli_fetch_array($resultFol);
if ($ResId['ident'] == NULL) {
    $id = 1;
}else{
    $id = $ResId['ident'] + 1;
}

$folio = Crear($id);



$queryDatos = "SELECT * FROM datosfactura WHERE Id_User = $Identificador AND Almacen = $Almacen";
$resDatos  = $cbd->query($queryDatos);
$datosFact    = mysqli_fetch_array($resDatos);

$Ruta = $datosFact['src'];
$cer = $Ruta.$datosFact['cert'];
$key = $Ruta.$datosFact['keyy'];
$cerpem = $Ruta."cer.pem";
$keypem = $Ruta."key.pem";

$contrato = $datosFact['Contrato'];
$userpade = $datosFact['Usuario'];
$passpade = $datosFact['Pass'];



$cfdi_sellado = sellarXML($folio, $cer, $keypem);



$client = new SoapClient("https://timbrado.pade.mx/servicio/Timbrado3.3?wsdl");

$opciones[0] = "GENERAR_PDF";
$cont = 1;

if (isset($_SESSION['ordencompra'])) {
    $ordencompra = $_SESSION['ordencompra'];
    $addenda2 = base64_encode($ordencompra);
    $opciones[$cont] = "OBSERVACIONES:".$addenda2;
    $cont = $cont + 1;
}else{

}

if (isset($_SESSION['mail'])) {
    $mail = $_SESSION['mail'];
    $opciones[$cont] = "ENVIAR_AMBOS:".$mail;
    $cont = $cont + 1;
}else{

}

$params = array(
    "contrato" => $contrato,
    "usuario" => $userpade,
    "passwd" => $passpade,
    "cfdiXml" => $cfdi_sellado,
    "opciones" => $opciones,
);

$response = $client->__soapCall("timbrado", array($params));

$xmlres = $response->return;


$xml_doc = new DOMDocument();
$xml_doc->loadXML($xmlres);



$UUID = $xml_doc->getElementsByTagName('UUID'); 


if (isset($UUID->item(0)->nodeValue)) {
    $idSat = $xml_doc->getElementsByTagName('id'); 

    $uid = (string)$UUID->item(0)->nodeValue;

    $msj = $xml_doc->getElementsByTagName('mensaje'); 

    $selloSAT = $xml_doc->getElementsByTagName('selloSAT'); 
    $selloSatTexto = (string)$selloSAT->item(0)->nodeValue;

    $selloCFD = $xml_doc->getElementsByTagName('selloCFD'); 
    $selloCFDTexto = (string)$selloCFD->item(0)->nodeValue;

    $noCertificadoSAT = $xml_doc->getElementsByTagName('noCertificadoSAT'); 
    $noCertificadoSATTexto = (string)$noCertificadoSAT->item(0)->nodeValue;

    $FechaTimbrado = $xml_doc->getElementsByTagName('FechaTimbrado'); 
    $FechaTimbradoTexto = (string)$FechaTimbrado->item(0)->nodeValue;

    $pdf64 = $xml_doc->getElementsByTagName('pdfBase64'); 
    $pdfFactura = (string)$pdf64->item(0)->nodeValue;

    $pdfFactura = base64_decode($pdfFactura);

    $rutaPDF = "Facturacion/PDF/Facturas/".$uid.".pdf";
    $PdfName = $uid.".pdf";
    $RutaPdfRaiz = $dominio."Facturacion/PDF/Facturas/";

    $RutaXMLRaiz = $dominio."Facturacion/XML/Facturas/";
    $XMLName = $uid.".xml";

    if($archivo = fopen($rutaPDF, "w+")){
        fwrite($archivo, $pdfFactura);
        fclose($archivo);
    }
 
    $xmlBase64 = $xml_doc->getElementsByTagName('xmlBase64'); 

    $timbrado = base64_decode($xmlBase64->item(0)->nodeValue);
    $cfdi_final = new DOMDocument();
    $cfdi_final->loadXML($timbrado);
    $RrutaXML = "Facturacion/XML/Facturas/".$uid.".xml";
    $cfdi_final->save($RrutaXML);

    $queryVenta2 = "SELECT Importe, Total FROM ventas  WHERE Id_User = $Identificador AND Vendedor = '$vendedor' AND Almacen = $Almacen AND  Venta = $venta";
    $resVenta2  = $cbd->query($queryVenta2);
    $datosVenta2 = mysqli_fetch_array($resVenta2);

    $Subtotal = $datosVenta2['Importe'];
    $Total = $datosVenta2['Total'];

    $Adeudo = 0;
    $Estado = "";

    $MPago = $_SESSION["MetodoPago"];
    if ($MPago == 'PUE') {
        $Adeudo = 0;
        $Estado = "PAGADO";
  }elseif ($MPago == 'PPD') {
      $Adeudo = $Total;
      $Estado = "PENDIENTE";
  }

    $sql = "INSERT INTO facturas VALUES ($id, '$RutaPdfRaiz', '$PdfName', '$RutaXMLRaiz', '$XMLName','$Receptor', '$rfcr', '$Serie', '$id', $Subtotal, $Total, 0.00,
     $Adeudo, 'FACTURA', '$fecha', '$Estado', '$uid', '0', '$MPago', 0, 'VIGENTE', 'VIGENTE', 'VIGENTE', $Identificador, $Almacen)";
    $cbd->query($sql);

    $sql = "UPDATE ventas SET facturada = 'TRUE' WHERE Id_User = $Identificador AND Vendedor = '$vendedor' AND Almacen = $Almacen AND Venta = $venta";
    $cbd->query($sql);

    unset($_SESSION["vendedor"]);
    unset($_SESSION["venta"]);
    unset($_SESSION["Receptor"]);
    unset($_SESSION["rfcr"]);
    unset($_SESSION["uso"]);
    unset($_SESSION["formaPago"]);
    unset($_SESSION["MetodoPago"]);
    unset($_SESSION["Serie"]);
    unset($_SESSION["ordencompra"]);
    unset($_SESSION["mail"]);
    unset($_SESSION["cadena"]);

    header("Location: ".$dominio."Facturacion/PDF/Facturas/".$uid.".pdf");

}else{
    $msj = $xml_doc->getElementsByTagName('mensaje'); 
    echo "Error";
    echo "<br>";
    echo utf8_decode($msj->item(0)->nodeValue);
    
}


function sellarXML($cfdi, $archivo_cer, $archivo_pem) {
    $private = openssl_pkey_get_private(file_get_contents($archivo_pem));
    $certificado = str_replace(array('\n', '\r'), '', base64_encode(file_get_contents($archivo_cer)));

    $xdoc = new DomDocument();
    $xdoc->loadXML($cfdi) or die("XML invalido");

    $c = $xdoc->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/3', 'Comprobante')->item(0); 
    $c->setAttribute('Certificado', $certificado);

    $XSL = new DOMDocument();
    $XSL->load('XSLT/cadenaoriginal_3_3.xslt');
    
    $proc = new XSLTProcessor;
    $proc->importStyleSheet($XSL);

    $cadena_original = $proc->transformToXML($xdoc);
    $_SESSION['cadena'] = $cadena_original;

    openssl_sign($cadena_original, $sig, $private, OPENSSL_ALGO_SHA256);
    $sello = base64_encode($sig);
    $c->setAttribute('Sello', $sello);
    
    return $xdoc->saveXML();
}



function Crear($Folio){
    include("conexion.php");

    $fecha_actual = str_replace(' ', 'T', date('Y-m-d H:i:s', (strtotime ("-1 Hours"))));
    $profile = $_SESSION['user'];
    $Identificador = $_SESSION["Id_User"];
    $dominio = $_SESSION["dominio"];
    $vendedor = $_SESSION["vendedor"];
    $venta = $_SESSION["venta"];
    $uso = $_SESSION["uso"];
    $formaPago = $_SESSION["formaPago"];
    $MetodoPago = $_SESSION["MetodoPago"];
    $Receptor = $_SESSION["Receptor"];
    $rfcr = $_SESSION["rfcr"];
    $Almacen = $_SESSION['Almacen'];
    $Serie = $_SESSION["Serie"]; 

    $queryVenta = "SELECT * FROM ventas  WHERE Id_User = $Identificador AND Vendedor = '$vendedor' AND Almacen = $Almacen AND  Venta = $venta";
    $resVenta  = $cbd->query($queryVenta);
    $datosVenta = mysqli_fetch_array($resVenta);

    $queryImp = "SELECT  SUM(TotImp) as Total , Impuesto  FROM partventa  WHERE Id_User = $Identificador AND Vendedor = '$vendedor' AND Almacen =  $Almacen AND  Venta = $venta GROUP BY Impuesto ";
    $resImp = $cbd->query($queryImp);

    $TotalImp = "SELECT  SUM(TotImp) as Total FROM partventa  WHERE Id_User = $Identificador AND Vendedor = '$vendedor' AND Almacen = $Almacen AND  Venta = $venta";
    $resTot = $cbd->query($TotalImp);
    $TotalImpuestos = mysqli_fetch_array($resTot);
 
    $queryPart = "SELECT * FROM partventa WHERE Id_User = $Identificador AND Vendedor = '$vendedor' AND Almacen = $Almacen AND  Venta = $venta ";
    $resPart = $cbd->query($queryPart);

    $queryDatos = "SELECT * FROM datosfactura WHERE Id_User = $Identificador AND Almacen = $Almacen";
    $resDatos = $cbd->query($queryDatos);
    $datosFact = mysqli_fetch_array($resDatos);

    $xml = new DomDocument('1.0', 'UTF-8');


 
     $cfdi_Comprobante = $xml->createElement('cfdi:Comprobante');
     $cfdi_Comprobante = $xml->appendChild($cfdi_Comprobante);
     $cfdi_Comprobante->setAttribute('Certificado', '');
     $cfdi_Comprobante->setAttribute('Fecha', $fecha_actual);
     $cfdi_Comprobante->setAttribute('Folio', $Folio);
     $cfdi_Comprobante->setAttribute('FormaPago', $formaPago);
     $cfdi_Comprobante->setAttribute('LugarExpedicion', $datosFact['ExpeditionPlace']);
     $cfdi_Comprobante->setAttribute('MetodoPago', $MetodoPago);
     $cfdi_Comprobante->setAttribute('Moneda', 'MXN');
     $cfdi_Comprobante->setAttribute('NoCertificado', $datosFact['NumCertificado']);
     $cfdi_Comprobante->setAttribute('Sello', '');
     $cfdi_Comprobante->setAttribute('Serie', $Serie);
     $cfdi_Comprobante->setAttribute('SubTotal', $datosVenta['Importe']);
     $cfdi_Comprobante->setAttribute('TipoDeComprobante', 'I');
     $cfdi_Comprobante->setAttribute('Total', $datosVenta['Total']);
     $cfdi_Comprobante->setAttribute('Version', '3.3');
     $cfdi_Comprobante->setAttribute('xmlns:cfdi', 'http://www.sat.gob.mx/cfd/3');
     $cfdi_Comprobante->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
     $cfdi_Comprobante->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd');

     $cfdi_Emisor = $xml->createElement('cfdi:Emisor');
     $cfdi_Emisor = $cfdi_Comprobante->appendChild($cfdi_Emisor);
     $cfdi_Emisor->setAttribute('Nombre', utf8_encode ($datosFact['Emisor']));
     $cfdi_Emisor->setAttribute('RegimenFiscal', $datosFact['RegimenFiscal']);
     $cfdi_Emisor->setAttribute('Rfc', $datosFact['RFC']);

     $cfdi_Receptor = $xml->createElement('cfdi:Receptor');
     $cfdi_Receptor = $cfdi_Comprobante->appendChild($cfdi_Receptor);
     $cfdi_Receptor->setAttribute('Nombre', $Receptor);
     $cfdi_Receptor->setAttribute('Rfc', $rfcr);
     $cfdi_Receptor->setAttribute('UsoCFDI', $uso);

     $cfdi_Conceptos = $xml->createElement('cfdi:Conceptos');
     $cfdi_Conceptos = $cfdi_Comprobante->appendChild($cfdi_Conceptos);

     while ($datosPart = mysqli_fetch_array($resPart)) {

        $int = intval($datosPart['Impuesto']);
        $int = sprintf("%02d", $int);
        $final = '0.'.$int.'0000';
        
        $queryProd = "SELECT unidad, claveProdServ, claveUnidad FROM productos WHERE Id_User = $Identificador AND Almacen = $Almacen AND  Nombre = " ."'" .$datosPart['Articulo']."'". " AND descripcion = ". "'".$datosPart['Descripcion']."'";
        $resProd  = $cbd->query($queryProd);
        $datosProd = mysqli_fetch_array($resProd);

        $cfdi_Concepto = $xml->createElement('cfdi:Concepto');
        $cfdi_Concepto = $cfdi_Conceptos->appendChild($cfdi_Concepto);
        $cfdi_Concepto->setAttribute('Cantidad', $datosPart['Cantidad']);
        $cfdi_Concepto->setAttribute('ClaveProdServ', $datosProd['claveProdServ']);
        $cfdi_Concepto->setAttribute('ClaveUnidad', $datosProd['claveUnidad']);
        $cfdi_Concepto->setAttribute('Descripcion', $datosPart['Descripcion']);
        $cfdi_Concepto->setAttribute('Importe', $datosPart['Importe']);
        $cfdi_Concepto->setAttribute('NoIdentificacion', $datosPart['Articulo']);
        $cfdi_Concepto->setAttribute('Unidad', $datosProd['unidad']);
        $cfdi_Concepto->setAttribute('ValorUnitario', $datosPart['Precio']);
    
        $cfdi_Impuestos = $xml->createElement('cfdi:Impuestos');
        $cfdi_Impuestos = $cfdi_Concepto->appendChild($cfdi_Impuestos);
    
        $cfdi_Traslados = $xml->createElement('cfdi:Traslados');
        $cfdi_Traslados = $cfdi_Impuestos->appendChild($cfdi_Traslados);
    
        $cfdi_Traslado = $xml->createElement('cfdi:Traslado');
        $cfdi_Traslado = $cfdi_Traslados->appendChild($cfdi_Traslado);
        $cfdi_Traslado->setAttribute('Base', $datosPart['Importe']);
        $cfdi_Traslado->setAttribute('Importe', $datosPart['TotImp']);
        $cfdi_Traslado->setAttribute('Impuesto', '002');
        $cfdi_Traslado->setAttribute('TasaOCuota', $final);
        $cfdi_Traslado->setAttribute('TipoFactor', 'Tasa');

     }

     $cfdi_Impuestos = $xml->createElement('cfdi:Impuestos');
     $cfdi_Impuestos = $cfdi_Comprobante->appendChild($cfdi_Impuestos);
     $cfdi_Impuestos->setAttribute('TotalImpuestosTrasladados', $TotalImpuestos['Total']);

     $cfdi_Traslados = $xml->createElement('cfdi:Traslados');
     $cfdi_Traslados = $cfdi_Impuestos->appendChild($cfdi_Traslados);

     while ($datosImp = mysqli_fetch_array($resImp)) {
     
        $int = intval($datosImp['Impuesto']);
        $int = sprintf("%02d", $int);
        $final = '0.'.$int.'0000';
        $cfdi_Traslado = $xml->createElement('cfdi:Traslado');
        $cfdi_Traslado = $cfdi_Traslados->appendChild($cfdi_Traslado);
        $cfdi_Traslado->setAttribute('Impuesto', '002');
        $cfdi_Traslado->setAttribute('TipoFactor', 'Tasa');
        $cfdi_Traslado->setAttribute('TasaOCuota',$final );
        $cfdi_Traslado->setAttribute('Importe', $datosImp['Total']);

     }

     return $xml->saveXML();

}



?>