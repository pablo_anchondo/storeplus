<?php
    session_start();
    include("conexion.php");
    // Determina si se ha iniciado sesión 
    if (isset($_SESSION['user'])) {
        echo "";
    } else {
        echo '<script> window.location="index.php"; </script>';
    }
    // Determina si es administrador o vendedor
    if (isset($_SESSION['Vendedor'])) {
        echo '<script> window.location="index.php"; </script>';
    } else {
        echo "";
    }
    // Inicializamos variables de sesión
    $profile       = $_SESSION['user'];
    $Identificador = $_SESSION["Id_User"];
    $dominio       = $_SESSION["dominio"];
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/select2-bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/select2.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/Entradas.js" ></script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
      <!--// Navigation bar -->
      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li class="active"><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
                  <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <?php 
         // Consultas para llenar los select
         $querito = 'select descripcion from productos where Id_User = '.$Identificador.' AND Almacen = '.$_SESSION["Almacen"].' order by descripcion ASC ';
         $result = $cbd->query($querito);
      ?>
      <!-- Contenedor proncipal -->
      <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Entradas</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
               <br>
               <div class="table-responsive container-fluid tablaSalidas">
                  <table id="TableBody" class="table table-striped table-bordered">
                     <tr>
                        <td align="center" class="TituloVerde" COLSPAN="4">Detalles de Entrada</td>
                     </tr>
                     <tr>
                        <th class="headAzul">N°</th>
                        <th class="headAzul">Cantidad</th>
                        <th class="headAzul">Descripción</th>
                        <th class="headAzul">Existencia</th>
                     </tr>
                  </table>
               </div>
               <!-- Se crea la tabla -->
               <br><br>
               <div class="table-responsive container-fluid">
                  <table class="table table-striped table-bordered">
                     <tr>
                        <td align="center" class="TituloVerde" COLSPAN="5">Detalles de Producto o Servicio</td>
                     </tr>
                     <tr>
                        <th class="headAzul col-lg-4">Producto</th>
                        <th class="headAzul" >Cantidad</th>
                        <th class="headAzul">Concepto Entrada</th>
                        <th class="headAzul">Agregar</th>
                        <th class="headAzul">Borrar</th>
                     </tr>
                     <tr>
                        <td>
                           <select class="form-control" id="prod">
                              <?php while ($fila = mysqli_fetch_array($result)){ ?>
                              <option value="<?php echo $fila['descripcion'];?>"><?php echo $fila['descripcion'];?></option>
                              <?php } ?>
                           </select>
                        </td>
                        <td>
                           <input onkeypress="return validarTamano(this, 3)" type="number" id="cant" class="form-control textColor" min="1" max="999" value="1">
                        </td>
                        <td>
                           <select class="form-control" id="conceptoSalida">
                              <option value="Ajuste">Ajuste</option>
                              <option value="Bonificación">Bonificación</option>
                              <option value="Regalo">Regalo</option>
                              <option value="Transpaso">Transpaso</option>
                           </select>
                        </td>
                        <td class="col-xs-2 col-lg-2">
                           <button onclick="Consulta()" class="btn btn-primary col-xs-12 col-lg-12"><span class="icon-plus"></span></button>
                        </td>
                        <td class="col-xs-2 col-lg-2">
                           <button onclick="borrar()" class="btn btn-warning col-xs-12 col-lg-12"><span class='icon-bin' ></span></button>
                        </td>
                     </tr>
                  </table>
                  <br>
                  <textarea id="Comentarios" class="form-control bgtext" placeholder="Comentarios"></textarea>
               </div>
               <br>
               <div class="alert alert-success alert-dismissible" id="alerta" align="center">
               </div>
               <br>
               <div class="container-fluid">
                  <button onclick="Entradas()" class="btn btn-info col-xs-12 col-lg-12">Continuar</button>
               </div>
               <br>
            </div>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/select2.min.js" ></script>
      <script type="text/javascript" src="js/select2_locale_es.js" ></script>
      <script>
        $(document).ready(function() { 
            $("#prod").select2({
        
                });

        });
      </script>
   </body>
</html>