<?php
session_start();
include("conexion.php");
// Determina si se ha iniciado sesión 
if (isset($_SESSION['user'])) {
    echo "";
} else {
    echo '<script> window.location="index.php"; </script>';
}

// Determina si es administrador o vendedor
if (isset($_SESSION['Vendedor'])) {
    echo '<script> window.location="index.php"; </script>';
} else {
    echo "";
}

$profile       = $_SESSION['user'];
$Identificador = $_SESSION["Id_User"];
$dominio       = $_SESSION["dominio"];

?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <script type="text/javascript" src="js/Empresa.js" ></script>
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript">Datos()</script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
      <?php
         // Consultas para llenar los select
         $querito = 'select * from almacen where Id_User = '.$Identificador.' order by Nombre ASC ';
         $result = $cbd->query($querito);
         
         $querito2 = 'select * from almacen where Id_User = '.$Identificador.' AND Almacen = '. $_SESSION["Almacen"];
         $result2 = $cbd->query($querito2);
         $fila3 = mysqli_fetch_array($result2);
         ?>
      <!--// Navigation bar -->
      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li class="active"><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
                  <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- Contenedor proncipal -->
      <div class="container-fluid">
      <div class="cabezera" align="center">
         <h3 class="Titulo">Empresa</h3>
      </div>
      <div class="contenido">
         <div class="tablita table-responsive table-bordered contenido">
            <br>
            <!--// Botones de opciones -->
            <div class="bs-example" align="">

   
               <div class="alert alert-success alert-dismissible" id="alerta" align="center">
               </div>
               <div class="container-fluid">
               <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-office"></i> Empresa:</strong></h4>
                    <input type="text" class="form-control textColor" id="Name">
                </div>
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-location"></i> Dirección:</strong></h4>
                    <input type="text" class="form-control textColor" id="dir">
                </div>
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-compass2"></i> Codigo Postal:</strong></h4>
                    <input type="text" class="form-control textColor" id="cp" onkeypress="return valida(event)">
                </div>
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-key2"></i> R.F.C:</strong></h4>
                    <input type="text" class="form-control textColor" id="rfc" onkeypress="return validarTamano(this, 13)">
                </div>
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-phone"></i> Lada:</strong></h4>
                    <input type="text" class="form-control textColor" id="Lada" onkeypress="return validarTel(this, 3, event)">
                </div>
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-phone"></i> Telefono:</strong></h4>
                    <input type="text" class="form-control textColor" id="tel" onkeypress="return validarTel(this, 7, event)">
                </div>
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-sphere"></i> Página web:</strong></h4>
                    <input type="text" class="form-control textColor" id="web">
                </div>
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-images"></i> Logo Empresa:</strong></h4>
                    <form method="POST" id="formulario" enctype="multipart/form-data">
                        <input class="form-control" id="file" name="file" type="file" />
                    </form>
                </div>
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-file-text2"></i> Mensaje Ticket:</strong></h4>
                    <input type="text" class="form-control textColor" id="Ticket">
                </div>
                <div class="form-group col-lg-3" >
                    <h4 class="textoBlack"><strong><i class="icon-mail4"></i> Email:</strong></h4>
                    <input type="text" class="form-control textColor" id="mail">
                </div>
                <div class="form-group col-lg-6" >
                    <h4 class="textoBlack"><strong> Aceptar:</strong></h4>
                    <button class=" col-xs-12  btn btn-success" onclick="modificar()">Aceptar</button>
                </div>
               <br>
               </div>
            </div>
         </div>
      </div>

      <!-- Contenedor principal -->

        <div class="cabezera" align="center">
            <h3 class="Titulo">Configuración Adicional</h3>
        </div>
        <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
            <br>
            <ul class="nav nav-pills" role="tablist">
               <li role="presentation" class="dropdown active col-xs-12 col-lg-12" align="center">
                  <a href="#" class="dropdown-toggle" id="drop4" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Desplegar Opciones <span class="caret"></span> </a>
                  <ul class="dropdown-menu col-xs-12 col-lg-12" align="center" id="menu1" aria-labelledby="drop4">
                     <li><a href="#Vendedor" data-toggle="modal">Agregar Vendedor</a></li>
                     <li><a href="<?php echo $dominio;?>Respaldo.php" target="_blank" >Respaldo de Información</a></li>
                     <li><a href="Formato/Formatos.xlsx" download="Formatos.xlsx" >Descargar Formatos</a></li>
                     <li><a href="#" onclick="AlmacenPred()">Seleccionar Almacen</a></li>
                     <li><a href="#AddAlmacen" data-toggle="modal" >Agregar Almacen</a></li>
                     <li><a href="#ModAlmacen" data-toggle="modal" >Modificar Almacen</a></li>
                  </ul>
               </li>
                <br>  <br>  <br>  

            <div class="container-fluid">
               <div class="form-group col-lg-4" >
                    <h4 class="textoBlack"><strong><i class="icon-home2"></i> Almacen:</strong></h4>
                    <select class="form-control textColor" id="almacen">
                           <?php while ($fila2 = mysqli_fetch_array($result)){ ?>
                           <option value="<?php echo $fila2['Almacen'];?>"><?php echo $fila2['Nombre'];?></option>
                           <?php } ?>
                        </select>
                </div>
                <div class="form-group col-lg-4" >
                    <h4 class="textoBlack"><strong><i class="icon-home2"></i> Actual:</strong></h4>
                        <input value="<?php echo $fila3['Nombre'];?>" type="text" class="form-control " id="AlmacenAct" >
                </div>

                <div class="form-group col-lg-4" >
                    <h4 class="textoBlack"><strong><i class="icon-cloud-upload"></i> Importar Datos:</strong></h4>
                    <form method="POST" id="formularioExcel" enctype="multipart/form-data">
                           <input class="form-control" id="excel" name="excel" type="file" />
                        </form>
                </div>


            </div>
                


               
  
               <br>  <br>  <br>  <br>  
                
            </div>
        </div>
    




      <!-- Modal de Alta Empleados -->
      <div class="container">
         <div class="modal fade " id="Vendedor">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Alta Empleados</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaVend" align="center">
                     </div>
                     <table class="table">
                        <tr>
                           <td class="col-lg-6"><label class="text3">Nombre</label></td>
                           <td class="col-lg-6"><input class="form-control" id="NameProv"></td>
                        </tr>
                        <tr>
                           <td class="col-lg-6"><label class="text3">Usuario</label></td>
                           <td class="col-lg-6"><input class="form-control" id="UsuVend"></td>
                        </tr>
                        <tr>
                           <td class="col-lg-6"><label class="text3">Contraseña</label></td>
                           <td class="col-lg-6"><input class="form-control" id="PassVend"></td>
                        </tr>
                        <tr>
                        
                        </tr>
                        
                        <tr>
                            <td COLSPAN='2'>
                            <div class="form-check form-check-inline" align="center">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input separachk" type="checkbox" id="Inventario">
                            <label class="form-check-label" for="Inventario"> Inventario </label>
                            <input class="form-check-input separachk" type="checkbox" id="Compras">
                            <label class="form-check-label" for="Compras"> Compras </label>
                            <input class="form-check-input separachk" type="checkbox" id="Control">
                            <label class="form-check-label" for="Control"> Control </label>
                            <input class="form-check-input separachk" type="checkbox" id="Reportes">
                            <label class="form-check-label" for="Reportes"> Reportes </label>
                            <input class="form-check-input separachk" type="checkbox" id="Operaciones">
                            <label class="form-check-label" for="Operaciones"> Operaciones </label>
                        </div>
                        </div>
                            </td>
                        </tr>

                        <tr>
                           <td class="col-lg-6"><button onclick="AddVend()" id="delete" name="delete" class="btn btn-success col-xs-12 col-lg-12">Agregar</button></td>
                           <td class="col-lg-6"><button class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal de Alta Almacen -->
      <div class="container">
         <div class="modal fade " id="AddAlmacen">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Alta Almacen</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaAlm" align="center">
                     </div>
                     <table class="table">
                        <tr>
                           <td class="col-lg-6"><label class="text3">Nombre Almacen</label></td>
                           <td class="col-lg-6"><input class="form-control" id="NameAlm"></td>
                        </tr>
                        <tr>
                           <td class="col-lg-6"><button onclick="Almacen()" id="delete" name="delete" class="btn btn-success col-xs-12 col-lg-12">Agregar</button></td>
                           <td class="col-lg-6"><button class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal de Modificar Almacen Actual -->
      <div class="container">
         <div class="modal fade " id="ModAlmacen">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header HeadPanel">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Modificar Almacen Actual</h2>
                  </div>
                  <div class="modal-body cuerpoM" align="center">
                     <div class="alert alert-success alert-dismissible" id="alertaModAlm" align="center">
                     </div>
                     <table class="table">
                        <tr>
                           <td class="col-lg-6"><label class="text3">Nuevo Nombre</label></td>
                           <td class="col-lg-6"><input class="form-control" id="NameModAlm"></td>
                        </tr>
                        <tr>
                           <td class="col-lg-6"><button onclick="ModAlmacen()"  class="btn btn-success col-xs-12 col-lg-12">Agregar</button></td>
                           <td class="col-lg-6"><button class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>