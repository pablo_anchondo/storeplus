<?php  
	session_start();
      include("conexion.php");
      // Determina si se ha iniciado sesión 
	if (isset($_SESSION['user'])) {
		echo "";
	}else{
		echo '<script> window.location="index.php"; </script>';
	}

	$profile = $_SESSION['user'];
	$Identificador = $_SESSION["Id_User"];
	$dominio = $_SESSION["dominio"];
	$Almacen = $_SESSION['Almacen'];
   ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <link rel="shortcut icon" href="img/favicon.ico">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/estilos.css">
      <link rel="stylesheet" type="text/css" href="fonts/style.css">
      <link rel="stylesheet" type="text/css" href="css/paneles.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/emrpesa.css">
      <link rel="stylesheet" type="text/css" href="css/Tablas.css">
      <script type="text/javascript" src="js/Devoluciones.js"></script>
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <title>Store-Plus</title>
   </head>
   <body>
      <?php
         // Consultas para llenar los select
         $QueryVentas = 'select * from ventas where Id_User = '.$Identificador.' AND Almacen = '.$_SESSION["Almacen"].' order by Venta ASC ';
         $resultVentas = $cbd->query($QueryVentas);
         
         $queryVend = "select Vendedor from ventas where Almacen = ".$Almacen." AND Id_User = ".$Identificador." GROUP BY Vendedor";
         $resultVend = $cbd->query($queryVend);
      ?>
      <!--// Navigation bar -->
      <nav class="navbar navbar-default navbar-fixed-static navcolor">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="menu.php"><img src="img/favicon.ico"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="<?php echo $dominio;?>menu.php">Menú</a></li>
                  <li><a href="<?php echo $dominio;?>Productos.php">Inventario</a></li>
                  <li class="active"><a href="<?php echo $dominio;?>tpv.php" >Punto de Venta</a></li>
                  <li><a href="<?php echo $dominio;?>compras.php" > Compras</a></li>
                  <li><a href="<?php echo $dominio;?>Reportes.php"> Reportes</a></li>
                  <li><a href="<?php echo $dominio;?>Operaciones.php"> Operaciones</a></li>
                  <li><a href="<?php echo $dominio;?>clients.php" > Control</a></li>
                  <li><a href="<?php echo $dominio;?>Empresa.php"> Empresa</a></li>
                  <li><a href="<?php echo $dominio;?>Informacion.php"> Información</a></li>                   
                  <li><a href="<?php echo $dominio;?>Facturacion.php"> Facturación</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $profile; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- Contenedor proncipal -->
      <div class="container-fluid">
         <div class="cabezera" align="center">
            <h3 class="Titulo">Devolución</h3>
         </div>
         <div class="contenido">
            <div class="tablita table-responsive table-bordered contenido">
               <div class="container-fluid">
                  <div class="form-group">
                     <label class="control-label  col-lg-2">
                        <h4 class="textoBlack"><strong><i class="icon-user-tie"></i> Vendedores:</strong></h4>
                     </label>
                     <div class="col-lg-4">
                        <select class="form-control" id="Vendedors" onchange="getVents()">
                           <option value="nulo">Seleccionar Vendedor</option>
                           <?php while ($filaVend = mysqli_fetch_array($resultVend)){ ?>
                           <option value="<?php echo $filaVend['Vendedor'];?>"><?php echo $filaVend['Vendedor'] ;?></option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-lg-2">
                           <h4 class="textoBlack"><strong><i class="icon-list-numbered"></i> Numero de venta:</strong></h4>
                        </label>
                        <div class="col-lg-4" id="datosVenta">
                           <select class="form-control" id="venta" onchange="DatosTabla()">
                              <option value="nulo">Seleccionar Venta</option>
                              <?php while ($filaVenta = mysqli_fetch_array($resultVentas)){ ?>
                              <option value="<?php echo $filaVenta['Venta'];?>"><?php echo $filaVenta['Venta'];?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <br>
               <!-- Se crea la tabla -->
               <div class="table-responsive" id="TablaDevoluciones">
                  <table class="table table-striped table-bordered">
                     <tr>
                        <td align="center" class="TituloVerde" COLSPAN="6">Tabla De Devoluciones</td>
                     </tr>
                     <tr>
                        <th class="headAzul">Producto</th>
                        <th class="headAzul">Cantidad</th>
                        <th class="headAzul">Precio</th>
                        <th class="headAzul">Importe</th>
                        <th class="headAzul">Impuesto</th>
                        <th class="headAzul">Total</th>
                     </tr>
                     <tr>
                        <td class="celda"><br> </td>
                        <td class="celda"> </td>
                        <td class="celda"> </td>
                        <td class="celda"> </td>
                        <td class="celda"> </td>
                        <td class="celda"> </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- Modal de confirmación devolución total -->
      <div class="container">
         <div class="modal fade " id="DTotal">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header color">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Devolución Total</h2>
                  </div>
                  <div class="modal-body" align="center">
                     <h3>Seguro que desea devolver</h3>
                     <br>
                     <div class="alert alert-success alert-dismissible" id="alertaDev" align="center">
                     </div>
                     <table class="table">
                        <tr>
                           <td><button onclick="DevolucionT()" id="delete" name="delete" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal de confirmación devolución parcial -->
      <div class="container">
         <div class="modal fade " id="Dpartial">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header panel-header color">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title" align="center">Devolución Parcial</h2>
                  </div>
                  <div class="modal-body" align="center">
                     <h3>Seguro que desea devolver</h3>
                     <br>
                     <div class="alert alert-success alert-dismissible" id="alertaDevP" align="center">
                     </div>
                     <table class="table">
                        <tr>
                           <td><button onclick="DevolucionPartial()" id="delete" name="delete" class="btn btn-success col-xs-12 col-lg-12">Continuar</button></td>
                           <td><button  class="btn btn-danger col-xs-12 col-lg-12" data-dismiss="modal">Cancelar</button></td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>